package net.cnri.cordra;

import net.cnri.util.StreamUtil;

import java.io.*;

public interface DirectIo {
    InputStream getInputAsInputStream() throws IOException;
    Reader getInputAsReader() throws IOException;

    default byte[] getInputAsBytes() throws IOException {
        try (InputStream in = getInputAsInputStream()) {
            if (in == null) return null;
            return StreamUtil.readFully(in);
        }
    }

    default String getInputAsString() throws IOException {
        try (Reader reader = getInputAsReader()) {
            if (reader == null) return null;
            return StreamUtil.readFully(reader);
        }
    }

    OutputStream getOutputAsOutputStream() throws IOException;
    Writer getOutputAsWriter() throws IOException;

    default void writeOutputBytes(byte[] output) throws IOException {
        getOutputAsOutputStream().write(output);
    }

    default void writeOutputString(String output) throws IOException {
        getOutputAsWriter().write(output);
    }

    String getInputMediaType();
    String getInputFilename();
    void setOutputMediaType(String mediaType);
    void setOutputFilename(String filename);
}
