package net.cnri.cordra.tools;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Setup {



    public static void main(String[] args) {

    }

    /*
    creates in rootDir
    ./cordra
    ./cordra/types
    ./cordra/objects
    ./cordra/design
    ./cordra/payloads

     */
    public void createStub(String rootDir) throws Exception {
        Files.createDirectories(Paths.get(rootDir, Constants.CORDRA_DIR));
        Files.createDirectories(Paths.get(rootDir, Constants.TYPES_DIR));
        Files.createDirectories(Paths.get(rootDir, Constants.OBJECTS_DIR));
        Files.createDirectories(Paths.get(rootDir, Constants.DESIGN_DIR));
        Files.createDirectories(Paths.get(rootDir, Constants.PAYLOADS_DIR));
    }
}
