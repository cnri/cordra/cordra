package net.cnri.cordra.tools;

public class Constants {
    public static final String CORDRA_DIR = "cordra";
    public static final String TYPES_DIR = CORDRA_DIR + "/types";
    public static final String OBJECTS_DIR = CORDRA_DIR + "/objects";
    public static final String DESIGN_DIR = CORDRA_DIR + "/design";
    public static final String PAYLOADS_DIR = CORDRA_DIR + "/payloads";
    public static final String CONFIG_FILE = CORDRA_DIR + "/config.json";
}
