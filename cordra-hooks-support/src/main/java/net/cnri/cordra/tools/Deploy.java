package net.cnri.cordra.tools;

import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.NotFoundCordraException;
import net.cnri.cordra.util.GsonUtility;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Deploy {

    public static void deploy(String rootDir) throws CordraException, IOException {
        Path rootPath = Paths.get(rootDir);
        if (!Files.notExists(rootPath)) {
            throw new NotFoundCordraException("Cordra dir cannot be found");
        }
        loadConfig(rootPath);
    }

    private static Config loadConfig(Path rootPath) throws IOException, NotFoundCordraException {
        Path configPath = rootPath.resolve(Constants.CONFIG_FILE);
        if (!Files.notExists(configPath)) {
            throw new NotFoundCordraException("Config file cannot be found");
        } else {
            String configJson = readToString(configPath);
            Config config = GsonUtility.getGson().fromJson(configJson, Config.class);
            return config;
        }
    }

    public static String readToString(Path path) throws IOException {
        List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
        return String.join("\n", lines);
    }

}
