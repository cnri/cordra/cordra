package net.cnri.cordra;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.Payload;
import net.cnri.cordra.util.GsonUtility;

import java.io.IOException;
import java.util.*;

public class HooksContext {

    public String objectId;
    public CordraObject.AccessControlList effectiveAcl;
    public String userId;
    public List<String> groups;
    public List<String> aclCreate;
    public Boolean grantAuthenticatedAccess;
    public JsonElement authContext;
    public Boolean isSearch;
    public Boolean isCreate;
    public Boolean isUpdate;
    public Boolean isCordraCallApi;
    public String payload;
    public Long start;
    public Long end;
    public List<Payload> newPayloads;
    public List<String> payloadsToDelete;
    public Boolean isNew;
    public Boolean isDryRun;
    public Map<String, JsonElement> pointerToSchemaMap;
    public CordraObject originalObject;
    public CordraObject beforeSchemaValidationResult;
    public JsonObject requestContext;
    public JsonObject attributes;

    private transient DirectIo directIo;
    private transient boolean gotDirectIo = false;
    private transient boolean gotParams = false;

    public HooksContext() {}

    public static HooksContext from(Map<String, Object> context, DirectIo directIo) {
        HooksContext result = new HooksContext();
        if (context.containsKey("attributes")) result.attributes = (JsonObject) context.get("attributes");
        if (context.containsKey("objectId")) result.objectId = (String) context.get("objectId");
        if (context.containsKey("effectiveAcl")) {
            CordraObject.AccessControlList clone = clone(context.get("effectiveAcl"), CordraObject.AccessControlList.class);
            result.effectiveAcl = clone;
        }
        if (context.containsKey("userId")) result.userId = (String) context.get("userId");
        if (context.containsKey("groups")) {
            result.groups = getAsUnmodifiableList(context, "groups");
        }
        if (context.containsKey("aclCreate")) {
            result.aclCreate = getAsUnmodifiableList(context,"aclCreate");
        }
        if (context.containsKey("grantAuthenticatedAccess")) {
            result.grantAuthenticatedAccess = Boolean.valueOf((Boolean) context.get("grantAuthenticatedAccess"));
        }
        if (context.containsKey("isSearch")) {
            result.isSearch = Boolean.valueOf((Boolean) context.get("isSearch"));
        }
        if (context.containsKey("isCreate")) {
            result.isCreate = Boolean.valueOf((Boolean) context.get("isCreate"));
        }
        if (context.containsKey("isUpdate")) {
            result.isUpdate = Boolean.valueOf((Boolean) context.get("isUpdate"));
        }
        if (context.containsKey("isCordraCallApi")) {
            result.isCordraCallApi = Boolean.valueOf((Boolean) context.get("isCordraCallApi"));
        }
        if (context.containsKey("payload")) result.payload = (String) context.get("payload");
        if (context.containsKey("start")) {
            result.start = Long.valueOf((Long) context.get("start"));
        }
        if (context.containsKey("end")) {
            result.end = Long.valueOf((Long) context.get("end"));
        }
        if (context.containsKey("newPayloads")) {
            @SuppressWarnings("unchecked")
            List<Payload> newPayloads = (List<Payload>) context.get("newPayloads");
            List<Payload> clone = clone(newPayloads, new ArrayList<Payload>(){}.getClass());
            result.newPayloads = clone;
        }
        if (context.containsKey("isNew")) {
            result.isNew = Boolean.valueOf((Boolean) context.get("isNew"));
        }
        if (context.containsKey("isDryRun")) {
            result.isDryRun = Boolean.valueOf((Boolean) context.get("isDryRun"));
        }
        if (context.containsKey("originalObject")) {
            JsonElement originalObject = (JsonElement) context.get("originalObject");
            result.originalObject = GsonUtility.getGson().fromJson(originalObject, CordraObject.class);
        }
        if (context.containsKey("pointerToSchemaMap")) {
            @SuppressWarnings("unchecked")
            Map<String, JsonElement> pointerToSchemaMap = (Map<String, JsonElement>) context.get("pointerToSchemaMap");
            JsonElement json = GsonUtility.getGson().toJsonTree(pointerToSchemaMap);
            Map<String, JsonElement> clone = GsonUtility.getGson().fromJson(json, new TypeToken<Map<String, JsonElement>>() {}.getType());
            result.pointerToSchemaMap = clone;
        }
        if (context.containsKey("beforeSchemaValidationResult")) {
            JsonElement beforeSchemaValidationResult = (JsonElement) context.get("beforeSchemaValidationResult");
            result.beforeSchemaValidationResult = GsonUtility.getGson().fromJson(beforeSchemaValidationResult, CordraObject.class);
        }
        result.directIo = directIo;
        if (context.containsKey("requestContext")) {
            result.requestContext = (JsonObject) context.get("requestContext");
        }
        return result;
    }

    private static List<String> getAsUnmodifiableList(Map<String, Object> context, String key) {
        Object obj = context.get(key);
        if (obj == null) {
            return null;
        } else {
            @SuppressWarnings("unchecked")
            List<String> list = (List<String>) obj;
            return Collections.unmodifiableList(list);
        }
    }

    private static <T> T clone(Object o, Class<T> c) {
        JsonElement j = GsonUtility.getGson().toJsonTree(o);
        return GsonUtility.getGson().fromJson(j, c);
    }

    public JsonElement getParams() throws IOException {
        if (gotDirectIo) throw new Error("Can't use context.params after getting direct IO");
        if (directIo == null) {
            return null;
        }
        String inputString = directIo.getInputAsString();
        JsonElement params = JsonParser.parseString(inputString);
        gotParams = true;
        return params;
    }

    public boolean gotDirectIo() {
        return gotDirectIo;
    }

    public DirectIo getDirectIo() {
        if (gotParams) throw new Error("Can't get direct IO after getting params");
        gotDirectIo = true;
        return directIo;
    }
}
