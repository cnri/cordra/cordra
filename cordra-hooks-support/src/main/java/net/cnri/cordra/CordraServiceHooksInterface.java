package net.cnri.cordra;

import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.SearchRequest;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.RequestAuthenticationInfo;
import net.handle.hdllib.HandleValue;

import java.util.List;

@SuppressWarnings("unused")
public interface CordraServiceHooksInterface {

    default List<HandleValue> createHandleValues(CordraObject co, HooksContext context) {
        return null;
    }

    default String customizeQuery(String query, HooksContext context) {
        return query;
    }

    default SearchRequest customizeQueryAndParams(SearchRequest queryAndParams, HooksContext context) {
        return queryAndParams;
    }

    default AuthenticationResult authenticate(RequestAuthenticationInfo authInfo, HooksContext context) throws CordraException {
        return null;
    }
}
