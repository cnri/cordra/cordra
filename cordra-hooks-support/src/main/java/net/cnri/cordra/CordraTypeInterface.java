package net.cnri.cordra;

import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.auth.DefaultAcls;

@SuppressWarnings("unused")
public interface CordraTypeInterface {

    default String generateId(CordraObject co, HooksContext context) {
        return null;
    }

    default boolean isGenerateIdLoopable() {
        return false;
    }

    default DefaultAcls getAuthConfig(HooksContext context) throws CordraException {
        return null;
    }

    default CordraObject beforeSchemaValidation(CordraObject obj, HooksContext context) throws CordraException {
        return obj;
    }

    default CordraObject beforeSchemaValidationWithId(CordraObject obj, HooksContext context) throws CordraException {
        return obj;
    }

    default CordraObject onObjectResolution(CordraObject obj, HooksContext context) throws CordraException {
        return obj;
    }

    default void onPayloadResolution(CordraObject co, HooksContext context) throws CordraException { }

    default CordraObject objectForIndexing(CordraObject obj, HooksContext context) throws CordraException {
        return obj;
    }

    default void beforeStorage(CordraObject co, HooksContext context) throws CordraException {}

    default void afterCreateOrUpdate(CordraObject co, HooksContext context) throws CordraException {}

    default void beforeDelete(CordraObject co, HooksContext context) throws CordraException {}

    default void afterDelete(CordraObject co, HooksContext context) throws CordraException {}

    default void onBeforeUnload() throws CordraException {}

    default void onUnload() throws CordraException {}

    default void onLoad() throws CordraException {}
}
