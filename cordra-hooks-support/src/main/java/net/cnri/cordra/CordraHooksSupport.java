package net.cnri.cordra;

import java.util.Map;
import java.util.concurrent.Callable;

import com.google.gson.JsonElement;

import net.cnri.cordra.api.CordraClient;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;

public interface CordraHooksSupport {
    default CordraClient getCordraClient() { return getHooksSupportCordraClient(); }

    HooksSupportCordraClient getHooksSupportCordraClient();

    void addShutdownHook(Callable<Void> shutdownHook);
    default void addShutdownHook(Runnable shutdownHook) {
        addShutdownHook(() -> { shutdownHook.run(); return null; });
    }

    String signWithCordraKey(String payload, boolean useJsonSerialization) throws CordraException;

    boolean verifyWithCordraKey(String jwt) throws CordraException;

    // TODO currently undocumented
    Map<String, JsonElement> getPointerToSchemaMap(CordraObject co) throws CordraException;

    boolean isThisInstanceLeader();
}
