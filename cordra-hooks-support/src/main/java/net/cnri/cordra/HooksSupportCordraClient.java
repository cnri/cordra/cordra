package net.cnri.cordra;

import net.cnri.cordra.api.CordraClient;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;

import java.io.InputStream;

public interface HooksSupportCordraClient extends CordraClient {

    CordraObject getType(String type) throws CordraException;

    CordraObject getDesign() throws CordraException;

    CordraObject getWithBypassHooks(String id) throws CordraException;

    InputStream getPayloadWithBypassHooks(String id, String payloadName, Long start, Long end) throws CordraException;

    default InputStream getPayloadWithBypassHooks(String id, String payloadName) throws CordraException {
        return getPayloadWithBypassHooks(id, payloadName, null, null);
    }
}
