package net.cnri.cordra;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CordraMethod {
    String[] name() default {};
    String[] names() default {};
    String[] value() default {};
    boolean allowGet() default false;
}
