package net.cnri.cordra.util;

import com.google.gson.JsonObject;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.Options;
import net.cnri.jws.JsonWebSignature;
import net.cnri.jws.JsonWebSignatureFactory;
import org.apache.commons.codec.binary.Hex;

import java.security.PrivateKey;
import java.security.SecureRandom;

public class AuthUtil {

    private static final SecureRandom random = new SecureRandom();

    public static JsonObject buildAuthTokenInputFrom(Options options, String defaultUsername, String defaultPassword) throws InternalErrorCordraException {
        JsonObject json = new JsonObject();
        if (options.authTokenInput != null) {
            json = options.authTokenInput;
        } else if (options.useDefaultCredentials) {
            json.addProperty("grant_type", "password");
            json.addProperty("username", defaultUsername);
            json.addProperty("password", defaultPassword);
        } else if (options.token != null) {
            json.addProperty("grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer");
            json.addProperty("assertion", options.token);
        } else {
            String userInfo = options.userId;
            if (userInfo == null) userInfo = options.username;
            if (options.privateKey != null) {
                String jwt = generateJwt(userInfo, options.privateKey).serialize();
                json.addProperty("grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer");
                json.addProperty("assertion", jwt);
            } else if (options.password != null) {
                json.addProperty("grant_type", "password");
                json.addProperty("username", userInfo);
                json.addProperty("password", options.password);
            }
        }
        return json;
    }

    public static JsonWebSignature generateJwt(String iss, PrivateKey privateKey) throws InternalErrorCordraException {
        long nowSeconds = System.currentTimeMillis() / 1000L;
        JsonObject claims = new JsonObject();
        claims.addProperty("iss", iss);
        claims.addProperty("sub", iss);
        claims.addProperty("jti", generateJti());
        claims.addProperty("iat", nowSeconds);
        claims.addProperty("exp", nowSeconds + 600);
        String claimsJson = claims.toString();
        JsonWebSignature jwt;
        try {
            jwt = JsonWebSignatureFactory.getInstance().create(claimsJson, privateKey);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
        return jwt;
    }

    private static String generateJti() {
        byte[] bytes = new byte[10];
        random.nextBytes(bytes);
        return Hex.encodeHexString(bytes);
    }
}
