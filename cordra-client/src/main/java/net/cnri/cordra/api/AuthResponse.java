package net.cnri.cordra.api;

import java.util.List;

public class AuthResponse {
    public boolean active = false;
    @Deprecated
    public transient boolean isActiveSession = false;
    public String userId;
    public String username;
    public Long exp;
    public List<String> typesPermittedToCreate;
    public List<String> groupIds;

    public AuthResponse() {}

    @Deprecated
    public AuthResponse(boolean active, String userId, String username, List<String> typesPermittedToCreate) {
        this(active, userId, username, typesPermittedToCreate, null);
    }

    @Deprecated
    public AuthResponse(boolean active, String userId, String username, List<String> typesPermittedToCreate, List<String> groupIds) {
        this(active, userId, username, typesPermittedToCreate, groupIds, null);
    }

    public AuthResponse(boolean active, String userId, String username, List<String> typesPermittedToCreate, List<String> groupIds, Long exp) {
        this.active = active;
        this.isActiveSession = active;
        this.userId = userId;
        this.username = username;
        this.typesPermittedToCreate = typesPermittedToCreate;
        this.groupIds = groupIds;
        this.exp = exp;
    }
}
