package net.cnri.cordra.api;

import java.util.List;

public class AuthTokenResponse extends AuthResponse {
    public String token;

    public AuthTokenResponse() {}

    public AuthTokenResponse(boolean active, String token, String userId, String username, List<String> typesPermittedToCreate, List<String> groupIds, Long exp) {
        super(active, userId, username, typesPermittedToCreate, groupIds, exp);
        this.token = token;
    }
}
