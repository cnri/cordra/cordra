package net.cnri.cordra.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.dona.doip.util.tls.AllTrustingTrustManager;
import net.dona.doip.util.tls.AutoSelfSignedKeyManager;
import net.dona.doip.util.tls.TlsProtocolAndCipherSuiteConfigurationUtil;
import net.dona.doip.util.tls.X509CertificateGenerator;

import org.eclipse.jetty.alpn.server.ALPNServerConnectionFactory;
import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.http2.server.HTTP2CServerConnectionFactory;
import org.eclipse.jetty.http2.server.HTTP2ServerConnectionFactory;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.util.component.LifeCycle;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.JettyWebXmlConfiguration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmbeddedServerCordraClient implements CordraClient {
    private static final Logger logger = LoggerFactory.getLogger(EmbeddedServerCordraClient.class);
    private static final SecureRandom random = new SecureRandom();

    private static class SystemPropertiesLifeCycleListener extends AbstractLifeCycle.AbstractLifeCycleListener {
        private final Properties toSet;

        public SystemPropertiesLifeCycleListener(Properties toSet) {
            this.toSet = toSet;
        }

        @Override
        public void lifeCycleStarting(LifeCycle anyLifeCycle) {
            System.getProperties().putAll(toSet);
        }
    }

    // retain for legacy
    @Deprecated
    public static EmbeddedServerCordraClient newInstance(URL cordraWarResource) throws Exception {
        Path cordraWarPath = Paths.get(cordraWarResource.toURI());
        return newInstance(cordraWarPath);
    }

    public static EmbeddedServerCordraClient newInstance(Path cordraWarPath) throws Exception {
        return newInstance(cordraWarPath, -1, null);
    }

    public static EmbeddedServerCordraClient newInstance(Path cordraWarPath, int mongoPort, Function<String, CordraClient> clientProvider) throws Exception {
        String mongoConfig = null;
        if (mongoPort > -1) {
            mongoConfig = "{\"storage\": { \"module\" : \"mongodb\", \"options\" : {\"connectionUri\" : \"mongodb://localhost:" + mongoPort + "\"}}," +
                "\"sessions\": { \"module\" : \"mongodb\", \"options\" : {\"connectionUri\" : \"mongodb://localhost:" + mongoPort + "\"}}}";
        }
        return newInstanceWithConfig(cordraWarPath, mongoConfig, clientProvider);
    }

    public static EmbeddedServerCordraClient newInstanceWithConfig(Path cordraWarPath, String config, Function<String, CordraClient> clientProvider) throws Exception {
        return newInstanceWithConfig(cordraWarPath, config, null, clientProvider);
    }

    public static EmbeddedServerCordraClient newInstanceWithConfig(Path cordraWarPath, String config, Function<String, CordraClient> clientProvider, String password) throws Exception {
        return newInstanceWithConfig(cordraWarPath, config, null, clientProvider, password);
    }

    public static EmbeddedServerCordraClient newInstanceWithConfig(Path cordraWarPath, String config, Path jarPath, Function<String, CordraClient> clientProvider) throws Exception {
        return newInstanceWithConfig(cordraWarPath, config, jarPath, clientProvider, "changeit");
    }

    @SuppressWarnings("resource")
    public static EmbeddedServerCordraClient newInstanceWithConfig(Path cordraWarPath, String config, Path jarPath, Function<String, CordraClient> clientProvider, String password) throws Exception {
        Path tempDir = Files.createTempDirectory("cordra.data");
        System.setProperty("cordra.data", tempDir.toString());
        {
            Path repoInitJsonPath = tempDir.resolve("repoInit.json");
            JsonObject repoInit = new JsonObject();
            repoInit.addProperty("adminPassword", password);
            JsonObject design = new JsonObject();
            design.addProperty("allowInsecureAuthentication", true);
            repoInit.add("design", design);
            JsonObject adminPublicKey = new JsonObject();
            design.add("adminPublicKey", adminPublicKey);
            adminPublicKey.addProperty("kty", "RSA");
            adminPublicKey.addProperty("n", "rLt4enTZI4QnM8mHE_nIIzaN8ZROcHl07H4tSgSRFHATq_ZKfelVngTM-3_bHK-Z-f_bbIbScuQ0an7LiKdJUR-rXEJpXHT11DSL1CDKEWylWELiG-pMO01HtIH96anb2N5JUFffhyidaOElWdTVIL6XmU_Uimif6335xbV_Ubk");
            adminPublicKey.addProperty("e", "AQAB");
            JsonObject doipConfig = new JsonObject();
            doipConfig.addProperty("enabled", false);
            design.add("doip", doipConfig);
            JsonObject handleServerConfig = new JsonObject();
            handleServerConfig.addProperty("enabled", false);
            design.add("handleServerConfig", handleServerConfig);
            String repoInitJson = repoInit.toString();
            Files.write(repoInitJsonPath, repoInitJson.getBytes(StandardCharsets.UTF_8));
        }
        int httpPort = 0;
        int httpsPort = -1;
        if (config != null) {
            Path cordraConfigPath = tempDir.resolve("config.json");
            Files.write(cordraConfigPath, config.getBytes(StandardCharsets.UTF_8));
            JsonObject configJson = JsonParser.parseString(config).getAsJsonObject();
            if (configJson.get("httpPort") != null) {
                httpPort = configJson.get("httpPort").getAsInt();
            }
            if (configJson.get("httpsPort") != null) {
                httpsPort = configJson.get("httpsPort").getAsInt();
            }
        }

        Server server = new Server();
        Configuration.ClassList classlist = Configuration.ClassList.setServerDefault(server);
        classlist.addBefore(JettyWebXmlConfiguration.class.getName(), AnnotationConfiguration.class.getName());
        WebAppContext webapp = new WebAppContext();
// parentLoaderPriority may be useful for some classpath issues in testing, but let's make it an option if we never need it, and not change the classloading behavior on external users of this class
//        webapp.setParentLoaderPriority(true);
        webapp.setContextPath("/cordra");
        webapp.setWar(cordraWarPath.toString());
        webapp.getSystemClasspathPattern().add("org.slf4j.");
        webapp.getSystemClasspathPattern().add("org.apache.log4j.");
        webapp.getSystemClasspathPattern().add("org.apache.logging.");
        webapp.getSystemClasspathPattern().add("org.apache.commons.logging.");
        webapp.getSystemClasspathPattern().add("com.google.gson.");
        webapp.getSystemClasspathPattern().add("com.fasterxml.jackson.");
        webapp.getSystemClasspathPattern().add("com.github.fge.");
        webapp.getSystemClasspathPattern().add("com.networknt.schema.");
        webapp.getSystemClasspathPattern().add("net.cnri.cordra.");
        webapp.getSystemClasspathPattern().add("com.google.common.cache.");
        webapp.getSystemClasspathPattern().add("org.apache.http."); // for DOIP
        webapp.getSystemClasspathPattern().add("net.dona.doip."); // for DOIP

        if (jarPath != null && Files.isDirectory(jarPath)) {
            try (Stream<Path> files = Files.walk(jarPath)) {
                String extraClasspath =
                    files
                        .filter(p -> Files.isRegularFile(p) && p.getFileName().toString().toLowerCase(Locale.ROOT).endsWith(".jar"))
                        .map(path -> path.toAbsolutePath().toString())
                        .collect(Collectors.joining(";")); // note: this is a Jetty convention, not the Java path separator
                webapp.setExtraClasspath(extraClasspath);
            }
        }

        // disable graal warnings
        final Properties systemProperties = new Properties();
        systemProperties.setProperty("polyglot.engine.WarnInterpreterOnly", "false");
        server.addLifeCycleListener(new SystemPropertiesLifeCycleListener(systemProperties));

        ContextHandlerCollection handlers = new ContextHandlerCollection();
        handlers.addHandler(webapp);
        server.setHandler(handlers);
        addConnectors(server, httpPort, httpsPort);
        server.start();

        String httpUri = null;
        String secureUri = null;
        for (Connector connector : server.getConnectors()) {
            int port = ((NetworkConnector) connector).getLocalPort();
            if (connector.getProtocols().contains("ssl")) {
                secureUri = "https://localhost:" + port + "/cordra/";
            } else {
                httpUri = "http://localhost:" + port + "/cordra/";
            }
        }
        String baseUri = secureUri != null ? secureUri : httpUri;
        if (clientProvider == null) {
            clientProvider = uri -> {
                try {
                    return new TokenUsingHttpCordraClient(uri, "admin", password);
                } catch (CordraException e) {
                    throw new RuntimeException(e);
                }
            };
        }
        CordraClient client = clientProvider.apply(baseUri);
        return new EmbeddedServerCordraClient(tempDir, httpUri, secureUri, server, client);
    }

    private static void addConnectors(Server server, int httpPort, int httpsPort) {
        logger.info("initializing HTTP on port " + httpPort);
        HttpConfiguration httpConfig = new HttpConfiguration();
        httpConfig.setOutputBufferSize(32768);
        httpConfig.setRequestHeaderSize(8192);
        httpConfig.setResponseHeaderSize(8192);
        httpConfig.setSendDateHeader(false);
        httpConfig.setSendServerVersion(false);
        if (httpsPort > 0) {
            httpConfig.setSecureScheme("https");
            httpConfig.setSecurePort(httpsPort);
        }
        ServerConnector httpConnector = new ServerConnector(server, new HttpConnectionFactory(httpConfig));
        httpConnector.setPort(httpPort);
        httpConnector.setIdleTimeout(30000);
        server.addConnector(httpConnector);

        if (httpsPort > -1) {
            logger.info("initializing HTTPS on port " + httpsPort);
            try {
                SSLContext sslContext = SSLContext.getInstance("TLS");
                KeyManager[] keyManagers = createKeyManagers();
                sslContext.init(keyManagers, new TrustManager[]{new AllTrustingTrustManager()}, random);
                SslContextFactory sslContextFactory = new SslContextFactory();
                sslContextFactory.setSslContext(sslContext);
                sslContextFactory.setIncludeCipherSuites(TlsProtocolAndCipherSuiteConfigurationUtil.ENABLED_CIPHER_SUITES);
                sslContextFactory.setUseCipherSuitesOrder(true);
                sslContextFactory.setIncludeProtocols(TlsProtocolAndCipherSuiteConfigurationUtil.ENABLED_PROTOCOLS);
                sslContextFactory.setEndpointIdentificationAlgorithm(null);

                HttpConfiguration httpsConfig = new HttpConfiguration(httpConfig);
                httpsConfig.addCustomizer(new SecureRequestCustomizer());
                HTTP2ServerConnectionFactory http2 = null;
                ALPNServerConnectionFactory alpn = null;
                boolean useHttp2 = !System.getProperty("java.version").startsWith("1.8");
                String protocol = HttpVersion.HTTP_1_1.asString();
                if (useHttp2) {
                    http2 = new HTTP2ServerConnectionFactory(httpsConfig);
                    alpn = new ALPNServerConnectionFactory();
                    alpn.setDefaultProtocol(HttpVersion.HTTP_1_1.asString());
                    protocol = alpn.getProtocol();
                }
                SslConnectionFactory ssl = new SslConnectionFactory(sslContextFactory, protocol);
                HttpConnectionFactory http = new HttpConnectionFactory(httpsConfig);
                HTTP2CServerConnectionFactory http2C = null;
                if (useHttp2) {
                    http2C = new HTTP2CServerConnectionFactory(httpsConfig);
                }
                ServerConnector sslConnector;
                if (useHttp2) {
                    sslConnector = new ServerConnector(server, ssl, alpn, http2, http, http2C);
                } else {
                    sslConnector = new ServerConnector(server, ssl, http);
                }
                sslConnector.setPort(httpsPort);
                sslConnector.setIdleTimeout(30000);
                server.addConnector(sslConnector);

            } catch (Exception e) {
                logger.error("Error initializing https: " + e.getMessage());
            }
        }
    }

    private static KeyManager[] createKeyManagers() throws Exception {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair keys = kpg.generateKeyPair();
        String httpsId = "testing";
        X509Certificate cert = X509CertificateGenerator.generate(httpsId, keys.getPublic(), keys.getPrivate());
        return new KeyManager[] { new AutoSelfSignedKeyManager(httpsId, cert, keys.getPrivate()) };
    }

    private final Server server;
    private final Path dataDir;
    private final String httpUri;
    private final String secureUri;
    private final CordraClient client;
    private boolean closed = false;

    private EmbeddedServerCordraClient(Path dataDir, String httpUri, String secureUri, Server server, CordraClient client) {
        this.secureUri = secureUri;
        this.httpUri = httpUri;
        this.server = server;
        this.dataDir = dataDir;
        this.client = client;
    }

    public CordraClient getClient() {
        return client;
    }

    public String getBaseUri() {
        return secureUri != null ? secureUri : httpUri;
    }

    public String getHttpUri() {
        return httpUri;
    }

    public String getSecureUri() {
        return secureUri;
    }

    public Server getServer() {
        return server;
    }

    @Override
    public void close() throws IOException, CordraException {
        client.close();
        synchronized (this) {
            if (!closed) {
                closed = true;
                try {
                    server.stop();
                    deleteDirectory(dataDir);
                } catch (IOException e) {
                    throw e;
                } catch (Exception e) {
                    throw new IOException(e);
                }
            }
        }
    }

    private static void deleteDirectory(Path dataDir) throws IOException {
        Files.walkFileTree(dataDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

//    public static void main(String[] args) throws Exception {
//        BasicConfigurator.configure();
//        EmbeddedServerCordraClient.newInstance(Paths.get("/Users/bhadden/cordra.war"));
//        // close
//    }

    @Override
    public String getDefaultUsername() {
        return client.getDefaultUsername();
    }

    @Override
    public String getDefaultPassword() {
        return client.getDefaultPassword();
    }

    @Override
    public Gson getGson() {
        return client.getGson();
    }

    @Override
    public void setGson(Gson gson) {
        client.setGson(gson);
    }

    @Override
    public CordraObject get(String id) throws CordraException {
        return client.get(id);
    }

    @Override
    public CordraObject get(String id, String username, String password) throws CordraException {
        return client.get(id, username, password);
    }

    @Override
    public CordraObject get(String id, Options options) throws CordraException {
        return client.get(id, options);
    }

    @Override
    public InputStream getPayload(String id, String payloadName) throws CordraException {
        return client.getPayload(id, payloadName);
    }

    @Override
    public InputStream getPayload(String id, String payloadName, String username, String password) throws CordraException {
        return client.getPayload(id, payloadName, username, password);
    }

    @Override
    public InputStream getPayload(String id, String payloadName, Options options) throws CordraException {
        return client.getPayload(id, payloadName, options);
    }

    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end) throws CordraException {
        return client.getPartialPayload(id, payloadName, start, end);
    }

    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end, String username, String password) throws CordraException {
        return client.getPartialPayload(id, payloadName, start, end, username, password);
    }

    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        return client.getPartialPayload(id, payloadName, start, end, options);
    }

    @Override
    public CordraObject create(CordraObject d) throws CordraException {
        return client.create(d);
    }

    @Override
    public CordraObject create(CordraObject d, String username, String password) throws CordraException {
        return client.create(d, username, password);
    }

    @Override
    public CordraObject create(CordraObject d, boolean isDryRun, String username, String password) throws CordraException {
        return client.create(d, isDryRun, username, password);
    }

    @Override
    public CordraObject create(CordraObject d, boolean isDryRun) throws CordraException {
        return client.create(d, isDryRun);
    }

    @Override
    public CordraObject create(CordraObject d, Options options) throws CordraException {
        return client.create(d, options);
    }

    @Override
    public CordraObject update(CordraObject d) throws CordraException {
        return client.update(d);
    }

    @Override
    public CordraObject update(CordraObject d, String username, String password) throws CordraException {
        return client.update(d, username, password);
    }

    @Override
    public CordraObject update(CordraObject d, boolean isDryRun, String username, String password) throws CordraException {
        return client.update(d, isDryRun, username, password);
    }

    @Override
    public CordraObject update(CordraObject d, boolean isDryRun) throws CordraException {
        return client.update(d, isDryRun);
    }

    @Override
    public CordraObject update(CordraObject d, Options options) throws CordraException {
        return client.update(d, options);
    }

    @Override
    public List<String> listMethods(String objectId) throws CordraException {
        return client.listMethods(objectId);
    }

    @Override
    public List<String> listMethods(String objectId, String username, String password) throws CordraException {
        return client.listMethods(objectId, username, password);
    }

    @Override
    public List<String> listMethods(String objectId, Options options) throws CordraException {
        return client.listMethods(objectId, options);
    }

    @Override
    public List<String> listMethodsForType(String type, boolean isStatic) throws CordraException {
        return client.listMethodsForType(type, isStatic);
    }

    @Override
    public List<String> listMethodsForType(String type, boolean isStatic, String username, String password) throws CordraException {
        return client.listMethodsForType(type, isStatic, username, password);
    }

    @Override
    public List<String> listMethodsForType(String type, boolean isStatic, Options options) throws CordraException {
        return client.listMethodsForType(type, isStatic, options);
    }

    @Override
    public JsonElement call(String objectId, String methodName, JsonElement params) throws CordraException {
        return client.call(objectId, methodName, params);
    }

    @Override
    public JsonElement call(String objectId, String methodName, JsonElement params, String username, String password) throws CordraException {
        return client.call(objectId, methodName, params, username, password);
    }

    @Override
    public JsonElement call(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        return client.call(objectId, methodName, params, options);
    }

    @Override
    public JsonElement callForType(String type, String methodName, JsonElement params) throws CordraException {
        return client.callForType(type, methodName, params);
    }

    @Override
    public JsonElement callForType(String type, String methodName, JsonElement params, String username, String password) throws CordraException {
        return client.callForType(type, methodName, params, username, password);
    }

    @Override
    public JsonElement callForType(String type, String methodName, JsonElement params, Options options) throws CordraException {
        return client.callForType(type, methodName, params, options);
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads) throws CordraException {
        return client.publishVersion(objectId, versionId, clonePayloads);
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads, String username, String password) throws CordraException {
        return client.publishVersion(objectId, versionId, clonePayloads, username, password);
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads, Options options) throws CordraException {
        return client.publishVersion(objectId, versionId, clonePayloads, options);
    }

    @Override
    public List<VersionInfo> getVersionsFor(String objectId) throws CordraException {
        return client.getVersionsFor(objectId);
    }

    @Override
    public List<VersionInfo> getVersionsFor(String objectId, String username, String password) throws CordraException {
        return client.getVersionsFor(objectId, username, password);
    }

    @Override
    public List<VersionInfo> getVersionsFor(String id, Options options) throws CordraException {
        return client.getVersionsFor(id, options);
    }

    @Override
    public void delete(String id) throws CordraException {
        client.delete(id);
    }

    @Override
    public void delete(String id, String username, String password) throws CordraException {
        client.delete(id, username, password);
    }

    @Override
    public void delete(String id, Options options) throws CordraException {
        client.delete(id, options);
    }

    @Override
    public SearchResults<CordraObject> list() throws CordraException {
        return client.list();
    }

    @Override
    public SearchResults<String> listHandles() throws CordraException {
        return client.listHandles();
    }

    @Override
    public SearchResults<CordraObject> list(Options options) throws CordraException {
        return client.list(options);
    }

    @Override
    public SearchResults<String> listHandles(Options options) throws CordraException {
        return client.listHandles(options);
    }

    @Override
    public SearchResults<CordraObject> search(String query) throws CordraException {
        return client.search(query);
    }

    @Override
    public SearchResults<CordraObject> search(String query, String username, String password) throws CordraException {
        return client.search(query, username, password);
    }

    @Override
    public SearchResults<CordraObject> search(String query, Options options) throws CordraException {
        return client.search(query, options);
    }

    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params) throws CordraException {
        return client.search(query, params);
    }

    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params, String username, String password) throws CordraException {
        return client.search(query, params, username, password);
    }

    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params, Options options) throws CordraException {
        return client.search(query, params, options);
    }

    @Override
    public SearchResults<String> searchHandles(String query) throws CordraException {
        return client.searchHandles(query);
    }

    @Override
    public SearchResults<String> searchHandles(String query, String username, String password) throws CordraException {
        return client.searchHandles(query, username, password);
    }

    @Override
    public SearchResults<String> searchHandles(String query, Options options) throws CordraException {
        return client.searchHandles(query, options);
    }

    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params) throws CordraException {
        return client.searchHandles(query, params);
    }

    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params, String username, String password) throws CordraException {
        return client.searchHandles(query, params, username, password);
    }

    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params, Options options) throws CordraException {
        return client.searchHandles(query, params, options);
    }

    @Override
    public boolean authenticate() throws CordraException {
        return client.authenticate();
    }

    @Override
    public boolean authenticate(String username, String password) throws CordraException {
        return client.authenticate(username, password);
    }

    @Override
    public AuthResponse authenticateAndGetResponse() throws CordraException {
        return client.authenticateAndGetResponse();
    }

    @Override
    public AuthResponse authenticateAndGetResponse(String username, String password) throws CordraException {
        return client.authenticateAndGetResponse(username, password);
    }

    @Override
    public AuthResponse authenticateAndGetResponse(Options options) throws CordraException {
        return client.authenticateAndGetResponse(options);
    }

    @Override
    public void changePassword(String newPassword) throws CordraException {
        client.changePassword(newPassword);
    }

    @Override
    public void changePassword(String username, String password, String newPassword) throws CordraException {
        client.changePassword(username, password, newPassword);
    }

    @Override
    public void changePassword(String newPassword, Options options) throws CordraException {
        client.changePassword(newPassword, options);
    }

    @Override
    public String getContentAsJson(String id) throws CordraException {
        return client.getContentAsJson(id);
    }

    @Override
    public <T> T getContent(String id, Class<T> klass) throws CordraException {
        return client.getContent(id, klass);
    }

    @Override
    public CordraObject create(String type, String contentJson) throws CordraException {
        return client.create(type, contentJson);
    }

    @Override
    public CordraObject create(String type, String contentJson, String username, String password) throws CordraException {
        return client.create(type, contentJson, username, password);
    }

    @Override
    public CordraObject update(String id, String contentJson) throws CordraException {
        return client.update(id, contentJson);
    }

    @Override
    public CordraObject update(String id, String contentJson, String username, String password) throws CordraException {
        return client.update(id, contentJson, username, password);
    }

    @Override
    public CordraObject create(String type, Object content) throws CordraException {
        return client.create(type, content);
    }

    @Override
    public CordraObject create(String type, Object content, String username, String password) throws CordraException {
        return client.create(type, content, username, password);
    }

    @Override
    public CordraObject update(String id, Object content) throws CordraException {
        return client.update(id, content);
    }

    @Override
    public CordraObject create(String type, String contentJson, boolean isDryRun) throws CordraException {
        return client.create(type, contentJson, isDryRun);
    }

    @Override
    public CordraObject create(String type, String contentJson, boolean isDryRun, String username, String password) throws CordraException {
        return client.create(type, contentJson, isDryRun, username, password);
    }

    @Override
    public CordraObject update(String id, String contentJson, boolean isDryRun) throws CordraException {
        return client.update(id, contentJson, isDryRun);
    }

    @Override
    public CordraObject update(String id, String contentJson, boolean isDryRun, String username, String password) throws CordraException {
        return client.update(id, contentJson, isDryRun, username, password);
    }

    @Override
    public CordraObject create(String type, Object content, boolean isDryRun) throws CordraException {
        return client.create(type, content, isDryRun);
    }

    @Override
    public CordraObject create(String type, Object content, boolean isDryRun, String username, String password) throws CordraException {
        return client.create(type, content, isDryRun, username, password);
    }

    @Override
    public CordraObject update(String id, Object content, boolean isDryRun) throws CordraException {
        return client.update(id, content, isDryRun);
    }

    @Override
    public void reindexBatch(List<String> batchIds, Options options) throws CordraException {
        client.reindexBatch(batchIds, options);
    }

    @Override
    public SearchResults<CordraObject> get(Collection<String> ids) throws CordraException {
        return client.get(ids);
    }

    @Override
    public SearchResults<CordraObject> listByType(List<String> types) throws CordraException {
        return client.listByType(types);
    }

    @Override
    public SearchResults<String> listHandlesByType(List<String> types) throws CordraException {
        return client.listHandlesByType(types);
    }

    @Override
    public JsonElement call(String objectId, String methodName, InputStream input) throws CordraException {
        return client.call(objectId, methodName, input);
    }

    @Override
    public JsonElement call(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        return client.call(objectId, methodName, input, options);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, JsonElement params) throws CordraException {
        return client.callAsResponse(objectId, methodName, params);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        return client.callAsResponse(objectId, methodName, params, options);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, InputStream input) throws CordraException {
        return client.callAsResponse(objectId, methodName, input);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        return client.callAsResponse(objectId, methodName, input, options);
    }

    @Override
    public JsonElement callForType(String type, String methodName, InputStream input) throws CordraException {
        return client.callForType(type, methodName, input);
    }

    @Override
    public JsonElement callForType(String type, String methodName, InputStream input, Options options) throws CordraException {
        return client.callForType(type, methodName, input, options);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, JsonElement params) throws CordraException {
        return client.callForTypeAsResponse(type, methodName, params);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, JsonElement params, Options options) throws CordraException {
        return client.callForTypeAsResponse(type, methodName, params, options);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, InputStream input) throws CordraException {
        return client.callForTypeAsResponse(type, methodName, input);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, InputStream input, Options options) throws CordraException {
        return client.callForTypeAsResponse(type, methodName, input, options);
    }

    @Override
    public CallResponse getPayloadAsResponse(String id, String payloadName) throws CordraException {
        return client.getPayloadAsResponse(id, payloadName);
    }

    @Override
    public CallResponse getPayloadAsResponse(String id, String payloadName, Options options) throws CordraException {
        return client.getPayloadAsResponse(id, payloadName, options);
    }

    @Override
    public CallResponse getPartialPayloadAsResponse(String id, String payloadName, Long start, Long end) throws CordraException {
        return client.getPartialPayloadAsResponse(id, payloadName, start, end);
    }

    @Override
    public CallResponse getPartialPayloadAsResponse(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        return client.getPartialPayloadAsResponse(id, payloadName, start, end, options);
    }

    @Override
    public AuthTokenResponse getAuthToken(Options options) throws CordraException {
        return client.getAuthToken(options);
    }

    @Override
    public AuthResponse introspectToken(Options options) throws CordraException {
        return client.introspectToken(options);
    }

    @Override
    public void revokeToken(Options options) throws CordraException {
        client.revokeToken(options);
    }


}
