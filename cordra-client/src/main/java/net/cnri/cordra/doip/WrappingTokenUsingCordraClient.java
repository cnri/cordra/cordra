package net.cnri.cordra.doip;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import net.cnri.cordra.api.*;
import net.cnri.cordra.util.GsonUtility;
import net.dona.doip.client.ServiceInfo;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class WrappingTokenUsingCordraClient implements CordraClient {

    private final Gson gson = GsonUtility.getGson();
    private final ClientAuthCache authCache;
    private final CordraClient client;

    public WrappingTokenUsingCordraClient(CordraClient client) {
        this.client = client;
        this.authCache = new ClientAuthCache();
    }

    public WrappingTokenUsingCordraClient(CordraClient client, long authCacheTimeoutMs) {
        this.client = client;
        this.authCache = new ClientAuthCache(authCacheTimeoutMs);
    }

    public static WrappingTokenUsingCordraClient createWithHttpDoipCordraClient(String baseUri, String username, String password) throws CordraException {
        DoipCordraClient doipCordraClient = DoipCordraClient.createWithHttpDoipClient(baseUri, username, password);
        WrappingTokenUsingCordraClient wrappingTokenUsingCordraClient = new WrappingTokenUsingCordraClient(doipCordraClient);
        return wrappingTokenUsingCordraClient;
    }

    public static WrappingTokenUsingCordraClient createWithHttpDoipCordraClient(ServiceInfo serviceInfo, String username, String password) throws CordraException {
        DoipCordraClient doipCordraClient = DoipCordraClient.createWithHttpDoipClient(serviceInfo, username, password);
        WrappingTokenUsingCordraClient wrappingTokenUsingCordraClient = new WrappingTokenUsingCordraClient(doipCordraClient);
        return wrappingTokenUsingCordraClient;
    }

    public static WrappingTokenUsingCordraClient createWithNativeDoipCordraClient(ServiceInfo serviceInfo, String username, String password) {
        DoipCordraClient doipCordraClient = DoipCordraClient.createWithNativeDoipClient(serviceInfo, username, password);
        WrappingTokenUsingCordraClient wrappingTokenUsingCordraClient = new WrappingTokenUsingCordraClient(doipCordraClient);
        return wrappingTokenUsingCordraClient;
    }

    public static WrappingTokenUsingCordraClient createWithHttpCordraClient(String baseUri, String username, String password) throws CordraException {
        HttpCordraClient cordra = new HttpCordraClient(baseUri, username, password);
        WrappingTokenUsingCordraClient wrappingTokenUsingCordraClient = new WrappingTokenUsingCordraClient(cordra);
        return wrappingTokenUsingCordraClient;
    }

    @Override
    public void close() throws IOException, CordraException {
        client.close();
    }

    @Override
    public String getDefaultUsername() {
        return client.getDefaultUsername();
    }

    @Override
    public String getDefaultPassword() {
        return client.getDefaultPassword();
    }

    @Override
    public CordraObject get(String id, Options options) throws CordraException {
        return run((Options o) -> {
            return client.get(id, o);
        }, options, true);
    }

    @Override
    public InputStream getPayload(String id, String payloadName, Options options) throws CordraException {
        return run((Options o) -> {
            return client.getPayload(id, payloadName, o);
        }, options, true);
    }

    @Override
    public CallResponse getPayloadAsResponse(String id, String payloadName, Options options) throws CordraException {
        return run((Options o) -> {
            return client.getPayloadAsResponse(id, payloadName, o);
        }, options, true);
    }

    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        return run((Options o) -> {
            return client.getPartialPayload(id, payloadName, start, end, o);
        }, options, true);
    }

    @Override
    public CallResponse getPartialPayloadAsResponse(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        return run((Options o) -> {
            return client.getPartialPayloadAsResponse(id, payloadName, start, end, o);
        }, options, true);
    }

    @Override
    public CordraObject create(CordraObject d, Options options) throws CordraException {
        boolean isRetryable = !hasInputStreams(d);
        return run((Options o) -> {
            return client.create(d, o);
        }, options, isRetryable);
    }

    private boolean hasInputStreams(CordraObject d) {
        if (d.payloads == null) {
            return false;
        }
        for (Payload p : d.payloads) {
            if (p.getInputStream() != null) {
                return true;
            }
        }
        return false;
    }

    @Override
    public CordraObject update(CordraObject d, Options options) throws CordraException {
        boolean isRetryable = !hasInputStreams(d);
        return run((Options o) -> {
            return client.update(d, o);
        }, options, isRetryable);
    }

    @Override
    public List<String> listMethods(String objectId, Options options) throws CordraException {
        return run((Options o) -> {
            return client.listMethods(objectId, o);
        }, options, true);
    }

    @Override
    public List<String> listMethodsForType(String type, boolean isStatic, Options options) throws CordraException {
        return run((Options o) -> {
            return client.listMethodsForType(type, isStatic, o);
        }, options, true);
    }

    @Override
    public JsonElement call(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        return run((Options o) -> {
            return client.call(objectId, methodName, params, o);
        }, options, true);
    }

    @Override
    public JsonElement call(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        return run((Options o) -> {
            return client.call(objectId, methodName, input, o);
        }, options, false);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        return run((Options o) -> {
            return client.callAsResponse(objectId, methodName, params, o);
        }, options, true);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        return run((Options o) -> {
            return client.callAsResponse(objectId, methodName, input, o);
        }, options, false);
    }

    @Override
    public JsonElement callForType(String type, String methodName, JsonElement params, Options options) throws CordraException {
        return run((Options o) -> {
            return client.callForType(type, methodName, params, o);
        }, options, true);
    }

    @Override
    public JsonElement callForType(String type, String methodName, InputStream input, Options options) throws CordraException {
        return run((Options o) -> {
            return client.callForType(type, methodName, input, o);
        }, options, false);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, JsonElement params, Options options) throws CordraException {
        return run((Options o) -> {
            return client.callForTypeAsResponse(type, methodName, params, o);
        }, options, true);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, InputStream input, Options options) throws CordraException {
        return run((Options o) -> {
            return client.callForTypeAsResponse(type, methodName, input, o);
        }, options, false);
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads, Options options) throws CordraException {
        return run((Options o) -> {
            return client.publishVersion(objectId, versionId, clonePayloads, o);
        }, options, true);
    }

    @Override
    public List<VersionInfo> getVersionsFor(String objectId, Options options) throws CordraException {
        return run((Options o) -> {
            return client.getVersionsFor(objectId, o);
        }, options, true);
    }

    @Override
    public void delete(String objectId, Options options) throws CordraException {
        run((Options o) -> {
            client.delete(objectId, o);
            return null;
        }, options, true);
    }

    @Override
    public SearchResults<CordraObject> list(Options options) throws CordraException {
        return run((Options o) -> {
            return client.list(o);
        }, options, true);
    }

    @Override
    public SearchResults<String> listHandles(Options options) throws CordraException {
        return run((Options o) -> {
            return client.listHandles(o);
        }, options, true);
    }

    @Override
    public SearchResults<CordraObject> search(String query, Options options) throws CordraException {
        return run((Options o) -> {
            return client.search(query, o);
        }, options, true);
    }

    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params, Options options) throws CordraException {
        return run((Options o) -> {
            return client.search(query, params, o);
        }, options, true);
    }

    @Override
    public SearchResults<String> searchHandles(String query, Options options) throws CordraException {
        return run((Options o) -> {
            return client.searchHandles(query, o);
        }, options, true);
    }

    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params, Options options) throws CordraException {
        return run((Options o) -> {
            return client.searchHandles(query, params, o);
        }, options, true);
    }

    @Override
    public AuthResponse authenticateAndGetResponse(Options options) throws CordraException {
        return run((Options o) -> {
            return client.authenticateAndGetResponse(o);
        }, options, true);
    }

    @Override
    public void changePassword(String newPassword, Options options) throws CordraException {
        client.changePassword(newPassword, options);
    }

    @Override
    public void reindexBatch(List<String> batchIds, Options options) throws CordraException {
        run((Options o) -> {
            client.reindexBatch(batchIds, o);
            return null;
        }, options, true);
    }

    @Override
    public AuthTokenResponse getAuthToken(Options options) throws CordraException {
        return client.getAuthToken(options);
    }

    @Override
    public AuthResponse introspectToken(Options options) throws CordraException {
        return client.introspectToken(options);
    }

    @Override
    public void revokeToken(Options options) throws CordraException {
        client.revokeToken(options);
    }

    private Options newOptionsForSession(Options oldOptions, String token) {
        String json = gson.toJson(oldOptions);
        Options options = gson.fromJson(json, Options.class);
        options.token = token;
        options.useDefaultCredentials = false;
        return options;
    }

    private TokenStatus tokenFor(String usernameParam, String passwordParam, Options options) throws CordraException {
        String token = authCache.getCachedToken(usernameParam, passwordParam);
        if (token != null) return new TokenStatus(token, true);
        token = acquireNewToken(options);
        authCache.storeToken(usernameParam, passwordParam, token);
        return new TokenStatus(token, false);
    }

    public <R> R run(ThrowingFunction<Options, R> f, Options originalOptions, boolean isRetryable) throws CordraException {
        boolean isCachedToken = false;
        UserKeyAndPassword userKeyAndPassword = userKeyAndPasswordOfOptions(originalOptions);
        String userKey = userKeyAndPassword.userKey;
        String passwordForUser = userKeyAndPassword.passwordForUser;
        Options optionsForRequest = originalOptions;
        try {
            if (!originalOptions.useDefaultCredentials && originalOptions.token != null) {
                isCachedToken = false;
            } else if (userKey != null) {
                TokenStatus tokenStatus = tokenFor(userKey, passwordForUser, originalOptions);
                isCachedToken = tokenStatus.isCached;
                optionsForRequest = newOptionsForSession(originalOptions, tokenStatus.token);
            } else {
                isCachedToken = false;
            }
            R result = f.apply(optionsForRequest);
            return result;
        } catch (UnauthorizedCordraException uce) {
            if (!isCachedToken) {
                throw uce;
            }
            authCache.removeToken(userKey);
            if (!isRetryable) {
                throw uce;
            }
            TokenStatus tokenStatus = tokenFor(userKey, passwordForUser, originalOptions);
            optionsForRequest = newOptionsForSession(originalOptions, tokenStatus.token);
            R result = f.apply(optionsForRequest);
            return result;
        }
    }

    public String acquireNewToken(Options options) throws CordraException {
        try {
            AuthTokenResponse authTokenResponse = client.getAuthToken(options);
            return authTokenResponse.token;
        } catch (BadRequestCordraException e) {
            JsonElement response = e.getResponse();
            throw new UnauthorizedCordraException(response, e);
//            throw new UnauthorizedCordraException(e);
        }
    }

    @FunctionalInterface
    public interface ThrowingFunction<T, R> {
        R apply(T t) throws CordraException;
    }

    static class TokenStatus {
        final String token;
        final boolean isCached;

        public TokenStatus(String token, boolean isCached) {
            this.token = token;
            this.isCached = isCached;
        }
    }

    private static class UserKeyAndPassword {
        public String userKey;
        public String passwordForUser;
    }

    public UserKeyAndPassword userKeyAndPasswordOfOptions(Options originalOptions) {
        UserKeyAndPassword result = new UserKeyAndPassword();
        if (originalOptions.useDefaultCredentials) {
            result.userKey = client.getDefaultUsername();
            result.passwordForUser = client.getDefaultPassword();
        } else if (originalOptions.userId != null) {
            result.userKey = originalOptions.userId;
            if (originalOptions.token == null && originalOptions.privateKey == null) {
                result.passwordForUser = originalOptions.password;
            }
        } else {
            result.userKey = originalOptions.username;
            if (originalOptions.token == null && originalOptions.privateKey == null) {
                result.passwordForUser = originalOptions.password;
            }
        }
        return result;
    }
}
