package net.cnri.cordra.doip;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import net.cnri.cordra.api.*;
import net.cnri.cordra.api.QueryParams;
import net.cnri.cordra.api.SearchResults;
import net.cnri.cordra.util.AuthUtil;
import net.cnri.cordra.util.DelegatedCloseableInputStream;
import net.cnri.cordra.util.GsonUtility;
import net.cnri.util.StreamUtil;
import net.dona.doip.*;
import net.dona.doip.client.*;
import net.dona.doip.client.http.HttpDoipClient;
import net.dona.doip.client.transport.DoipClientResponse;
import net.dona.doip.util.InDoipMessageUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.Map.Entry;

public class DoipCordraClient implements CordraClient {

    public static final String OP_AUTH_TOKEN = "20.DOIP/Op.Auth.Token";
    public static final String OP_AUTH_INTROSPECT = "20.DOIP/Op.Auth.Introspect";
    public static final String OP_AUTH_REVOKE = "20.DOIP/Op.Auth.Revoke";

    protected DoipClientInterface client;
    protected String serviceId;
    protected ServiceInfo serviceInfo;
    protected final String username;
    protected final String password;
    protected PasswordAuthenticationInfo defaultAuthInfo;

    public DoipCordraClient(DoipClientInterface client, ServiceInfo serviceInfo, String username, String password) {
        this.serviceInfo = serviceInfo;
        this.client = client;
        this.serviceId = serviceInfo == null ? "service" : serviceInfo.serviceId;
        this.username = username;
        this.password = password;
        this.defaultAuthInfo = new PasswordAuthenticationInfo(username, password);
    }

    public DoipCordraClient(ServiceInfo serviceInfo, String username, String password) {
        this.serviceInfo = serviceInfo;
        this.serviceId = serviceInfo == null ? "service" : serviceInfo.serviceId;
        this.client = new DoipClient();
        this.username = username;
        this.password = password;
        this.defaultAuthInfo = new PasswordAuthenticationInfo(username, password);
    }

    public static DoipCordraClient createWithHttpDoipClient(String baseUri, String username, String password) throws CordraException {
        try {
            DoipClientInterface client = new HttpDoipClient(baseUri);
            return new DoipCordraClient(client, null, username, password);
        } catch (DoipException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public static DoipCordraClient createWithHttpDoipClient(ServiceInfo serviceInfo, String username, String password) throws CordraException {
        try {
            DoipClientInterface client = new HttpDoipClient(serviceInfo);
            return new DoipCordraClient(client, serviceInfo, username, password);
        } catch (DoipException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public static DoipCordraClient createWithNativeDoipClient(ServiceInfo serviceInfo, String username, String password) {
        DoipClientInterface client = new DoipClient();
        return new DoipCordraClient(client, serviceInfo, username, password);
    }

    @Override
    public void close() throws IOException, CordraException {
        client.close();
    }

    @Override
    public String getDefaultUsername() {
        return username;
    }

    @Override
    public String getDefaultPassword() {
        return password;
    }

    private static void setRequestContextIfNeeded(JsonObject attributes, Options options) {
        if (!attributes.has("requestContext") && options.requestContext != null) {
            attributes.add("requestContext", options.requestContext);
        }
    }

    @Override
    public CordraObject get(String id, Options options) throws CordraException {
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            // false is the default
            //attributes.addProperty("includeElementData", false);
            setRequestContextIfNeeded(attributes, options);
            if (options.filter != null) {
                attributes.add("filter", GsonUtility.getGson().toJsonTree(options.filter));
            }
            DigitalObject result = client.retrieve(id, attributes, authInfo, serviceInfo);
            CordraObject co = DoipUtil.toCordraObject(result);
            return co;
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public AuthTokenResponse getAuthToken(Options options) throws CordraException {
        try {
            JsonObject jsonInput = AuthUtil.buildAuthTokenInputFrom(options, username, password);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            setRequestContextIfNeeded(attributes, options);
            JsonElement responseJson = client.performOperationWithJsonResponse(serviceId, OP_AUTH_TOKEN, null, attributes, jsonInput, serviceInfo);
            AccessTokenResponse accessTokenResponse = GsonUtility.getGson().fromJson(responseJson, AccessTokenResponse.class);
            AuthTokenResponse authTokenResponse = new AuthTokenResponse(accessTokenResponse.active, accessTokenResponse.access_token, accessTokenResponse.userId, accessTokenResponse.username, accessTokenResponse.typesPermittedToCreate, accessTokenResponse.groupIds, accessTokenResponse.exp);
            return authTokenResponse;
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public AuthResponse introspectToken(Options options) throws CordraException {
        try {
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            setRequestContextIfNeeded(attributes, options);
            JsonElement jsonElement = client.performOperationWithJsonResponse(serviceId, "20.DOIP/Op.Auth.Introspect", null, attributes, buildTokenInput(options), serviceInfo);
            return GsonUtility.getGson().fromJson(jsonElement, AuthResponse.class);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        }
    }

    @Override
    public void revokeToken(Options options) throws CordraException {
        try {
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            setRequestContextIfNeeded(attributes, options);
            client.performOperationWithJsonResponse(serviceId, "20.DOIP/Op.Auth.Revoke", null, attributes, buildTokenInput(options), serviceInfo);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        }
    }

    private JsonElement buildTokenInput(Options options) {
        JsonObject json = new JsonObject();
        if (options.token != null) {
            json.addProperty("token", options.token);
        }
        return json;
    }

    @Override
    public SearchResults<CordraObject> get(Collection<String> ids) throws CordraException {
        throw new UnsupportedOperationException();
    }

    @Override
    public InputStream getPayload(String id, String payloadName, Options options) throws CordraException {
        InputStream in = null;
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        setRequestContextIfNeeded(attributes, options);
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            in = client.retrieveElement(id, payloadName, authInfo, serviceInfo, attributes);
            return in;
        } catch (DoipException de) {
            closeQuietly(in);
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            closeQuietly(in);
            throw new InternalErrorCordraException(e);
        }
    }

    private static void closeQuietly(InputStream in) {
        if (in != null) try { in.close(); } catch (Exception ex) { }
    }

    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        InputStream in = null;
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        setRequestContextIfNeeded(attributes, options);
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            in = client.retrievePartialElement(id, payloadName, start, end, authInfo, serviceInfo, attributes);
            return in;
        } catch (DoipException de) {
            closeQuietly(in);
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            closeQuietly(in);
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public CordraObject create(CordraObject co, Options options) throws CordraException {
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            DigitalObject dobj = DoipUtil.ofCordraObject(co, true);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            setRequestContextIfNeeded(attributes, options);
            DigitalObject result = client.create(dobj, authInfo, serviceInfo, attributes);
            CordraObject coResult = DoipUtil.toCordraObject(result);
            return coResult;
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public CordraObject update(CordraObject co, Options options) throws CordraException {
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            DigitalObject dobj = DoipUtil.ofCordraObject(co, true);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            setRequestContextIfNeeded(attributes, options);
            DigitalObject result = client.update(dobj, authInfo, serviceInfo, attributes);
            CordraObject coResult = DoipUtil.toCordraObject(result);
            return coResult;
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public void delete(String id, Options options) throws CordraException {
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            setRequestContextIfNeeded(attributes, options);
            client.delete(id, authInfo, serviceInfo, attributes);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public SearchResults<CordraObject> list() throws CordraException {
        return search("*:*");
    }

    private boolean isJson(CallHeaders headers) {
        if (headers == null || headers.mediaType == null) return false;
        String type = headers.mediaType.toLowerCase(Locale.ROOT);
        if ("application/json".equals(type)) return true;
        if (!type.startsWith("application/json")) return false;
        char nextChar = type.charAt("application/json".length());
        if (nextChar == ' ' || nextChar == '\t' || nextChar ==';') return true;
        return false;
    }

    @SuppressWarnings("resource")
    private CallResponse extractCallResponse(DoipClientResponse resp, InDoipMessage input) throws IOException {
        InDoipMessage in = resp.getOutput();
        InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(in);
        CallResponse result = new CallResponse();
        DoipClientResponse finalResp = resp;
        result.body = new DelegatedCloseableInputStream(firstSegment.getInputStream(), () -> {
            if (input != null) try { input.close(); } catch (Exception e) { }
            try { in.close(); } catch (Exception e) { }
            try { finalResp.close(); } catch (Exception e) { }
        });
        if (firstSegment.isJson()) result.headers.mediaType = "application/json";
        return result;
    }

    private JsonElement extractJson(DoipClientResponse resp) throws IOException {
        try (InDoipMessage in = resp.getOutput()) {
            InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(in);
            if (firstSegment == null) return null;
            if (firstSegment.isJson()) return firstSegment.getJson();
            try (
                InputStream is = firstSegment.getInputStream();
                InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
            ) {
                String jsonString = StreamUtil.readFully(isr);
                if (jsonString.trim().isEmpty()) return null;
                return JsonParser.parseString(jsonString);
            }
        }
    }

    private InDoipMessage buildInDoipMessage(InputStream input, Options options) {
        boolean isJson = isJson(options.callHeaders);
        return new InDoipMessageFromCollection(Collections.singletonList(new InDoipSegmentFromInputStream(isJson, input)));
    }

    public void addFilenameAndMediaTypeToAttributesIfNecessary(JsonObject attributes, Options options) {
        if (attributes != null && !attributes.has("filename") && options.getCallHeaders() != null && options.callHeaders.filename != null) {
            attributes.addProperty("filename", options.callHeaders.filename);
        }
        if (attributes != null && !attributes.has("mediaType") && options.getCallHeaders() != null && options.callHeaders.mediaType != null) {
            attributes.addProperty("mediaType", options.callHeaders.mediaType);
        }
    }

    @Override
    public JsonElement callForType(String type, String methodName, JsonElement params, Options options) throws CordraException {
        AuthenticationInfo authInfo = authInfoFor(options);
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        addFilenameAndMediaTypeToAttributesIfNecessary(attributes, options);
        attributes.addProperty("isCallForType", "true");
        setRequestContextIfNeeded(attributes, options);
        try (DoipClientResponse resp = client.performOperation(type, methodName, authInfo, attributes, params, serviceInfo)) {
            if (!DoipConstants.STATUS_OK.equals(resp.getStatus())) {
                throw DoipClient.doipExceptionFromDoipResponse(resp);
            }
            return extractJson(resp);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public JsonElement callForType(String type, String methodName, InputStream input, Options options) throws CordraException {
        AuthenticationInfo authInfo = authInfoFor(options);
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        addFilenameAndMediaTypeToAttributesIfNecessary(attributes, options);
        attributes.addProperty("isCallForType", "true");
        setRequestContextIfNeeded(attributes, options);
        try (
            InDoipMessage inputAsInDoipMessage = buildInDoipMessage(input, options);
            DoipClientResponse resp = client.performOperation(type, methodName, authInfo, attributes, inputAsInDoipMessage, serviceInfo);
        ) {
            if (!DoipConstants.STATUS_OK.equals(resp.getStatus())) {
                throw DoipClient.doipExceptionFromDoipResponse(resp);
            }
            return extractJson(resp);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    @SuppressWarnings("resource")
    public CallResponse callForTypeAsResponse(String type, String methodName, JsonElement params, Options options) throws CordraException {
        AuthenticationInfo authInfo = authInfoFor(options);
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        addFilenameAndMediaTypeToAttributesIfNecessary(attributes, options);
        attributes.addProperty("isCallForType", "true");
        setRequestContextIfNeeded(attributes, options);
        DoipClientResponse resp = null;
        try {
            resp = client.performOperation(type, methodName, authInfo, attributes, params, serviceInfo);
            if (!DoipConstants.STATUS_OK.equals(resp.getStatus())) {
                throw DoipClient.doipExceptionFromDoipResponse(resp);
            }
            return extractCallResponse(resp, null);
        } catch (DoipException de) {
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    @SuppressWarnings("resource")
    public CallResponse callForTypeAsResponse(String type, String methodName, InputStream input, Options options) throws CordraException {
        AuthenticationInfo authInfo = authInfoFor(options);
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        addFilenameAndMediaTypeToAttributesIfNecessary(attributes, options);
        attributes.addProperty("isCallForType", "true");
        setRequestContextIfNeeded(attributes, options);
        DoipClientResponse resp = null;
        InDoipMessage inputAsInDoipMessage = null;
        try {
            inputAsInDoipMessage = buildInDoipMessage(input, options);
            resp = client.performOperation(type, methodName, authInfo, attributes, inputAsInDoipMessage, serviceInfo);
            if (!DoipConstants.STATUS_OK.equals(resp.getStatus())) {
                throw DoipClient.doipExceptionFromDoipResponse(resp);
            }
            return extractCallResponse(resp, inputAsInDoipMessage);
        } catch (DoipException de) {
            if (inputAsInDoipMessage != null) try { inputAsInDoipMessage.close(); } catch (Exception ex) { }
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            if (inputAsInDoipMessage != null) try { inputAsInDoipMessage.close(); } catch (Exception ex) { }
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public JsonElement call(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        AuthenticationInfo authInfo = authInfoFor(options);
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        addFilenameAndMediaTypeToAttributesIfNecessary(attributes, options);
        setRequestContextIfNeeded(attributes, options);
        try (DoipClientResponse resp = client.performOperation(objectId, methodName, authInfo, attributes, params, serviceInfo)) {
            if (!DoipConstants.STATUS_OK.equals(resp.getStatus())) {
                throw DoipClient.doipExceptionFromDoipResponse(resp);
            }
            return extractJson(resp);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public JsonElement call(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        AuthenticationInfo authInfo = authInfoFor(options);
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        addFilenameAndMediaTypeToAttributesIfNecessary(attributes, options);
        setRequestContextIfNeeded(attributes, options);
        try (
            InDoipMessage inputAsInDoipMessage = buildInDoipMessage(input, options);
            DoipClientResponse resp = client.performOperation(objectId, methodName, authInfo, attributes, inputAsInDoipMessage, serviceInfo);
        ) {
            if (!DoipConstants.STATUS_OK.equals(resp.getStatus())) {
                throw DoipClient.doipExceptionFromDoipResponse(resp);
            }
            return extractJson(resp);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    @SuppressWarnings("resource")
    public CallResponse callAsResponse(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        AuthenticationInfo authInfo = authInfoFor(options);
        DoipClientResponse resp = null;
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        addFilenameAndMediaTypeToAttributesIfNecessary(attributes, options);
        setRequestContextIfNeeded(attributes, options);
        try {
            resp = client.performOperation(objectId, methodName, authInfo, attributes, params, serviceInfo);
            if (!DoipConstants.STATUS_OK.equals(resp.getStatus())) {
                throw DoipClient.doipExceptionFromDoipResponse(resp);
            }
            return extractCallResponse(resp, null);
        } catch (DoipException de) {
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    @SuppressWarnings("resource")
    public CallResponse callAsResponse(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        AuthenticationInfo authInfo = authInfoFor(options);
        DoipClientResponse resp = null;
        InDoipMessage inputAsInDoipMessage = null;
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        addFilenameAndMediaTypeToAttributesIfNecessary(attributes, options);
        setRequestContextIfNeeded(attributes, options);
        try {
            inputAsInDoipMessage = buildInDoipMessage(input, options);
            resp = client.performOperation(objectId, methodName, authInfo, attributes, inputAsInDoipMessage, serviceInfo);
            if (!DoipConstants.STATUS_OK.equals(resp.getStatus())) {
                throw DoipClient.doipExceptionFromDoipResponse(resp);
            }
            return extractCallResponse(resp, inputAsInDoipMessage);
        } catch (DoipException de) {
            if (inputAsInDoipMessage != null) try { inputAsInDoipMessage.close(); } catch (Exception ex) { }
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            if (inputAsInDoipMessage != null) try { inputAsInDoipMessage.close(); } catch (Exception ex) { }
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public List<String> listMethods(String objectId, Options options) throws CordraException {
        JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
        setRequestContextIfNeeded(attributes, options);
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            List<String> res = new ArrayList<>(client.listOperations(objectId, authInfo, serviceInfo, attributes));
            //res.remove(DoipConstants.OP_LIST_OPERATIONS);
            res.remove(DoipConstants.OP_RETRIEVE);
            res.remove(DoipConstants.OP_UPDATE);
            res.remove(DoipConstants.OP_DELETE);
            return res;
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params, Options options) throws CordraException {
        net.dona.doip.client.SearchResults<DigitalObject> doResults = null;
        try {
            doResults = searchIdsOrFull("full", DigitalObject.class, serviceId, query, params, options);
            return new CordraObjectFromDigitalObjectSearchResults(doResults);
        } catch (DoipException de) {
            closeQuietly(doResults);
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            closeQuietly(doResults);
            throw new InternalErrorCordraException(e);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params, Options options) throws CordraException {
        net.dona.doip.client.SearchResults<String> idResults = null;
        try {
            idResults = searchIdsOrFull("id", String.class, serviceId, query, params, options);
            return new CordraIdFromDoipIdSearchResults(idResults);
        } catch (DoipException de) {
            closeQuietly(idResults);
            throw cordraExceptionFromDoipException(de);
        } catch (Exception e) {
            closeQuietly(idResults);
            throw new InternalErrorCordraException(e);
        }
    }

    private <T> net.dona.doip.client.SearchResults<T> searchIdsOrFull(String type, Class<T> klass, String targetId, String query, QueryParams params, Options options) throws DoipException {
        DoipClientResponse resp = null;
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            if (params == null) params = QueryParams.DEFAULT;
            JsonObject queryParametersAsJsonObject = GsonUtility.getGson().toJsonTree(params).getAsJsonObject();
            for (Entry<String, JsonElement> entry : queryParametersAsJsonObject.entrySet()) {
                attributes.add(entry.getKey(), entry.getValue());
            }
            setRequestContextIfNeeded(attributes, options);
            attributes.addProperty("query", query);
            if (type == null) {
                type = "full";
            }
            attributes.addProperty("type", type);
            resp = client.performOperation(targetId, DoipConstants.OP_SEARCH, authInfo, attributes, serviceInfo);
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return new DoipSearchResults<>(resp, klass);
            } else {
                throw AbstractDoipClient.doipExceptionFromDoipResponse(resp);
            }
        } catch (Exception e) {
            closeQuietly(resp);
            if (e instanceof DoipException) throw (DoipException) e;
            throw new DoipException(e);
        }
    }

    private static void closeQuietly(AutoCloseable response) {
        if (response != null) try { response.close(); } catch (Exception ex) { }
    }

    @Override
    public SearchResults<String> listHandles() throws CordraException {
        return searchHandles("*:*");
    }

    @Override
    public AuthResponse authenticateAndGetResponse(Options options) throws CordraException {
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            setRequestContextIfNeeded(attributes, options);
            JsonElement jsonElement = client.performOperationWithJsonResponse(serviceId, "20.DOIP/Op.CheckCredentials", authInfo, attributes, serviceInfo);
            return GsonUtility.getGson().fromJson(jsonElement, AuthResponse.class);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        }
    }

    @Override
    public void changePassword(String newPassword, Options options) throws CordraException {
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            attributes.addProperty("password", newPassword);
            setRequestContextIfNeeded(attributes, options);
            client.performOperationWithJsonResponse(serviceId, "20.DOIP/Op.ChangePassword", authInfo, attributes, serviceInfo);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        }
    }

    @Override
    public void reindexBatch(List<String> batchIds, Options options) throws CordraException {
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            attributes.addProperty("lockObjects", options.reindexBatchLockObjects);
            JsonElement input = GsonUtility.getGson().toJsonTree(batchIds);
            setRequestContextIfNeeded(attributes, options);
            client.performOperationWithJsonResponse(serviceId, "20.DOIP/Op.ReindexBatch", authInfo, attributes, input, serviceInfo);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        }
    }

    @Override
    public List<VersionInfo> getVersionsFor(String id, Options options) throws CordraException {
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            setRequestContextIfNeeded(attributes, options);
            JsonElement jsonElement = client.performOperationWithJsonResponse(id, "20.DOIP/Op.Versions.Get", authInfo, attributes, serviceInfo);
            JsonArray jsonArray = jsonElement.getAsJsonArray();
            List<VersionInfo> versionInfos = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                VersionInfo versionInfo = GsonUtility.getGson().fromJson(jsonObject, VersionInfo.class);
                versionInfos.add(versionInfo);
            }
            return versionInfos;
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        }
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads, Options options) throws CordraException {
        try {
            AuthenticationInfo authInfo = authInfoFor(options);
            JsonObject attributes = (options.attributes != null) ? options.attributes : new JsonObject();
            if (versionId != null) {
                attributes.addProperty("versionId", versionId);
            }
            attributes.add("clonePayloads", new JsonPrimitive(clonePayloads));
            setRequestContextIfNeeded(attributes, options);
            JsonElement jsonElement = client.performOperationWithJsonResponse(objectId, "20.DOIP/Op.Versions.Publish", authInfo, attributes, serviceInfo);
            return GsonUtility.getGson().fromJson(jsonElement, VersionInfo.class);
        } catch (DoipException de) {
            throw cordraExceptionFromDoipException(de);
        }
    }

    protected AuthenticationInfo authInfoFor(Options options) {
        if (options == null) {
            return null;
        } else if (options.useDefaultCredentials) {
            return defaultAuthInfo;
        } else if (options.token != null) {
            return new TokenAuthenticationInfo(options.userId, options.token, options.asUserId);
        } else if (options.privateKey != null) {
            if (options.userId != null) {
                return new PrivateKeyAuthenticationInfo(options.userId, options.privateKey, options.asUserId);
            } else {
                return new PrivateKeyAuthenticationInfo(options.username, options.privateKey, options.asUserId);
            }
        } else if (options.username != null && options.password != null) {
            return new PasswordAuthenticationInfo(options.username, options.password, options.asUserId);
        } else {
            return null;
        }
    }

    protected CordraException cordraExceptionFromDoipException(DoipException e) {
        String doipStatus = e.getStatusCode();
        String message = e.getMessage();
        if (DoipConstants.STATUS_CONFLICT.equals(doipStatus)) {
            return new ConflictCordraException(message, e);
        } else if (DoipConstants.STATUS_BAD_REQUEST.equals(doipStatus)) {
            if (message != null) {
                // TODO reconsider this
                if (message.startsWith("Too many authentication requests")) {
                    return new BadRequestCordraException(e.getResponse(), 429, e);
                }
            }
            return new BadRequestCordraException(e.getResponse(), e);
        } else if (DoipConstants.STATUS_NOT_FOUND.equals(doipStatus)) {
            return new NotFoundCordraException(message, e);
        } else if (DoipConstants.STATUS_DECLINED.equals(doipStatus)) {
            return new NotFoundCordraException(message, e);
        } else if (DoipConstants.STATUS_UNAUTHENTICATED.equals(doipStatus)) {
            JsonObject response = CordraException.responseForMessage(message).getAsJsonObject();
            if ("Password change required".equals(message)) {
                response.addProperty(UnauthorizedCordraException.PASSWORD_CHANGE_REQUIRED_PROPERTY, true);
            }
            UnauthorizedCordraException unauthorizedCordraException = new UnauthorizedCordraException(response, e);
            return unauthorizedCordraException;
        } else if (DoipConstants.STATUS_FORBIDDEN.equals(doipStatus)) {
            return new ForbiddenCordraException(message, e);
        } else if (DoipConstants.STATUS_ERROR.equals(doipStatus)) {
            return new InternalErrorCordraException(message, e);
        } else {
            return new InternalErrorCordraException(message, e);
        }
    }
}