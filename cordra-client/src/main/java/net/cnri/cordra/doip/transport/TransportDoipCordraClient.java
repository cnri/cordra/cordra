package net.cnri.cordra.doip.transport;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.api.*;
import net.cnri.cordra.doip.DoipUtil;
import net.cnri.cordra.util.DelegatedCloseableInputStream;
import net.cnri.cordra.util.GsonUtility;
import net.cnri.util.StreamUtil;
import net.dona.doip.*;
import net.dona.doip.client.DigitalObject;
import net.dona.doip.client.transport.*;
import net.dona.doip.util.ErrorMessageUtil;
import net.dona.doip.util.InDoipMessageUtil;
import net.cnri.jws.JsonWebSignature;
import net.cnri.jws.JsonWebSignatureFactory;
import org.apache.commons.codec.binary.Hex;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.util.*;

public class TransportDoipCordraClient implements CordraClient {

    private TransportDoipClient client;
    private String serviceId; //handle of the DOIP service
    private Gson gson;
    private final SecureRandom random;
    protected final String username;
    protected final String password;
    private DoipConnectionPool connectionPool;

    public TransportDoipCordraClient(String host, int port, String serviceId, String username, String password) throws Exception {
        this(buildConnectionOptions(host, port, serviceId), username, password);
    }

    private static ConnectionOptions buildConnectionOptions(String host, int port, String serverId) {
        ConnectionOptions connectionOptions = new ConnectionOptions();
        connectionOptions.address = host;
        connectionOptions.port = port;
        connectionOptions.serverId = serverId;
        return connectionOptions;
    }

    public TransportDoipCordraClient(ConnectionOptions connectionOptions, String username, String password) throws Exception {
        client = new TransportDoipClient();
        gson = GsonUtility.getGson();
        this.serviceId = connectionOptions.serverId;
        this.username = username;
        this.password = password;
        this.random = new SecureRandom();
        connectionPool = new DoipConnectionPool(10, client, connectionOptions);
    }

    @SuppressWarnings("resource")
    @Override
    public CordraObject get(String id, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = id;
        headers.operationId = DoipConstants.OP_RETRIEVE;
        headers.authentication = authenticationObjectFor(options);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (DoipClientResponse resp = conn.sendCompactRequest(headers)) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                try (InDoipMessage in = resp.getOutput()) { //Consider resp.getContent() rename
                    CordraObject resultCo = DoipUtil.cordraObjectFromSegments(in);
                    return resultCo;
                }
            } else if (resp.getStatus().equals(DoipConstants.STATUS_NOT_FOUND)) {
                return null;
            }else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @Override
    public SearchResults<CordraObject> get(Collection<String> ids) throws CordraException {
        throw new UnsupportedOperationException();
    }

    @SuppressWarnings("resource")
    @Override
    public InputStream getPayload(String id, String payloadName, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = id;
        headers.operationId = DoipConstants.OP_RETRIEVE;
        headers.authentication = authenticationObjectFor(options);
        headers.attributes = new JsonObject();
        headers.attributes.addProperty("element", payloadName);
        //headers.attributes.addProperty("includeElementData", "true");
        DoipConnection conn = null;
        DoipClientResponse resp = null;
        try {
            conn = connectionPool.get();
            resp = conn.sendCompactRequest(headers);
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                    InDoipMessage in = resp.getOutput();
                    InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(in);
                    if (firstSegment == null) {
                        throw new InternalErrorCordraException("Missing first segment");
                    }
                    return getPayloadInputStreamWithCorrectClose(firstSegment, resp, conn, connectionPool);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            closeQuietly(resp, conn, connectionPool);
            throw cox;
        } catch (Exception e) {
            closeQuietly(resp, conn, connectionPool);
            throw new InternalErrorCordraException(e);
        }
    }

    private static void closeQuietly(DoipClientResponse response, DoipConnection conn, DoipConnectionPool connectionPool) {
        if (response != null) try { response.close(); } catch (Exception ex) { }
        if (conn != null) try { connectionPool.release(conn); } catch (Exception e) {}
    }

    private static InputStream getPayloadInputStreamWithCorrectClose(InDoipSegment doipSegment, DoipClientResponse response, DoipConnection conn, DoipConnectionPool connectionPool) {
        return new DelegatedCloseableInputStream(doipSegment.getInputStream(), () -> closeQuietly(response, conn, connectionPool));
    }

    @SuppressWarnings("resource")
    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = id;
        headers.operationId = DoipConstants.OP_RETRIEVE;
        headers.authentication = authenticationObjectFor(options);
        headers.attributes = new JsonObject();
        headers.attributes.addProperty("element", payloadName);
        JsonObject range = new JsonObject();
        range.addProperty("start", start);
        range.addProperty("end", end);
        headers.attributes.add("range", range);
        DoipConnection conn = null;
        DoipClientResponse resp = null;
        try {
            conn = connectionPool.get();
            resp = conn.sendCompactRequest(headers);
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                    InDoipMessage in = resp.getOutput();
                    InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(in);
                    if (firstSegment == null) {
                        throw new InternalErrorCordraException("Missing first segment");
                    }
                    return getPayloadInputStreamWithCorrectClose(firstSegment, resp, conn, connectionPool);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            closeQuietly(resp, conn, connectionPool);
            throw cox;
        } catch (Exception e) {
            closeQuietly(resp, conn, connectionPool);
            throw new InternalErrorCordraException(e);
        }
        }

    @SuppressWarnings("resource")
    @Override
    public CordraObject create(CordraObject co, Options options) throws CordraException {
        DigitalObject dobj = DoipUtil.ofCordraObject(co);
        JsonObject dobjJson = gson.toJsonTree(dobj).getAsJsonObject();
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = serviceId;
        headers.operationId = DoipConstants.OP_CREATE;
        headers.authentication = authenticationObjectFor(options);
        List<InDoipSegment> segments = new ArrayList<>();
        InDoipSegment dobjSegment = new InDoipSegmentFromJson(dobjJson);
        segments.add(dobjSegment);
        if (co.payloads != null) {
            for (Payload p : co.payloads) {
                JsonObject elementSegmentJson = new JsonObject();
                elementSegmentJson.addProperty("id", p.name);
                InDoipSegment elementHeaderSegment = new InDoipSegmentFromJson(elementSegmentJson);
                segments.add(elementHeaderSegment);
                InDoipSegment elementBytesSegment = new InDoipSegmentFromInputStream(false, p.getInputStream());
                segments.add(elementBytesSegment);
            }
        }
        InDoipMessage inMessage = new InDoipMessageFromCollection(segments);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (DoipClientResponse resp = conn.sendRequest(headers, inMessage)) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                try (InDoipMessage in = resp.getOutput()) { //Consider resp.getContent() rename
                    CordraObject resultCo = DoipUtil.cordraObjectFromSegments(in);
                    return resultCo;
                }
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    private static DoipConnection getConnectionOrThrow(DoipConnectionPool connectionPool) throws CordraException {
        try {
            DoipConnection conn = connectionPool.get();
            return conn;
        } catch (IOException ioe) {
            throw new InternalErrorCordraException(ioe);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public CordraObject update(CordraObject co, Options options) throws CordraException {
        DigitalObject dobj = DoipUtil.ofCordraObject(co);
        JsonObject dobjJson = gson.toJsonTree(dobj).getAsJsonObject();
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = co.id;
        headers.operationId = DoipConstants.OP_UPDATE;
        headers.authentication = authenticationObjectFor(options);
        List<InDoipSegment> segments = new ArrayList<>();
        InDoipSegment dobjSegment = new InDoipSegmentFromJson(dobjJson);
        segments.add(dobjSegment);
        if (co.payloads != null) {
            for (Payload p : co.payloads) {
                InputStream payloadIn = p.getInputStream();
                if (payloadIn != null) {
                    JsonObject elementSegmentJson = new JsonObject();
                    elementSegmentJson.addProperty("id", p.name);
                    InDoipSegment elementHeaderSegment = new InDoipSegmentFromJson(elementSegmentJson);
                    segments.add(elementHeaderSegment);
                    InDoipSegment elementBytesSegment = new InDoipSegmentFromInputStream(false, payloadIn);
                    segments.add(elementBytesSegment);
                }
            }
        }
        InDoipMessage inMessage = new InDoipMessageFromCollection(segments);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (DoipClientResponse resp = conn.sendRequest(headers, inMessage)) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                try (InDoipMessage in = resp.getOutput()) { //Consider resp.getContent() rename
                    CordraObject resultCo = DoipUtil.cordraObjectFromSegments(in);
                    return resultCo;
                }
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public void delete(String id, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = id;
        headers.operationId = DoipConstants.OP_DELETE;
        headers.authentication = authenticationObjectFor(options);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (DoipClientResponse resp = conn.sendCompactRequest(headers)) {
            if (!resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                throw cordraExceptionFromDoipStatus(resp);
            }
        }catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @Override
    public SearchResults<CordraObject> list() throws CordraException {
        return search("*:*");
    }

    private boolean isJson(CallHeaders headers) {
        if (headers == null || headers.mediaType == null) return false;
        String type = headers.mediaType.toLowerCase(Locale.ROOT);
        if ("application/json".equals(type)) return true;
        if (!type.startsWith("application/json")) return false;
        char nextChar = type.charAt("application/json".length());
        if (nextChar == ' ' || nextChar == '\t' || nextChar ==';') return true;
        return false;
    }

    private InDoipMessage buildInDoipMessage(InputStream input, Options options) {
        boolean isJson = isJson(options.callHeaders);
        return new InDoipMessageFromCollection(Collections.singletonList(new InDoipSegmentFromInputStream(isJson, input)));
    }

    @SuppressWarnings("resource")
    private CallResponse extractCallResponse(DoipClientResponse resp, InDoipMessage input) throws IOException {
        InDoipMessage in = resp.getOutput();
        InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(in);
        CallResponse result = new CallResponse();
        DoipClientResponse finalResp = resp;
        result.body = new DelegatedCloseableInputStream(firstSegment.getInputStream(), () -> {
            if (input != null) try { input.close(); } catch (Exception e) { }
            try { in.close(); } catch (Exception e) { }
            try { finalResp.close(); } catch (Exception e) { }
        });
        if (firstSegment.isJson()) result.headers.mediaType = "application/json";
        return result;
    }

    private JsonElement extractJson(DoipClientResponse resp) throws IOException {
        try (InDoipMessage in = resp.getOutput()) {
            InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(in);
            if (firstSegment == null) return null;
            if (firstSegment.isJson()) return firstSegment.getJson();
            try (
                    InputStream is = firstSegment.getInputStream();
                    InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
            ) {
                String jsonString = StreamUtil.readFully(isr);
                if (jsonString.trim().isEmpty()) return null;
                return JsonParser.parseString(jsonString);
            }
        }
    }

    @SuppressWarnings("resource")
    @Override
    public JsonElement callForType(String type, String methodName, JsonElement params, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.attributes = new JsonObject();
        headers.attributes.addProperty("isCallForType", "true");
        headers.targetId = type;
        headers.operationId = methodName;
        headers.authentication = authenticationObjectFor(options);
        headers.input = params;
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (DoipClientResponse resp = conn.sendCompactRequest(headers)) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return extractJson(resp);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public JsonElement callForType(String type, String methodName, InputStream input, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.attributes = new JsonObject();
        headers.attributes.addProperty("isCallForType", "true");
        headers.targetId = type;
        headers.operationId = methodName;
        headers.authentication = authenticationObjectFor(options);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (
                InDoipMessage inputAsInDoipMessage = buildInDoipMessage(input, options);
                DoipClientResponse resp = conn.sendRequest(headers, inputAsInDoipMessage);
        ) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return extractJson(resp);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, JsonElement params, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.attributes = new JsonObject();
        headers.attributes.addProperty("isCallForType", "true");
        headers.targetId = type;
        headers.operationId = methodName;
        headers.authentication = authenticationObjectFor(options);
        headers.input = params;
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        DoipClientResponse resp = null;
        try {
            resp = conn.sendCompactRequest(headers);
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return extractCallResponse(resp, null);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            try { resp.close(); } catch (Exception ex) { }
            throw cox;
        } catch (Exception e) {
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, InputStream input, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.attributes = new JsonObject();
        headers.attributes.addProperty("isCallForType", "true");
        headers.targetId = type;
        headers.operationId = methodName;
        headers.authentication = authenticationObjectFor(options);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        DoipClientResponse resp = null;
        InDoipMessage inputAsInDoipMessage = null;
        try {
            inputAsInDoipMessage = buildInDoipMessage(input, options);
            resp = conn.sendRequest(headers, inputAsInDoipMessage);
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return extractCallResponse(resp, null);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            if (inputAsInDoipMessage != null) try { inputAsInDoipMessage.close(); } catch (Exception ex) { }
            try { resp.close(); } catch (Exception ex) { }
            throw cox;
        } catch (Exception e) {
            if (inputAsInDoipMessage != null) try { inputAsInDoipMessage.close(); } catch (Exception ex) { }
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public JsonElement call(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = objectId;
        headers.operationId = methodName;
        headers.authentication = authenticationObjectFor(options);
        headers.input = params;
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (DoipClientResponse resp = conn.sendCompactRequest(headers)) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return extractJson(resp);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public JsonElement call(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = objectId;
        headers.operationId = methodName;
        headers.authentication = authenticationObjectFor(options);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (
                InDoipMessage inputAsInDoipMessage = buildInDoipMessage(input, options);
                DoipClientResponse resp = conn.sendRequest(headers, inputAsInDoipMessage);
        ) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return extractJson(resp);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public CallResponse callAsResponse(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = objectId;
        headers.operationId = methodName;
        headers.authentication = authenticationObjectFor(options);
        headers.input = params;
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        DoipClientResponse resp = null;
        try {
            resp = conn.sendCompactRequest(headers);
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return extractCallResponse(resp, null);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            try { resp.close(); } catch (Exception ex) { }
            throw cox;
        } catch (Exception e) {
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public CallResponse callAsResponse(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = objectId;
        headers.operationId = methodName;
        headers.authentication = authenticationObjectFor(options);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        InDoipMessage inputAsInDoipMessage = null;
        DoipClientResponse resp = null;
        try {
            inputAsInDoipMessage = buildInDoipMessage(input, options);
            resp = conn.sendRequest(headers, inputAsInDoipMessage);
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return extractCallResponse(resp, null);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            if (inputAsInDoipMessage != null) try { inputAsInDoipMessage.close(); } catch (Exception ex) { }
            try { resp.close(); } catch (Exception ex) { }
            throw cox;
        } catch (Exception e) {
            if (inputAsInDoipMessage != null) try { inputAsInDoipMessage.close(); } catch (Exception ex) { }
            if (resp != null) try { resp.close(); } catch (Exception ex) { }
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public List<String> listMethods(String objectId, Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = objectId;
        headers.operationId = DoipConstants.OP_LIST_OPERATIONS;
        headers.authentication = authenticationObjectFor(options);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (DoipClientResponse resp = conn.sendCompactRequest(headers)) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                try (InDoipMessage in = resp.getOutput()) {
                    InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(in);
                    if (firstSegment == null) {
                        throw new BadRequestCordraException("Missing first segment in response");
                    }
                    List<String> results = GsonUtility.getGson().fromJson(firstSegment.getJson(), new TypeToken<List<String>>(){}.getType());
                    results.remove(DoipConstants.OP_LIST_OPERATIONS);
                    results.remove(DoipConstants.OP_RETRIEVE);
                    results.remove(DoipConstants.OP_UPDATE);
                    results.remove(DoipConstants.OP_DELETE);
                    return results;
                }
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    private static String sortFieldsToString(List<SortField> sortFields) {
        if (sortFields != null && !sortFields.isEmpty()) {
            List<String> sortFieldsForTransport = new ArrayList<>(sortFields.size());
            for(SortField sortField : sortFields) {
                if(sortField.isReverse()) sortFieldsForTransport.add(sortField.getName() + " DESC");
                else sortFieldsForTransport.add(sortField.getName());
            }
            if (!sortFieldsForTransport.isEmpty()) {
                return String.join(",", sortFieldsForTransport);
            }
        }
        return null;
    }

    @SuppressWarnings("resource")
    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params, Options options) throws CordraException {//        if (params == null) params = QueryParams.DEFAULT;
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        DoipClientResponse resp = null;
        try {
            resp = search("full", query, params, options, conn);
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                return new TransportDoipCordraClientSearchResults<>(resp, conn, connectionPool, CordraObject.class);
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            closeQuietly(resp, conn, connectionPool);
            throw cox;
        } catch (Exception e) {
            closeQuietly(resp, conn, connectionPool);
            throw new InternalErrorCordraException(e);
        }
    }

    private DoipClientResponse search(String type, String query, QueryParams params, Options options, DoipConnection conn) throws IOException, InternalErrorCordraException {
        if (params == null) params = QueryParams.DEFAULT;
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = serviceId;
        headers.operationId = DoipConstants.OP_SEARCH;
        headers.attributes = new JsonObject();
        headers.attributes.addProperty("query", query);
        headers.attributes.addProperty("pageNum", params.getPageNum());
        headers.attributes.addProperty("pageSize", params.getPageSize());
        if (type == null) {
            type = "full";
        }
        headers.attributes.addProperty("type", type);
        if (params.getSortFields() != null) {
            String sortFields = sortFieldsToString(params.getSortFields());
            headers.attributes.addProperty("sortFields", sortFields);
        }
        headers.authentication = authenticationObjectFor(options);
        DoipClientResponse resp = conn.sendCompactRequest(headers);
        return resp;
    }

    @Override
    public SearchResults<String> listHandles() throws CordraException {
        return searchHandles("*:*");
    }

    @SuppressWarnings("resource")
    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params, Options options) throws CordraException {
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (DoipClientResponse resp = search("id", query, params, options, conn)) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                try (InDoipMessage in = resp.getOutput()) {
                    return new TransportDoipCordraClientSearchResults<>(resp, conn, connectionPool, String.class);
                }
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @SuppressWarnings("resource")
    @Override
    public AuthResponse authenticateAndGetResponse(Options options) throws CordraException {
        DoipRequestHeaders headers = new DoipRequestHeaders();
        headers.targetId = serviceId;
        headers.operationId = DoipConstants.OP_HELLO;
        headers.authentication = authenticationObjectFor(options);
        DoipConnection conn = getConnectionOrThrow(connectionPool);
        try (DoipClientResponse resp = conn.sendCompactRequest(headers)) {
            if (resp.getStatus().equals(DoipConstants.STATUS_OK)) {
                AuthResponse authResponse = new AuthResponse(true, options.userId, options.userId == null ? options.username : null, null, null, null);
                return authResponse;
//            } else if (resp.getStatus().equals(DoipConstants.STATUS_UNAUTHENTICATED)) {
//                AuthResponse authResponse = new AuthResponse(false, options.userId, options.userId == null ? options.username : null, null);
//                return authResponse;
            } else {
                throw cordraExceptionFromDoipStatus(resp);
            }
        } catch (CordraException cox) {
            throw cox;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            try { connectionPool.release(conn); } catch (Exception e) {}
        }
    }

    @Override
    public void changePassword(String newPassword) throws CordraException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void changePassword(String usernameParam, String passwordParam, String newPassword) throws CordraException {
        throw new UnsupportedOperationException();
    }

    private JsonObject authenticationObjectFor(Options options) throws InternalErrorCordraException {
        JsonObject authentication = new JsonObject();
        if (options.useDefaultCredentials) {
            authentication.addProperty("username", username);
            authentication.addProperty("password", password);
        } else if (options.token != null) {
            authentication.addProperty("token", options.token);
        } else if (options.privateKey != null) {
            String issuer;
            if (options.userId != null) {
                issuer = options.userId;
            } else {
                issuer = options.username;
            }
            String token = createBearerToken(options.privateKey, issuer);
            authentication.addProperty("token", token);
        } else if (options.username != null && options.password != null) {
            authentication.addProperty("username", options.username);
            authentication.addProperty("password", options.password);
        } else {
            return null; // anonymous
        }
        if (options.asUserId != null) {
            authentication.addProperty("asUserId", options.getAsUserId());
        }
        return authentication;
    }

    private String createBearerToken(PrivateKey privateKey, String iss) throws InternalErrorCordraException {
        long nowSeconds = System.currentTimeMillis() / 1000L;
        JsonObject claims = new JsonObject();
        claims.addProperty("iss", iss);
        claims.addProperty("sub", iss);
        claims.addProperty("jti", generateJti());
        claims.addProperty("iat", nowSeconds);
        claims.addProperty("exp", nowSeconds + 600);
        String claimsJson = claims.toString();
        JsonWebSignature jwt;
        try {
            jwt = JsonWebSignatureFactory.getInstance().create(claimsJson, privateKey);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
        return jwt.serialize();
    }

    private String generateJti() {
        byte bytes[] = new byte[10];
        random.nextBytes(bytes);
        String hex = Hex.encodeHexString(bytes);
        return hex;
    }

    private CordraException cordraExceptionFromDoipStatus(DoipClientResponse resp) throws IOException {
        String doipStatus = resp.getStatus();
        String message = ErrorMessageUtil.getMessageFromErrorResponse(resp);
        if (DoipConstants.STATUS_CONFLICT.equals(doipStatus)) {
            return new ConflictCordraException(message);
        } else if (DoipConstants.STATUS_BAD_REQUEST.equals(doipStatus)) {
            return new BadRequestCordraException(message);
        } else if (DoipConstants.STATUS_NOT_FOUND.equals(doipStatus)) {
            return new NotFoundCordraException(message);
        } else if (DoipConstants.STATUS_DECLINED.equals(doipStatus)) {
            return new NotFoundCordraException(message);
        } else if (DoipConstants.STATUS_UNAUTHENTICATED.equals(doipStatus)) {
            return new UnauthorizedCordraException(message);
        } else if (DoipConstants.STATUS_FORBIDDEN.equals(doipStatus)) {
            return new ForbiddenCordraException(message);
        } else if (DoipConstants.STATUS_ERROR.equals(doipStatus)) {
            return new InternalErrorCordraException(message);
        } else {
            return new InternalErrorCordraException(message);
        }
    }
}
