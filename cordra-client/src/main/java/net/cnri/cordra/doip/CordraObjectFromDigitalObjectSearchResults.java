package net.cnri.cordra.doip;

import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.FacetResult;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.SearchResults;
import net.cnri.cordra.api.UncheckedCordraException;
import net.cnri.cordra.util.GsonUtility;
import net.dona.doip.client.DigitalObject;

import java.util.Iterator;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

public class CordraObjectFromDigitalObjectSearchResults implements SearchResults<CordraObject> {

    private net.dona.doip.client.SearchResults<DigitalObject> doResults;

    public CordraObjectFromDigitalObjectSearchResults(net.dona.doip.client.SearchResults<DigitalObject> doResults) {
        this.doResults = doResults;
    }

    @Override
    public List<FacetResult> getFacets() {
        JsonArray doipFacets = doResults.getFacets();
        if (doipFacets != null) {
            List<FacetResult> result = GsonUtility.getGson().fromJson(doipFacets, new TypeToken<List<FacetResult>>(){}.getType());
            return result;
        }
        return null;
    }
    
    @Override
    public int size() {
        return doResults.size();
    }

    @Override
    public Iterator<CordraObject> iterator() {
        return new CordraObjectsFromDigitalObjectsIterator(doResults.iterator());
    }

    @Override
    public void close() {
        if (doResults != null) {
            doResults.close();
        }
    }

    private class CordraObjectsFromDigitalObjectsIterator implements Iterator<CordraObject> {

        private Iterator<DigitalObject> doIter;

        public CordraObjectsFromDigitalObjectsIterator(Iterator<DigitalObject> doIter) {
            this.doIter = doIter;
        }

        @Override
        public boolean hasNext() {
            try {
                return doIter.hasNext();
            } catch (Exception e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        }

        @Override
        public CordraObject next() {
            try {
                DigitalObject dobj = doIter.next();
                CordraObject co = DoipUtil.toCordraObject(dobj);
                return co;
            } catch (Exception e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        }
    }
}
