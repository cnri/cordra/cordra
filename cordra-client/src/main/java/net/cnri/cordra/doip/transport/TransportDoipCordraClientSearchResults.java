package net.cnri.cordra.doip.transport;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import net.cnri.cordra.api.*;
import net.cnri.cordra.doip.DoipUtil;
import net.cnri.cordra.util.GsonUtility;
import net.dona.doip.InDoipMessage;
import net.dona.doip.InDoipSegment;
import net.dona.doip.client.DigitalObject;
import net.dona.doip.client.transport.DoipClientResponse;
import net.dona.doip.client.transport.DoipConnection;
import net.dona.doip.client.transport.DoipConnectionPool;
import net.dona.doip.util.InDoipMessageUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class TransportDoipCordraClientSearchResults<T> implements SearchResults<T> {
    private final Gson gson = GsonUtility.getGson();
    private int size;
    private DoipClientResponse resp;
    private InDoipMessage in;
    private boolean closed = false;
    private JsonReader jsonReader;
    private DoipConnection conn;
    private DoipConnectionPool connectionPool;
    private final Class<T> klass;

    public TransportDoipCordraClientSearchResults(DoipClientResponse resp, DoipConnection conn, DoipConnectionPool connectionPool, Class<T> klass) throws BadRequestCordraException, IOException {
        this.klass = klass;
        this.resp = resp;
        this.conn = conn;
        this.connectionPool = connectionPool;
        in = resp.getOutput();
        InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(in);
        if (firstSegment == null) {
            throw new BadRequestCordraException("Missing input");
        }
        InputStream inputStream = firstSegment.getInputStream();
        @SuppressWarnings("resource")
        InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
        this.jsonReader = new JsonReader(isr);
        jsonReader.beginObject();
        @SuppressWarnings("hiding")
        int size = -1;
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            if ("size".equals(name)) {
                size = jsonReader.nextInt();
            } else if ("results".equals(name)) {
                jsonReader.beginArray();
                break;
            } else {
                jsonReader.nextString();
            }
        }
        this.size = size;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<T> iterator() {
        return new DoipMessageIterator();
    }

    @Override
    public void close() {
        closed = true;
        if (in != null) try { in.close(); } catch (Exception e) {}
        if (resp != null) try { resp.close(); } catch (Exception e) {}
        if (conn != null) try { connectionPool.release(conn); } catch (Exception e) {}
    }

    private class DoipMessageIterator implements Iterator<T> {
        private Boolean hasNextResult;

        @Override
        public boolean hasNext() {
            if (hasNextResult != null) return hasNextResult.booleanValue();
            if (closed) throw new IllegalStateException("Already closed");
            try {
                boolean res = jsonReader.hasNext();
                hasNextResult = res;
                if (res == false) {
                    close();
                }
                return res;
            } catch (IOException e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public T next() {
            if (hasNextResult == null) hasNext();
            if (!hasNextResult) throw new NoSuchElementException();
            hasNextResult = null;
            try {
                if (klass == CordraObject.class) {
                    DigitalObject dobj = gson.fromJson(jsonReader, DigitalObject.class);
                    CordraObject co = DoipUtil.toCordraObject(dobj);
                    return (T) co;
                } else if (klass == String.class) {
                    String handle = jsonReader.nextString();
                    return (T) handle;
                } else {
                    throw new AssertionError("Unexpected class " + klass);
                }
            } catch (Exception e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        }
    }
}
