package net.cnri.cordra.doip;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.api.FacetResult;
import net.cnri.cordra.api.SearchResults;
import net.cnri.cordra.util.GsonUtility;

import java.util.Iterator;
import java.util.List;

public class CordraIdFromDoipIdSearchResults implements SearchResults<String> {

    private net.dona.doip.client.SearchResults<String> idResults;

    public CordraIdFromDoipIdSearchResults(net.dona.doip.client.SearchResults<String> idResults) {
        this.idResults = idResults;
    }

    @Override
    public List<FacetResult> getFacets() {
        JsonArray doipFacets = idResults.getFacets();
        if (doipFacets != null) {
            List<FacetResult> result = GsonUtility.getGson().fromJson(doipFacets, new TypeToken<List<FacetResult>>(){}.getType());
            return result;
        }
        return null;
    }

    @Override
    public int size() {
        return idResults.size();
    }

    @Override
    public Iterator<String> iterator() {
        return idResults.iterator();
    }

    @Override
    public void close() {
        if (idResults != null) {
            idResults.close();
        }
    }
}
