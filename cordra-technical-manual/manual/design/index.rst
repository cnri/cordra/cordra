Application Design
==================

.. toctree::
   :maxdepth: 1
   :glob:

   introduction
   identifiers
   schemas
   design-object
   payloads
   authentication-and-access-controls
   javascript-lifecycle-hooks
   type-methods
   java-hooks-and-methods
   handle-integration
   object-versioning
   object-hashing
