.. _release_notes:

Release Notes
=============

What's New in Cordra v2.0
-------------------------

Please visit `Cordra v2.0 Features <https://www.cordra.org/cordra.html>`__ for the core features offered by
version 2 of the Cordra software.

Below you will find a list of incremental changes made to the software.

Changes in 2.5.2 release (2023-08-29):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning::

   Cordra v2.5.2 is a security update.

   Cordra v2.5.0 and v2.5.1 could potentially leak "internalMetadata" of objects via the 20.DOIP/Op.Relationships.Get operation or the /relationships API endpoint.
   The internalMetadata of User objects includes the salt and hash of the user password.  The password is not stored so is never leaked, but the leaked information
   potentially allows offline brute force password search.  This leaking only happens if the User objects are the target of handleReference properties,
   for example, User objects which are members of Group objects.  Additionally, this leaking is prevented if the User type has an
   onObjectResolution hook.

   Earlier versions of Cordra can allow object retrieval which bypasses the onObjectResolution hook, via the 20.DOIP/Op.Relationships.Get operation
   or the /relationships API endpoint.  This bypassing only happens for objects which contain a handleReference link to another object.  When the relationships
   of the target object are accessed, in earlier Cordra versions, the source object would be retrieved bypassing any onObjectResolution hook.

Bugfixes:

* Ensure that the 20.DOIP/Op.Relationships.Get operation and the /relationships API endpoint do not leak Cordra object internalMetadata.
* Ensure that the 20.DOIP/Op.Relationships.Get operation and the /relationships API endpoint always apply any relevant onObjectResolution hook to retrieved objects.

Changes in 2.5.1 release (2023-08-07):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning::

   Cordra v2.5.1 is a critical security update.

   Cordra v2.5.1 changes the behavior of object updates which alter
   the type of an object.  The new default is for such updates to be
   prevented.  If you need such updates, you will need to set design
   flag ``enableTypeChangeViaUpdate``, and carefully consider the ACLs
   and lifecycle hooks that will now affect such updates.

   In previous versions, only the write
   ACL of the old type and the lifecycle hooks of the new type would
   affect such an update.  Unless specifically prevented, this would
   allow users authorized to update any object, to cause the object to
   have a type that the users were not expected to be authorized to
   create.  That includes types like Schema that are only intended to
   be created or updated by administrators, and which allow code
   execution via lifecycle hooks.

   Going forward, the create ACL of the new type also affects the
   operation, which is sufficient to prevent the security issue with
   typical settings.  Additionally, lifecycle hooks allow more
   fine-grained control, both at the old type and the new type.  In
   general, if lifecycle hooks and ACLs would allow a user to delete
   an object and then create an object of the new type, then (when
   ``enableTypeChangeViaUpdate`` is active) the user will be able to
   update the object to have the new type.  Since the lifecycle hook
   interaction may be subtle, the ability to update types is now
   disabled by default.

New features:

* New design flag ``enableTypeChangeViaUpdate``, false by default, can be set true to allow
  the standard 0.DOIP/Op.Update operation to change the type of an object.
* An update which changes the type of an object accesses ACLs and lifecycle hooks as if it were a deletion
  (of an object of the original type) followed by a creation (of an object of the new type).
  In particular the write ACL of the old type and create ACL of the new type must both allow the operation;
  and the ``beforeDelete`` hook for the old type and ``beforeSchemaValidation`` hook of the new type
  both run; and the ``beforeSchemaValidation`` hook runs with the context attribute ``isNew`` set to true.
  The lifecycle hook context attribute ``isUpdate`` is set to true for the relevant hooks
  in case special type-update-specific logic is wanted.

Bugfixes:

* Ensure that users can call operations they are authorized to call, even if
  they are not authorized to retrieve the target object.
* Fix authentication backoff when it occurs with the "root" servlet instead of under /cordra.
* Fix issues with startup scripts when Cordra is installed in a path with space characters.

Changes in 2.5.0 release (2023-04-30):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning::

   As of Cordra v2.5.0, the default JavaScript engine is GraalVM JavaScript instead of Nashorn.
   We expect most Cordra JavaScript to work without change in the GraalVM JavaScript engine.
   In case your JavaScript requires Nashorn, you can configure Cordra to continue using Nashorn
   by adding to config.json::

      "javascript": {
          "engine": "nashorn"
      }

.. warning::

   The DOIP API for HTTP Clients from Cordra v2.3.0 until Cordra v2.4.0 would copy request "attributes" into request "input"
   for GET requests.  This is not done by default in Cordra v2.5.0 and later; instead the implementation of type methods can
   be sensitive to both attributes and input, separately.  To retain the earlier behavior, you can set the design property
   ``useLegacyAttributesAsInputForDoipGet``.

.. warning::

  As of Cordra v2.5.0, the ``onObjectResolution`` hook applies by default to the objects returned the the caller
  of create or update operations.  An indicator ``isCreate`` or ``isUpdate`` is available in the hook context
  of the ``onObjectResolution`` call.  To retain the earlier behavior where ``onObjectResolution`` is not called
  with the result of create or update operations, you can set the design property ``useLegacySkipOnObjectResolutionForCreateAndUpdate``.

New features:

* Cordra uses GraalVM JavaScript (instead of the deprecated Nashorn engine) by default; see :ref:`javascript-version`.
* Cordra JavaScript can use more recent JavaScript features, up to ECMAScript 2022.
* Cordra JavaScript can use ESM modules (import/export) instead of CJS modules (require/exports); see :ref:`esm-modules`.
* Java 17 support.
* Support for hooks and type methods written in Java, see :ref:`java-hooks-and-methods`.
* Service-level hooks (in "design" JavaScript or with Java annotation ``@CordraServiceHooks``) can serve
  as global default lifecycle hooks used for all object types.
* More Cordra APIs now accessible via DOIP, see :doc:`../api/doip-api-for-http-clients`.
* Sort order interface in Cordra UI.
* User-defined menus and query forms for Cordra UI; see :ref:`navBarLinks`.
* Support for custom authentication UI in Cordra UI; see :ref:`external-authentication-provider-custom-ui`.
* Cordra JSON Schema property allowing an input to be disabled in Cordra UI; see :ref:`disableInUi`.
* Added support for Elasticsearch 8 and dropped support for Elasticsearch 6. Current supported versions of Elasticsearch are 7 and 8.
* Upgrade MongoDb Java driver dependency to 4.7, compatible with MongoDb server versions from 2.6 to 6.0 inclusive.
* New module "cordra-client" for use in JavaScript hooks, allowing arbitrary calls against the host Cordra; see :ref:`cordra_client_module`.
* The implementations of type methods can now be responsive to request "attributes" as well as "input"; see :ref:`type-methods-attributes`.
* Indexing and searching of date-times via index fields prefixed with ``date_``; see :ref:`querying-date-time-properties`.
* Request context (see :ref:`requestContext`) is now available via DOIP by setting ``attributes.requestContext``.
* The ``onObjectResolution`` hook now applies to the results of create and update operations as returned to the caller.
* The authenticate hook can now return an arbitrary "authContext" which is visible in other hooks; see :ref:`authenticate`.
* The ``createHandleValues`` hook can be defined in the standard design JavaScript, instead of only on the handleMintingConfig JavaScript.
* The ``generateId`` hook can be defined on individual types, not just as a service-level hook.
* Allow faceted search to specify ``maxBuckets``; see :ref:`facets`.
* ReindexBatch administrative API can reindex all objects from storage, or objects selected by query; see :ref:`reindex_batch_operation`.
* QueryParams class of Java client library now has a builder() method.
* New design flag "isReadOnly"; see :ref:`design-object`.
* New config option for MongoDb storage "useLegacyIdField", which is true by default (and so no change is required for existing installations).
  This can be set to false to use "_id" instead of "id".
  Note that if you have an existing MongoDb collection you would need to migrate your data before changing this option.
* New Schema top level property "indexObjects" which can be set to false to prevent indexing of objects of that type; see :ref:`indexObjectsSchemaConfig`.
* Moved "indexPayloads" property from inside the JsonSchema to a top level property of the Schema Cordra object; see :ref:`indexPayloadsSchemaConfig`.
* New query parameter "includeScore" which, if set to true, causes search results to include the "score" from the index service.
* New query parameter "includeVersions" which, if set to true, causes search results to include Cordra version objects (see :ref:`objectVersioning`).
* Cordra webapp is included in the distribution as ``cordra.war`` instead of ``ROOT.war``, along with a simple
  ``ROOT.war`` which forwards all requests to ``cordra.war``.
* If Cordra is configured to listen on "localhost", now the default, it will listen on both the IPv4 localhost 127.0.0.1 and the IPv6 localhost ::1.
* Cordra can be configured to listen on multiple addresses by using config.json property "listenAddresses" instead of or in addition to "listenAddress";
  similarly DOIP and DO-IRP listener configuration now allow "listenAddresses".
* It is now possible to specify a ``--number-of-threads`` with "export-tool" and "import-tool"; see :ref:`import_export`.
* Improved behavior of export-tool and import-tool with very large payloads.
* Allow search of objects by ACL using search fields like ``acl/readers/_``.
* Index fields starting with ``sort_`` are now searchable with the default Lucene index (in addition to their existing use for sorting and faceting).
* GET /startupStatus will return a 500 error in case of startup failure for ease of monitoring.
* Internal JavaScript module 'cordraUtil' renamed to 'cordra-util', but the older name also works.
* New worked examples :doc:`../extensions/neo4j-as-an-additional-index` and :doc:`../extensions/partial-replication-aggregation`.
* OpenAPI-compatible documentation for :doc:`../api/doip-api-for-http-clients`.

Bugfixes:

* When attempting to access a non-existent object, return "Missing object" only when the authenticated user has default read permission;
  otherwise return an auth error.  (This matches Cordra 2.3.0 behavior inadvertently changed in Cordra 2.4.0.)
* Fixed API bug that prevented DELETE with jsonPointer.
* Improve deletion of old temporary files from webapps-temp directory.
* 500 errors using the batchUpload API will be logged.
* Ensure that method ACLs on an instance override (rather than add to) default method ACLs.


Changes in 2.4.0 release (2022-02-20):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Upgrade log4j2 dependency to 2.17.1.
* Cordra will use the more specific "Conflict" response code (409) instead of "Bad Request" (400) when an attempt is made
  to create a schema or user with the same name as an existing schema or user.
* New lifecycle hook "authenticate"; see :ref:`authenticate`.
* New lifecycle hook "customizeQueryAndParams"; see :ref:`customizeQueryAndParams`.
* New lifecycle hook "beforeStorage"; see :ref:`beforeStorage`.
* New lifecycle hook "beforeSchemaValidationWithId", which can make use of a generated identifier,
  in contrast to "beforeSchemaValidation" which can affect a to-be-generated identifier; see :ref:`javascript-lifecycle-hooks`.
* Lifecycle hooks and type methods in JavaScript can now return promises, allowing the use of asynchronous libraries.
* Documentation of :ref:`as-user` feature; now admits finer control via authenticate hook.
* Numeric indexing and searching of JSON numbers via index fields prefixed with ``num_``; see :ref:`querying-numeric-properties`.
* New POST /batchUpload API; see :ref:`batchUploadApi`.
* Individual Schema objects can have an "authConfig" property, which takes precedence over
  "schemaAcls" on the "authConfig" of the design object; see :ref:`authorization`.
* Individual Schema objects may have a "getAuthConfig()" static method in their JavaScript; see :ref:`getAuthConfig`.
* S3 storage configuration allows "endpoint" and "signerOverride" to support non-standard S3-compatible services.
* S3 storage allows getting credentials implicitly from the environment.
* Elasticsearch configuration now allows multiple baseUris.
* Cordra UI screen for accessing method calls.
* New library function cordraUtil.verifyWithKey; see :ref:`cordra_util_module`.
* New library function cordraUtil.getGroupsForUser; see :ref:`cordra_util_module`.
* New library function cordraUtil.validateWithSchema; see :ref:`cordra_util_module`.
* cordraUtil.verifyWithCordraKey now only checks signature, does not interpret JWT claims.
* New library cordra-hooks-support to facilitate writing Cordra lifecycle hooks or type methods in Java; see :ref:`calling-java-from-javascript`.
* New library cordra-embedded-test-server to facilitate starting a Cordra for automated testing.
* Key simplification: a typical Cordra install will generate only a single keypair to be used for
  HTTPS, DOIP, handle registration, and signing.
* TokenUsingCordraClient and the JavaScript cordra-client can now be constructed with an explicit client-side idle timeout on the cache of tokens (default is 10 minutes),
  after which the client will assume it needs to reauthenticate instead of using a cached token.
* Possible performance improvement with MongoDB storage by removing extraneous find calls for objects without payloads.
* Warm up JSON Schema Java objects and JavaScript lifecycle hooks for possible performance gains.
* Bugfix with Tomcat sessions and auth backoff to prevent a possible infinite loop scenario with certain unfortunately timed requests.
* Bugfix for retention of payloadReaders ACL across object update.
* Bugfix to ensure that internalMetadata is not visible to schema methods.
* Workaround for rare Gson issue of illegal access for empty collections on JDK 16+.
* Allow single-instance Cordra to be configured with Kafka-based replication, which was previously only available
  in a distributed Cordra installation.
* Allow faceted search on aclRead and aclWrite fields.
* Include the object id in the default index field for an object.
* Bugfix to prevent an error making a faceted query for a field not yet indexed, in the Lucene backend.
* Allow Cordra to be configured to start with neither HTTP nor HTTPS port enabled.
* The SchemaImporter tool can take both ``--object`` and ``--name/--schema/--javascript`` and will sensibly combine the input.
* Make Cordra UI more useful for recovery when there is a problem with design JavaScript.
* Ensure default character encoding of UTF-8 on HTTP requests.
* New Cordra extension called Collab in prototype form. See :doc:`../extensions/collab-prototype`.

Changes in 2.3.2 release (2021-12-15):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Upgrade log4j2 dependency to 2.16.0 to ensure Cordra is not vulnerable to the "log4shell" vulnerability
  `CVE-2021-45046 <https://nvd.nist.gov/vuln/detail/CVE-2021-45046>`__.

Changes in 2.3.1 release (2021-12-11):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Upgrade log4j2 dependency to 2.15.0 to ensure Cordra is not vulnerable to the "log4shell" vulnerability
  `CVE-2021-44228 <https://nvd.nist.gov/vuln/detail/CVE-2021-44228>`__.
* Upgrade Netty dependency to 4.1.69.Final.
* JsonPruner (utility class in cordra-schema-util) now prunes correctly to schemas using "oneOf".

Changes in 2.3.0 release (2021-04-01):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Endpoint of DOIP API for HTTP clients is /doip; see :doc:`../api/doip-api-for-http-clients`.
* Access tokens for invoking DOIP operations can be managed using operation identifiers 20.DOIP/Op.Auth.Token, 20.DOIP/Op.Auth.Introspect, and 20.DOIP/Op.Auth.Revoke;
  see :ref:`doip-api-for-http-clients-access-token-operations`.
* Operation 0.DOIP/Op.ListOperations and the listMethods API now only list operations/methods for which the calling
  user has ACL-level permission.
* Aliases for service and core operation ids in DOIP; see :ref:`doip-api-for-http-clients-aliases`.
* New NPM project `@cnri/cordra-tool <https://www.npmjs.com/package/@cnri/cordra-tool>`__
  to facilitate development and testing of Cordra configuration of types, operations, and lifecycle hooks.
  See the README.md in the NPM package for details.
* Global static methods can be placed on the design object and can be accessed as DOIP custom operations targeting the service object;
  see :ref:`serviceLevelStaticMethods`.
* New lifecycle hook customizeQuery allows restricting or augmenting user-supplied queries; see :ref:`customizeQuery`.
* New lifecycle hooks afterCreateOrUpdate and afterDelete; see :ref:`javascript-lifecycle-hooks`.
* Lifecycle hooks for the Design object and Type objects; see :ref:`hooksForCordraDesignAndSchema`.
* The convenience API endpoints /initData, /design, and /schemas now properly apply ACLs for the Design object and Type objects.
* New query parameters "facets" and "filterQueries"; see :ref:`searchApi` and :ref:`facets`.
* Schema validation now properly validate "format":"date" and "format":"time".
* The "createHandleValues" lifecycle hook allows access to the "context" parameter, which can be used to inspect
  "context.effectiveAcl" to determine whether an object is public.
* The "onObjectResolution" lifecycle hook can now inspect a context property "isSearch" to determine if it is being apply to
  a standalone object resolution or to a search result.
* New ACL "payloadReaders" allows further restricting which object readers are authorized to read payloads.
* Type methods called using GET (or POST with empty body) allow passing input via a query parameter called "params".
* Cordra JavaScript comes with polyfills supporting many features up to ECMAScript 2017, to facilitate transpiling
  next-generation JavaScript source into ES5 for use in Cordra.
* Cordra schema JavaScript allows more control over the returned server response when throwing errors;
  see :ref:`throwing_errors_in_schema_javascript`.
* The JavaScript cordra-client now throws objects which are instanceof Error for better stack traces.

Changes in 2.2.0 release (2020-10-06):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Cordra schemas in Type objects can now reference other Cordra schemas using $ref property; see :ref:`schemaRef`.
* Type methods and lifecycle hooks have access to Cordra object payloads; before only the JSON portion of
  the digital object was accessible to those methods. See :ref:`cordra_module`.
* Type methods can now accept and return arbitrary byte streams (instead of just JSON); see :ref:`directIo`.
* Type methods, for use with the REST API, can specify that they can be invoked using HTTP GET (instead of just POST);
  see :ref:`allowGet`.
* The onObjectResolution hook can now control access during payload retrievals and
  the new onPayloadResolution hook can produce bytes or override stored bytes during a payload retrieval;
  see :ref:`javascript-lifecycle-hooks`.
* The JavaScript cordra-client now works with Node.js, in addition to the browser.
* A bug was fixed to ensure the createHandleValues JavaScript hook returns handle records as intended.
* A bug was fixed to stop memory a leak issue during payload creation and update.
* A bug was fixed to expose object.metadata to beforeSchemaValidation hook on update.
* The listMethods call is now available to authorized readers (instead of just writers).
* Default authorization configuration is changed for new Cordra installs. New installs by default require authentication
  to read any objects (other than Type/Schema objects and the design object which are public). If you prefer the old default,
  edit the authorization configuration via the design object or the admin interface.  Existing installations after software update
  will continue to work as before.
* A new cordra-schema-util library is introduced with utility functions related to Jackson and schema validation,
  including certain utilities formerly part of cordra-client; developers can leverage this library for performing
  project-specific checks.
* Authentication functionality will now use the response from onObjectResolution hook applied to user objects; before only stored
  user objects were considered.
* Cordra distribution now includes hdl-convert-key script to facilitate converting keys among Handle, PEM, and JWK formats.
* cordraUtil.signWithKey, cordraUtil.signWithCordraKey, and cordraUtil.getCordraPublicKey utility methods are added to the
  embedded JavaScript library for use in Cordra lifecycle hooks and type methods to facilitate the production and checking of signatures.
* DOIP configuration allows setting serviceName and serviceDescription, which will be included in the response to the Hello request.
* The SchemaImporter tool will preserve existing JavaScript when updating a Type/Schema object;
  to delete existing JavaScript during an import procedure, a new --delete-javascript flag can be specified.

Changes in 2.1.0 release (2020-05-08):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Allow use of Elasticsearch as Cordra indexer (this did not work correctly in v2.0.0).
* Fix issue with type method call locking (object passed to method is now acquired inside the lock).
* Cordra UI object editor will now retain falsy non-required properties either fetched from server or
  from "Edit As Json". Falsy non-required properties can be added or removed using "Edit As Json".
* Allow type methods to take input other than JSON objects (for example, strings).
* TokenUsingCordraClient, when a request fails due to a server-invalidated token, will retry with a new token.
* Add new ``/search`` HTTP API for searching; enable search requests to be sent as JSON via POST; see :ref:`searchApi`.
* Enable search with "queryJson" parameter to match content of object against supplied JSON; see :ref:`searchApi`.
* Enable system properties to control trusted HTTPS certificates when using HttpCordraClient; see :ref:`client_tls_configuration`.
* Update dependencies, notably ZooKeeper, Curator, and Kafka.
* Facilitate use of TLS with ZooKeeper, Kafka, and other services; see :doc:`../configuration/enabling-tls`.
* Add getOrNull method to cordra-client.js.
  If the digital object does not exist, get method throws an exception; getOrNull method instead returns null value.
* Support new configuration options for Solr and Elasticsearch; see :doc:`../configuration/indexing-backends`.
* Allow limited use of ES6 (that supported by Nashorn) in JavaScript used with Cordra types.
  Notably "let" and "const" are usable even in Java 8; later Java versions offer partial support for
  arrow functions, for...of loops, and backtick-delimited strings.
* Support sending exceptionally long queries to Solr (previously Solr would by default reject queries
  over seven or eight thousand characters long).
* Cordra UI now incorporates dependencies that it formerly fetched from CDNs.
* Built-in handle server now uses the same listenAddress as Cordra's HTTP interfaces by default.
* Remove unexpected error.log messaging about DOIP interface when using Cordra in a servlet container.
* Fix bug preventing use of filesystem bdbje storage with multi storage configuration.
* Filesystem bdbje storage can have the path to the Cordra data directory overridden by configuration; see :doc:`../configuration/storage-backends`.
* Custom storage backends can automatically receive the path to the Cordra data directory as
  property cordraDataDir of their configuration options; see :doc:`../configuration/storage-backends`.
* Use "exec" in command-line scripts to ensure that only one process is spawned.
* Copy secure properties of a digital object when creating a version of that object.
* Prevent simultaneous changes to digital objects when their versions are being created.
* Make HttpCordraClient.getNewHttpClient and other methods available for overriding in a subclass to change HTTP client configuration.

.. warning::

   Cordra Beta v2.0 versions released after 2018-08-01 include an improved
   JavaScript API which is incompatible with earlier released versions of
   Cordra Beta v2.0.  *If* your Cordra configuration includes schema JavaScript,
   see :ref:`here <legacy-js>` for an upgrade path.

   Cordra users upgrading from early versions of the Cordra Beta v2.0, who
   did not use schema JavaScript (apart from the default User schema JavaScript,
   which will be automatically upgraded if it has not been edited),
   do not in general need to take any action.

   Also, earlier versions of Cordra would return all results to searches with
   pageSize=0.  To restore the former behavior, you can add
   "useLegacySearchPageSizeZeroReturnsAll":true to the Cordra :ref:`design-object`.
   By default a search with pageSize=0 returns the number of
   matched objects but no object content.

   As of Cordra Beta v2.0 versions released after 2019-06-01,
   Kafka-based replication no longer includes payloads in the
   replication messages. If you are using replication and you need payloads to
   replicate there is a boolean property "includePayloadsInReplicationMessages"
   that can be set to true on the Design object.  Note that the current
   implementation of replication with "includePayloadsInReplicationMessages"
   may require special Kafka configuration
   or may not be suitable when there are large payloads.

   Cordra Beta v2.0 versions released after 2019-08-02 only support Elasticsearch version 6 and 7
   as indexing backends. If you have an existing Elasticsearch 5 index, you'll need to upgrade and
   reindex.

   Cordra v2.0.0 and later only support using access tokens to create HTTP
   REST API sessions. If your application uses the cookie-based :ref:`legacySessionsApi`, you will need to
   upgrade to use the new Token API. To restore this former behavior, you can add "useLegacySessionsApi":true
   to the Cordra design object. See :ref:`tokenApi` for details on the new API.

   As of Cordra v2.0.0, all authentication requests must be made over
   a secure HTTPS connection. To allow authentication over insecure channels, you can add
   "allowInsecureAuthentication":true to the Cordra design object.

   Cordra v2.0.0 uses memory sessions by default.  If you have a distributed Cordra installation which uses
   Tomcat session replication, you will need to configure the Cordra session manager to use Tomcat-managed sessions.
   See :ref:`sessions-configuration`.

Changes in 2.0.0 release (2019-10-09):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Add built-in identifier resolution; see :ref:`identifiers` and :ref:`handle-integration`.
* Ensure Cordra object ids are syntactically valid handles.
* Make DOIP listener active on port 9000 by default.
* Add handle values to Cordra object id records for DOIP clients to locate objects.
* Add ``generateId`` JavaScript lifecycle hook; see :ref:`generateId`.
* Allow handleReference in schemas to refer to objects of any types, or any types except a
  fixed list, instead of requiring a fixed list of allowed types.
* Allow authentication only over HTTPS by default.
* New options for HTTPS configuration, in particular to allow updating certificate without restart;
  see :ref:`https_configuration`.
* Change default session management to memory sessions; add separate configuration option
  for using Tomcat-managed sessions. See :ref:`sessions-configuration`.
* Add cordraUtil.js module; see :ref:`cordra_util_module`.
* Prevent MongoDbStorage from storing JSON numbers not representable as MongoDB numbers.
* Make client-supplied requestContext available to JavaScript hooks; see :ref:`requestContext`.
* Add parameter ``filter`` to search and retrieval APIs to allow returning only parts of the objects
  specified by JSON pointers.
* Upgrade dependencies; support Elasticsearch 6 and 7 (but not 5).
* Add :ref:`tokenApi` and deprecate :ref:`legacySessionsApi`.
* Add script to allow easier creation of Handle key pairs.
* Support providing jar files in data/lib and sub-directories.
* New API GET /check-credentials to test authentication whether direct or token/session-based.
* Add batch files, e.g., startup and shutdown, for Windows.
* Update technical manual significantly.


Changes in 2019-06-12 beta release:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* :ref:`objectHashing`, which allows hashes of the object content to automatically be included in object metadata.
* Fix bug to ensure that any errors resulting from sessions setup (see :ref:`sessions-configuration`) are visible at startup.
* Prevent creating a digital object with a zero-character, i.e., empty, identifier.  If the use of previous versions of Cordra
  resulted in digital objects with empty identifiers, you can
  delete them with this recovery API call: ``DELETE /objects/?deleteObjectWithEmptyId``.
* Ensure that initial default schemas have appropriate createdOn and modifiedOn metadata.
* Versions (see :ref:`objectVersioning`) are now immutable by default; they can be made mutable by setting
  a Design object flag "enableVersionEdits".
* Improved Cordra software performance.
* Fix bug that in rare cases could allow user and group changes to not be immediately visible to the portion of Cordra
  process that authenticates users.
* Fixes to migration from Cordra v1.
* UI fix to prevent issues with schemas containing spaces.
* Allow configuration of cookies used for Cordra sessions; see :ref:`design-object`.
* Kafka-based replication no longer includes payloads in the
  replication messages. If you are using replication and you need payloads to
  replicate there is a boolean property "includePayloadsInReplicationMessages"
  that can be set to true on the Design object.  Note that the current
  implementation of replication with "includePayloadsInReplicationMessages"
  may require special Kafka configuration
  or may not be suitable when there are large payloads.
* Storage modules "custom" and "multi"; see :ref:`storage-configuration`.
* To facilitate clients passing contextual information to the storage backend, HTTP API calls admit a
  query parameter "requestContext".  This will be made available to the instance of StorageChooser used
  by the "multi" storage module.  See :ref:`multiStorage`.

Changes in 2019-04-09 beta release:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Fix bug which prevented starting additional webapps in data/webapps.
* Add new config.json property reindexing.async; reindexing.priorityTypes no longer causes async reindexing automatically.  See :ref:`reindexing`.
* Improve documentation around possible issues reindexing when using types like JavaScriptDirectory.

Changes in 2019-03-29 beta release:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Ensure that sources of internal CNRI libraries are included in distribution.
* Allow minRf to be configured in Solr indexer configuration.
* Fix client tools ExportByQuery and ImportObjects which can now optionally connect to a MongoDB backend for internal metadata.
* New server-side tools "export-tool" and "import-tool" which can connect directly to Cordra storage in order to export and
  import objects; also "ids-by-query" to retrieve a list of ids from a running Cordra.  See :ref:`import_export`.
* Improve performance of reindexing under Elasticsearch.
* Make it so that components of Cordra object "metadata" are indexed under fields with names like "metadata/createdOn", etc.
* New MongoDB storage configuration option "maxTimeMsLongRunning", which defaults to a large value, to prevent processing
  timeouts on slow reindexing operations.
* New HTTP API for searches which returns only object ids instead of full objects, using query parameter "&ids".
* Fixed bug causing incorrectly sorted search results when using MongoDB storage.
* Fixed bug causing metadata "createdOn" and "modifiedOn" to differ for a newly created object.

Changes in 2019-03-09 beta release:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Substantial changes to UI.
* Configurable session management backend; see :ref:`sessions-configuration`.
* User schemas can include flags to activate/deactivate users; see :ref:`auth-accountActive`.
* Single-instance Cordra installation allows additional jar files to be made available to Cordra by placing in data/lib directory.
* A file setenv.sh next to startup will be run by startup (for ease of setting environment variables in automatic installations).
* Remove all internal dependence on objatt\_ fields in the index.  This allows ignoring those fields in a Solr or Elasticsearch install, if desired to save index disk space.
* Schemas can indicate that certain fields should not be stored or retrievable plain, but instead stored as a hash and salt which can be validated.  Useful for secure tokens.  See :ref:`secureProperty`.
* New API GET /startupStatus to indicate when startup has partially failed; intended to be used in situations where HTTP access to Cordra is much easier than checking logs.
  See :ref:`startup-status`.
* Upgrade Jetty backend in single-instance install; now supports HTTP/2 in Java 9 or later.
* /uploadObjects API now should use POST rather than PUT.
* GET /acls now only requires read permission.

Changes in 2019-01-31 beta release:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* New objectForIndexing JavaScript hook to adjust how the object is indexed; see :ref:`objectForIndexingExample`.
* Required properties with schema ``cordra.type.autoGeneratedField``
  were previously populated only if present with some value, even the
  empty string; now they are auto-generated even if missing.
* Changed default value of reindexing configuration property ``batchSize`` to 16, which allows better
  performance with the default ``"lockDuringBackgroundReindex": true``.
* Fixed UI bug which prevented saving objects with missing but not
  required enum and boolean properties.
* In the UI, the admin schema editor now allows editing schema JavaScript.
* In the UI, added and edited schemas are now usable immediately
  instead of requiring a page refresh.
* MongoDB storage now allow configuration of databaseName, collectionName, and gridFsBucketName.
* Fixed bug which could cause schemas to be unknown to Cordra
  after a reindex in certain configurations.

Changes in 2019-01-11 beta release:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Improvements to logging of reindexing, and speed of reindexing when using MongoDB storage.
* UI fix to prevent possible XSS in use of Toastr to show error messages.
* Configurable ACLs for schema methods; see :ref:`authorizationSchemaMethods`.

Changes in 2018-12-06 beta release:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Background reindexing fix to ensure objects are (by default) locked during reindexed; see :ref:`reindexing`.
* New /reindexBatch API; see :ref:`reindex_batch_api`.
* Update documentation for /uploadObjects API.

Changes in 2018-11-27 beta release:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* General performance improvements.
* Cordra authorization now allows groups to include other
  groups. Note: to make use of this feature, existing deployments will
  need to edit the Group schema to allow the "users" property to
  include handles of objects of type "Group" as well as type "User".
* Reindexing performance improvements and new configuration options; see :ref:`reindexing`.
* Ensure "Content-Type: application/json; charset=UTF-8" in more responses.

Version 1.0
-----------

* Version 1.0.7 fixes a sporadic classloading issue experienced rarely by some users.

* Version 1.0.6 has several minor bugfixes: HTTPS no longer asks for a client-side certificate;
  Handle resolution is aware of recent GHR
  changes; and the internal implementation of payload indexing is
  streamlined.

* Version 1.0.5 fixes a performance bottleneck in indexing new objects,
  and also includes the full source needed to build Cordra.

* Version 1.0.4 adds HTTP Range requests, as well as the "indexPayloads"
  property to allow turning off indexing of payloads.

* Version 1.0.3 changes how payloads are associated with Cordra objects.
  Now any Cordra object can be associated with zero or more named
  payloads. Payloads are no longer associated with locations in the JSON
  and do not need to be defined in the schema.

* Version 1.0.3 improves handle minting configuration to allow handles to
  redirect to the Cordra UI, the JSON of the Cordra object, payloads of
  the Cordra object, or URLs included in the JSON. There is also a handle
  updater to allow changes to handle records to be performed in bulk.

* Version 1.0.2 includes a bug fix that prevented groups from referencing
  users correctly.
