APIs
====

.. toctree::
   :maxdepth: 1
   :glob:

   introduction
   doip
   doip-api-for-http-clients
   rest-api
   search
