.. _api_introduction:

Introduction
============

Cordra provides three different programmable interfaces in addition to a graphical interface for web browsers.

Digital Object Interface Protocol (DOIP) including :doc:`doip-api-for-http-clients`, HTTP REST API, and 
Digital Object Identifier Resolution Protocol (DO-IRP) are the three programmable
interfaces. The software distribution includes client libraries, both a :doc:`Java version <../client/rest-java>` and a
:doc:`JavaScript/TypeScript version <../client/javascript>`.


Digital Object Interface Protocol (DOIP) Interface
--------------------------------------------------

DOIP is a communication protocol that specifies how clients may interact with digital objects (DOs) that are managed by
DOIP services. The method of such interaction is primarily using identifiers associated with digital objects, including
those that represent operations, types, and clients.

DOIP is an appropriate choice for users who are interested in an architectural style focused on invoking identified
operations, or who focus on persistence or interoperability benefits.

For details about the Cordra's implementation of DOIP along with examples, see :doc:`doip`. For information about
DOIP client library, see :doc:`../client/doip-java`.  See also :doc:`doip-api-for-http-clients`.
See also the `DOIP v2 specification
<https://www.dona.net/sites/default/files/2018-11/DOIPv2Spec_1.pdf>`__.


HTTP REST API
-------------

An HTTP API reduces the entry barrier and enables users to leverage Cordra's features using most HTTP client libraries.
We recommend the use of :doc:`doip-api-for-http-clients` which allows the use of the DOIP paradigm for clients using HTTP. 

Cordra also provides an older RESTful HTTP API for interacting with digital objects, which is still supported.


Digital Object Identifier Resolution Protocol (DO-IRP) Interface
----------------------------------------------------------------

DO-IRP is a rapid resolution protocol for retrieving state information, such as location, public keys, and digests, of a digital
object from its identifier.

DO-IRP is originally defined under a different name in RFCs 3650, 3651, and 3652. DO-IRP specification, and its evolution, is
currently overseen by the DONA Foundation as part of the `Handle System <https://www.dona.net/handle-system>`__.

Cordra provides an DO-IRP interface enabling clients to rapidly resolve digital object identifiers to their state information.
You may access DO-IRP client libraries `here <http://handle.net/client_download.html>`__.

See :ref:`identifiers` and :ref:`handle-integration` for more information about configuring Cordra identifiers and
its DO-IRP interface.
See also the `DO-IRP v3.0 specification <https://www.dona.net/sites/default/files/2022-06/DO-IRPV3.0--2022-06-30.pdf>`__. 
