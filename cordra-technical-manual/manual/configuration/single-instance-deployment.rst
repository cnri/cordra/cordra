.. _single_instance_deployment:

Deploying Single-Instance Cordra
================================

.. contents:: In This Section
    :backlinks: none
    :local:

Software Distribution Details
-----------------------------

The Cordra software can be downloaded from `here <https://www.cordra.org>`__. There are
two options available: One which is pre-configured to use our hosted identifier (aka handle)
service for a trial that allows digital objects to be locatable in the Internet, and one which
you can configure to use your own handle server at a later point when you deem it is important for your users to auto
locate digital objects. For more information about the Handle technology and setting up a handle server, please see
`here <https://handle.net/download_hnr.html>`__ .

For most users evaluating Cordra, we recommend trialing the hosted handle service by requesting a pre-configured
Cordra software distribution. See `here <https://www.cordra.org/download.html>`__ for details.

.. note::
     The pre-configured Cordra software distribution includes a handle prefix and a necessary key pair
     useful for a quick start. We strongly recommend that you update the key pair if security is important
     during the trial.

.. _local-single-deploy:

Locally Run Instance
--------------------

Before starting Cordra for the first time, the admin password needs to be set in a ``repoInit.json`` file
under the ``data`` directory.  This file should also contain the desired handle prefix, from which the unique
identifiers will be generated; if the prefix is omitted, the prefix ``test`` will be used by default to create objects.

Pre-configured Cordra software distribution downloaded from the Cordra website includes the prefix,
but admin password must be set prior to starting Cordra for the first time.

Example ``data/repoInit.json``::

    {
        "adminPassword": "changeit",
        "prefix": "20.5000.123.TEST"
    }

Once data/repoInit.json is in place, you can start Cordra with the ``startup`` script, and stop it with the ``shutdown``
script.

When Cordra starts, the admin password will be securely stored in the system and the ``repoInit.json`` file will be
deleted. The admin password can then be changed using the UI while signed in as admin.

Cordra can be started by running the ``startup`` script in the distribution directory. The Cordra UI will be
available at http://localhost:8080/ and https://localhost:8443. You can sign in as admin in order to further configure
your Cordra server, if desired.

Except on Java 8, the GraalVM JavaScript engine used by JavaScript lifecycle hooks and methods
supports compilation for improved performance.  Certain JAR files for GraalVM compilation
are not distributed with Cordra.  To acquire these JAR files, you can use the ``bin/fetch-graalvm-compiler-jars`` script.

Cordra can by stopped by running the ``shutdown`` script.

Deploying Using Apache Tomcat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Cordra software can be run using a servlet container instead of running in a standalone fashion as described above.
The details below describe how to run Cordra using Apache Tomcat.

The Cordra war file is located in the Cordra distribution at ``sw/webapps-priority/cordra.war``. It can be renamed to
``ROOT.war``, for instance, if desired.
(Note that ``sw/webapps-priority/ROOT.war`` in the Cordra distribution is included simply to forward requests 
to ``cordra.war`` by default.)

You should copy the war file into the Tomcat ``webapps`` directory,

The only change needed from a default Tomcat installation is to specify the Cordra data directory. This is done using
Java system properties, which can be set in Tomcat via an environment variable ``CATALINA_OPTS``. This setting can be
set using the file ``bin/setenv.sh`` which can be created as a sibling of ``bin/catalina.sh``. Ensure
that ``bin/setenv.sh`` has the following contents::

   CATALINA_OPTS="-Dcordra.data=/path/to/cordra/datadir ${CATALINA_OPTS}"

``CATALINA_OPTS`` can also be used to specify memory configuration, such as::

   CATALINA_OPTS="-Xmx2G -Dcordra.data=/path/to/cordra/datadir ${CATALINA_OPTS}"

Logging can be configured with a ``log4j2.xml`` file in the ``cordra.data`` directory.

Note: we recommend adding the following attributes to the Connector configuration in server.xml::

    relaxedPathChars="[]|{}^&#x5c;&#x60;&quot;&lt;&gt;" relaxedQueryChars="[]|{}^&#x5c;&#x60;&quot;&lt;&gt;"

This prevents Tomcat from giving errors if certain characters are used unencoded in URLs.
Additionally we recommend in catalina.properties::

    org.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true
    org.apache.catalina.connector.CoyoteAdapter.ALLOW_BACKSLASH=true


.. _tomcat-graalvm-javascript-compilation:

Configuring Tomcat for GraalVM JavaScript Compilation
#####################################################

Except on Java 8, the GraalVM JavaScript engine used by JavaScript lifecycle hooks and methods
supports compilation for improved performance.  To enable this on Tomcat, copy the files from the Cordra
distribution in ``sw/lib/graalvm``, and also the non-distributed files which can be downloaded using
the script ``bin/fetch-graalvm-compiler-jars``,
to a directory accessible to Tomcat; in the following we assume they
are in a directory ``graalvm`` which is a sibling of ``webapps``.  Then ensure that ``bin/setenv.sh``
has the following contents (in addition to the setting of cordra.data described above)::

    CATALINA_OPTS="-XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCI --upgrade-module-path=$CATALINA_BASE/graalvm ${CATALINA_OPTS}"

Finally add to ``conf/context.xml``, under ``<Context>``::

    <Loader delegate="true"/>

(If for some reason adding the ``Loader`` configuration is not desired, an alternative is to remove
those JAR files from the Cordra webapp's ``WEB-INF/lib`` directory which are also found in
``graalvm``.)

.. _docker-single-deploy:

Docker Instance
---------------

The Cordra distribution comes with the files necessary to build a Docker image that includes the Cordra software
distribution.

There are four environment variables that can be set for the Docker image:

* ``CORDRA_ADMIN_PASS`` (REQUIRED) - password for the admin user
* ``CORDRA_BASE_URI`` - base uri for this Cordra HTTP API endpoint
* ``CORDRA_PREFIX`` - Handle prefix to use
* ``CORDRA_HDL_ADMIN`` - Handle admin for the prefix

These variables can be set on the command line, or in the ``variables.env`` file located in the ``docker`` folder.

To build the Docker image, use the following command::

    docker build -t cordra -f docker/Dockerfile .

You can start Cordra using the following command::

    docker run -it -p8080:8080 -p8443:8443 -p2641:2641 -p9000:9000 --env-file docker/variables.env cordra

To persist the Cordra data directory, the following command can be used.  Note that the ``"$(pwd)"/data`` directory
must exist in order for this command to work::

    docker run -it -p8080:8080 -p8443:8443 -p2641:2641 -p9000:9000 \
            --env-file docker/variables.env \
            --mount type=bind,source="$(pwd)"/data,target=/opt/cordra/data \
            cordra

.. _docker-single-deploy-preconfigured:

Pre-configured Cordra Distribution
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have downloaded a pre-configured Cordra distribution from the Cordra website, the package will include
``variables.env`` file with the CORDRA_PREFIX and CORDRA_HDL_ADMIN variables populated. You should edit that file
to fill in values for other environment variables including the admin password.

You can then build and run the Docker image as described above.

Deploying using Docker Compose
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You may also use Docker compose to modify the Docker image built using the above instructions further. This is useful
if you need to include the key pair your Cordra will use, or if you would like to modify the config.json file. There is a
sample ``docker-compose.yml`` configuration file included that shows how to start a single instance of Cordra.

You can start Cordra by running this command from inside the ``docker`` subdirectory::

    docker-compose up


Additional Configuration
------------------------

There are a number of configuration options available for deploying Cordra. To learn more, see
:ref:`here <configuration_introduction>`.

Production Considerations
~~~~~~~~~~~~~~~~~~~~~~~~~

To deploy a single-instance Cordra in for productions, you will want to configure Cordra as a service. You will also
likely want to put a reverse proxy in front of Cordra. For an example of how such a deployment might look, see
:ref:`single_instance_production`.

External Indexing and Storage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cordra can be configured to use external indexing and storage services by editing the ``data/config.json`` file in the Cordra
distribution. An example might look like this::

    {
        "httpPort": 8080,
        "listenAddress": "0.0.0.0",

        "index" : {
            "module" : "elasticsearch",
            "options" : {
                "address" : "localhost",
                "port"    : "9200",
                "addressScheme"  : "http",
                "index.mapping.total_fields.limit": "2000"
            }
        }

        "storage" : {
            "module" : "s3",
            "options" : {
                "accessKey" : "XXXXXXXXXXXXX",
                "secretKey"    : "XXXXXXXXXXXXX",
                "bucketName"  : "test.cordra.org",
                "s3KeyPrefix": "testing1234",
                "region": "us-east-1"
            }
        }
    }

For more details on configuring external storage and indexing services, see :ref:`storage-configuration` and :ref:`indexing-configuration`.
