.. _adminUI:

Administrative UI
=================

Cordra includes a graphical interface for web browsers which
enables users to create, retrieve, update, delete, search, and protect digital objects. When
logged into the Cordra UI as an administrator, you will see an additional "Admin" dropdown menu
at the top of the screen.

.. image:: ../_static/admin/admin_menu.png
        :align: center
        :alt: Admin Dropdown Menu

Each of these options can be used to modify how Cordra looks and functions. The options are described below.

Types
-----

.. image:: ../_static/admin/types.png
        :align: center
        :alt: Types Admin

The Types page can be used to add and/or modify the types stored in Cordra. A list of types is shown. There are three
default types. To edit a type, click on its name. An editor will appear, showing the schema and JavaScript for the type.
Make any changes you would like, and then click the "Save" button.

New types can be added either one at a time or in bulk. To add an individual type, click the "Add"
button. In the dialog that pops up, choose a name for the type and a template. If you would like for an example JavaScript
to be populated in the editor, select the "Include example JavaScript" checkbox. Then click the "Add" button.
The dialog will close, and the new type will be available in the editing interface. Make any additional changes
required, and then click the "Save" button to save the new type.

To add multiple types at once, you will first need JSON file containing the types. Here is an example file::

    {
      "results": [
        {
          "id": "test/171a0606f7c74580fd39",
          "type": "Schema",
          "content": {
            "identifier": "test/171a0606f7c74580fd39",
            "name": "Group",
            "schema": < Schema json omitted for brevity >,
            "javascript": < JavaScript omitted for brevity >
          },
          "metadata": {
            "createdOn": 1535479938849,
            "createdBy": "admin",
            "modifiedOn": 1535479938855,
            "modifiedBy": "admin",
            "txnId": 65
          }
        },
        {
          "id": "test/171a0606f7c74580fd39",
          "type": "Schema",
          "content": {
            "identifier": "test/171a0606f7c74580fd39",
            "name": "Document",
            "schema": < Schema json omitted for brevity >
          },
          "metadata": {
            "createdOn": 1535479938849,
            "createdBy": "admin",
            "modifiedOn": 1535479938855,
            "modifiedBy": "admin",
            "txnId": 65
          }
        },
      ]
    }

The format of the file is similar to the format of the response to an object query. You can download json for
types currently in Cordra using the Search API::

    GET /search?query=type:"Schema"

The results of this query can be edited to create a new file for upload. Extra fields like pageNum and pageSize
do not need to be removed.

To upload the types file, first click the "Load from file" button in the Types admin UI. Next, select the
file to upload. If you would like to delete existing types, check the checkbox indicting such. If you choose
not to delete existing types first, an error will be throw if you try to upload a duplicate type. Click the "Load"
button to load the types into Cordra.

Design Object
-------------

See :ref:`design-object` for details.

.. _ui-config:

UI Menu
-------

Some UI configuration is stored as JSON within Cordra. Clicking on the UI menu will bring up an
editor that can be used to modify this configuration.

Example UI Configuration::

    {
        "title": "RepositoryTest",
        "allowUserToSpecifySuffixOnCreate": false,
        "initialFragment": "urls/intro.html",
        "relationshipsButtonText": "Show Relationships",
        "navBarLinks": [
            {
                "type": "url",
                "title": "External link",
                "url": "https://example.com/"
            },
            {
                "type": "about",
                "title": "About"
            },
            {
                "type": "menu",
                "title": "Queries",
                "links": [
                    {
                        "type": "query",
                        "title": "All Objects",
                        "query": "*:*",
                        "sortFields": [
                            {
                                "name": "metadata/modifiedOn",
                                "reverse": true
                            }
                        ]
                    },
                    {
                        "type": "query",
                        "title": "Documents createdBy user",
                        "query": "+type:Document +(metadata/createdBy:\"{0}\") +(/description:{1})",
                        "queryForm": {
                            "description": "Form to select use and specify words in /description",
                            "fields": [
                                {
                                    "name": "Username",
                                    "type": "handleReference",
                                    "types": [
                                        "User"
                                    ]
                                },
                                {
                                    "name": "Words in description",
                                    "type": "string"
                                }
                            ]
                        }
                    }
                ]
            },
            {
                "type": "typeDropdown",
                "title": "Types"
            }
        ]
    }

Here are the attributes available in the UI configuration object.

.. tabularcolumns:: |\X{3}{7}|\X{4}{7}|

=================================   ====================
Attribute name                      Description
=================================   ====================
title                               The text used in the title bar to
                                    identify the service.

relationshipsButtonText             The text shown on the button used
                                    to show the relationships between
                                    objects.

allowUserToSpecifySuffixOnCreate    Provides an input box for the
                                    suffix of the object id when
                                    creating objects. The prefix is
                                    set in the handleMintingConfig.

allowUserToSpecifyHandleOnCreate    Provides an input box for the
                                    complete object id when
                                    creating objects.

initialQuery                        A query to be loaded if none is
                                    present when the app is loaded.

initialFragment                     A hash fragment to be loaded if
                                    none is present when the app is
                                    loaded. This can be used to run a
                                    query on page load or show a
                                    document.

initialSortFields                   Sort fields to use in the UI if
                                    none is specified.

hideTypeInObjectEditor              Do not display object type under
                                    object ID in UI when editing.

numTypesForCreateDropdown           Number of types to display in the
                                    creation dropdown. If more than
                                    this number are available, a
                                    search interface will be shown
                                    instead.

aclUiSearchTypes                    A list of types to be used in the
                                    UI for editing ACLs; typically
                                    the list of types which represent
                                    users and groups.

navBarLinks                         An array of objects used for
                                    adding links to the navigation
                                    bar. Details below.

searchResults                       Configuration for search results. Details below.

customAuthentication                Configuration of custom UI for client
                                    interaction with a 3rd party authentication
                                    provider to acquire an access token.
                                    See :ref:`external-authentication-provider` for details.
=================================   ====================

.. _navBarLinks:

navBarLinks
~~~~~~~~~~~

.. tabularcolumns:: |\X{2}{6}|\X{4}{6}|

=================================   ====================
Attribute name                      Description
=================================   ====================
type                                Can be ``query``, ``typeDropdown``, ``typeObjectsDropdown``,
                                    ``url``, ``page``, ``about``, or ``menu``.

title                               The text used on the link.

query                               If the type is ``query`` this
                                    attribute holds the query to run.

sortFields                          Sort fields for the query
                                    results.

url                                 If the type is ``url`` this
                                    attribute holds the url.

location                            If the type is ``page`` this attribute holds the location of the page.

links                               If the type is ``menu`` this property contains
                                    an array of link object that should be on
                                    menu.

queryForm                           If the type is query this property contains
                                    configuration for a form that can be used to
                                    populate a pattern query.
=================================   ====================

Link Type ``query``::

    {
        "type": "query",
        "title": "All Objects",
        "query": "*:*",
        "sortFields": [
            {
                "name": "metadata/modifiedOn",
                "reverse": true
            }
        ]
    }

A link of type query can contain a query and sortFields configuration. When clicked on that query will be
performed and the results shown in the UI. The above example searches for all objects sorted by most recently
modified.

Link Type ``query`` with a ``queryForm``::

    {
        "type": "query",
        "title": "Documents createdBy user",
        "query": "+type:Document +(metadata/createdBy:\"{0}\") +(/description:{1})",
        "queryForm": {
            "description": "Form to select use and specify words in /description",
            "fields": [
                {
                    "name": "Username",
                    "type": "handleReference",
                    "types": [
                        "User"
                    ]
                },
                {
                    "name": "Words in description",
                    "type": "string"
                }
            ]
        }
    }

Sometimes it can be helpful to present a UI form for entering values that are then composed into a query.
Here the query contains the numeric place holders ``{0}`` and ``{1}``. The ``queryForm`` contains an array of fields.
Each field will be rendered in a UI form for the user to interact with. When the query is run the values
from those fields will be substituted into the numeric place holds in the query based on field order in
the `fields` array. For example here the first field value will replace the ``{0}`` and the second field
value will replace the ``{1}``.

The above example uses ``string`` and ``handleReference`` field types. Available types are:

string: This will provide a text box in the form. The value of that text box will be substituted into the corresponding
numeric place holder in the query.

boolean:  This will provide a check box in the form. The value from the checkbox will be inserted into the query as
the strings "true" or "false"

enum: Use this in combination with an array property ``allowedValues``. The will provide a select box containing the values.

handleReference: This field type will create a search box in the form. The user can enter term to search for objects and select
an object from the results. The id of the selected object will inserted into the queries numeric placeholder.
Use this in combination with array properties ``types`` and optionally ``excludeTypes``. The values in those
arrays controls what types of objects may be selected.

Link Type ``url``::

    {
        "type": "url",
        "title": "External link",
        "url": "https://example.com/"
    }

The ``url`` link when clicked on will show the page at the http url in an iframe embedded in the Cordra UI.

Link Type ``page``::

    {
        "type": "page",
        "title": "Home",
        "location": "#pages/home"
    }

The ``page`` link when clicked on will show the internal page at the given url. If the ``location`` attribute does not
begin with a hash (#), one will be prepended.

Link Type ``about``::

    {
        "type": "about",
        "title": "About"
    }

The ``about`` link will display a page showing the Cordra server version information.

Link Type ``typeDropdown``::

    {
        "type": "typeDropdown",
        "title": "Show Objects of Type",
        "maxItems": 15
    }

The ``typeDropdown`` link will add a menu listing the names of the types in the system. Clicking
on one of those names will search for all objects of that type.

Link Type ``typeObjectsDropdown``::

    {
        "type": "typeObjectsDropdown",
        "title": "Types",
        "maxItems": 15
    }

The ``typeObjectsDropdown`` link will add a menu listing the names of the types in the system. Clicking
on one of those names will show the Schema object with that name.

Link Type ``menu``::

    {
        "type": "menu",
        "title": "Queries",
        "links": [
            {
                "type": "query",
                "title": "All Objects",
                "query": "*:*",
                "sortFields": [
                    {
                        "name": "metadata/modifiedOn",
                        "reverse": true
                    }
                ]
            },
            {
                "type": "query",
                "title": "Documents createdBy",
                "query": "+type:Document"
            }
        ]
    }

Here we create a menu in the navigation bar that contains links to two queries.

searchResults
~~~~~~~~~~~~~

.. tabularcolumns:: |\X{2}{6}|\X{4}{6}|

=================================   ====================
Attribute name                      Description
=================================   ====================
includeType                         Include type in results display.

includeModifiedDate                 Include modification date in
                                    results display.

includeCreatedDate                  Include creation date in
                                    results display.
=================================   ====================

customAuthentication
~~~~~~~~~~~~~~~~~~~~

.. tabularcolumns:: |\X{2}{6}|\X{4}{6}|

=================================   ====================
Attribute name                      Description
=================================   ====================
url                                 Url of the custom html page
                                    to be inserted into an iframe
                                    in the authentication dialog.

tabName                             Name of the tab to be for this
                                    custom authentication in the
                                    authentication dialog.


height                              Optional height to set the iframe.
=================================   ====================


.. _auth-config:

Authorization Menu
------------------

Default access control lists are stored as JSON within Cordra. Clicking on the Authorization menu
will bring up an editor that can be used to modify this configuration.

Example Authorization Configuration::

    {
      "schemaAcls": {
        "User": {
          "defaultAclRead": [
            "authenticated"
          ],
          "defaultAclWrite": [
            "self"
          ],
          "aclCreate": []
        },
        "CordraDesign": {
          "defaultAclRead": [
            "public"
          ],
          "defaultAclWrite": [],
          "aclCreate": []
        },
        "Schema": {
          "defaultAclRead": [
            "public"
          ],
          "defaultAclWrite": [],
          "aclCreate": []
        }
      },
      "defaultAcls": {
        "defaultAclRead": [
          "authenticated"
        ],
        "defaultAclWrite": [
          "creator"
        ],
        "aclCreate": [
          "authenticated"
        ]
      }
    }

For more information on configuring ACLs in Cordra, see :ref:`authorization`.

Handle Records Menu
-------------------

This screen can be used to modify Cordra's Handle minting configuration. Once the configuration is modified, you can
use the "Update All Handles" button to propagate changes to the affects Handle records. For more information on
configuring Cordra and Handle integration, see :ref:`handle-integration`.

Security Menu
-------------

This screen can be used to reset the password for the built-in Cordra ``admin`` user.

.. _design-javascript:

Design JavaScript Menu
----------------------

JavaScript can be added to the Design object for the purposes of programmatically generating object ids when creating
new objects. see :ref:`generateId`, as well as for customizing queries, see :ref:`customizeQuery`.

Additionally, service-level static type methods can be defined in the Design object JavaScript, in the same way they are defined
on Type objects. See :ref:`serviceLevelStaticMethods` for details.

.. warning::

   Care must be taken when importing Type methods using ``require`` statements. Since the Design object
   is sometimes loaded before any Types it might be requiring from, top-level require statements may fail.
   In such cases, you may need to import the module from within the method instead.
