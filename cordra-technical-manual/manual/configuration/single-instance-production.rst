.. _single_instance_production:

Production Single Instance Deployment
=====================================

This document describes an approach that could be used to set up a single instance of Cordra for production. This guide is written using an Ubuntu or Debian operating system. The commands will need to be adjusted for other systems.

Software Installation and Prerequisites
---------------------------------------

You will need Java 8+, Nginx, and unzip. You can install these with the following commands::

    sudo apt update
    sudo apt install unzip openjdk-11-jdk-headless nginx

You will also need to install certbot for managing HTTPS certificates. Please follow the instructions located on the `Certbot website <https://certbot.eff.org/instructions>`__. Only follow the instructions through installing the software.

Download the Cordra distribution zip from https://www.cordra.org and save it on the server. Extract the zip file to
``/opt/cordra/``.

Create a ``cordra`` user and make sure they have full access to the install directory.

You will also need a domain configured to point at the public IP address on your server. Setting up such a domain is
outside of the scope of this document.

Finally, make sure ports 80, 443, 2641, and 9000 are publicly accessible through any firewall or security measures.

Configuration
-------------

#. Create an Nginx config file for cordra at ``/etc/nginx/sites-available/cordra``. Replace ``cordra.example.org``
   in the config file with your domain.::

    server {
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name cordra.example.org;

        access_log            /var/log/nginx/cordra.access.log;

        location / {
            proxy_pass https://localhost:8443;
            proxy_set_header        Host $host;
            proxy_set_header        X-Real-IP $remote_addr;
            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header        X-Forwarded-Proto $scheme;
        }
    }

#. Enable the Cordra site and disable the default site::

    sudo ln -s /etc/nginx/sites-available/cordra /etc/nginx/sites-enabled/
    sudo rm /etc/nginx/sites-enabled/default
    sudo systemctl reload nginx

#. Enable HTTPS with a Let's Encrypt certificate.  Replace ``cordra.example.org`` with your domain::

    sudo certbot --nginx -d cordra.example.org

#. In Cordra's data directory, configure the admin password using repoInit.json. Leave the defaults in config.json. Specifically, be sure to
   listen on localhost only, using port 8443. If you need to change the port, update the Nginx config as appropriate.
#. Start Cordra using the normal startup.sh script to test that everything is working. Shutdown using shutdown.sh.
#. Create ``/lib/systemd/system/cordra.service`` which looks like the following. Edit the user, Group, and paths as appropriate to your installation::

    [Unit]
    Description=Cordra Service
    After=network.target

    [Service]
    Type=simple
    User=cordra
    Group=cordra
    WorkingDirectory=/opt/cordra
    RestartSec=10
    Restart=always
    StandardOutput=file:/opt/cordra/data/logs/service.log
    StandardError=file:/opt/cordra/data/logs/service.log
    ExecStart=/opt/cordra/startup
    ExecStop=/opt/cordra/shutdown

    [Install]
    WantedBy=multi-user.target

#. Enable the Cordra service and start Cordra::

    sudo ln -s /lib/systemd/system/cordra.service /etc/systemd/system/
    sudo systemctl enable cordra
    sudo systemctl start cordra
