Extensions and Applications
===========================

.. toctree::
   :maxdepth: 1
   :glob:

   calling-java-from-javascript
   document-repository
   medical-records-application
   person-registry
   user-registration
   object-linking
   oai-pmh
   sending-emails
   external-authentication-provider
   collab-prototype
   neo4j-as-an-additional-index
   partial-replication-aggregation
