import fs from 'fs';
import path from 'path';
import $RefParser from '@apidevtools/json-schema-ref-parser';

const finalDir = path.resolve('./dist/openapi');
const srcDir = path.resolve('openapi/src');

async function build() {
    fs.mkdirSync(finalDir, { recursive: true });
    // copy static files
    [
        'stoplight-elements.7.7.5.min.css',
        'stoplight-elements.wc.7.7.5.min.js'
    ].forEach(file => {
        fs.copyFileSync(path.resolve(srcDir, file), path.resolve(finalDir, file));
    });
    // modify index file
    let index = fs.readFileSync(path.resolve(srcDir, 'index.html'), 'utf-8');
    index = index.replace('openapi.yml', 'cordra-doip.json');
    fs.writeFileSync(path.resolve(finalDir, 'index.html'), index);
    // output fully resolved json
    const schema = await $RefParser.bundle(path.resolve(srcDir, 'openapi.yml'));
    const jsonPath = path.resolve(finalDir, 'cordra-doip.json');
    fs.writeFileSync(jsonPath, JSON.stringify(schema, null, 2));
}

build().catch(console.error);
