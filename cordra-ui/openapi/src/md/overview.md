This document describes an OpenAPI specification for the the Digital Object Interface Protocol (DOIP) API that HTTP clients can use to interact with Cordra. DOIP API is an RPC-style API designed to be comfortable to develop against, with easy-to-construct URLs and JSON message bodies that convey the essence of DOIP requests and responses. For more details, see [Cordra DOIP API for HTTP Clients](https://www.cordra.org/documentation/api/doip-api-for-http-clients.html).

### Summary of Operations

A summary of various operations along with their identifiers and descriptions is listed below. Operation identifiers that begin with 0.DOIP are defined in DOIP v2 specification. Operation identifiers that begin with 20.DOIP represent Cordra-specific operations.

| Operation       | Target  | Description |
|-----------------| ------- | -- |
| Auth.Token      | Service | Create a new access token |
| Auth.Introspect | Service | Get access token information |
| Auth.Revoke     | Service | Invalidate access token |
| Hello           | Service | Say hello to the service |
| Create          | Service | Create a digital object |
| Retrieve        | Object  | Retrieve a digital object |
| Update          | Object  | Update a digital object |
| Delete          | Object  | Delete a digital object |
| Search          | Service | Search for digital objects |

#### Aliases

For the convenience of users, short aliases are enabled for referring to the service identifier and to operation identifiers. In the case of the service identifier, `targetId=service` is considered to be a shortcut for `targetId=<prefix>/service`.

The following table shows operation aliases usable with Cordra.

| Operation Alias | Operation Handle           |
|-----------------|----------------------------|
| Auth.Token      | 20.DOIP/Op.Auth.Token      |
| Auth.Introspect | 20.DOIP/Op.Auth.Introspect |
| Auth.Revoke     | 20.DOIP/Op.Auth.Revoke     |
| Hello           | 0.DOIP/Op.Hello            |
| Create          | 0.DOIP/Op.Create           |
| Retrieve        | 0.DOIP/Op.Retrieve         |
| Update          | 0.DOIP/Op.Update           |
| Delete          | 0.DOIP/Op.Delete           |