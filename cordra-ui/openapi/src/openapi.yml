openapi: 3.1.0
info:
  title: Cordra DOIP API for HTTP Clients
  description:
    $ref: './md/overview.md'
  contact:
    email: support@cordra.org
  license:
    name: Cordra
    url: https://www.cordra.org/download.html#section-licenses
  version: 2.5.0
externalDocs:
  description: Find out more about Cordra
  url: https://www.cordra.org
servers:
  - url: https://localhost:8443/cordra/doip
    description: Your own local server
security:
  - {}
  - basicAuth: []
  - bearerAuth: []
tags:
  - name: Cordra-specific Operations
  - name: DOIP Operations
paths:
  /20.DOIP/Op.Auth.Token:
    post:
      tags:
        - Cordra-specific Operations
      summary: Create access token
      description: Used to generate a new access token for a given user
      security: []
      parameters:
        - $ref: '#/components/parameters/serviceTargetParam'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required: [ grant_type, username, password ]
              properties:
                grant_type:
                  type: string
                  enum: [ password ]
                username:
                  type: string
                  example: test@example.com
                password:
                  type: string
                  example: password
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  access_token:
                    type: string
                    description: The newly created access token
                    example: 14f874o9i4ohgjv19bds6wvov
                  token_type:
                    type: string
                    description: Always "Bearer"
                    example: Bearer
                  active:
                    type: boolean
                    description: Whether or not the token is active; always "true" for successful calls of the /auth/token API.
                    example: true
                  username:
                    type: string
                    description: Username of the authenticated user
                    example: test@example.com
                  userId:
                    type: string
                    description: UserId of the authenticated user
                    example: 20.5000.EXAMPLE/987654321
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: '#/components/responses/400BadRequest'
              example:
                error: invalid_grant
                message: Authentication failed
                error_description: Authentication failed
  /20.DOIP/Op.Auth.Introspect:
    post:
      tags:
        - Cordra-specific Operations
      summary: Check access token status
      description: Find more information about the status of a given access token
      security: []
      parameters:
        - $ref: '#/components/parameters/serviceTargetParam'
        - name: full
          in: query
          description: If `true`, then the response will contain the array "groupIds".
          schema:
            type: boolean
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required: [ token ]
              properties:
                token:
                  type: string
                  example: 14f874o9i4ohgjv19bds6wvov
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: object
                required: [ active ]
                properties:
                  active:
                    type: boolean
                    description: Whether or not the token is active
                    example: true
                  username:
                    type: string
                    description: Username of the authenticated user
                    example: test@example.com
                  userId:
                    type: string
                    description: UserId of the authenticated user
                    example: 20.5000.EXAMPLE/987654321
                  typesPermittedToCreate:
                    type: array
                    description: A list of object Types this user can create
                    items:
                      type: string
                    example:
                      - Document
                      - User
                      - Group
                  groupIds:
                    type: array
                    description: A singleton list with the party the user is a member of
                    items:
                      type: string
                    example:
                      - 20.5000.EXAMPLE/111111111
  /20.DOIP/Op.Auth.Revoke:
    post:
      tags:
        - Cordra-specific Operations
      summary: Revoke specified access token
      description: Revoke the given access token. Token revocation would typically occur when a user logs out of the system.
      security: []
      parameters:
        - $ref: '#/components/parameters/serviceTargetParam'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required: [ token ]
              properties:
                token:
                  type: string
                  example: 14f874o9i4ohgjv19bds6wvov
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: object
                required: [ active ]
                properties:
                  active:
                    type: boolean
                    description: Whether or not the token is active
                    example: false
  /20.DOIP/Op.ReindexBatch:
    post:
      tags:
        - Cordra-specific Operations
      summary: Reindex batch
      security:
        - {}
        - basicAuth: []
        - bearerAuth: []
      parameters:
        - $ref: '#/components/parameters/serviceTargetParam'
        - name: all
          in: query
          description: Specifies that all objects in storage will be reindexed.
          schema:
            type: boolean
        - name: query
          in: query
          description: Instead of explicitly specifying each object to reindex a query can be supplied. All results of that query will be reindexed.
          schema:
            type: string
            example: 'type:Document'
        - name: lockObjects
          in: query
          description: If set to true each object will be locked during reindexing (default true).
          schema:
            type: boolean
      description: |-
        Reindex all objects specified in the request.
      requestBody:
        $ref: '#/components/requestBodies/ReindexBatchRequest'
      responses:
        '200':
          description: |-
            DOIP status code: 0.DOIP/Status.001
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                required:
                  - msg
              example:
                msg: success
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '500':
          $ref: '#/components/responses/500ServerError'
  /20.DOIP/Op.BatchUpload:
    post:
      tags:
        - Cordra-specific Operations
      summary: Batch upload
      security:
        - {}
        - basicAuth: []
        - bearerAuth: []
      parameters:
        - $ref: '#/components/parameters/serviceTargetParam'
        - name: format
          in: query
          description: Specifies the format of the supplied body. If omitted it is assumed to be a JSON array of Digital Objects or a search results of Digital Objects. Alternatively this param can be set to 'ndjson' and the body can contain newline separated JSON objects.
          schema:
            type: string
        - name: failFast
          in: query
          description: If set to true the entire batch will abort when the first error is encountered (default false).
          schema:
            type: boolean
        - name: parallel
          in: query
          description: If set to true the objects in the batch will be processed in parallel (default true).
          schema:
            type: boolean
      description: |-
        Create or update all objects listed in the request.
      requestBody:
        $ref: '#/components/requestBodies/BatchUploadRequest'
      responses:
        '200':
          description: |-
            DOIP status code: 0.DOIP/Status.001
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BatchUploadResponse'
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '500':
          $ref: '#/components/responses/500ServerError'
  /20.DOIP/Op.Versions.Publish:
    post:
      tags:
        - Cordra-specific Operations
      summary: Publish Version
      security:
        - {}
        - basicAuth: []
        - bearerAuth: []
      description: |-
        Create an immutable copy of the target object.
      parameters:
        - $ref: '#/components/parameters/targetParam'
        - name: versionId
          in: query
          description: The new id for the copy being created.
          schema:
            type: string
        - name: clonePayloads
          in: query
          description: Boolean indicating if the payloads of the target should be cloned.
          schema:
            type: boolean
            example: false
      responses:
        '200':
          description: |-
            DOIP status code: 0.DOIP/Status.001
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/VersionInfo'
              examples:
                VersionInfo:
                  $ref: '#/components/examples/VersionInfoResponse'
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '409':
          $ref: '#/components/responses/409Conflict'
        '500':
          $ref: '#/components/responses/500ServerError'
  /20.DOIP/Op.Versions.Get:
    post:
      tags:
        - Cordra-specific Operations
      summary: Get Versions
      security:
        - {}
        - basicAuth: []
        - bearerAuth: []
      description: |-
        Get published versions of the target object.
      parameters:
        - $ref: '#/components/parameters/targetParam'
      responses:
        '200':
          description: |-
            DOIP status code: 0.DOIP/Status.001
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/VersionInfo'
              examples:
                ListVersionInfo:
                  $ref: '#/components/examples/GetVersionsResponse'
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '500':
          $ref: '#/components/responses/500ServerError'
  /20.DOIP/Op.Relationships.Get:
    post:
      tags:
        - Cordra-specific Operations
      summary: Get Relationships
      security:
        - {}
        - basicAuth: []
        - bearerAuth: []
      description: |-
        Get objects related to the target object.
      parameters:
        - $ref: '#/components/parameters/targetParam'
        - name: outboundOnly
          in: query
          description: Boolean indicating if the request should only consider outbound relationships (default false).
          schema:
            type: boolean
            example: false
      responses:
        '200':
          description: |-
            DOIP status code: 0.DOIP/Status.001
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RelationshipsResponse'
              examples:
                GetRelationships:
                  $ref: '#/components/examples/GetRelationshipsResponse'
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '500':
          $ref: '#/components/responses/500ServerError'
  /0.DOIP/Op.Hello:
    post:
      tags:
        - DOIP Operations
      summary: Hello
      security: []
      description: |-
        An operation to allow a client to get information about the DOIP service.
      parameters:
        - $ref: '#/components/parameters/serviceTargetParam'
      responses:
        '200':
          description: |-
            The request was processed successfully. DOIP status code: 0.DOIP/Status.001
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DoipServiceInfoDigitalObject'
              examples:
                HelloResponse:
                  $ref: '#/components/examples/HelloResponse'
            '*:*': {}
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '409':
          $ref: '#/components/responses/409Conflict'
        '500':
          $ref: '#/components/responses/500ServerError'
  /0.DOIP/Op.ListOperations:
    post:
      tags:
        - DOIP Operations
      summary: List operations
      security:
        - {}
        - basicAuth: []
        - bearerAuth: []
      description: |-
        An operation to request the list of operations that can be invoked on the target DO.
      parameters:
        - $ref: '#/components/parameters/serviceOrTargetParam'
      responses:
        '200':
          description: |-
            The request was processed successfully. DOIP status code: 0.DOIP/Status.001
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
              examples:
                Service:
                  $ref: '#/components/examples/ListOperationsServiceResponse'
            '*:*': {}
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '409':
          $ref: '#/components/responses/409Conflict'
        '500':
          $ref: '#/components/responses/500ServerError'
  /0.DOIP/Op.Create:
    post:
      tags:
        - DOIP Operations
      summary: Create object
      description: |-
        Creation of objects sends operation `0.DOIP/Op.Create` to the service object `<prefix>/service`. Regardless of
        the type of the digital object in the system, the same operation is used. The input should be a Digital Object
        specifying the `type` to be created and the `content`. The output will be the Digital Object with its new `id`
        and `metadata`, and possibly changes to the `content` automatically populated by the system.
      parameters:
        - $ref: '#/components/parameters/serviceTargetParam'
      requestBody:
        $ref: '#/components/requestBodies/DigitalObjectRequest'
      responses:
        '200':
          $ref: '#/components/responses/DigitalObjectResponse'
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '409':
          $ref: '#/components/responses/409Conflict'
        '500':
          $ref: '#/components/responses/500ServerError'
  /0.DOIP/Op.Retrieve:
    post:
      tags:
        - DOIP Operations
      summary: Retrieve object
      security:
        - {}
        - basicAuth: []
        - bearerAuth: []
      description: |-
        Also allows GET.
        
        Retrieval of objects from the system is done using the operation `0.DOIP/Op.Retrieve`. Regardless of the type
        of the digital object in the system, the same operation is used. Retrieval returns a JSON response with a
        Digital Object structure, which includes the identifier, the type, system-maintained metadata, plus of course
        the type-specific content of the object.
  
        An element (instead of object metadata) can be retrieved using the request attribute
        `attributes.element=elementName`.
      parameters:
        - $ref: '#/components/parameters/targetParam'
        - name: element
          in: query
          description: The name of the payload to be retrieved from an object.
          schema:
            type: string
            example: payload
      responses:
        '200':
          description: |-
            The request was processed successfully. If the request was to retrieve a Digital Object, the response will
            be a JSON representation of that object. If the request was to retrieve an object payload, the response will
            have a media type matching the requested payload. DOIP status code: 0.DOIP/Status.001
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DigitalObject'
              examples:
                Document:
                  $ref: '#/components/examples/DocumentResponse'
                User:
                  $ref: '#/components/examples/UserResponse'
                Group:
                  $ref: '#/components/examples/GroupResponse'
            '*:*': {}
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '409':
          $ref: '#/components/responses/409Conflict'
        '500':
          $ref: '#/components/responses/500ServerError'
  /0.DOIP/Op.Update:
    post:
      tags:
        - DOIP Operations
      summary: Update object
      description: |-
        To update an object send operation 0.DOIP/Op.Update with the target the object to be updated. Regardless of the
        type of the digital object in the system, the same operation is used. The input should be a Digital Object
        specifying the new `content` (keeping the `id`, `type`, and other properties not in the content is fine but
        optional) as a complete replacement. The output will be the Digital Object with its new `metadata` and possibly
        changes to the `content` automatically populated by the system.
      parameters:
        - $ref: '#/components/parameters/targetParam'
      requestBody:
        $ref: '#/components/requestBodies/DigitalObjectRequest'
      responses:
        '200':
          $ref: '#/components/responses/DigitalObjectResponse'
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '409':
          $ref: '#/components/responses/409Conflict'
        '500':
          $ref: '#/components/responses/500ServerError'
  /0.DOIP/Op.Delete:
    post:
      tags:
        - DOIP Operations
      summary: Delete object
      description: |-
        To delete an object send operation 0.DOIP/Op.Delete with the target the object to be deleted. Regardless of the
        type of the digital object in the system, the same operation is used. The input and output (in the case of
        success) are empty.
      parameters:
        - $ref: '#/components/parameters/targetParam'
      responses:
        '200':
          description: Successful operation
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '409':
          $ref: '#/components/responses/409Conflict'
        '500':
          $ref: '#/components/responses/500ServerError'
  /0.DOIP/Op.Search:
    post:
      tags:
        - DOIP Operations
      summary: Search
      description: |-
        Also allows GET.

        Search for objects that match the given parameters

        The query syntax is the Lucene syntax used by Lucene, Solr, and Elasticsearch. Here are three versions of the
        equivalent documentation from the three providers:
        - https://lucene.apache.org/core/8_6_0/queryparser/org/apache/lucene/queryparser/classic/package-summary.html
        - https://lucene.apache.org/solr/guide/8_6/the-standard-query-parser.html#specifying-terms-for-the-standard-query-parser
        - https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#query-string-syntax

        This syntax supports fielded search, where the fields use JSON Pointer (RFC 6901) format to specify locations
        in the structure of the object `content` (but the field names for JSON Pointers into arrays have particular
        indices replaced with underscore `_` in order to allow searching the entire array).

        For more information and detailed examples about the query syntax see
        [Query Syntax](https://www.cordra.org/documentation/api/search.html).
      security: []
      parameters:
        - $ref: '#/components/parameters/serviceTargetParam'
        - name: query
          in: query
          required: true
          description: The search to be performed
          schema:
            type: string
            example: My Document
        - name: pageNum
          in: query
          description: The page number to be returned, starting with 0
          schema:
            type: number
            default: 0
            example: 0
        - name: pageSize
          in: query
          description: |-
            The page size to be returned; if missing or negative, all results will be returned; if zero, no results are
            returned, but the "size" and if requested "facets" are still returned
          schema:
            type: number
            default: -1
            example: 5
        - name: sortFields
          in: query
          description: A comma-separated list of sort specifications, each of which is a field name optionally followed by ASC or DESC
          schema:
            type: string
            example: 'type, /name DESC'
        - name: type
          in: query
          description: If set to "id", return just object ids. If set to "full", return full object data.
          schema:
            type: string
            enum: [ id, full ]
            default: full
        - name: facets
          in: query
          description: A JSON array of objects that contain the fields to facet results by
          schema:
            type: array
            items:
              $ref: '#/components/schemas/SearchResults/properties/facets/items'
            example:
              - { "field": "/name" }
        - name: filterQueries
          in: query
          description: A JSON array of query strings to filter the results by
          schema:
            type: array
            items:
              type: string
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SearchResults'
        '400':
          $ref: '#/components/responses/400BadRequest'
        '401':
          $ref: '#/components/responses/401Unauthenticated'
        '403':
          $ref: '#/components/responses/403Forbidden'
        '404':
          $ref: '#/components/responses/404NotFound'
        '409':
          $ref: '#/components/responses/409Conflict'
        '500':
          $ref: '#/components/responses/500ServerError'
components:
  schemas:
    ErrorResponse:
      type: object
      required:
        - message
      properties:
        message:
          type: string
          description: A description of the error
    DigitalObject:
      type: object
      required: [ type ]
      properties:
        id:
          type: string
        type:
          type: string
          enum: [ User, Group, Document ]
        attributes:
          type: object
          properties:
            content:
              oneOf:
                - $ref: '#/components/schemas/Document'
                - $ref: '#/components/schemas/User'
                - $ref: '#/components/schemas/Group'
                - {}
        metadata:
          type: object
          properties:
            createdOn:
              type: number
            createdBy:
              type: string
            modifiedOn:
              type: number
            modifiedBy:
              type: string
      discriminator:
        propertyName: type
    DoipServiceInfoDigitalObject:
      type: object
      required: [ type ]
      properties:
        id:
          type: string
        type:
          type: string
          enum: [ "0.TYPE/DOIPServiceInfo" ]
        attributes:
          type: object
          properties:
            ipAddress:
              type: string
            port:
              type: integer
            protocol:
              type: string
            protocolVersion:
              type: string
            cordraVersion:
              type: object
              properties:
                number:
                  type: string
                timestamp:
                  type: string
                id:
                  type: string
              required:
                - number
                - timestamp
                - id
            publicKey:
              type: object
              properties:
                kty:
                  type: string
                n:
                  type: string
                e:
                  type: string
              required:
                - kty
                - n
                - e
          required:
            - ipAddress
            - port
            - protocol
            - protocolVersion
            - cordraVersion
            - publicKey
        metadata:
          type: object
          properties:
            createdOn:
              type: number
            createdBy:
              type: string
            modifiedOn:
              type: number
            modifiedBy:
              type: string
      discriminator:
        propertyName: type
    VersionInfo:
      type: object
      properties:
        id:
          type: string
        type:
          type: string
        versionOf:
          type: string
        publishedBy:
          type: string
        publishedOn:
          type: integer
        isTip:
          type: boolean
      required:
        - id
        - type
        - versionOf
        - publishedBy
        - publishedOn
        - isTip
    RelationshipsResponse:
      type: object
      properties:
        nodes:
          type: array
          items:
            type: object
            properties:
              id:
                type: string
              label:
                type: string
            required:
              - id
        edges:
          type: array
          items:
            type: object
            properties:
              from:
                type: string
              to:
                type: string
              style:
                type: string
              jsonPointer:
                type: string
            required:
              - from
              - to
              - style
              - jsonPointer
        results:
          type: array
          description: A list of Digital objects related to the target.
          items:
             $ref: '#/components/schemas/DigitalObject'
      required:
        - nodes
        - edges
        - results
    BatchUploadResponse:
      type: object
      properties:
        results:
          type: array
          items:
            type: object
            properties:
              position:
                type: integer
              responseCode:
                type: integer
              response:
                type: object
            required:
              - position
              - responseCode
              - response
        success:
          type: boolean
      required:
        - results
        - success
    SearchResults:
      type: object
      properties:
        size:
          type: number
          description: The number of results across all pages
          example: 83
        facets:
          type: array
          items:
            type: object
            required: [ field ]
            properties:
              field:
                type: string
                description: Field to facet results by
                example: /name
              buckets:
                type: array
                items:
                  type: object
                  properties:
                    value:
                      type: string
                      description: The value of the field for this facet
                      example: My Document
                    count:
                      type: number
                      description: The count of results for this value for the field
                      example: 10
                    filterQuery:
                      type: string
                      description: A query that can be used in subsequent requests to further restrict the results
                      example: sort_/name:"My Document"
          description: |-
            Only included if "facets" are specified in the request. A list of counts by bucket for each facet
            in the request. Each bucket for a facet also includes a "filterQuery" that can be sent in a
            subsequent request under "filterQueries" to further restrict the results
        results:
          oneOf:
            - type: array
              description: A list of Digital objects matching the query
              items:
                $ref: '#/components/schemas/DigitalObject'
            - type: array
              description: A list of object ids matching the query
              items:
                type: string
                example: 20.5000.EXAMPLE/123456789
    Document:
      $ref: './schemas/document.schema.json'
    Group:
      $ref: './schemas/group.schema.json'
    User:
      $ref: './schemas/user.schema.json'
  examples:
    HelloResponse:
      summary: Hello
      value:
        $ref: './examples/response/hello.json'
    VersionInfoResponse:
      summary: VersionInfo
      value:
        $ref: './examples/response/version-info.json'
    GetVersionsResponse:
      summary: GetVersions
      value:
        $ref: './examples/response/get-versions.json'
    GetRelationshipsResponse:
      summary: GetRelationships
      value:
        $ref: './examples/response/get-relationships.json'
    ListOperationsServiceResponse:
      summary: Hello
      value:
        $ref: './examples/response/list-operations-service-response.json'
    UserResponse:
      summary: User
      value:
        $ref: './examples/response/user.json'
    GroupResponse:
      summary: Group
      value:
        $ref: './examples/response/group.json'
    DocumentResponse:
      summary: Document
      value:
        $ref: './examples/response/document.json'
    UserRequest:
      summary: User
      value:
        $ref: './examples/request/user.json'
    GroupRequest:
      summary: Group
      value:
        $ref: './examples/request/group.json'
    DocumentRequest:
      summary: Document
      value:
        $ref: './examples/request/document.json'
  parameters:
    serviceOrTargetParam:
      name: targetId
      in: query
      required: true
      description: The ID of the object for this operation
      schema:
        type: string
        enum: [
          service,
          20.5000.EXAMPLE/123456789
        ]
    serviceTargetParam:
      name: targetId
      in: query
      required: true
      description: The ID of the service
      schema:
        type: string
        enum: [ service ]
    targetParam:
      name: targetId
      in: query
      required: true
      description: The ID of the object for this operation
      schema:
        type: string
        example: 20.5000.EXAMPLE/123456789
  requestBodies:
    DigitalObjectRequest:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/DigitalObject'
          examples:
            Document:
              $ref: '#/components/examples/DocumentRequest'
            User:
              $ref: '#/components/examples/UserRequest'
            Group:
              $ref: '#/components/examples/GroupRequest'
    BatchUploadRequest:
      required: true
      content:
        application/json:
          schema:
            type: array
            description: A list of Digital objects.
            items:
              $ref: '#/components/schemas/DigitalObject'
          example:
            {}
    ReindexBatchRequest:
      required: true
      content:
        application/json:
          schema:
            type: array
            items:
              type: string
          example:
            - 20.5000.EXAMPLE/123456789
  responses:
    DigitalObjectResponse:
      description: 'The request was processed successfully. DOIP status code: 0.DOIP/Status.001'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/DigitalObject'
          examples:
            Document:
              $ref: '#/components/examples/DocumentResponse'
            User:
              $ref: '#/components/examples/UserResponse'
            Group:
              $ref: '#/components/examples/GroupResponse'
    400BadRequest:
      description: 'There was something wrong with the structure or content of the request. DOIP status codes: 0.DOIP/Status.101, 0.DOIP/Status.200'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorResponse'
          example:
            message: ': object instance has properties which are not allowed by the schema: [\"#email\"]'
    401Unauthenticated:
      description: 'The client must authenticate to perform the attempted operation. DOIP status code: 0.DOIP/Status.102'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorResponse'
          example:
            message: 'Unauthenticated'
    403Forbidden:
      description: 'The client was not permitted to perform the attempted operation. DOIP status code: 0.DOIP/Status.103'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorResponse'
          example:
            message: 'Forbidden'
    404NotFound:
      description: 'The requested digital object could not be found. DOIP status code: 0.DOIP/Status.104'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorResponse'
          example:
            message: 'Missing object: 20.5000.EXAMPLE/987654321'
    409Conflict:
      description: 'There was a conflict preventing the request from being executed. DOIP status code: 0.DOIP/Status.105'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorResponse'
          example:
            message: 'Missing object: 20.5000.EXAMPLE/987654321'
    500ServerError:
      description: 'There was an internal server error. DOIP status code: 0.DOIP/Status.500'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorResponse'
          example:
            message: 'An unexpected server error occurred'
  securitySchemes:
    basicAuth:
      type: http
      scheme: basic
    bearerAuth:
      type: http
      scheme: bearer
      description: |-
        The Cordra-specific Operations include operations to obtain an access token, check its status, and revoke it. 
        
        A valid access token can be provided, instead of authentication credentials, for various operations. The
        system provides an access token only after a successful authentication, and the token is valid for 30 minutes
        from last use. Each valid use renews the lifetime.
        
        Security and performance improvements are usually noted with the use of tokens instead of authentication
        credentials.
        
        See the <a href="../paths/20.DOIP-Op.Auth.Token/post">20.DOIP/Op.Auth.Token operation</a> for detail on
        acquiring access tokens.

