/* eslint-disable import/no-extraneous-dependencies */
import inject from '@rollup/plugin-inject';
import basicSsl from '@vitejs/plugin-basic-ssl';

export default {
    root: './src',
    base: './',
    envDir: '../',
    logLevel: 'warn',
    build: {
        outDir: '../dist',
        sourcemap: true,
        minify: false,
        reportCompressedSize: false,
        emptyOutDir: true,
        rollupOptions: {
            maxParallelFileOps: 2,
            output: {
                manualChunks: (id) => {
                    return id.includes('node_modules') ? 'vendor' : undefined;
                }
            }
        }
    },
    plugins: [
        inject({
            exclude: '**/bootstrap.css',
            jQuery: 'jquery',
            $: 'jquery'
        }),
        basicSsl()
    ],
    server: {
        port: 8081
    },
    test: {
        environment: 'jsdom',
        setupFiles: './test/setup.ts',
        reporters: ['junit', 'default'],
        outputFile: '../build/test-results.xml'
    }
};
