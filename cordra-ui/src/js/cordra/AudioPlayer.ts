export class AudioPlayer {
    constructor(containerDiv: JQuery, audioUri: string) {
        const width = 500;
        const audio = $('<audio style="width:' + width + 'px" controls>');
        audio.attr("src", audioUri);
        containerDiv.append(audio);
    }
}
