export class VideoPlayer {
    constructor(containerDiv: JQuery, videoUri: string) {
        const width = 500;
        const video = $('<video style="width:' + width + 'px" controls>');
        video.attr("src", videoUri);
        containerDiv.append(video);
    }
}
