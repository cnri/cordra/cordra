import { JsonSchema } from "tv4";
import { Constants } from "./Constants";
import { JsonUtil } from "./JsonUtil";

export const SchemaUtil = {
    getDeepCordraSchemaProperty
};

function getDeepCordraSchemaProperty(obj: JsonSchema, ...args: string[]): unknown {
    args.unshift(Constants.CORDRA_SCHEMA_KEYWORD);
    let res = JsonUtil.getDeepProperty(obj, ...args);
    if (res) return res;
    args[0] = Constants.OLD_REPOSITORY_SCHEMA_KEYWORD;
    res = JsonUtil.getDeepProperty(obj, ...args);
    return res;
}
