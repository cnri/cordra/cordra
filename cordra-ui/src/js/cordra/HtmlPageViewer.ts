export class HtmlPageViewer {
    constructor(containerDiv: JQuery, options: { url?: string; embedded: boolean }) {
        containerDiv.empty();
        if (options.url) {
            if (options.embedded) {
                this.showEmbeddedPage(containerDiv, options.url);
            } else {
                this.showFramedPage(containerDiv, options.url);
            }
        } else {
            this.showNotFoundPage(containerDiv);
        }
    }

    showNotFoundPage(containerDiv: JQuery): void {
        const notFound = `<div>Page Not Found</div>`;
        containerDiv.html(notFound);
    }

    showEmbeddedPage(containerDiv: JQuery, url: string): void {
        $.get(url, data => {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
            containerDiv.html(data);
            containerDiv.css('width', '100%'); // still needed?
            containerDiv.css('min-height', '217px'); // still needed?
            containerDiv.css('max-height', '1200px'); // still needed?
        })
        .catch(() => this.showNotFoundPage(containerDiv));
    }

    showFramedPage(containerDiv: JQuery, url: string): void {
        const iframe: JQuery<HTMLIFrameElement> = $(
            '<iframe style="width:100%; min-height:217px; max-height:1200px; border:0; margin:0; overflow:scroll;"></iframe>'
        );
        iframe.attr('src', url);
        iframe.on("load", () => {
            if (iframe[0].contentDocument) {
                iframe.height(iframe[0].contentDocument.body.scrollHeight);
            }
            if (217 === iframe.height()) {
                iframe.css("min-height", "900px");
            }
        });
        containerDiv.append(iframe);
    }
}
