import { default as JsonEditorOnline, JSONEditorOptions } from 'jsoneditor';
import { CordraObject } from '@cnri/cordra-client';

export interface MethodsList {
    instanceMethods: string[];
    staticMethods: string[];
}

interface MethodWithStatic {
    name: string;
    isStatic: boolean;
}

export class ObjectMethods {
    private readonly methodSelect: JQuery;
    private readonly methodAttributesDiv: JQuery;
    private readonly methodAttributesEditor: JsonEditorOnline;
    private readonly methodInputDiv: JQuery;
    private readonly methodInputEditor: JsonEditorOnline;
    private readonly outputDiv: JQuery;
    private readonly outputEditor: JsonEditorOnline;

    private methodsList: MethodWithStatic[] = [];
    private readonly typeForCall: string;
    private readonly objectId: string;

    constructor(
            containerDiv: JQuery,
            objectId: string,
            type: string,
            contentPlusMeta: CordraObject) {
        this.objectId = objectId;
        if (type === "Schema") {
            this.typeForCall = contentPlusMeta.content.name;
        } else {
            this.typeForCall = type;
        }

        const html = $(`
            <div class="object-editor-toolbar col-md-12 pull-right">
                <form id="methodsForm" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="methodSelect" class="col-sm-1 control-label">Operation</label>
                        <div class="col-sm-8">
                            <select id="methodSelect" class="form-control"></select>
                        </div>
                        <button id="callButton" class="col-sm-1 btn btn-sm btn-primary">
                            <span>Invoke</span>
                        </button>
                        <button id="clearButton" class="col-sm-1 btn btn-sm btn-primary">
                            <span>Clear</span>
                        </button>
                    </div>
                </form>
            </div>
                <div class="card card-body bg-light" id=ioDiv>
                <div class="form-group col-md-6" id=input>
                    <label for="methodAttributes" style="display: block;">Attributes</label>
                    <div id="methodAttributes" style="height: 200px"></div>
                    <label for="methodInput" style="display: block;">Input</label>
                    <div id="methodInput" style="height: 300px"></div>
                </div>
                <div class="form-group col-md-6" id=output>
                    <label for="methodOutput" style="display: block;">Output</label>
                    <div id="methodOutput" style="height: 500px"></div>
                </div>
            </div>            
        `);
        containerDiv.append(html);

        const callButton = $('#callButton');
        callButton.on("click", () => this.onCallMethodClick());

        const clearButton = $('#clearButton');
        clearButton.on("click", () => this.onClearClick());

        const methodsForm = $('#methodsForm');
        methodsForm.on("submit", () => {
            return false;
        });

        this.methodSelect = $('#methodSelect');
        this.methodAttributesDiv = $("#methodAttributes");
        this.methodInputDiv = $("#methodInput");
        this.outputDiv = $("#methodOutput");

        const methodOpts = {
            ace,
            theme: "ace/theme/textmate",
            mode: "code",
            modes: ["code", "tree"], // allowed modes
            onError(err: Error) {
                console.error(err);
            }
        } as JSONEditorOptions;
        this.methodAttributesEditor = new JsonEditorOnline(this.methodAttributesDiv[0], methodOpts, {});
        this.methodInputEditor = new JsonEditorOnline(this.methodInputDiv[0], methodOpts, {});

        const ouptutOpts = {
            ace,
            theme: "ace/theme/textmate",
            mode: "code",
            modes: ["code", "tree"], // allowed modes
            onError(err: Error) {
                console.error(err);
            }
        } as JSONEditorOptions;
        this.outputEditor = new JsonEditorOnline(this.outputDiv[0], ouptutOpts);
        this.outputEditor.setText("");
        APP.disableJsonEditorOnline(this.outputEditor);
        this.listMethods();
    }

    onCallMethodClick(): void {
        this.callMethod();
    }

    onClearClick(): void {
        this.outputEditor.setText("");
    }

    listMethods(): void {
        APP.listInstanceAndStaticMethods(
            this.objectId,
            this.typeForCall,
            (m: MethodsList) => this.onListMethodsSuccess(m)
        );
    }

    onListMethodsSuccess(methods: MethodsList): void {
        const methodsList: MethodWithStatic[] = [];
        methods.instanceMethods.sort();
        methods.staticMethods.sort();
        for (const methodName of methods.instanceMethods) {
            const method = {
                isStatic: false,
                name: methodName
            };
            methodsList.push(method);
        }
        for (const methodName of methods.staticMethods) {
            const method = {
                isStatic: true,
                name: methodName
            };
            methodsList.push(method);
        }
        for (const method of methodsList) {
            let displyName = method.name;
            if (method.isStatic) {
                displyName += " [static]";
            }
            const option = $(
                '<option value="' + method.name + '">' + displyName + "</option>"
            );
            this.methodSelect.append(option);
        }
        this.methodsList = methodsList;
    }

    callMethod(): void {
        const selectedIndex = this.methodSelect.prop('selectedIndex');
        const method = this.methodsList[selectedIndex];
        const attributesText = this.methodAttributesEditor.getText();
        let attributes = undefined;
        if (attributesText) {
            try {
                attributes = JSON.parse(attributesText);
            } catch (error) {
                APP.notifications.alertError("Attributes is not valid JSON.");
                return;
            }
        }
        const inputText = this.methodInputEditor.getText();
        let input = undefined;
        if (inputText) {
            try {
                input = JSON.parse(inputText);
            } catch (error) {
                APP.notifications.alertError("Input is not valid JSON.");
                return;
            }
        }
        if (method.isStatic) {
            APP.callMethodForTypeAsResponse(
                this.typeForCall,
                method.name,
                input,
                attributes,
                (resp: Response) => this.onCallSuccess(resp)
            );
        } else {
            APP.callMethodAsResponse(
                this.objectId,
                method.name,
                input,
                attributes,
                (resp: Response) => this.onCallSuccess(resp)
            );
        }
    }

    onCallSuccess(response: Response): void {
        response
            .text()
            .then((responseText) => {
                try {
                    const json = JSON.parse(responseText);
                    const prettyText = JSON.stringify(json, null, 2);
                    this.outputEditor.setText(prettyText);
                } catch (error) {
                    this.outputEditor.setText(responseText);
                    console.log(response);
                }
            })
            .catch(console.error);
    }
}
