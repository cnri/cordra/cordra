// TODO get this from cordra-client?
import { Utf8 } from './Utf8';
import { Utf16 } from './Utf16';
import { Base64 } from './Base64';
import { Hex } from './Hex';

export const Encoder = { Utf8, Utf16, Hex, Base64 };
