package net.cnri.cordra.doip;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.Payload;
import net.cnri.util.StreamUtil;
import net.dona.doip.InDoipMessage;
import net.dona.doip.InDoipSegment;
import net.dona.doip.client.DigitalObject;
import net.dona.doip.client.Element;
import net.dona.doip.util.GsonUtility;
import net.dona.doip.util.InDoipMessageUtil;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DoipUtil {

    public static net.dona.doip.client.DigitalObject ofCordraObject(CordraObject co) {
        return ofCordraObject(co, false);
    }

    public static net.dona.doip.client.DigitalObject ofCordraObject(CordraObject co, boolean includeInputStreams) {
        net.dona.doip.client.DigitalObject digitalObject = new DigitalObject();
        digitalObject.id = co.id;
        digitalObject.type = co.type;
        digitalObject.attributes = new JsonObject();
        digitalObject.attributes.add("content", co.content);
        if (co.acl != null) digitalObject.attributes.add("acl", GsonUtility.getGson().toJsonTree(co.acl));
        if (co.metadata != null) digitalObject.attributes.add("metadata", GsonUtility.getGson().toJsonTree(co.metadata));
        if (co.responseContext != null) digitalObject.attributes.add("responseContext", co.responseContext);
        if (co.userMetadata != null) {
            for (Map.Entry<String, JsonElement> attribute :  co.userMetadata.entrySet()) {
                digitalObject.attributes.add(doipAttributeOfUserMetadataKey(attribute.getKey()), attribute.getValue());
            }
        }
        digitalObject.elements = null;
        if (co.payloads != null && !co.payloads.isEmpty()) {
            digitalObject.elements = new ArrayList<>();
            for (Payload payload : co.payloads) {
                Element el = new Element();
                el.id = payload.name;
                if (payload.size >= 0) el.length = payload.size;
                el.type = payload.mediaType;
                el.attributes = new JsonObject();
                if (payload.filename != null) el.attributes.addProperty("filename", payload.filename);
                if (includeInputStreams) {
                    @SuppressWarnings("resource")
                    InputStream in = payload.getInputStream();
                    if (in != null) {
                        el.in = in;
                    }
                }
                digitalObject.elements.add(el);
            }
        }
        return digitalObject;
    }

    // Prefix userMetadata. whenever needed to avoid clobbering
    private static String doipAttributeOfUserMetadataKey(String key) {
        if ("content".equals(key)) return "userMetadata.content";
        else if ("acl".equals(key)) return "userMetadata.acl";
        else if ("metadata".equals(key)) return "userMetadata.metadata";
        else if ("responseContext".equals(key)) return "userMetadata.responseContext";
        else if (key.startsWith("userMetadata.")) return "userMetadata." + key;
        else return key;
    }

    // Remove userMetadata. prefix
    private static String userMetadataKeyOfDoipAttribute(String key) {
        if (key.startsWith("userMetadata.")) return key.substring(13);
        else return key;
    }

    public static CordraObject toCordraObject(DigitalObject digitalObject) {
        if (digitalObject == null) return null;
        return toCordraObject(digitalObject, false);
    }

    public static CordraObject toCordraObject(DigitalObject digitalObject, boolean includeInputStreams) {
        if (digitalObject == null) return null;
        CordraObject co = new CordraObject();
        co.id = digitalObject.id;
        co.type = digitalObject.type;
        if (digitalObject.attributes != null) {
            JsonObject userMetadata = new JsonObject();
            for (Map.Entry<String, JsonElement> attribute :  digitalObject.attributes.entrySet()) {
                if ("content".equals(attribute.getKey())) {
                    co.content = digitalObject.attributes.get("content");
                } else if ("acl".equals(attribute.getKey())) {
                    co.acl = GsonUtility.getGson().fromJson(digitalObject.attributes.get("acl"), CordraObject.AccessControlList.class);
                } else if ("metadata".equals(attribute.getKey())) {
                    co.metadata = GsonUtility.getGson().fromJson(digitalObject.attributes.get("metadata"), CordraObject.Metadata.class);
                } else if ("responseContext".equals(attribute.getKey())) {
                    JsonElement responseContextElement = digitalObject.attributes.get("responseContext");
                    if (responseContextElement.isJsonObject()) {
                        co.responseContext = responseContextElement.getAsJsonObject();
                    }
                } else {
                    userMetadata.add(userMetadataKeyOfDoipAttribute(attribute.getKey()), attribute.getValue());
                }
            }
            if (!userMetadata.keySet().isEmpty()) {
                co.userMetadata = userMetadata;
            }
        }
        if (digitalObject.elements != null && digitalObject.elements.size() > 0) {
            co.payloads = new ArrayList<>();
            for (Element el : digitalObject.elements) {
                Payload p = new Payload();
                p.name = el.id;
                p.mediaType = el.type;
                if (el.length != null) {
                    p.size = el.length;
                }
                if (el.attributes != null) {
                    if (el.attributes.has("filename")) {
                        p.filename = el.attributes.get("filename").getAsString();
                    }
                }
                if (includeInputStreams) {
                    if (el.in != null) {
                        p.setInputStream(el.in);
                    }
                }
                co.payloads.add(p);
            }
        }
        return co;
    }

    public static CordraObject cordraObjectFromSegments(InDoipMessage input) throws CordraException, IOException {
        InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(input);
        if (firstSegment == null) {
            throw new BadRequestCordraException("Missing input");
        }
        DigitalObject digitalObject = GsonUtility.getGson().fromJson(extractJson(firstSegment), DigitalObject.class);
        CordraObject co = DoipUtil.toCordraObject(digitalObject);
        if (co.payloads != null) {
            Map<String, Payload> payloads = new HashMap<>();
            for (Payload p : co.payloads) {
                payloads.put(p.name, p);
            }
            Iterator<InDoipSegment> segments = input.iterator();
            while (segments.hasNext()) {
                InDoipSegment headerSegment = segments.next();
                String payloadName;
                try {
                    payloadName = extractJson(headerSegment).getAsJsonObject().get("id").getAsString();
                } catch (Exception e) {
                    throw new BadRequestCordraException("Unexpected element header");
                }
                if (!segments.hasNext()) {
                    throw new BadRequestCordraException("Unexpected end of input");
                }
                InDoipSegment elementBytesSegment = segments.next();
                Payload p = payloads.get(payloadName);
                if (p == null) {
                    throw new BadRequestCordraException("No such element " + payloadName);
                }
                try (InputStream elementInputStream = elementBytesSegment.getInputStream()) {
                    ByteArrayInputStream in = persistInputStream(elementInputStream);
                    p.setInputStream(in);
                    // TODO change the following if in is changed from a ByteArrayInputStream
                    p.size = in.available();
                }
            }
        } else {
            if (!InDoipMessageUtil.isEmpty(input)) {
                throw new BadRequestCordraException("Unexpected input segments");
            }
        }
        return co;
    }

    private static ByteArrayInputStream persistInputStream(InputStream in) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte[] buf = new byte[8192];
        int r;
        while ((r = in.read(buf)) > 0) {
            bout.write(buf, 0, r);
        }
        return new ByteArrayInputStream(bout.toByteArray());
    }

    public static JsonElement extractJson(InDoipSegment segment) throws IOException, CordraException {
        if (segment == null) return null;
        if (segment.isJson()) return segment.getJson();
        try (
                InputStream is = segment.getInputStream();
                InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
        ) {
            String jsonString = StreamUtil.readFully(isr);
            if (jsonString.trim().isEmpty()) return null;
            return JsonParser.parseString(jsonString);
        } catch (JsonParseException e) {
            throw new BadRequestCordraException("Unable to parse input as JSON");
        }
    }
}
