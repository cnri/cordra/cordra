package net.cnri.cordra.api;

import java.util.Collections;
import java.util.List;

/**
 * Parameters to a repository search, such as pagination and sorting.
 */
public class QueryParams {
    /**
     * Default query parameters.  Passing {@code null} to repository search methods amounts to using this.  No pagination and no sorting.
     */
    public static final QueryParams DEFAULT = new QueryParams();

    private final List<SortField> sortFields;
    private final int pageNum;
    private final int pageSize;
    private final List<FacetSpecification> facets;
    private final List<String> filterQueries;
    private final List<String> filter;
    private final boolean includeScore;
    private final boolean includeVersions;

    // Included to support Gson
    /**
     * Constructs default QueryParams, with pageSize = -1 (returns all results).
     * Prefer {@link #DEFAULT}.
     */
    public QueryParams() {
        this(0, -1);
    }

    /**
     * Constructs a QueryParams.
     * Prefer {@link #builder()}.
     *
     * @param pageNum the page number to return.  Starts at 0.  Ignored if pageSize &lt;= 0.
     * @param pageSize the number of objects to return.  PageSize of &lt; 0 means return all.
     */
    public QueryParams(int pageNum, int pageSize) {
        this(pageNum, pageSize, Collections.emptyList());
    }

    public QueryParams(int pageNum, int pageSize, List<SortField> sortFields) {
        this(pageNum, pageSize, sortFields, null, null);
    }

    public QueryParams(int pageNum, int pageSize, List<SortField> sortFields, List<FacetSpecification> facets, List<String> filterQueries) {
        this(pageNum, pageSize, sortFields, facets, filterQueries, null);
    }

    public QueryParams(int pageNum, int pageSize, List<SortField> sortFields, List<FacetSpecification> facets, List<String> filterQueries, List<String> filter) {
        this(pageNum, pageSize, sortFields, facets, filterQueries, filter, false, false);
    }

    private QueryParams(int pageNum, int pageSize, List<SortField> sortFields, List<FacetSpecification> facets, List<String> filterQueries, List<String> filter, boolean includeScore, boolean includeVersions) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.sortFields = sortFields;
        this.facets = facets;
        this.filterQueries = filterQueries;
        this.filter = filter;
        this.includeScore = includeScore;
        this.includeVersions = includeVersions;
    }

    @Deprecated
    public int getPageNumber() {
        return pageNum;
    }

    public int getPageNum() {
        return pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public List<SortField> getSortFields() {
        return sortFields;
    }

    public List<String> getFilterQueries() { return filterQueries; }

    public List<FacetSpecification> getFacets() { return facets; }

    public List<String> getFilter() {
        return filter;
    }

    public boolean getIncludeScore() {
        return includeScore;
    }

    public boolean getIncludeVersions() {
        return includeVersions;
    }

    /**
     * Returns a builder for {@code QueryParams}.
     * @return a builder for {@code QueryParams}.
     */
    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("hiding")
    /**
     * A builder for {@code QueryParams}.
     */
    public static class Builder {
        private int pageNum = 0;
        private int pageSize = -1;
        private List<SortField> sortFields = Collections.emptyList();
        private List<FacetSpecification> facets = null;
        private List<String> filterQueries = null;
        private List<String> filter = null;
        private boolean includeScore = false;
        private boolean includeVersions = false;

        public Builder() { }

        public Builder pageNum(int pageNum) {
            this.pageNum = pageNum;
            return this;
        }

        public Builder pageSize(int pageSize) {
            this.pageSize = pageSize;
            return this;
        }

        public Builder sortFields(List<SortField> sortFields) {
            this.sortFields = sortFields;
            return this;
        }

        public Builder facets(List<FacetSpecification> facets) {
            this.facets = facets;
            return this;
        }

        public Builder filterQueries(List<String> filterQueries) {
            this.filterQueries = filterQueries;
            return this;
        }

        public Builder filter(List<String> filter) {
            this.filter = filter;
            return this;
        }

        public Builder includeScore(boolean includeScore) {
            this.includeScore = includeScore;
            return this;
        }

        public Builder includeVersions(boolean includeVersions) {
            this.includeVersions = includeVersions;
            return this;
        }

        public QueryParams build() {
            return new QueryParams(pageNum, pageSize, sortFields, facets, filterQueries, filter, includeScore, includeVersions);
        }
    }

}