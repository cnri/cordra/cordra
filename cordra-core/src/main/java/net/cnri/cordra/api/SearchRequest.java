package net.cnri.cordra.api;

import com.google.gson.JsonElement;

import java.util.List;

public class SearchRequest {
    public String query;
    public JsonElement queryJson;
    public int pageNum = 0;
    public int pageSize = -1;
    public boolean ids = false;
    public List<SortField> sortFields;
    public List<String> filter = null;
    public boolean full = true;
    public List<String> filterQueries;
    public List<FacetSpecification> facets;
    public boolean includeScore;
    public boolean includeVersions;

    public SearchRequest() {
        // empty default constructor
    }

    public SearchRequest(String query, QueryParams params, boolean ids) {
        this.query = query;
        this.ids = ids;
        this.pageNum = params.getPageNum();
        this.pageSize = params.getPageSize();
        this.sortFields = params.getSortFields();
        this.filter = params.getFilter();
        this.filterQueries = params.getFilterQueries();
        this.facets = params.getFacets();
        this.includeScore = params.getIncludeScore();
        this.includeVersions = params.getIncludeVersions();
    }

    public QueryParams getParams() {
        return QueryParams.builder()
            .pageNum(pageNum)
            .pageSize(pageSize)
            .sortFields(sortFields)
            .facets(facets)
            .filterQueries(filterQueries)
            .filter(filter)
            .includeScore(includeScore)
            .includeVersions(includeVersions)
            .build();
    }
}
