package net.cnri.cordra.api;

public class FacetSpecification {

    public String field;
    public int maxBuckets = 10;

    public FacetSpecification() {}

    public FacetSpecification(String field) {
        this.field = field;
    }

    public FacetSpecification(String field, int maxBuckets) {
        this.field = field;
        this.maxBuckets = maxBuckets;
    }
}
