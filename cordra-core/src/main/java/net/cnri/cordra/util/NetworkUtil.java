package net.cnri.cordra.util;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;

public class NetworkUtil {
    public static boolean canUseIPv6() {
        if (Boolean.parseBoolean(System.getProperty("java.net.preferIPv4Stack"))) return false;
        try {
            for (NetworkInterface intf : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                for (InetAddress addr : Collections.list(intf.getInetAddresses())) {
                    if (addr instanceof Inet6Address) return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
