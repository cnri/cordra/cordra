@echo off

REM Get the full name of the directory where the Cordra code is installed
set PRG=%~dp0%
set CORDRADIR=%PRG%..

set CP=%CORDRADIR%\sw\lib\*;%CORDRADIR%\sw\lib\cordra-client\*;%CORDRADIR%\sw\lib\servletContainer\*;%CORDRADIR%\data\lib\*

REM two spaces expected before %%v in the following line
for /f tokens^=3^ delims^=.^"^  %%v in ('java -version 2^>^&1 ^| findstr /i version') do (set JAVA_VERSION=%%v)

if %JAVA_VERSION%==1 set JAVA_VERSION=8

if %JAVA_VERSION%==8 (
    java -Dpolyglot.engine.WarnInterpreterOnly=false -cp "%CP%" net.cnri.cordra.startup.CordraStartup --data "%CORDRADIR%\data" --webapps-priority "%CORDRADIR%\sw\webapps-priority"
) else (
    java -XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCI --upgrade-module-path="%CORDRADIR%\sw\lib\graalvm" -cp "%CP%" net.cnri.cordra.startup.CordraStartup --data "%CORDRADIR%\data" --webapps-priority "%CORDRADIR%\sw\webapps-priority"
)
