@echo off
setlocal
set PRG=%~dp0%
set CORDRADIR=%PRG%..
cd "%CORDRADIR%\sw\lib\graalvm\"
powershell -Command "(New-Object Net.WebClient).DownloadFile('https://repo1.maven.org/maven2/org/graalvm/compiler/compiler/22.0.0.2/compiler-22.0.0.2.jar', 'compiler-22.0.0.2.jar')"
powershell -Command "(New-Object Net.WebClient).DownloadFile('https://repo1.maven.org/maven2/org/graalvm/compiler/compiler-management/22.0.0.2/compiler-management-22.0.0.2.jar', 'compiler-management-22.0.0.2.jar')"
endlocal
