@echo off
setlocal
set PRG=%~dp0%
set CORDRADIR=%PRG%..
cd "%CORDRADIR%\sw\lib\"
powershell -Command "(New-Object Net.WebClient).DownloadFile('https://repo1.maven.org/maven2/org/graalvm/tools/chromeinspector/22.0.0.2/chromeinspector-22.0.0.2.jar', 'chromeinspector-22.0.0.2.jar')"
powershell -Command "(New-Object Net.WebClient).DownloadFile('https://repo1.maven.org/maven2/org/graalvm/tools/profiler/22.0.0.2/profiler-22.0.0.2.jar', 'profiler-22.0.0.2.jar')"
endlocal
