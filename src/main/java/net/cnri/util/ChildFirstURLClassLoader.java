package net.cnri.util;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Predicate;

public class ChildFirstURLClassLoader extends URLClassLoader {
    private final ClassLoader system;
    private final Predicate<String> shouldLoadFromParentPredicate;

    public ChildFirstURLClassLoader(URL[] urls, ClassLoader parent, Predicate<String> shouldLoadFromParentPredicate) {
        super(urls, parent);
        this.system = getSystemClassLoader();
        this.shouldLoadFromParentPredicate = shouldLoadFromParentPredicate;
    }

    private boolean shouldLoadFromParent(String name) {
        return this.shouldLoadFromParentPredicate.test(name);
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        synchronized (getClassLoadingLock(name)) {
            Class<?> klass = findLoadedClass(name);
            if (klass != null) {
                if (resolve) resolveClass(klass);
                return klass;
            }
            if (shouldLoadFromParent(name)) {
                return super.loadClass(name, resolve);
            }
            try {
                return system.loadClass(name);
            } catch (ClassNotFoundException e) {
                // ignored
            }
            try {
                klass = findClass(name);
                if (resolve) resolveClass(klass);
                return klass;
            } catch (ClassNotFoundException e) {
                // ignored, equivalent exception will be thrown below if parent lacks it
            }
            return super.loadClass(name, resolve);
        }
    }

    @Override
    public URL getResource(String name) {
        if (shouldLoadFromParent(name)) {
            return super.getResource(name);
        }
        {
            URL resource = system.getResource(name);
            if (resource != null) return resource;
        }
        {
            URL resource = findResource(name);
            if (resource != null) return resource;
        }
        return super.getResource(name);
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        if (shouldLoadFromParent(name)) {
            return super.getResources(name);
        }
        Set<URL> resources = new LinkedHashSet<>();
        {
            Enumeration<URL> resourcesEnum = system.getResources(name);
            resources.addAll(Collections.list(resourcesEnum));
        }
        {
            Enumeration<URL> resourcesEnum = findResources(name);
            resources.addAll(Collections.list(resourcesEnum));
        }
        {
            Enumeration<URL> resourcesEnum = super.getResources(name);
            resources.addAll(Collections.list(resourcesEnum));
        }
        return Collections.enumeration(resources);
    }
}
