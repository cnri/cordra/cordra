package net.cnri.cordra;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.*;
import net.cnri.cordra.storage.mongodb.MongoDbUtil;
import net.cnri.cordra.util.JacksonUtil;

import org.bson.Document;
import java.lang.reflect.Type;

/**
 * A factory for singleton {@code Gson} instances which can properly serialize {@code Stream}, {@code PublicKey}, and {@code PrivateKey},
 * as well as {@code JsonNode} and {@code org.bson.Document}.
 */
public class GsonUtility {
    /**
     * Returns a {@code GsonBuilder} with adapters to properly serialize {@code Stream}s.
     */
    public static GsonBuilder setup(GsonBuilder gsonBuilder) {
        net.cnri.cordra.util.GsonUtility.setup(gsonBuilder);
        gsonBuilder.registerTypeHierarchyAdapter(JsonNode.class, new JsonNodeTypeAdapter());
        gsonBuilder.registerTypeAdapter(Document.class, new DocumentTypeAdapter());
        return gsonBuilder;
    }

    /**
     * Returns a {@code Gson}.
     */
    public static Gson getGson() {
        return GsonHolder.gson;
    }

    /**
     * Returns a {@code Gson} which is configured for pretty-printing.
     */
    public static Gson getPrettyGson() {
        return PrettyGsonHolder.prettyGson;
    }

    private static class GsonHolder {
        static Gson gson;
        static {
            gson = GsonUtility.setup(new GsonBuilder().disableHtmlEscaping()).create();
        }
    }

    private static class PrettyGsonHolder {
        static Gson prettyGson;
        static {
            prettyGson = GsonUtility.setup(new GsonBuilder().disableHtmlEscaping().setPrettyPrinting()).create();
        }
    }

    public static class JsonNodeTypeAdapter implements JsonSerializer<JsonNode> {

        @Override
        public JsonElement serialize(JsonNode jsonNode, Type typeOfSrc, JsonSerializationContext context) {
            return JacksonUtil.jacksonToGson(jsonNode);
        }
    }

    public static class DocumentTypeAdapter implements JsonSerializer<Document> {
        @Override
        public JsonElement serialize(Document src, Type typeOfSrc, JsonSerializationContext context) {
            return MongoDbUtil.mongoObjectToJsonElement(src);
        }
    }
}
