package net.cnri.cordra.util.cmdline;

import com.google.gson.Gson;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import net.cnri.cordra.CordraConfigSource;
import net.cnri.cordra.CordraServiceFactory;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.SearchResults;
import net.cnri.cordra.model.CordraConfig;
import net.cnri.cordra.storage.CordraStorage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ExportTool {

    private CordraStorage storage;
    private Path outputDir;
    private boolean hashDirs;
    private AtomicLong count = new AtomicLong(0L);
    private List<String> ids;
    private final boolean progress;
    private ExecutorServiceManager executorServiceManager;
    private final int numberOfThreads;

    public ExportTool(CordraConfig config, Path basePath, Path outputDir, boolean hashDirs, List<String> ids, boolean progress, int numberOfThreads) throws IOException, CordraException {
        storage = CordraServiceFactory.getStorage(config, basePath, false);
        this.outputDir = outputDir;
        this.hashDirs = hashDirs;
        this.ids = ids;
        this.progress = progress;
        this.numberOfThreads = numberOfThreads;
    }

    public void run() throws Exception {
        executorServiceManager = new ExecutorServiceManager(numberOfThreads);
        executorServiceManager.start();
        if (outputDir != null) {
            if (ids == null) {
                writeAllObjectsToDirectory();
            } else {
                writeLimitedObjectsToDirectory(ids);
            }
            if (count.get() == 1L) {
                System.out.println("Exported " + count.get() + " object.");
            } else {
                System.out.println("Exported " + count.get() + " objects.");
            }
        } else {
            if (ids == null) {
                writeAllObjectsToToStandardOut();
            } else {
                writeLimitedObjectsToStandardOut(ids);
            }
        }
        executorServiceManager.stop();
    }

    private void writeAllObjectsToDirectory() throws Exception {
        try (SearchResults<CordraObject> results = storage.list()) {
            writeResultsToDirectory(results);
        }
    }

    private void writeLimitedObjectsToDirectory(List<String> limitedIds) throws Exception {
        try (SearchResults<CordraObject> results = storage.get(limitedIds)) {
            writeResultsToDirectory(results);
        }
    }

    private void writeAllObjectsToToStandardOut() throws Exception {
        try (SearchResults<CordraObject> results = storage.list()) {
            writeResultsToStandardOut(results);
        }
    }

    private void writeLimitedObjectsToStandardOut(List<String> limitedIds) throws Exception {
        try (SearchResults<CordraObject> results = storage.get(limitedIds)) {
            writeResultsToStandardOut(results);
        }
    }

    private void writeResultsToDirectory(SearchResults<CordraObject> results) throws Exception {
        for (CordraObject co : results) {
            executorServiceManager.submit(() -> exportCordraObjectToDirectory(co), "Exception exporting " + co.id);
        }
    }

    public void exportCordraObjectToDirectory(CordraObject co) throws CordraException, IOException {
        //CordraObjectWithPayloadsAsStrings cos = CordraObjectWithPayloadsAsStrings.fromCordraObject(co, storage);
        String filename = HashDirectory.convertToFileName(co.id);
        Path path;
        if (hashDirs) {
            String hashPath = HashDirectory.hashPathFor(co.id, 6, 3);
            Path hashDir = outputDir.resolve(hashPath);
            Files.createDirectories(hashDir);
            path = hashDir.resolve(filename);
        } else {
            path = outputDir.resolve(filename);
        }
        try (BufferedWriter w = Files.newBufferedWriter(path)) {
            CordraObjectWriter.writeCordraObjectToWriterStreamingPayloads(co, storage, w);
            //GsonUtility.getPrettyGson().toJson(cos, w);
        }
        count.getAndIncrement();
        if (progress) {
            System.out.print("Progress: " + count.get() + " \r");
        }
    }

    private void exportCordraObjectToStandardOut(CordraObject co) throws CordraException, IOException {
        //CordraObjectWithPayloadsAsStrings cos = CordraObjectWithPayloadsAsStrings.fromCordraObject(co, storage);
        synchronized (System.out) {
            //gson.toJson(cos, System.out);
            BufferedWriter w = new BufferedWriter(new OutputStreamWriter(System.out));
            CordraObjectWriter.writeCordraObjectToWriterStreamingPayloads(co, storage, w);
            System.out.println();
        }
        count.getAndIncrement();
        if (progress) {
            System.err.print("Progress: " + count.get() + " \r");
        }
    }

    private void writeResultsToStandardOut(SearchResults<CordraObject> results) throws Exception {
        for (CordraObject co : results) {
            executorServiceManager.submit(() -> exportCordraObjectToStandardOut(co), "Exception exporting " + co.id);
        }
    }

    public static void main(String[] args) throws Exception {
        OptionSet options = parseOptions(args);
        String config = (String) options.valueOf("c");
        Path basePath = null;
        if (options.has("d")) {
            String basePathString = (String) options.valueOf("d");
            basePath = Paths.get(basePathString);
        }
        boolean hashDirs = options.has("t");
        Gson gson = GsonUtility.getPrettyGson();
        CordraConfig cordraConfig;
        try (BufferedReader br = Files.newBufferedReader(Paths.get(config))) {
            cordraConfig = gson.fromJson(br, CordraConfig.class);
        }
        cordraConfig = CordraConfigSource.getDefaultConfigIfNull(cordraConfig);
        String storageModule = cordraConfig.storage.module;
        boolean needsDataDirectory = ("hds".equals(storageModule) || "bdbje".equals(storageModule));
        if (needsDataDirectory && basePath == null) {
            System.err.println(
                "Error.\n" +
                    "Given your config.json, your Cordra server relies on a filesystem database for storing Cordra Objects.\n" +
                    "You need to specify the /data directory with the -d option."
                );
            System.exit(1);
        }
        Path outputDir = null;
        if (options.has("o")) {
            String output = (String) options.valueOf("o");
            outputDir = Paths.get(output);
            Files.createDirectories(outputDir);
        }
        List<String> ids = null;
        if (options.has("l")) {
            Path idsPath = Paths.get((String)options.valueOf("l"));
            ids = Files.readAllLines(idsPath);
        }
        if (options.has("i")) {
            @SuppressWarnings("unchecked")
            List<String> idsFromArgs = (List<String>) options.valuesOf("i");
            if (ids != null) {
                ids.addAll(idsFromArgs);
            } else {
                ids = idsFromArgs;
            }
        }
        int numberOfThreads;
        if (options.has("number-of-threads")) {
            numberOfThreads = (Integer) options.valueOf("number-of-threads");
        } else {
            numberOfThreads = 1;
        }
        ExportTool exportTool = new ExportTool(cordraConfig, basePath, outputDir, hashDirs, ids, options.has("progress"), numberOfThreads);
        exportTool.run();
    }

    private static OptionSet parseOptions(String[] args) throws IOException {
        OptionParser parser = new OptionParser();
        parser.acceptsAll(Arrays.asList("h", "help"), "Prints help").forHelp();
        parser.acceptsAll(Arrays.asList("c", "config"), "Path to Cordra's config.json").withRequiredArg().required();
        parser.acceptsAll(Arrays.asList("o", "output"), "Path to output directory").withRequiredArg();
        parser.acceptsAll(Arrays.asList("s", "stdout"), "Write output as newline-delimited json to stdout");
        parser.acceptsAll(Arrays.asList("d", "data"), "Path to cordra data directory, necessary if using bdbje storage").withRequiredArg();
        parser.acceptsAll(Arrays.asList("t", "tree"), "Splits directories into a tree based on the hash of the object id");
        parser.acceptsAll(Arrays.asList("i", "id"), "Id of object to export. Multiple can be used").withRequiredArg();
        parser.acceptsAll(Arrays.asList("l", "ids"), "Path to a file containing a newline separated list of object ids to export").withRequiredArg();
        parser.acceptsAll(Arrays.asList("P", "progress"), "Show progress");
        parser.acceptsAll(Arrays.asList("n", "number-of-threads"), "Number of threads used to export Cordra Objects (default 1)").withRequiredArg().ofType(Integer.class);
        OptionSet options;
        try {
            options = parser.parse(args);
        } catch (OptionException e) {
            System.err.println("Error parsing options: " + e.getMessage());
            parser.printHelpOn(System.err);
            System.exit(1);
            return null;
        }
        if (options.has("h")) {
            System.out.println("This tool connects directly to a Cordra storage backend and exports full Cordra objects," +
                    "includes payloads and internal metadata.");
            parser.printHelpOn(System.out);
            System.exit(1);
            return null;
        }
        if (!options.has("s") && !options.has("o")) {
            System.err.println("Error. Either the 's' or 'o' option must be provided");
            parser.printHelpOn(System.err);
            System.exit(1);
            return null;
        }
        if (options.has("s") && options.has("o")) {
            System.err.println("Error. Both the 's' and 'o' options are provided");
            parser.printHelpOn(System.err);
            System.exit(1);
            return null;
        }
        return options;
    }
}
