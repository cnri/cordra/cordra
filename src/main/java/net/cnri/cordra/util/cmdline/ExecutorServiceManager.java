package net.cnri.cordra.util.cmdline;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecutorServiceManager {
    private static final Logger logger = LoggerFactory.getLogger(ExecutorServiceManager.class);

    private final int numberOfThreads;

    private ExecutorService exec;
    private boolean executorServiceIsOn;

    public ExecutorServiceManager(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
        exec = null;
        executorServiceIsOn = false;
    }

    public void start() {
        if (executorServiceIsOn) {
            throw new IllegalStateException("ExecutorService is already on.");
        }
        if (numberOfThreads > 1) {
            // throttle the number of queued tasks
            BlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>(numberOfThreads * 3) {
                @Override
                public boolean offer(Runnable e) {
                    try {
                        put(e);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                        return false;
                    }
                    return true;
                }
            };
            exec = new ThreadPoolExecutor(numberOfThreads, numberOfThreads, 0, TimeUnit.MILLISECONDS, queue);
        }
        executorServiceIsOn = true;
    }

    public void stop() {
        if (!executorServiceIsOn) {
            throw new IllegalStateException("ExecutorService is off; it cannot be turned off.");
        }
        if (numberOfThreads > 1) {
            exec.shutdown();
            try {
                exec.awaitTermination(Integer.MAX_VALUE, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        executorServiceIsOn = false;
    }

    public void submit(ThrowingRunnable task, String errorMessage) throws Exception {
        if (!executorServiceIsOn) {
            throw new IllegalStateException("ExecutorService is off; tasks cannot be submitted to it.");
        }
        if (numberOfThreads <= 1) {
            task.run();
            return;
        }
        exec.submit(() -> {
            try {
                task.run();
            } catch (Exception e) {
                logger.error(errorMessage, e);
            }
            return null;
        });
    }

    @FunctionalInterface
    public static interface ThrowingRunnable {
        void run() throws Exception;
    }
}
