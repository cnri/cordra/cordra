package net.cnri.cordra.util.cmdline;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import net.cnri.cordra.CordraConfigSource;
import net.cnri.cordra.CordraServiceFactory;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.*;
import net.cnri.cordra.model.CordraConfig;
import net.cnri.cordra.storage.CordraStorage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

public class ImportTool {

    private final CordraStorage storage;
    private final Path inputDir;
    private static final Gson gson = GsonUtility.getPrettyGson();
    private AtomicLong count = new AtomicLong(0L);
    private final boolean progress;
    private final boolean deleteAllExceptDesign;
    private final boolean deleteDesign;
    private final AtomicLong deleted = new AtomicLong(0L);
    private ExecutorServiceManager executorServiceManager;
    private ExecutorServiceManager requestExecutor;
    private final int numberOfThreads;


    public ImportTool(CordraConfig config, Path basePath, Path inputDir, boolean progress, int numberOfThreads, boolean deleteAll, boolean deleteDesign) throws IOException, CordraException {
        storage = CordraServiceFactory.getStorage(config, basePath, false);
        this.inputDir = inputDir;
        this.progress = progress;
        this.deleteAllExceptDesign = deleteAll;
        this.deleteDesign = deleteDesign;
        this.numberOfThreads = numberOfThreads;
    }

    public void run() throws Exception {
        if (deleteDesign || deleteAllExceptDesign) {
            System.out.println("Deleting objects before import");
        }
        if (deleteDesign) {
            CordraObject design = storage.get("design");
            if (design != null) {
                deleteObject("design");
            }
        }
        executorServiceManager = new ExecutorServiceManager(numberOfThreads);
        if (deleteAllExceptDesign) {
            try (SearchResults<String> ids = storage.listHandles()) {
                executorServiceManager.start();
                for (String id : ids) {
                    if (!"design".equals(id)) {
                        executorServiceManager.submit(() -> deleteObject(id), "Error deleting " + id);
                    }
                }
                executorServiceManager.stop();
            }
        }
        if (deleteDesign || deleteAllExceptDesign) {
            System.out.println("\nDone. Deleted " + deleted.get() + " objects");
        }
        executorServiceManager.start();
        if (inputDir != null) {
            if (Files.exists(inputDir)) {
                try (
                    Stream<Path> pathStream = Files
                    .walk(inputDir)
                    .filter(Files::isRegularFile)
                    .filter(path -> !path.getFileName().toString().equals(".DS_Store"));
                ) {
                    for (Path p : (Iterable<Path>) pathStream::iterator) {
                        executorServiceManager.submit(() -> importPathStreaming(p), "Error importing " + p);
                    }
                }
            }
        } else {
            processInputFromStandardInStreaming();
            //processInputFromStandardIn();
        }
        executorServiceManager.stop();
        if (requestExecutor != null) {
            requestExecutor.stop();
        }
        if (count.get() == 1L) {
            System.out.println("Imported " + count.get() + " object.");
        } else {
            System.out.println("Imported " + count.get() + " objects.");
        }
    }

    private void deleteObject(String id) throws CordraException {
        storage.delete(id);
        deleted.getAndIncrement();
        if (progress) {
            System.out.print("Delete progress: " + deleted.get() + " \r");
        }
    }

    private void importPathStreaming(Path p) throws Exception {
        try (BufferedReader br = Files.newBufferedReader(p)) {
            JsonReader jsonReader = new JsonReader(br);
            importObjectFromReader(jsonReader, false);
        }
    }

    private void importObjectFromReader(JsonReader jsonReader, boolean sendCordraRequestsInParallel) throws Exception {
        CordraObject co = null;
        Map<String, File> payloadTempFiles = new HashMap<>();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            if ("payloads".equals(name)) {
                payloadTempFiles = readPayloadsToTempFiles(jsonReader);
            } else if ("cordraObject".equals(name)) {
                co = gson.fromJson(jsonReader, CordraObject.class);
            }
        }
        jsonReader.endObject();
        if (sendCordraRequestsInParallel) {
            final CordraObject coForClosure = co;
            final Map<String, File> payloadTempFilesForClosure = payloadTempFiles;
            requestExecutor.submit(() -> writeToStorageAndCleanUp(coForClosure, payloadTempFilesForClosure), "Error submitting reques to cordra " + co.id);
        } else {
            writeToStorageAndCleanUp(co, payloadTempFiles);
        }
    }

    private void writeToStorageAndCleanUp(CordraObject co, Map<String, File> payloadTempFiles) throws FileNotFoundException, CordraException {
        attachTempFilesToCordraObject(co, payloadTempFiles);
        writeToStorage(co);
        deleteTempFiles(payloadTempFiles);
        //System.out.println();
        count.getAndIncrement();
        if (progress) {
            System.out.print("Progress: " + count.get() + " \r");
        }
    }


    private void deleteTempFiles(Map<String, File> payloadTempFiles) {
        for (File f : payloadTempFiles.values()) {
            f.delete();
        }
    }

    private void attachTempFilesToCordraObject(CordraObject co, Map<String, File> payloadTempFiles) throws FileNotFoundException {
        if (co.payloads != null) {
            for (Payload p : co.payloads) {
                String name = p.name;
                File payloadTempFile = payloadTempFiles.get(name);
                InputStream in = new FileInputStream(payloadTempFile);
                p.setInputStream(in);
            }
        }
    }

    private Map<String, File> readPayloadsToTempFiles(JsonReader jsonReader) throws IOException {
        Map<String, File> payloadTempFiles = new HashMap<>();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String payloadName = jsonReader.nextName();
            File payloadTempFile = readPayloadToTempFile(jsonReader);
            payloadTempFiles.put(payloadName, payloadTempFile);
        }
        jsonReader.endObject();
        return payloadTempFiles;
    }

    private File readPayloadToTempFile(JsonReader jsonReader) throws IOException {
        File tempFile = makeTempFile();
        JsonToken token = jsonReader.peek();
        if (token.equals(JsonToken.STRING)) {
            String base64 = jsonReader.nextString();
            try (FileOutputStream outputStream = new FileOutputStream(tempFile)) {
                byte[] decodedBytes = Base64.getDecoder().decode(base64);
                outputStream.write(decodedBytes);
            }
        } else {
            jsonReader.beginArray();
            try (FileOutputStream outputStream = new FileOutputStream(tempFile)) {
                while (jsonReader.hasNext()) {
                    String base64Chunk = jsonReader.nextString();
                    byte[] decodedBytes = Base64.getDecoder().decode(base64Chunk);
                    outputStream.write(decodedBytes);
                }
            }
            jsonReader.endArray();
        }
        return tempFile;
    }

    public static final String TEMP_FILE_PREFIX = "cordra-import-tool";

    private static File makeTempFile() throws IOException {
        Path tempFilePath = Files.createTempFile(TEMP_FILE_PREFIX, null);
        File file = tempFilePath.toFile();
        file.deleteOnExit();
        return file;
    }

    private void writeToStorage(CordraObject co) throws CordraException {
        if (exists(co.id)) {
            storage.update(co);
        } else {
            storage.create(co);
        }
    }

    public boolean exists(String id) throws CordraException {
        return storage.get(id) != null;
    }

    private void processInputFromStandardInStreaming() throws Exception {
        //If processing in a streaming style for payloads we cannot process in parallel here
        requestExecutor = new ExecutorServiceManager(numberOfThreads);
        requestExecutor.start();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8))) {
            JsonReader jsonReader = new JsonReader(br);
            jsonReader.setLenient(true);
            while (true) {
                JsonToken token = jsonReader.peek();
                if (token.equals(JsonToken.BEGIN_OBJECT)) {
                    importObjectFromReader(jsonReader, true);
                } else {
                    break;
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        OptionSet options = parseOptions(args);
        String config = (String) options.valueOf("c");
        Path basePath = null;
        if (options.has("d")) {
            String basePathString = (String) options.valueOf("d");
            basePath = Paths.get(basePathString);
        }
        CordraConfig cordraConfig;
        try (BufferedReader br = Files.newBufferedReader(Paths.get(config))) {
            cordraConfig = gson.fromJson(br, CordraConfig.class);
        }
        cordraConfig = CordraConfigSource.getDefaultConfigIfNull(cordraConfig);
        String storageModule = cordraConfig.storage.module;
        boolean needsDataDirectory = ("hds".equals(storageModule) || "bdbje".equals(storageModule));
        if (needsDataDirectory && basePath == null) {
            System.err.println(
                "Error.\n" +
                    "Given your config.json, your Cordra server relies on a filesystem database for storing Cordra Objects.\n" +
                    "You need to specify the /data directory with the -d option."
                );
            System.exit(1);
        }
        Path inputDir = null;
        if (options.has("i")) {
            String input = (String) options.valueOf("i");
            inputDir = Paths.get(input);
        }
        boolean showProgress = options.has("progress");
        int numberOfThreads;
        if (options.has("number-of-threads")) {
            numberOfThreads = (Integer) options.valueOf("number-of-threads");
        } else {
            numberOfThreads = 48;
        }
        boolean deleteAllExceptDesign = options.has("delete-all-first");
        boolean deleteDesign = options.has("delete-design-first");
        ImportTool importTool = new ImportTool(cordraConfig, basePath, inputDir, showProgress, numberOfThreads, deleteAllExceptDesign, deleteDesign);
        importTool.run();
    }

    private static OptionSet parseOptions(String[] args) throws IOException {
        OptionParser parser = new OptionParser();
        parser.acceptsAll(Arrays.asList("h", "help"), "Prints help").forHelp();
        parser.acceptsAll(Arrays.asList("c", "config"), "Path Cordra's config.json").withRequiredArg().required();
        parser.acceptsAll(Arrays.asList("i", "input"), "Path to input directory").withRequiredArg();
        parser.acceptsAll(Arrays.asList("s", "stdin"), "Read input as newline-delimited json from stdin");
        parser.acceptsAll(Arrays.asList("d", "data"), "Path to cordra data directory, necessary if using bdbje storage").withRequiredArg();
        parser.acceptsAll(Arrays.asList("P", "progress"), "Show progress");
        parser.accepts("delete-all-first", "Delete all objects (except Design Object) from storage before import");
        parser.accepts("delete-design-first", "Delete Design object from storage before import");
        parser.acceptsAll(Arrays.asList("n", "number-of-threads"), "Number of threads used to import Cordra Objects (default 48)").withRequiredArg().ofType(Integer.class);
        OptionSet options;
        try {
            options = parser.parse(args);
        } catch (OptionException e) {
            System.err.println("Error parsing options: " + e.getMessage());
            parser.printHelpOn(System.err);
            System.exit(1);
            return null;
        }
        if (options.has("h")) {
            System.out.println("This tool connects directly to a Cordra storage backend and imports full Cordra objects from the specified directory," +
                    "includes payloads and internal metadata.");
            parser.printHelpOn(System.out);
            System.exit(1);
            return null;
        }
        if (!options.has("s") && !options.has("i")) {
            System.err.println("Error. Either the 's' or 'i' option must be provided");
            parser.printHelpOn(System.err);
            System.exit(1);
            return null;
        }
        if (options.has("s") && options.has("i")) {
            System.err.println("Error. Both the 's' and 'i' options are provided");
            parser.printHelpOn(System.err);
            System.exit(1);
            return null;
        }
        return options;
    }
}
