package net.cnri.cordra.util.cmdline;

import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.UncheckedCordraException;
import net.cnri.cordra.collections.PersistentMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Base64;

public class Base64ChunkEncodingIterator implements PersistentMap.CloseableIterator<String> {

    private final InputStream in;

    private int chunkMultiple = 256;
    private int chunkSize = 3 * chunkMultiple; //chunk size must be a multiple of 3.

    private boolean closed = false;
    private String next;

    public Base64ChunkEncodingIterator(InputStream in) {
        this.in = in;
    }

    @Override
    public void close() {
        closed = true;
        if (in != null) try { in.close(); } catch (Exception e) {}
    }

    @Override
    public boolean hasNext() {
        if (closed) throw new IllegalStateException("Already closed");
        if (next != null) return true;
        try {
            next = nextChunkInBase64();
            if (next == null) {
                close();
                return false;
            } else {
                return true;
            }
        } catch (IOException e) {
            throw new UncheckedCordraException(new InternalErrorCordraException(e));
        }
    }

    private String nextChunkInBase64() throws IOException {
        byte[] buf = new byte[chunkSize];
        int off = 0;
        int r;
        while ((r = in.read(buf, off, chunkSize - off)) > 0) {
            off += r;
        }
        if (r == -1 && off == 0) {
            return null;
        }
        String encoded;
        if (off > 0 && off < chunkSize) {
            byte[] read = Arrays.copyOf(buf, off);
            encoded = Base64.getEncoder().encodeToString(read);
        } else {
            encoded = Base64.getEncoder().encodeToString(buf);
        }
        return encoded;
    }

    @Override
    public String next() {
        String result = next;
        if (next != null) {
            next = null;
            return result;
        } else {
            hasNext();
        }
        result = next;
        next = null;
        return result;
    }
}
