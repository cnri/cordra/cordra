package net.cnri.cordra.util.cmdline;

import com.google.gson.stream.JsonWriter;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.Payload;
import net.cnri.cordra.storage.CordraStorage;

import java.io.*;

public class CordraObjectWriter {

    /**
     * Output JSON has the following structure
     *  {
     *     "cordraObject": {...},
     *     "payloads": {
     *       "payloadName": ["base64 chunk of payload", ""base64 chunk of payload""]
     *     }
     *  }
     */
    public static void writeCordraObjectToWriterStreamingPayloads(CordraObject co, CordraStorage storage, BufferedWriter writer) throws IOException, CordraException {
        JsonWriter jsonWriter = new JsonWriter(writer);
        jsonWriter.beginObject();
        jsonWriter.name("cordraObject");
        GsonUtility.getGson().toJson(co, CordraObject.class, jsonWriter);
        if (co.payloads != null) {
            jsonWriter.name("payloads");
            jsonWriter.beginObject();
            for (Payload p : co.payloads) {
                try (InputStream in = storage.getPayload(co.id, p.name)) {
                    writePayload(jsonWriter, p, in);
                }
            }
            jsonWriter.endObject();
        }
        jsonWriter.endObject();
        jsonWriter.flush();
    }

    private static void writePayload(JsonWriter jsonWriter, Payload p, InputStream in) throws IOException {
        jsonWriter.name(p.name);
        jsonWriter.beginArray();
        try (Base64ChunkEncodingIterator iter = new Base64ChunkEncodingIterator(in)) {
            while (iter.hasNext()) {
                String base64Chunk = iter.next();
                jsonWriter.value(base64Chunk);
            }
        }
        jsonWriter.endArray();
    }
}
