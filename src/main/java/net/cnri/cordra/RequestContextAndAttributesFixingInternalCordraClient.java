package net.cnri.cordra;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.function.Function;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.cnri.cordra.AllHandlesUpdater.UpdateStatus;
import net.cnri.cordra.InternalCordraClient.JsonAndMediaType;
import net.cnri.cordra.api.AuthResponse;
import net.cnri.cordra.api.AuthTokenResponse;
import net.cnri.cordra.api.CallResponse;
import net.cnri.cordra.api.CallResponseHandler;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.CordraObject.AccessControlList;
import net.cnri.cordra.api.DefaultingFromCallResponseHandlerCordraClient;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.api.QueryParams;
import net.cnri.cordra.api.SearchResults;
import net.cnri.cordra.api.VersionInfo;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.model.FileMetadataResponse;
import net.cnri.cordra.relationships.Relationships;
import net.cnri.cordra.api.InstrumentedCordraClient.ThrowingRunnable;

public class RequestContextAndAttributesFixingInternalCordraClient implements HooksSupportCordraClient, DefaultingFromCallResponseHandlerCordraClient {

    private final InternalCordraClient delegate;

    public RequestContextAndAttributesFixingInternalCordraClient(InternalCordraClient delegate) {
        this.delegate = delegate;
    }

    private <R> R runFixingRequestContext(Callable<R> c, Options options) throws CordraException {
        RequestContext origRequestContext = RequestContextHolder.get();
        setUpRequestContextForOptions(options, origRequestContext);
        try {
            R result = c.call();
            return result;
        } catch (RuntimeException | Error | CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new AssertionError("Doesn't actually throw this", e);
        } finally {
            if (origRequestContext == null) {
                RequestContextHolder.clear();
            } else {
                RequestContextHolder.set(origRequestContext);
            }
        }
    }

    private void runFixingRequestContext(ThrowingRunnable r, Options options) throws CordraException {
        RequestContext origRequestContext = RequestContextHolder.get();
        setUpRequestContextForOptions(options, origRequestContext);
        try {
            r.run();
        } catch (RuntimeException | Error | CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new AssertionError("Doesn't actually throw this", e);
        } finally {
            if (origRequestContext == null) {
                RequestContextHolder.clear();
            } else {
                RequestContextHolder.set(origRequestContext);
            }
        }
    }

    private void setUpRequestContextForOptions(Options options, RequestContext origRequestContext) {
        JsonObject userContext = options.requestContext;
        JsonObject attributes = options.attributes;
        if (attributes != null && attributes.has("requestContext")) {
            JsonElement requestContextFromAttributes = attributes.get("requestContext");
            if (requestContextFromAttributes.isJsonObject()) {
                userContext = requestContextFromAttributes.getAsJsonObject();
            } else {
                userContext = null;
            }
        }
        RequestContext requestContext = new RequestContext();
        requestContext.setSystemCall(false);
        if (userContext != null) {
            requestContext.setRequestContext(userContext);
        } else if (origRequestContext != null) {
            requestContext.setRequestContext(origRequestContext.getRequestContext());
        }
        requestContext.setAttributes(attributes);
        RequestContextHolder.set(requestContext);
    }

    @Override
    public AuthResponse authenticateAndGetResponse(Options options) throws CordraException {
        return runFixingRequestContext(() -> runFixingRequestContext(() -> delegate.authenticateAndGetResponse(options), options), options);
    }

    @Override
    public CordraObject get(String id, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.get(id, options), options);
    }

    @Override
    public JsonElement call(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.call(objectId, methodName, params, options), options);
    }

    @Override
    public JsonElement callForType(String type, String methodName, JsonElement params, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.callForType(type, methodName, params, options), options);
    }

    @Override
    public InputStream getPayload(String id, String payloadName, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getPayload(id, payloadName, options), options);
    }

    @Override
    public CallResponse getPayloadAsResponse(String id, String payloadName, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getPayloadAsResponse(id, payloadName, options), options);
    }

    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getPartialPayload(id, payloadName, start, end, options), options);
    }

    @Override
    public CallResponse getPartialPayloadAsResponse(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getPartialPayloadAsResponse(id, payloadName, start, end, options), options);
    }

    @Override
    public CordraObject create(CordraObject d, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.create(d, options), options);
    }

    @Override
    public CordraObject update(CordraObject d, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.update(d, options), options);
    }

    @Override
    public List<String> listMethods(String objectId, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.listMethods(objectId, options), options);
    }

    @Override
    public List<String> listMethodsForType(String type, boolean isStatic, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.listMethodsForType(type, isStatic, options), options);
    }

    @Override
    public JsonElement call(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.call(objectId, methodName, input, options), options);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.callAsResponse(objectId, methodName, params, options), options);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, InputStream input, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.callAsResponse(objectId, methodName, input, options), options);
    }

    @Override
    public JsonElement callForType(String type, String methodName, InputStream input, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.callForType(type, methodName, input, options), options);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, JsonElement params, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.callForTypeAsResponse(type, methodName, params, options), options);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, InputStream input, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.callForTypeAsResponse(type, methodName, input, options), options);
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.publishVersion(objectId, versionId, clonePayloads, options), options);
    }

    @Override
    public List<VersionInfo> getVersionsFor(String id, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getVersionsFor(id, options), options);
    }

    @Override
    public void delete(String id, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.delete(id, options), options);
    }

    @Override
    public SearchResults<CordraObject> list(Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.list(options), options);
    }

    @Override
    public SearchResults<String> listHandles(Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.listHandles(options), options);
    }

    @Override
    public SearchResults<CordraObject> search(String query, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.search(query, options), options);
    }

    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.search(query, params, options), options);
    }

    @Override
    public SearchResults<String> searchHandles(String query, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.searchHandles(query, options), options);
    }

    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.searchHandles(query, params, options), options);
    }

    @Override
    public void changePassword(String newPassword, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.changePassword(newPassword, options), options);
    }

    @Override
    public void reindexBatch(List<String> batchIds, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.reindexBatch(batchIds, options), options);
    }

    @Override
    public AuthTokenResponse getAuthToken(Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getAuthToken(options), options);
    }

    @Override
    public AuthResponse introspectToken(Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.introspectToken(options), options);
    }

    @Override
    public void revokeToken(Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.revokeToken(options), options);
    }

    @Override
    public void getPayload(String id, String payloadName, CallResponseHandler handler, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.getPayload(id, payloadName, handler, options), options);
    }

    @Override
    public void getPartialPayload(String id, String payloadName, Long start, Long end, CallResponseHandler handler, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.getPartialPayload(id, payloadName, start, end, handler, options), options);
    }

    @Override
    public void call(String objectId, String methodName, JsonElement params, CallResponseHandler handler, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.call(objectId, methodName, params, handler, options), options);
    }

    @Override
    public void call(String objectId, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.call(objectId, methodName, input, handler, options), options);
    }

    @Override
    public void callForType(String type, String methodName, JsonElement params, CallResponseHandler handler, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.callForType(type, methodName, params, handler, options), options);
    }

    @Override
    public void callForType(String type, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.callForType(type, methodName, input, handler, options), options);
    }

    public JsonElement getJsonAtPointer(String id, String jsonPointer, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getJsonAtPointer(id, jsonPointer, options), options);
    }

    public JsonAndMediaType getJsonAndMediaTypeAtPointer(String id, String jsonPointer, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getJsonAndMediaTypeAtPointer(id, jsonPointer, options), options);
    }

    public FileMetadataResponse getPayloadMetadata(String id, String payloadName, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getPayloadMetadata(id, payloadName, options), options);
    }

    public void deletePayload(String id, String payloadName, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.deletePayload(id, payloadName, options), options);
    }

    public void deleteJsonAtPointer(String id, String jsonPointer, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.deleteJsonAtPointer(id, jsonPointer, options), options);
    }

    public AuthenticationResult internalAuthenticate(Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.internalAuthenticate(options), options);
    }

    public AuthResponse introspectToken(Options options, Function<String, Function<String, Object>> sessionGetter)
        throws CordraException {
        return runFixingRequestContext(() -> delegate.introspectToken(options, sessionGetter), options);
    }

    public void revokeToken(Options options, Consumer<String> sessionRevoker) throws CordraException {
        runFixingRequestContext(() -> delegate.revokeToken(options, sessionRevoker), options);
    }

    public void ensureAdmin(Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.ensureAdmin(options), options);
    }

    public Relationships getRelationshipsFor(String objectId, boolean outboundOnly, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getRelationshipsFor(objectId, outboundOnly, options), options);
    }

    public AccessControlList getAclFor(String objectId, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getAclFor(objectId, options), options);
    }

    public UpdateStatus getHandleUpdateStatus(Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getHandleUpdateStatus(options), options);
    }

    public CordraObject updateAtJsonPointer(String objectId, String jsonPointer, JsonElement replacementJsonData, Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.updateAtJsonPointer(objectId, jsonPointer, replacementJsonData, options), options);
    }

    public AuthTokenResponse getAuthToken(Options options, Function<Map<String, Object>, String> sessionCreator) throws CordraException {
        return runFixingRequestContext(() -> delegate.getAuthToken(options, sessionCreator), options);
    }

    public InitDataResponse getInitData(Options options) throws CordraException {
        return runFixingRequestContext(() -> delegate.getInitData(options), options);
    }

    public void updateAcls(String objectId, AccessControlList sAcl, Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.updateAcls(objectId, sAcl, options), options);
    }

    public void updateAllHandleRecords(Options options) throws CordraException {
        runFixingRequestContext(() -> delegate.updateAllHandleRecords(options), options);
    }

    /*** end methods with Options ***/

    @Override
    public String getDefaultUsername() {
        return delegate.getDefaultUsername();
    }

    @Override
    public String getDefaultPassword() {
        return delegate.getDefaultPassword();
    }

    @Override
    public SearchResults<CordraObject> get(Collection<String> ids) throws CordraException {
        return delegate.get(ids);
    }

    @Override
    public Gson getGson() {
        return delegate.getGson();
    }

    @Override
    public void setGson(Gson gson) {
        delegate.setGson(gson);
    }

    @Override
    public CordraObject get(String id) throws CordraException {
        return delegate.get(id);
    }

    @Override
    public CordraObject get(String id, String username, String password) throws CordraException {
        return delegate.get(id, username, password);
    }

    @Override
    public InputStream getPayload(String id, String payloadName) throws CordraException {
        return delegate.getPayload(id, payloadName);
    }

    @Override
    public InputStream getPayload(String id, String payloadName, String username, String password) throws CordraException {
        return delegate.getPayload(id, payloadName, username, password);
    }

    @Override
    public SearchResults<CordraObject> listByType(List<String> types) throws CordraException {
        return delegate.listByType(types);
    }

    @Override
    public CallResponse getPayloadAsResponse(String id, String payloadName) throws CordraException {
        return delegate.getPayloadAsResponse(id, payloadName);
    }

    @Override
    public SearchResults<String> listHandlesByType(List<String> types) throws CordraException {
        return delegate.listHandlesByType(types);
    }

    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end) throws CordraException {
        return delegate.getPartialPayload(id, payloadName, start, end);
    }

    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end, String username, String password) throws CordraException {
        return delegate.getPartialPayload(id, payloadName, start, end, username, password);
    }

    @Override
    public CallResponse getPartialPayloadAsResponse(String id, String payloadName, Long start, Long end) throws CordraException {
        return delegate.getPartialPayloadAsResponse(id, payloadName, start, end);
    }

    @Override
    public CordraObject create(CordraObject d) throws CordraException {
        return delegate.create(d);
    }

    @Override
    public CordraObject create(CordraObject d, String username, String password) throws CordraException {
        return delegate.create(d, username, password);
    }

    @Override
    public CordraObject create(CordraObject d, boolean isDryRun, String username, String password) throws CordraException {
        return delegate.create(d, isDryRun, username, password);
    }

    @Override
    public CordraObject create(CordraObject d, boolean isDryRun) throws CordraException {
        return delegate.create(d, isDryRun);
    }

    @Override
    public CordraObject update(CordraObject d) throws CordraException {
        return delegate.update(d);
    }

    @Override
    public CordraObject update(CordraObject d, String username, String password) throws CordraException {
        return delegate.update(d, username, password);
    }

    @Override
    public CordraObject update(CordraObject d, boolean isDryRun, String username, String password) throws CordraException {
        return delegate.update(d, isDryRun, username, password);
    }

    @Override
    public CordraObject update(CordraObject d, boolean isDryRun) throws CordraException {
        return delegate.update(d, isDryRun);
    }

    @Override
    public List<String> listMethods(String objectId) throws CordraException {
        return delegate.listMethods(objectId);
    }

    @Override
    public List<String> listMethods(String objectId, String username, String password) throws CordraException {
        return delegate.listMethods(objectId, username, password);
    }

    @Override
    public List<String> listMethodsForType(String type, boolean isStatic) throws CordraException {
        return delegate.listMethodsForType(type, isStatic);
    }

    @Override
    public List<String> listMethodsForType(String type, boolean isStatic, String username, String password) throws CordraException {
        return delegate.listMethodsForType(type, isStatic, username, password);
    }

    @Override
    public JsonElement call(String objectId, String methodName, JsonElement params) throws CordraException {
        return delegate.call(objectId, methodName, params);
    }

    @Override
    public JsonElement call(String objectId, String methodName, JsonElement params, String username, String password) throws CordraException {
        return delegate.call(objectId, methodName, params, username, password);
    }

    @Override
    public JsonElement call(String objectId, String methodName, InputStream input) throws CordraException {
        return delegate.call(objectId, methodName, input);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, JsonElement params) throws CordraException {
        return delegate.callAsResponse(objectId, methodName, params);
    }

    @Override
    public CallResponse callAsResponse(String objectId, String methodName, InputStream input) throws CordraException {
        return delegate.callAsResponse(objectId, methodName, input);
    }

    @Override
    public JsonElement callForType(String type, String methodName, JsonElement params) throws CordraException {
        return delegate.callForType(type, methodName, params);
    }

    @Override
    public JsonElement callForType(String type, String methodName, JsonElement params, String username, String password) throws CordraException {
        return delegate.callForType(type, methodName, params, username, password);
    }

    @Override
    public JsonElement callForType(String type, String methodName, InputStream input) throws CordraException {
        return delegate.callForType(type, methodName, input);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, JsonElement params) throws CordraException {
        return delegate.callForTypeAsResponse(type, methodName, params);
    }

    @Override
    public CallResponse callForTypeAsResponse(String type, String methodName, InputStream input) throws CordraException {
        return delegate.callForTypeAsResponse(type, methodName, input);
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads) throws CordraException {
        return delegate.publishVersion(objectId, versionId, clonePayloads);
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads, String username, String password) throws CordraException {
        return delegate.publishVersion(objectId, versionId, clonePayloads, username, password);
    }

    @Override
    public List<VersionInfo> getVersionsFor(String objectId) throws CordraException {
        return delegate.getVersionsFor(objectId);
    }

    @Override
    public List<VersionInfo> getVersionsFor(String objectId, String username, String password) throws CordraException {
        return delegate.getVersionsFor(objectId, username, password);
    }

    @Override
    public void delete(String id) throws CordraException {
        delegate.delete(id);
    }

    @Override
    public void delete(String id, String username, String password) throws CordraException {
        delegate.delete(id, username, password);
    }

    @Override
    public SearchResults<CordraObject> list() throws CordraException {
        return delegate.list();
    }

    @Override
    public SearchResults<String> listHandles() throws CordraException {
        return delegate.listHandles();
    }

    @Override
    public SearchResults<CordraObject> search(String query) throws CordraException {
        return delegate.search(query);
    }

    @Override
    public SearchResults<CordraObject> search(String query, String username, String password) throws CordraException {
        return delegate.search(query, username, password);
    }

    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params) throws CordraException {
        return delegate.search(query, params);
    }

    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params, String username, String password) throws CordraException {
        return delegate.search(query, params, username, password);
    }

    @Override
    public SearchResults<String> searchHandles(String query) throws CordraException {
        return delegate.searchHandles(query);
    }

    @Override
    public SearchResults<String> searchHandles(String query, String username, String password) throws CordraException {
        return delegate.searchHandles(query, username, password);
    }

    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params) throws CordraException {
        return delegate.searchHandles(query, params);
    }

    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params, String username, String password) throws CordraException {
        return delegate.searchHandles(query, params, username, password);
    }

    @Override
    public boolean authenticate() throws CordraException {
        return delegate.authenticate();
    }

    @Override
    public boolean authenticate(String username, String password) throws CordraException {
        return delegate.authenticate(username, password);
    }

    @Override
    public AuthResponse authenticateAndGetResponse() throws CordraException {
        return delegate.authenticateAndGetResponse();
    }

    @Override
    public AuthResponse authenticateAndGetResponse(String username, String password) throws CordraException {
        return delegate.authenticateAndGetResponse(username, password);
    }

    @Override
    public void changePassword(String newPassword) throws CordraException {
        delegate.changePassword(newPassword);
    }

    @Override
    public void changePassword(String username, String password, String newPassword) throws CordraException {
        delegate.changePassword(username, password, newPassword);
    }

    @Override
    public String getContentAsJson(String id) throws CordraException {
        return delegate.getContentAsJson(id);
    }

    @Override
    public <T> T getContent(String id, Class<T> klass) throws CordraException {
        return delegate.getContent(id, klass);
    }

    @Override
    public CordraObject create(String type, String contentJson) throws CordraException {
        return delegate.create(type, contentJson);
    }

    @Override
    public CordraObject create(String type, String contentJson, String username, String password) throws CordraException {
        return delegate.create(type, contentJson, username, password);
    }

    @Override
    public CordraObject update(String id, String contentJson) throws CordraException {
        return delegate.update(id, contentJson);
    }

    @Override
    public CordraObject update(String id, String contentJson, String username, String password) throws CordraException {
        return delegate.update(id, contentJson, username, password);
    }

    @Override
    public CordraObject create(String type, Object content) throws CordraException {
        return delegate.create(type, content);
    }

    @Override
    public CordraObject create(String type, Object content, String username, String password) throws CordraException {
        return delegate.create(type, content, username, password);
    }

    @Override
    public CordraObject update(String id, Object content) throws CordraException {
        return delegate.update(id, content);
    }

    @Override
    public CordraObject create(String type, String contentJson, boolean isDryRun) throws CordraException {
        return delegate.create(type, contentJson, isDryRun);
    }

    @Override
    public CordraObject create(String type, String contentJson, boolean isDryRun, String username, String password) throws CordraException {
        return delegate.create(type, contentJson, isDryRun, username, password);
    }

    @Override
    public CordraObject update(String id, String contentJson, boolean isDryRun) throws CordraException {
        return delegate.update(id, contentJson, isDryRun);
    }

    @Override
    public CordraObject update(String id, String contentJson, boolean isDryRun, String username, String password) throws CordraException {
        return delegate.update(id, contentJson, isDryRun, username, password);
    }

    @Override
    public CordraObject create(String type, Object content, boolean isDryRun) throws CordraException {
        return delegate.create(type, content, isDryRun);
    }

    @Override
    public CordraObject create(String type, Object content, boolean isDryRun, String username, String password) throws CordraException {
        return delegate.create(type, content, isDryRun, username, password);
    }

    @Override
    public CordraObject update(String id, Object content, boolean isDryRun) throws CordraException {
        return delegate.update(id, content, isDryRun);
    }

    @Override
    public void close() throws IOException, CordraException {
        // do not allow hooks authors to close the InternalCordraClient
    }

    @Override
    public void getPayload(String id, String payloadName, CallResponseHandler handler) throws CordraException {
        delegate.getPayload(id, payloadName, handler);
    }

    @Override
    public CordraObject getType(String type) throws CordraException {
        return delegate.getType(type);
    }

    @Override
    public CordraObject getDesign() throws CordraException {
        return delegate.getDesign();
    }

    @Override
    public CordraObject getWithBypassHooks(String id) throws CordraException {
        return delegate.getWithBypassHooks(id);
    }

    @Override
    public InputStream getPayloadWithBypassHooks(String id, String payloadName, Long start, Long end) throws CordraException {
        return delegate.getPayloadWithBypassHooks(id, payloadName, start, end);
    }

    @Override
    public void getPartialPayload(String id, String payloadName, Long start, Long end, CallResponseHandler handler) throws CordraException {
        delegate.getPartialPayload(id, payloadName, start, end, handler);
    }

    @Override
    public InputStream getPayloadWithBypassHooks(String id, String payloadName) throws CordraException {
        return delegate.getPayloadWithBypassHooks(id, payloadName);
    }

    @Override
    public ExecutorService getCallResponseHandlerExecutorService() {
        return delegate.getCallResponseHandlerExecutorService();
    }

    @Override
    public void call(String objectId, String methodName, JsonElement params, CallResponseHandler handler) throws CordraException {
        delegate.call(objectId, methodName, params, handler);
    }

    @Override
    public CallResponse buildCallResponse(ThrowingCallResponseHandlerConsumer consumer) throws CordraException {
        return delegate.buildCallResponse(consumer);
    }

    @Override
    public void call(String objectId, String methodName, InputStream input, CallResponseHandler handler) throws CordraException {
        delegate.call(objectId, methodName, input, handler);
    }

    @Override
    public void callForType(String type, String methodName, JsonElement params, CallResponseHandler handler) throws CordraException {
        delegate.callForType(type, methodName, params, handler);
    }

    @Override
    public void callForType(String type, String methodName, InputStream input, CallResponseHandler handler) throws CordraException {
        delegate.callForType(type, methodName, input, handler);
    }

    @Override
    public String toString() {
        return delegate.toString();
    }

    public String getHandleForSuffix(String suffix) {
        return delegate.getHandleForSuffix(suffix);
    }

    public void throwIfLegacySessionsDisallowed() throws CordraException {
        delegate.throwIfLegacySessionsDisallowed();
    }

    public boolean isDisableBackOffRequestParking() {
        return delegate.isDisableBackOffRequestParking();
    }
}
