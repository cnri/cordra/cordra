package net.cnri.cordra.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UiConfig {
    public String title;
    public String relationshipsButtonText;
    public Boolean allowUserToSpecifySuffixOnCreate;
    public Boolean allowUserToSpecifyHandleOnCreate;
    public Boolean hideTypeInObjectEditor;
    public SearchResultConfig searchResults;
    public String initialQuery = "*:*";
    public String initialFragment;
    public String initialSortFields;
    public JsonArray navBarLinks;
    public Integer numTypesForCreateDropdown = null;
    public List<String> aclUiSearchTypes = null;
    public JsonObject customAuthentication;
    public Map<String, PageConfig > customPages;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PageConfig {
        public String objectId;
        public String payloadName;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class SearchResultConfig {
        public Boolean includeType;
        public Boolean includeModifiedDate;
        public Boolean includeCreatedDate;
    }
}
