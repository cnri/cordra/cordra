package net.cnri.cordra.operations;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.auth.AuthenticationResult;
import net.dona.doip.DoipConstants;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class HelloOperation implements StaticCompactSystemOperation {

    public static final String ID = DoipConstants.OP_HELLO;

    private final CordraService cordraService;

    public HelloOperation(CordraService cordraService) {
        this.cordraService = cordraService;
    }

    @Override
    public JsonElement perform(JsonElement input, JsonObject attrubutes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        JsonObject processorConfig = cordraService.getDoipProcessorConfig();
        JsonObject hello = buildDoipServiceInfo(processorConfig);
        return hello;
    }

    public static JsonObject buildDoipServiceInfo(JsonObject processorConfig) {
        String serviceId = processorConfig.get("serviceId").getAsString();
        String address = processorConfig.has("address") ? processorConfig.get("address").getAsString() : null;
        String serviceName = processorConfig.has("serviceName") ? processorConfig.get("serviceName").getAsString() : null;
        String serviceDescription = processorConfig.has("serviceDescription") ? processorConfig.get("serviceDescription").getAsString() : null;
        int port = processorConfig.has("port") ? processorConfig.get("port").getAsInt() : -1;
        PublicKey publicKey = processorConfig.has("publicKey") ? net.dona.doip.util.GsonUtility.getGson().fromJson(processorConfig.get("publicKey"), PublicKey.class) : null;
        JsonObject hello = buildDoipServiceInfo(serviceId, address, serviceName, serviceDescription, port, publicKey);
        return hello;
    }

    public static JsonObject buildDoipServiceInfo(String serviceId, String address, String serviceName, String serviceDescription, int port, PublicKey publicKey) {
        Gson gson = GsonUtility.getGson();
        JsonObject res = new JsonObject();
        res.addProperty("id", serviceId);
        res.addProperty("type", "0.TYPE/DOIPServiceInfo");
        JsonObject atts = new JsonObject();
        atts.addProperty("ipAddress", address);
        atts.addProperty("port", port);
        atts.addProperty("protocol", "TCP"); // DOIP specification: internet protocol used by DOIP; TCP is the default
        atts.addProperty("protocolVersion", "2.0"); // DOIP specification: this is the highest version of the DOIP protocol supported

        JsonObject cordraVersion = getCordraVersion();
        if (cordraVersion != null) {
            atts.add("cordraVersion", cordraVersion);
        }
        if (serviceName != null) {
            atts.addProperty("serviceName", serviceName);
        }
        if (serviceDescription != null) {
            atts.addProperty("serviceDescription", serviceDescription);
        }
        if (publicKey != null) {
            atts.add("publicKey", gson.toJsonTree(publicKey));
        }
        res.add("attributes", atts);
        return res;
    }

    private static JsonObject getCordraVersion() {
        JsonObject version = null;
        String cordraVersion = System.getProperty("cordra.version.number");
        if (cordraVersion != null) {
            String number = System.getProperty("cordra.version.number");
            String timestamp = System.getProperty("cordra.version.timestamp");
            String id = System.getProperty("cordra.version.id");
            version = new JsonObject();
            version.addProperty("number", number);
            version.addProperty("timestamp", timestamp);
            version.addProperty("id", id);
        }
        return version;
    }

    @Override
    public OperationTarget getTarget() {
        return OperationTarget.SERVICE;
    }

    @Override
    public boolean allowsGet() {
        return true;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("public");
        return permission;
    }
}