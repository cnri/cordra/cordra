package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.DesignPlusSchemas;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.auth.AuthenticationResult;

import java.util.ArrayList;
import java.util.List;

public class GetDesignOperation implements StaticCompactSystemOperation {

    public final static String ID = "20.DOIP/Op.GetDesign";

    private final CordraService cordraService;

    public GetDesignOperation(CordraService cordraService) {
        this.cordraService = cordraService;
    }

    @Override
    public JsonElement perform(JsonElement input, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        DesignPlusSchemas design = cordraService.getDesignAsUser(authResult);
        return GsonUtility.getGson().toJsonTree(design);
    }

    @Override
    public OperationTarget getTarget() {
        return OperationTarget.SERVICE;
    }

    @Override
    public boolean allowsGet() {
        return true;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("public");
        return permission;
    }
}
