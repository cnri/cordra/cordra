package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.indexer.CordraTransaction;
import net.cnri.cordra.sync.TransactionManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class HighestSafeTxnIdForSearchOperation implements StaticCompactSystemOperation {

    public final static String ID = "20.DOIP/Op.HighestSafeTxnIdForSearch";

    private final CordraService cordraService;

    public HighestSafeTxnIdForSearchOperation(CordraService cordraService) {
        this.cordraService = cordraService;
    }

    @Override
    public JsonElement perform(JsonElement input, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        List<Long> allInFlightTxnIds = getAllInFlightTxnIds();
        Long lowestInFlightTxnId = getLowest(allInFlightTxnIds);
        Long highestIndexedTxnId = getHighestIndexedTxnId();
        Long highestSafeTxnIdForSearch;
        if (lowestInFlightTxnId == null) {
            highestSafeTxnIdForSearch = highestIndexedTxnId;
        } else {
            highestSafeTxnIdForSearch = lowestInFlightTxnId -1;
        }
        JsonObject result = new JsonObject();
        result.addProperty("highestSafeTxnIdForSearch", highestSafeTxnIdForSearch);
        return result;
    }

    private List<Long> getAllInFlightTxnIds() throws CordraException {
        TransactionManager transactionManager = cordraService.getTransactionManager();
        List<String> serviceIds = transactionManager.getCordraServiceIdsWithOpenTransactions();
        List<Long> allTxnIds = new ArrayList<>();
        for (String serviceId : serviceIds) {
            Iterator<Map.Entry<Long, CordraTransaction>> txnsIter = transactionManager.iterateTransactions(serviceId);
            List<Long> serviceTxnIds = collectTxnIds(txnsIter);
            allTxnIds.addAll(serviceTxnIds);
        }
        return allTxnIds;
    }

    private Long getHighestIndexedTxnId() throws CordraException {
        String query = "*:*";
        SortField sortByTxnId = new SortField("metadata/txnId", true);
        List<SortField> sortFields = new ArrayList<>();
        sortFields.add(sortByTxnId);
        QueryParams params = new QueryParams(0, 1, sortFields);
        cordraService.ensureIndexUpToDate();
        try (SearchResults<CordraObject> results = cordraService.search(query, params, false, null)) {
            Iterator<CordraObject> iter = results.iterator();
            if (iter.hasNext()) {
                CordraObject co = iter.next();
                return co.metadata.txnId;
            } else {
                return 0L; //consider returning null?
            }
        }
    }

    private Long getLowest(List<Long> txnIds) {
        Long lowest = null;
        for (Long txnId : txnIds) {
            if (lowest == null) {
                lowest = txnId;
            } else if (txnId < lowest) {
                lowest = txnId;
            }
        }
        return lowest;
    }


    private List<Long> collectTxnIds(Iterator<Map.Entry<Long, CordraTransaction>> txnsIter) {
        List<Long> result = new ArrayList<>();
        while (txnsIter.hasNext()) {
            Map.Entry<Long, CordraTransaction> txnEntry = txnsIter.next();
            Long txnId = txnEntry.getKey();
            result.add(txnId);
        }
        return result;
    }

    @Override
    public OperationTarget getTarget() {
        return OperationTarget.SERVICE;
    }

    @Override
    public boolean allowsGet() {
        return true;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("public");
        return permission;
    }
}
