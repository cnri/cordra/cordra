package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.cnri.cordra.DirectIo;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.NotFoundCordraException;
import net.cnri.cordra.auth.AuthenticationResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SystemOperations {

    private Map<String, SystemOperation> serviceOperations;
    private Map<String, SystemOperation> objectOperations;

    public SystemOperations() {
        serviceOperations = new HashMap<>();
        objectOperations = new HashMap<>();
    }

    public void add(String id, SystemOperation operation) {
        if (operation.getTarget() == OperationTarget.OBJECT) {
            objectOperations.put(id, operation);
        } else {
            serviceOperations.put(id, operation);
        }
    }

    public List<String> listOperations(String type, boolean isStatic) {
        OperationTarget target = targetForType(type);
        if (OperationTarget.SERVICE == target) {
            return new ArrayList<>(serviceOperations.keySet());
        } else {
            List<String> result = new ArrayList<>();
            for (Map.Entry<String, SystemOperation> entry : objectOperations.entrySet()) {
                SystemOperation op = entry.getValue();
                if (op.isStatic() == isStatic) {
                    result.add(entry.getKey());
                }
            }
            return result;
        }
    }

    public boolean hasOperation(String operationId, String type, boolean isStatic) {
        return hasOperation(operationId, targetForType(type), isStatic);
    }

    public boolean hasOperation(String operationId, OperationTarget target, boolean isStatic) {
        SystemOperation operation = getOperation(operationId, target, isStatic);
        if (operation != null) {
            return true;
        } else {
            return false;
        }
    }

    private SystemOperation getOperation(String operationId, OperationTarget target, boolean isStatic) {
        if (target == OperationTarget.SERVICE) {
            return serviceOperations.get(operationId);
        } else {
            SystemOperation op = objectOperations.get(operationId);
            if (op == null) {
                return null;
            }
            if (op.isStatic() == isStatic) {
                return op;
            } else {
                return null;
            }
        }
    }

    public JsonElement performOperation(CordraObject co, String operationId, String type, boolean isStatic, Map<String, Object> context, DirectIo directIo, AuthenticationResult authResult) throws CordraException {
        SystemOperation operation = getOperation(operationId, targetForType(type), isStatic);
        if (operation == null) {
            throw new NotFoundCordraException("Service does not support operation " + operationId);
        }
        HooksContext hooksContext = HooksContext.from(context, directIo);
        if (operation instanceof StaticStreamingSystemOperation) {
            StaticStreamingSystemOperation streamingOperation = (StaticStreamingSystemOperation)operation;
            JsonObject attributes = getAttributesFromContext(context);
            return streamingOperation.perform(directIo, attributes, authResult, hooksContext);
        } else if (operation instanceof StaticCompactSystemOperation) {
            StaticCompactSystemOperation compactOperation = (StaticCompactSystemOperation)operation;
            JsonElement input = getParams(directIo);
            JsonObject attributes = getAttributesFromContext(context);
            return compactOperation.perform(input, attributes, authResult, hooksContext);
        } else if (operation instanceof InstanceStreamingSystemOperation) {
            InstanceStreamingSystemOperation streamingOperation = (InstanceStreamingSystemOperation)operation;
            JsonObject attributes = getAttributesFromContext(context);
            return streamingOperation.perform(co, directIo, attributes, authResult, hooksContext);
        } else if (operation instanceof InstanceCompactSystemOperation) {
            InstanceCompactSystemOperation compactOperation = (InstanceCompactSystemOperation)operation;
            JsonElement input = getParams(directIo);
            JsonObject attributes = getAttributesFromContext(context);
            return compactOperation.perform(co, input, attributes, authResult, hooksContext);
        }
        return null;
    }

    private JsonObject getAttributesFromContext(Map<String, Object> context) {
        Object contextAttributes = context.get("attributes");
        if (contextAttributes != null) {
            return (JsonObject)contextAttributes;
        } else {
            return new JsonObject();
        }
    }

    private JsonElement getParams(DirectIo directIo) throws InternalErrorCordraException {
        try {
            String inputString = directIo.getInputAsString();
            if (inputString == null) {
                return null;
            }
            JsonElement params = JsonParser.parseString(inputString);
            return params;
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public boolean operationAllowsGet(String operationId) throws NotFoundCordraException {
        SystemOperation operation = serviceOperations.get(operationId);
        if (operation == null) {
            throw new NotFoundCordraException("Service does not support operation with id " + operationId);
        }
        return operation.allowsGet();
    }

    public List<String> getPermissionsFor(String operationId, String type, boolean isStatic) {
        return getPermissionsFor(operationId, targetForType(type), isStatic);
    }

    public List<String> getPermissionsFor(String operationId, OperationTarget target, boolean isStatic) {
        SystemOperation operation = getOperation(operationId, target, isStatic);
        if (operation != null) {
            return operation.permission();
        } else {
            return null;
        }
    }

    private OperationTarget targetForType(String type) {
        return (type == null) ? OperationTarget.SERVICE : OperationTarget.OBJECT;
    }
}
