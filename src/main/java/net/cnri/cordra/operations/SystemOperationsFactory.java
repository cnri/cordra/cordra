package net.cnri.cordra.operations;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.operations.versions.GetVersionsOperation;
import net.cnri.cordra.operations.versions.PublishVersionOperation;

public class SystemOperationsFactory {

    public static synchronized SystemOperations build(CordraService cordraService) {
        SystemOperations systemOperations = new SystemOperations();
        systemOperations.add(PublishVersionOperation.ID, new PublishVersionOperation(cordraService));
        systemOperations.add(GetVersionsOperation.ID, new GetVersionsOperation(cordraService));
        systemOperations.add(GetRelationshipsOperation.ID, new GetRelationshipsOperation(cordraService));
        systemOperations.add(ReindexBatchOperation.ID, new ReindexBatchOperation(cordraService));
        systemOperations.add(GetInitDataOperation.ID, new GetInitDataOperation(cordraService));
        systemOperations.add(GetDesignOperation.ID, new GetDesignOperation(cordraService));
        SystemOperation helloOperation = new HelloOperation(cordraService);
        systemOperations.add(HelloOperation.ID, helloOperation);
        systemOperations.add(BatchUploadOperation.ID, new BatchUploadOperation(InternalCordraClientFactory.get()));
        systemOperations.add(HighestSafeTxnIdForSearchOperation.ID, new HighestSafeTxnIdForSearchOperation(cordraService));
        systemOperations.add(ListOperationsOperation.ID, new ListOperationsOperation(cordraService));
        systemOperations.add(StaticListOperationsOperation.ID, new StaticListOperationsOperation(cordraService));
        return systemOperations;
    }
}
