package net.cnri.cordra.operations;

public enum OperationTarget {
    SERVICE,
    OBJECT
}
