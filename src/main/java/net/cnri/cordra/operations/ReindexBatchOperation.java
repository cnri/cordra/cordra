package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.*;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.QueryParams;
import net.cnri.cordra.api.SearchResults;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.util.JsonUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReindexBatchOperation implements StaticCompactSystemOperation {

    public static final String ID = "20.DOIP/Op.ReindexBatch";

    private final CordraService cordraService;

    public ReindexBatchOperation(CordraService cordraService) {
        this.cordraService = cordraService;
    }

    @Override
    public JsonElement perform(JsonElement input, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        boolean lockObjects = JsonUtil.getBooleanProperty(attributes, "lockObjects", true);
        String query = JsonUtil.getAsStringOrNull(attributes, "query");
        boolean all = JsonUtil.getBooleanProperty(attributes, "all", false);
        if (all) {
            reindexAll(lockObjects, authResult);
        } else if (query != null) {
            reindexQueryResults(query, lockObjects, authResult);
        } else {
            List<String> batch = GsonUtility.getGson().fromJson(input, new TypeToken<List<String>>() {}.getType());
            cordraService.reindexBatchIds(batch, lockObjects, authResult);
        }
        JsonObject result = new JsonObject();
        result.addProperty("msg", "success");
        return result;
    }

    private void reindexAll(boolean lockObjects, AuthenticationResult authResult) throws CordraException {
        int batchSize = cordraService.getCordraConfig().reindexing.batchSize;
        try (SearchResults<String> ids = cordraService.getStorage().listHandles()) {
            Iterator<String> iter = ids.iterator();
            while (true) {
                List<String> batch = takeBatch(iter, batchSize);
                if (batch.isEmpty()) break;
                cordraService.reindexBatchIds(batch, lockObjects, authResult);
                if (batch.size() < batchSize) break;
            }
        }
    }

    private void reindexQueryResults(String query, boolean lockObjects, AuthenticationResult authResult) throws CordraException {
        int batchSize = cordraService.getCordraConfig().reindexing.batchSize;
        cordraService.ensureIndexUpToDate();
        boolean excludeVersions = false;
        boolean isPostProcess = false;
        QueryParams params = new QueryParams();
        try (SearchResults<String> ids = cordraService.searchHandlesWithQueryCustomizationAndRestriction(query, params,
                isPostProcess, authResult, excludeVersions)) {
            Iterator<String> iter = ids.iterator();
            while (true) {
                List<String> batch = takeBatch(iter, batchSize);
                if (batch.isEmpty()) break;
                cordraService.reindexBatchIds(batch, lockObjects, authResult);
                if (batch.size() < batchSize) break;
            }
        }
    }

    private List<String> takeBatch(Iterator<String> ids, int batchSize) {
        List<String> batch = new ArrayList<>();
        for (int i = 0; i < batchSize; i++) {
            if (ids.hasNext()) {
                batch.add(ids.next());
            } else {
                break;
            }
        }
        return batch;
    }

    @Override
    public OperationTarget getTarget() {
        return OperationTarget.SERVICE;
    }

    @Override
    public boolean allowsGet() {
        return false;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("admin");
        return permission;
    }
}
