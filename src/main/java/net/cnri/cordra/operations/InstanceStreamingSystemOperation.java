package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.DirectIo;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.auth.AuthenticationResult;

public interface InstanceStreamingSystemOperation extends SystemOperation {

    JsonElement perform(CordraObject co, DirectIo directIo, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException;

    @Override
    default OperationTarget getTarget() {
        return OperationTarget.OBJECT;
    }

    @Override
    default boolean isStatic() {
        return false;
    }
}
