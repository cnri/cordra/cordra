package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.util.JsonUtil;
import net.dona.doip.DoipConstants;
import java.util.ArrayList;
import java.util.List;

public class ListOperationsOperation implements InstanceCompactSystemOperation {

    public static final String ID = DoipConstants.OP_LIST_OPERATIONS;

    private final CordraService cordraService;

    public ListOperationsOperation(CordraService cordraService) {
        this.cordraService = cordraService;
    }

    @Override
    public JsonElement perform(CordraObject co, JsonElement input, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        String objectId = co.id;
        String type = JsonUtil.getAsStringOrNull(attributes, "type");
        boolean includeCrud = JsonUtil.getBooleanProperty(attributes, "includeCrud", false);
        boolean isStatic = JsonUtil.getBooleanProperty(attributes, "static", false);
        List<String> result = cordraService.listMethodsForUser(authResult, type, objectId, isStatic, includeCrud);
        return GsonUtility.getGson().toJsonTree(result);
    }

    @Override
    public OperationTarget getTarget() {
        return OperationTarget.OBJECT;
    }

    @Override
    public boolean isStatic() {
        return false;
    }

    @Override
    public boolean allowsGet() {
        return true;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("public");
        return permission;
    }
}
