package net.cnri.cordra.operations.versions;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.VersionInfo;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.operations.InstanceCompactSystemOperation;
import java.util.ArrayList;
import java.util.List;

public class GetVersionsOperation implements InstanceCompactSystemOperation {

    public static final String ID = "20.DOIP/Op.Versions.Get";

    private final CordraService cordraService;

    public GetVersionsOperation(CordraService cordraService) {
        this.cordraService = cordraService;
    }

    @Override
    public JsonElement perform(CordraObject co, JsonElement input, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        List<CordraObject> versions = cordraService.getVersionsFor(co.id, authResult);
        List<VersionInfo> versionInfos = getVersionInfoListFor(versions);
        return GsonUtility.getGson().toJsonTree(versionInfos);
    }

    public static List<VersionInfo> getVersionInfoListFor(List<CordraObject> versions) {
        List<VersionInfo> result = new ArrayList<>();
        for (CordraObject version : versions) {
            VersionInfo versionInfo = getVersionInfoFor(version);
            result.add(versionInfo);
        }
        return result;
    }

    public static VersionInfo getVersionInfoFor(CordraObject co) {
        VersionInfo versionInfo = new VersionInfo();
        versionInfo.id = co.id;
        versionInfo.versionOf = co.metadata.versionOf;
        versionInfo.type = co.type;
        if (versionInfo.versionOf == null) {
            versionInfo.isTip = true;
            versionInfo.modifiedOn = co.metadata.modifiedOn;
        } else {
            versionInfo.publishedBy = co.metadata.publishedBy;
            versionInfo.publishedOn = co.metadata.publishedOn;
        }
        return versionInfo;
    }

    @Override
    public boolean allowsGet() {
        return true;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("readers");
        return permission;
    }
}
