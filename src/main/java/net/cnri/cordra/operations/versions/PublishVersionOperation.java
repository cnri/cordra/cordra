package net.cnri.cordra.operations.versions;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.*;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.VersionInfo;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.operations.InstanceCompactSystemOperation;
import net.cnri.cordra.util.JsonUtil;
import java.util.ArrayList;
import java.util.List;

public class PublishVersionOperation implements InstanceCompactSystemOperation {

    public static final String ID = "20.DOIP/Op.Versions.Publish";

    private final CordraService cordraService;

    public PublishVersionOperation(CordraService cordraService) {
        this.cordraService = cordraService;
    }

    @Override
    public JsonElement perform(CordraObject co, JsonElement input, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        String objectId = co.id;
        String versionId = JsonUtil.getAsStringOrNull(attributes, "versionId");
        boolean clonePayloads = JsonUtil.getBooleanProperty(attributes, "clonePayloads", true);
        try {
            CordraObject versionObject = cordraService.publishVersion(objectId, versionId, clonePayloads, authResult);
            VersionInfo versionInfo = GetVersionsOperation.getVersionInfoFor(versionObject);
            JsonElement result = GsonUtility.getGson().toJsonTree(versionInfo);
            return result;
        } catch (VersionException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        }
    }

    @Override
    public boolean allowsGet() {
        return false;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("creator");
        return permission;
    }
}
