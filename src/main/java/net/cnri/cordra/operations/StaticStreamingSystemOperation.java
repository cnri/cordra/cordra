package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.DirectIo;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.auth.AuthenticationResult;

public interface StaticStreamingSystemOperation extends SystemOperation {

    JsonElement perform(DirectIo directIo, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException;

    @Override
    default boolean isStatic() {
        return true;
    }
}
