package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.InvalidException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.relationships.Relationships;
import net.cnri.cordra.relationships.RelationshipsService;
import net.cnri.cordra.util.JsonUtil;
import java.util.ArrayList;
import java.util.List;

public class GetRelationshipsOperation implements InstanceCompactSystemOperation {

    public static final String ID = "20.DOIP/Op.Relationships.Get";

    private final RelationshipsService relationshipsService;

    public GetRelationshipsOperation(CordraService cordraService) {
        this.relationshipsService = new RelationshipsService(cordraService);
    }

    @Override
    public JsonElement perform(CordraObject co, JsonElement input, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        boolean outboundOnly = JsonUtil.getBooleanProperty(attributes, "outboundOnly", false);
        RelationshipsService.ObjectSerialization serialization = RelationshipsService.ObjectSerialization.DIGITAL_OBJECT;
        if (context.isCordraCallApi) {
            serialization = RelationshipsService.ObjectSerialization.CORDRA_OBJECT;
        }
        try {
            Relationships relationships = relationshipsService.getRelationshipsFor(co, outboundOnly, authResult, serialization);
            JsonElement result = GsonUtility.getGson().toJsonTree(relationships);
            return result;
        } catch (InvalidException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public boolean allowsGet() {
        return true;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("readers");
        return permission;
    }
}
