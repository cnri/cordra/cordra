package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.InitDataResponse;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.model.Version;
import java.util.ArrayList;
import java.util.List;

public class GetInitDataOperation implements StaticCompactSystemOperation {

    public static final String ID = "20.DOIP/Op.GetInitData";

    private final CordraService cordraService;

    public GetInitDataOperation(CordraService cordraService) {
        this.cordraService = cordraService;
    }

    @Override
    public JsonElement perform(JsonElement input, JsonObject attrubutes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        InitDataResponse initDataResponse = new InitDataResponse();
        initDataResponse.version = Version.getInstance();
        initDataResponse.isActiveSession = authResult.active;
        initDataResponse.username = authResult.username;
        initDataResponse.userId = authResult.userId;
        initDataResponse.typesPermittedToCreate = cordraService.getTypesPermittedToCreate(authResult);
        initDataResponse.design = cordraService.getDesignAsUser(authResult);
        return GsonUtility.getGson().toJsonTree(initDataResponse);
    }

    @Override
    public OperationTarget getTarget() {
        return OperationTarget.SERVICE;
    }

    @Override
    public boolean allowsGet() {
        return true;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("public");
        return permission;
    }
}