package net.cnri.cordra.operations;

import java.util.List;

public interface SystemOperation {

    OperationTarget getTarget();

    boolean allowsGet();

    List<String> permission();

    boolean isStatic();
}
