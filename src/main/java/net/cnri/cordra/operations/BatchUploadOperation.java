package net.cnri.cordra.operations;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.DirectIo;
import net.cnri.cordra.HooksContext;
import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.PreAuthenticatedOptions;
import net.cnri.cordra.util.JsonUtil;
import net.cnri.cordra.web.batch.BatchUpload;
import net.cnri.cordra.web.batch.BatchUploadProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class BatchUploadOperation implements StaticStreamingSystemOperation {

    private static Logger logger = LoggerFactory.getLogger(BatchUploadOperation.class);

    public static final String ID = "20.DOIP/Op.BatchUpload";

    private final InternalCordraClient internalCordra;

    public BatchUploadOperation(InternalCordraClient internalCordra) {
        this.internalCordra = internalCordra;
    }

    @Override
    public JsonElement perform(DirectIo directIo, JsonObject attributes, AuthenticationResult authResult, HooksContext context) throws CordraException {
        String formatParam = JsonUtil.getAsStringOrNull(attributes, "format");
        boolean isFailFast = JsonUtil.getBooleanProperty(attributes, "failFast", false);
        boolean isParallel = JsonUtil.getBooleanProperty(attributes, "parallel", true);
        BatchUpload.Format format = BatchUpload.Format.JSON_ARRAY_OR_SEARCH_RESULTS;
        if ("ndjson".equals(formatParam)) {
            format = BatchUpload.Format.NEWLINE_SEPARATED_JSON;
        }
        BatchUpload.ObjectSerialization serialization = BatchUpload.ObjectSerialization.DIGITAL_OBJECT;
        if (context.isCordraCallApi) {
            serialization = BatchUpload.ObjectSerialization.CORDRA_OBJECT;
        }
        Options options;
        if (authResult.active == true) options = PreAuthenticatedOptions.build(authResult);
        else options = new Options().setUseDefaultCredentials(false);
        try {
            BufferedReader reader = new BufferedReader(directIo.getInputAsReader());
            Writer writer = directIo.getOutputAsWriter();
            directIo.setOutputMediaType("application/json");
            BatchUpload batch = new BatchUpload(reader, writer, internalCordra, options, serialization, format, isFailFast, isParallel);
            BatchUploadProcessor.instance().process(batch);
        } catch (Exception e) {
            logger.error("Error loading objects", e);
            throw new InternalErrorCordraException(e);
        }
        return null;
    }

    @Override
    public OperationTarget getTarget() {
        return OperationTarget.SERVICE;
    }

    @Override
    public boolean allowsGet() {
        return false;
    }

    @Override
    public List<String> permission() {
        List<String> permission = new ArrayList<>();
        permission.add("authenticated");
        return permission;
    }
}
