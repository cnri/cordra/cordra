package net.cnri.cordra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InternalCordraClientFactory {
    private static Logger logger = LoggerFactory.getLogger(InternalCordraClientFactory.class);

    private static InternalCordraClient internalCordraClient;

    public static synchronized InternalCordraClient get() {
        if (internalCordraClient == null) {
            internalCordraClient = new InternalCordraClient(CordraServiceFactory.getCordraService());
        }
        return internalCordraClient;
    }

    public static synchronized void reset() {
        if (internalCordraClient != null) {
            try { internalCordraClient.close(); } catch (Exception e) { logger.warn("Error closing InternalCordraClient", e); }
        }
        internalCordraClient = null;
    }
}
