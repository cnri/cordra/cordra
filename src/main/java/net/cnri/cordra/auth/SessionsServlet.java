package net.cnri.cordra.auth;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.api.AuthResponse;
import net.cnri.cordra.api.AuthTokenResponse;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.api.UnauthorizedCordraException;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@WebServlet("/sessions/*")
public class SessionsServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(SessionsServlet.class);

    private static Gson gson = GsonUtility.getGson();

    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            internalCordra.throwIfLegacySessionsDisallowed();
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            boolean full = ServletUtil.getBooleanParameter(req, "full");
            options.full = full;
            AuthResponse authResponse = internalCordra.authenticateAndGetResponse(options);
            LegacyAuthResponse sessionResponse = new LegacyAuthResponse(authResponse);
            String responseJson = gson.toJson(sessionResponse);
            PrintWriter w = resp.getWriter();
            w.write(responseJson);
            w.close();
        } catch (InternalErrorCordraException e) {
            logger.error("Exception in GET /sessions", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Exception in GET /sessions", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            internalCordra.throwIfLegacySessionsDisallowed();
            ServletAuthUtil.throwIfDisallowedAuthenticatingOverHttp(req);
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            Function<Map<String, Object>, String> sessionCreator = atts -> {
                // the session will already be created during authentication
                HttpSession session = req.getSession(false);
                if (session == null) return null;
                return session.getId();
            };
            AuthTokenResponse authResponse;
            try {
                authResponse = internalCordra.getAuthToken(options, sessionCreator);
            } catch (BadRequestCordraException e) {
                if (isInvalidGrant(e)) {
                    throw new UnauthorizedCordraException(e.getMessage());
                } else {
                    throw e;
                }
            }
            String json = gson.toJson(new LegacyAuthResponse(authResponse));
            resp.getWriter().println(json);
        } catch (InternalErrorCordraException e) {
            logger.error("Exception in POST /sessions", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Exception in POST /sessions", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private boolean isInvalidGrant(BadRequestCordraException e) {
        if (e.getResponse() == null) return false;
        if (!e.getResponse().isJsonObject()) return false;
        if (!e.getResponse().getAsJsonObject().has("error")) return false;
        JsonElement errorElement = e.getResponse().getAsJsonObject().get("error");
        if (!errorElement.isJsonPrimitive()) return false;
        if (!errorElement.getAsJsonPrimitive().isString()) return false;
        return "invalid_grant".equals(errorElement.getAsString());
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            internalCordra.throwIfLegacySessionsDisallowed();
            ServletAuthUtil.throwIfDisallowedAuthenticatingOverHttp(req);
            String path = req.getPathInfo();
            if (path == null || path.isEmpty()) {
                resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
                ServletErrorUtil.forbidden(resp, "Forbidden");
                return;
            }

            path = path.substring(1);
            if (!"this".equals(path)) {
                resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
                ServletErrorUtil.forbidden(resp, "Forbidden");
                return;
            }
            HttpSession session = req.getSession(false);
            if (session != null) {
                session.invalidate();
            }
            PrintWriter w = resp.getWriter();
            AuthResponse authResponse = internalCordra.introspectToken(new Options());
            String json = gson.toJson(new LegacyAuthResponse(authResponse));
            w.println(json);
            w.close();
        } catch (InternalErrorCordraException e) {
            logger.error("Exception in DELETE /sessions", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Exception in DELETE /sessions", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    @SuppressWarnings("unused")
    private static class LegacyAuthResponse {
        boolean isActiveSession = false;
        String userId;
        String username;
        List<String> typesPermittedToCreate;
        List<String> groupIds;

        public LegacyAuthResponse(AuthResponse authResponse) {
            this.isActiveSession = authResponse.active;
            this.userId = authResponse.userId;
            this.username = authResponse.username;
            this.typesPermittedToCreate = authResponse.typesPermittedToCreate;
            this.groupIds = authResponse.groupIds;
        }
    }
}
