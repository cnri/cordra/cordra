package net.cnri.cordra.auth;

import net.cnri.cordra.api.CordraException;

public abstract class SessionOptions extends InternalRequestOptions {
    // We need to evaluate the session using the default logic after the authenticate hook runs
    public abstract PreAuthenticatedOptions evaluate() throws CordraException;
}
