package net.cnri.cordra.auth;

import java.util.List;

import com.google.gson.JsonElement;

public class PreAuthenticatedOptions extends InternalRequestOptions {
    public Boolean grantAuthenticatedAccess;
    public List<String> hookSpecifiedGroupIds;
    public boolean bypassCordraGroupObjects;
    public Long exp;
    public JsonElement authContext;
    public boolean isSession;

    public static PreAuthenticatedOptions build(AuthenticationResult authResult) {
        PreAuthenticatedOptions options = new PreAuthenticatedOptions();
        options.username = authResult.username;
        options.userId = authResult.userId;
        options.grantAuthenticatedAccess = authResult.grantAuthenticatedAccess;
        // this is setting up the PreAuthenticatedOptions so that it will use the groupIds as-is each time
        options.hookSpecifiedGroupIds = authResult.groupIds;
        options.bypassCordraGroupObjects = true;
        options.exp = authResult.exp;
        options.authContext = authResult.authContext;
        return options;
    }
}
