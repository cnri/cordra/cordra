package net.cnri.cordra.auth;

import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.Options;

public class InternalRequestOptions extends Options {
    public boolean isGet;
    public boolean isCordraCallApi = false;
    public boolean isChangePassword;
    public AuthenticationResult cachedResult;
    public CordraException cachedException;
    public boolean alreadyBackedOff;
}
