package net.cnri.cordra.auth;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.api.AuthResponse;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;
import net.cnri.servletcontainer.sessions.HttpSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.function.Function;

@WebServlet("/auth/introspect/*")
public class AuthIntrospectServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(AuthIntrospectServlet.class);

    private HttpSessionManager sessionManager;
    private InternalCordraClient internalCordra;

    private static Gson gson = GsonUtility.getGson();

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            sessionManager = (HttpSessionManager) getServletContext().getAttribute(HttpSessionManager.class.getName());
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            AuthTokenServlet.throwForAuthIfDisallowedAuthenticatingOverHttp(req);
            JsonObjectAndTokenRequest jsonObjectAndTokenRequest = AuthTokenServlet.getTokenRequest(req);
            TokenRequest tokenRequest = jsonObjectAndTokenRequest == null ? null : jsonObjectAndTokenRequest.tokenRequest;
            if (tokenRequest == null || tokenRequest.token == null) {
                error(resp, "invalid_request", "Invalid request");
                return;
            }
            Function<String, Function<String, Object>> sessionGetter = tokenParam -> {
                HttpSession session = sessionManager.getSession(req, tokenRequest.token, false);
                if (session == null) return null;
                return session::getAttribute;
            };
            boolean full = ServletUtil.getBooleanParameter(req, "full");
            Options options = new Options();
            options.token = tokenRequest.token;
            options.full = full;
            AuthResponse successResponse = internalCordra.introspectToken(options, sessionGetter);
            success(resp, successResponse);
        } catch (InternalErrorCordraException e) {
            logger.error("Exception in POST /auth/introspect", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Exception in POST /auth/introspect", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private void success(HttpServletResponse resp, AuthResponse response) throws IOException {
        resp.setStatus(HttpServletResponse.SC_OK);
        respondAsJson(response, resp);
    }

    private void error(HttpServletResponse resp, String error, String message) throws IOException {
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        ErrorResponse response = new ErrorResponse();
        response.error = error;
        response.error_description = message;
        response.message = message;
        respondAsJson(response, resp);
    }

    protected void respondAsJson(Object o, HttpServletResponse resp) throws IOException {
        try {
            gson.toJson(o, resp.getWriter());
        } catch (JsonIOException e) {
            throw new IOException("Unable to write JSON", e);
        }
        resp.getWriter().println();
    }

    @SuppressWarnings("unused")
    private static class ErrorResponse {
        public String error;
        public String error_description;
        public String message;
    }
}
