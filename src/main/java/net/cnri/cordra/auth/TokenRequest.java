package net.cnri.cordra.auth;

public class TokenRequest {
    public String grant_type;
    public String assertion;
    public String username;
    public String password;
    public String token;
}
