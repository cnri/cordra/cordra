package net.cnri.cordra.web;

import java.io.IOException;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;

public class PushbackServletInputStream extends ServletInputStream {

    private final ServletInputStream in;
    private int unreadByte = -1;
    private boolean closed;

    public PushbackServletInputStream(ServletInputStream in) {
        this.in = in;
    }

    public void unread(int b) throws IOException {
        if (closed) {
            throw new IOException("Stream closed");
        }
        if (unreadByte >= 0) {
            throw new IOException("Push back buffer is full");
        }
        unreadByte = b;
    }

    @Override
    public int read() throws IOException {
        if (unreadByte >= 0) {
            int res = unreadByte;
            unreadByte = -1;
            return res;
        }
        return in.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        return this.read(b, 0, b.length);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }
        boolean usedUnreadByte = false;
        if (unreadByte >= 0) {
            b[off] = (byte)unreadByte;
            unreadByte = -1;
            usedUnreadByte = true;
            off++;
            len--;
            if (len == 0) return 1;
        }
        int r = in.read(b, off, len);
        if (r < 0) {
            if (usedUnreadByte) return 1;
            return r;
        }
        if (usedUnreadByte) r++;
        return r;
    }

    @Override
    public int readLine(byte[] b, int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }
        boolean usedUnreadByte = false;
        if (unreadByte >= 0) {
            boolean isNewline = unreadByte == '\n';
            b[off] = (byte)unreadByte;
            unreadByte = -1;
            usedUnreadByte = true;
            off++;
            len--;
            if (len == 0) return 1;
            if (isNewline) return 1;
        }
        int r = in.readLine(b, off, len);
        if (r < 0) {
            if (usedUnreadByte) return 1;
            return r;
        }
        if (usedUnreadByte) r++;
        return r;
    }

    @Override
    public long skip(long n) throws IOException {
        if (n > 0 && unreadByte >= 0) {
            unreadByte = -1;
            return 1 + in.skip(n-1);
        }
        return in.skip(n);
    }

    @Override
    public boolean isFinished() {
        if (unreadByte >= 0) return false;
        return in.isFinished();
    }

    @Override
    public boolean isReady() {
        if (unreadByte >= 0) return true;
        return in.isReady();
    }

    @Override
    public void setReadListener(ReadListener readListener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int available() throws IOException {
        int inAvail = in.available();
        if (unreadByte >= 0 && inAvail < Integer.MAX_VALUE) {
            return 1 + inAvail;
        }
        return inAvail;
    }

    @Override
    public void close() throws IOException {
        closed = true;
        in.close();
    }
}
