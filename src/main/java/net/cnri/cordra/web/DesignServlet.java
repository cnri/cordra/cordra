package net.cnri.cordra.web;

import com.google.gson.JsonElement;
import net.cnri.cordra.*;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.cnri.cordra.api.Options;
import net.cnri.cordra.auth.ServletAuthUtil;

import net.cnri.cordra.operations.GetDesignOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/design/*")
public class DesignServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(DesignServlet.class);

    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    ///objects/design
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            JsonElement params = null;
            JsonElement resultJson = internalCordra.callForType("CordraDesign", GetDesignOperation.ID, params, options);
            resp.getWriter().write(resultJson.toString());
//            InitDataResponse initDataResponse = internalCordra.getInitData(options);
//            DesignPlusSchemas design = initDataResponse.design;
//            GsonUtility.getPrettyGson().toJson(design, resp.getWriter());
        } catch (InternalErrorCordraException e) {
            logger.error("Exception in GET /design", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Exception in GET /design", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }
}
