package net.cnri.cordra.web;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import net.cnri.cordra.InvalidException;
import net.cnri.util.StringUtils;
import net.handle.hdllib.Util;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class ServletUtil {

    static void setNoCaching(ServletResponse servletResponse) {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0);
    }

    public static boolean getBooleanParameter(HttpServletRequest req, String param) {
        String value = req.getParameter(param);
        if (value == null) return false;
        if (value.isEmpty()) return true;
        return Boolean.parseBoolean(value);
    }

    public static boolean getBooleanParameter(HttpServletRequest req, String param, boolean defaultValue) {
        String value = req.getParameter(param);
        if (value == null) return defaultValue;
        if (value.isEmpty()) return true;
        return Boolean.parseBoolean(value);
    }

    public static String getPath(HttpServletRequest servletReq) {
        String pathInfo = net.cnri.util.ServletUtil.pathExcluding(servletReq.getRequestURI(), servletReq.getContextPath() + servletReq.getServletPath());
        pathInfo = StringUtils.decodeURLIgnorePlus(pathInfo);
        return pathInfo;
    }

    public static boolean getBooleanAttribute(HttpSession session, String att) {
        Boolean attValue = (Boolean) session.getAttribute(att);
        if (attValue == null) return false;
        return attValue.booleanValue();
    }

    public static boolean getBooleanAttribute(HttpServletRequest req, String att) {
        Boolean attValue = (Boolean) req.getAttribute(att);
        if (attValue == null) return false;
        return attValue.booleanValue();
    }

    public static String streamToString(InputStream input, String encoding) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte buf[] = new byte[4096];
        int r;
        while ((r = input.read(buf)) >= 0) {
            bout.write(buf, 0, r);
        }
        if (encoding == null) {
            return Util.decodeString(bout.toByteArray());
        }
        else {
            return new String(bout.toByteArray(), encoding);
        }
    }

    private static final String APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    private static final int APPLICATION_X_WWW_FORM_URLENCODED_LENGTH = APPLICATION_X_WWW_FORM_URLENCODED.length();

    public static boolean isForm(HttpServletRequest req) {
        String contentType = req.getContentType();
        if (contentType == null) return false;
        contentType = contentType.toLowerCase(Locale.ENGLISH);
        if (contentType.startsWith(APPLICATION_X_WWW_FORM_URLENCODED)) {
            if (contentType.length() == APPLICATION_X_WWW_FORM_URLENCODED_LENGTH) return true;
            char ch = contentType.charAt(APPLICATION_X_WWW_FORM_URLENCODED_LENGTH);
            if (ch == ';' || ch == ' ' || ch == '\t') return true;
        }
        return false;
    }

    public static JsonElement getContentAsJsonElement(String jsonData) throws InvalidException {
        try {
            return JsonParser.parseString(jsonData);
        } catch (JsonParseException e) {
            throw new InvalidException("Unable to parse content", e);
        }
    }

    public static boolean isMultipart(HttpServletRequest req) {
        String contentType = req.getContentType();
        if (contentType == null) return false;
        contentType = contentType.toLowerCase(Locale.ENGLISH);
        return contentType.startsWith("multipart/form-data");
    }
}
