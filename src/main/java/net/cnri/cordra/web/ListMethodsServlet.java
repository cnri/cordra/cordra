package net.cnri.cordra.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.cnri.cordra.*;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;

import net.cnri.cordra.api.Options;
import net.cnri.cordra.auth.ServletAuthUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

@WebServlet("/listMethods/*")
public class ListMethodsServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(DesignServlet.class);

    private InternalCordraClient internalCordra;

        @Override
        public void init() throws ServletException {
            super.init();
            try {
                internalCordra = InternalCordraClientFactory.get();
            } catch (Exception e) {
                throw new ServletException(e);
            }
        }

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            try {
                String objectId = req.getParameter("objectId");
                String type = req.getParameter("type");
                Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
                boolean includeCrud = ServletUtil.getBooleanParameter(req, "includeCrud", false);
                if (includeCrud) options.setIncludeCrud(true);
                boolean isStatic = false;
                if (objectId == null) isStatic = ServletUtil.getBooleanParameter(req, "static");
                List<String> result;
                if (type == null && objectId == null) {
                    throw new BadRequestCordraException("Request must include 'type' or 'objectId'");
                } else if (type != null) {
                    result = internalCordra.listMethodsForType(type, isStatic, options);
                } else {
                    result = internalCordra.listMethods(objectId, options);
                }
                Gson gson = GsonUtility.getGson();
                String resultJson = gson.toJson(result);
                resp.getWriter().write(resultJson);
            } catch (InternalErrorCordraException e) {
                logger.error("Unexpected error calling listMethods", e);
                ServletErrorUtil.internalServerError(resp);
            } catch (CordraException e) {
                ServletErrorUtil.writeCordraException(resp, e);
            } catch (Exception e) {
                logger.error("Unexpected error calling listMethods", e);
                ServletErrorUtil.internalServerError(resp);
            }
        }
    }