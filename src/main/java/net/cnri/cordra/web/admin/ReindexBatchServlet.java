package net.cnri.cordra.web.admin;

import com.google.gson.*;
import net.cnri.cordra.*;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.operations.ReindexBatchOperation;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;
import net.cnri.util.StreamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet({ "/reindexBatch", "/reindexBatch/" })
public class ReindexBatchServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(ReindexBatchServlet.class);
    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        boolean lockObjects = ServletUtil.getBooleanParameter(req, "lockObjects", true);
        boolean all = ServletUtil.getBooleanParameter(req, "all", false);
        String query = req.getParameter("query");
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            options.reindexBatchLockObjects = lockObjects;
            JsonObject attributes = new JsonObject();
            attributes.addProperty("lockObjects", lockObjects);
            JsonArray batch = null;
            if (all) {
                attributes.addProperty("all", true);
            } else if (query != null) {
                attributes.addProperty("query", query);
            } else {
                String json = StreamUtil.readFully(req.getReader());
                batch = JsonParser.parseString(json).getAsJsonArray();
            }
            options.attributes = attributes;

            JsonElement resultJson = internalCordra.callForType("CordraDesign", ReindexBatchOperation.ID, batch, options);
            PrintWriter w = resp.getWriter();
            w.write(resultJson.toString());
            w.close();
        } catch (CordraException e) {
            ServletErrorUtil.internalServerError(resp);
            logger.error("Error reindexing batch", e);
        }
    }
}
