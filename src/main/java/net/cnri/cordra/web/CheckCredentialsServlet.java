package net.cnri.cordra.web;

import com.google.gson.Gson;
import net.cnri.cordra.*;
import net.cnri.cordra.api.AuthResponse;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.api.UnauthorizedCordraException;
import net.cnri.cordra.auth.ServletAuthUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/check-credentials/*")
public class CheckCredentialsServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(CheckCredentialsServlet.class);

    private InternalCordraClient internalCordra;
    private Gson gson;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            gson = GsonUtility.getGson();
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            AuthResponse sessionResponse = internalCordra.authenticateAndGetResponse(options);
            String responseJson = gson.toJson(sessionResponse);
            PrintWriter w = resp.getWriter();
            w.write(responseJson);
            w.close();
        } catch (InternalErrorCordraException e) {
            logger.error("Exception in GET /check-credentials", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (UnauthorizedCordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (CordraException e) {
            logger.error("Exception in GET /check-credentials", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }
}
