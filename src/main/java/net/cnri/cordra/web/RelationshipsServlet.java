package net.cnri.cordra.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.auth.InternalRequestOptions;
import net.cnri.cordra.auth.ServletAuthUtil;

import net.cnri.cordra.operations.GetRelationshipsOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/relationships/*")
public class RelationshipsServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(RelationshipsServlet.class);

    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            String path = ServletUtil.getPath(req);
            if (path == null || "".equals(path)) {
                ServletErrorUtil.badRequest(resp, "Missing objectId");
            } else {
                String objectId = path.substring(1);
                boolean outboundOnly = ServletUtil.getBooleanParameter(req, "outboundOnly");
                InternalRequestOptions options = ServletAuthUtil.getOptionsFromRequest(req, resp);
                JsonObject attributes = new JsonObject();
                attributes.addProperty("outboundOnly", outboundOnly);
                options.attributes = attributes;
                options.isCordraCallApi = true;
                JsonObject params = null;
                JsonElement relationships = internalCordra.call(objectId, GetRelationshipsOperation.ID, params, options);
                PrintWriter w = resp.getWriter();
                w.write(relationships.toString());
                w.close();
            }
        } catch (Exception e) {
            logger.error("Unexpected error getting relationships", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }
}
