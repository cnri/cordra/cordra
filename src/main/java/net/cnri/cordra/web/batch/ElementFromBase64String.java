package net.cnri.cordra.web.batch;

import net.dona.doip.client.Element;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

public class ElementFromBase64String extends Element {
    private String base64Element;

    public void setBase64Payload(String base64Element) {
        this.base64Element = base64Element;
    }

    public String getBase64Element() {
        return this.base64Element;
    }

    public void setBase64String(InputStream in) throws IOException {
        byte[] bytes = IOUtils.toByteArray(in);
        base64Element = Base64.getEncoder().encodeToString(bytes);
    }

    public InputStream getInputStream() {
        return new ByteArrayInputStream(Base64.getDecoder().decode(base64Element.replace('-', '+').replace('_', '/')));
    }
}
