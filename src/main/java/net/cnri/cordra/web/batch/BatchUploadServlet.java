package net.cnri.cordra.web.batch;

import com.google.gson.JsonObject;
import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.InternalRequestOptions;
import net.cnri.cordra.auth.PreAuthenticatedOptions;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.operations.BatchUploadOperation;
import net.cnri.cordra.web.CallServlet;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet({ "/batchUpload/*"})
public class BatchUploadServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(BatchUploadServlet.class);

    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private void doBatchUpload(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            InternalRequestOptions options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            AuthenticationResult authResult = internalCordra.internalAuthenticate(options);
            if (authResult.active == true) options = PreAuthenticatedOptions.build(authResult);
            String formatParam = req.getParameter("format");
            boolean isFailFast = ServletUtil.getBooleanParameter(req, "failFast", false);
            boolean isParallel = ServletUtil.getBooleanParameter(req, "parallel", true);
            JsonObject attributes = new JsonObject();
            if (formatParam != null) {
                attributes.addProperty("format", formatParam);
            }
            attributes.addProperty("failFast", isFailFast);
            attributes.addProperty("parallel", isParallel);
            options.attributes = attributes;
            options.isCordraCallApi = true;
            CallResponseHandler handler = new CallServlet.CallResponseHandlerForCallServlet(req, resp);
            InputStream in = req.getInputStream();
            internalCordra.callForType("CordraDesign", BatchUploadOperation.ID, in, handler, options);
        } catch (InternalErrorCordraException e) {
            logger.error("Error loading objects", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Error loading objects", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doBatchUpload(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doBatchUpload(req, resp);
    }
}
