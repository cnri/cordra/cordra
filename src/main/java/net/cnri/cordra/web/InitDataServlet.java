package net.cnri.cordra.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import net.cnri.cordra.*;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.operations.GetInitDataOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.cnri.cordra.api.CordraException;

@WebServlet("/initData/*")
public class InitDataServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(InitDataServlet.class);

    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            JsonElement params = null;
            JsonElement resultJson = internalCordra.callForType("CordraDesign", GetInitDataOperation.ID, params, options);
            resp.getWriter().write(resultJson.toString());
        } catch (InternalErrorCordraException e) {
            logger.error("Unexpected error calling get initData", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Unexpected error calling get initData", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }
}
