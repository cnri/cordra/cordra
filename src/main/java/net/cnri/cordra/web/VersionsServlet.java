package net.cnri.cordra.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.ServletAuthUtil;

import net.cnri.cordra.operations.versions.GetVersionsOperation;
import net.cnri.cordra.operations.versions.PublishVersionOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/versions/*")
public class VersionsServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(VersionsServlet.class);

    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * Lists all versions of the specified object
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String objectId = req.getParameter("objectId");
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            JsonElement versionInfosJson = internalCordra.call(objectId, GetVersionsOperation.ID, new JsonObject(), options);
            PrintWriter w = resp.getWriter();
            w.write(versionInfosJson.toString());
            w.close();
        } catch (InternalErrorCordraException e) {
            logger.error("Unexpected error calling get versions", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Unexpected error calling get versions", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    /**
     * Creates a new locked copy of the specified object and returns the new Id.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String objectId = req.getParameter("objectId");
        String versionId = req.getParameter("versionId");
        boolean clonePayloads = ServletUtil.getBooleanParameter(req, "clonePayloads", true);
        if (objectId != null) {
            try {
                Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
                JsonObject attributes = new JsonObject();
                if (versionId != null) {
                    attributes.addProperty("versionId", versionId);
                }
                attributes.addProperty("clonePayloads", clonePayloads);
                options.attributes = attributes;
                JsonObject params = null;
                JsonElement result = internalCordra.call(objectId, PublishVersionOperation.ID, params, options);
                PrintWriter w = resp.getWriter();
                w.write(result.toString());
                w.close();
            } catch (InternalErrorCordraException e) {
                logger.error("Unexpected error calling get versions", e);
                ServletErrorUtil.internalServerError(resp);
            } catch (CordraException e) {
                ServletErrorUtil.writeCordraException(resp, e);
            } catch (Exception e) {
                logger.error("Unexpected error calling get versions", e);
                ServletErrorUtil.internalServerError(resp);
            }
        }
    }
}
