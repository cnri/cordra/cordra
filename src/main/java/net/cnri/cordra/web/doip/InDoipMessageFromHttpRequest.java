package net.cnri.cordra.web.doip;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.cnri.cordra.GsonUtility;
import net.dona.doip.BadDoipException;
import net.dona.doip.DoipRequestHeadersWithRequestId;
import net.dona.doip.InDoipMessage;
import net.dona.doip.InDoipSegment;
import net.dona.doip.InDoipSegmentFromInputStream;
import net.dona.doip.InDoipSegmentFromJson;

public class InDoipMessageFromHttpRequest implements InDoipMessage {

    private final HttpServletRequest req;
    private final DoipRequestHeadersWithRequestId doipRequestHeaders;
    private final boolean isGetOrFormPostOrEmptyPost;
    private volatile boolean isClosed;
    private BadDoipException terminalException;
    private CompletableFuture<?> completer;
    private SpliteratorImpl spliterator;

    public InDoipMessageFromHttpRequest(HttpServletRequest req, DoipRequestHeadersWithRequestId doipRequestHeaders, boolean isGetOrFormPostOrEmptyPost) {
        this.req = req;
        this.doipRequestHeaders = doipRequestHeaders;
        this.isGetOrFormPostOrEmptyPost = isGetOrFormPostOrEmptyPost;
        this.spliterator = new SpliteratorImpl();
    }

    @Override
    public Stream<InDoipSegment> stream() {
        Stream<InDoipSegment> stream = StreamSupport.stream(spliterator, false);
        return stream.onClose(this::close);
    }

    @Override
    public void close() {
        if (terminalException != null) return;
        while (!isClosed) spliterator.tryAdvance(x -> {});
    }

    @Override
    public Iterator<InDoipSegment> iterator() {
        return Spliterators.iterator(spliterator);
    }

    public BadDoipException getTerminalException() {
        return terminalException;
    }

    public void setCompleter(CompletableFuture<?> completer) {
        this.completer = completer;
    }

    private static boolean isMultipartFormData(String type) {
        if (type == null) return false;
        type = type.toLowerCase(Locale.ROOT);
        if ("multipart/form-data".equals(type)) return true;
        if (!type.startsWith("multipart/form-data")) return false;
        char nextChar = type.charAt("multipart/form-data".length());
        if (nextChar == ' ' || nextChar == '\t' || nextChar ==';') return true;
        return false;
    }

    private class SpliteratorImpl extends Spliterators.AbstractSpliterator<InDoipSegment> {
        private FileItemIterator partIterator = null;
        private boolean sentFirstSegment;
        private boolean ignoredFirstFormData;
        private InDoipSegment pendingSegment;
        private final boolean isMultipart;
        private final boolean isMultipartFormData;

        public SpliteratorImpl() {
            super(Long.MAX_VALUE, Spliterator.IMMUTABLE | Spliterator.NONNULL | Spliterator.ORDERED);
            this.isMultipart = ServletFileUpload.isMultipartContent(req);
            this.isMultipartFormData = this.isMultipart && isMultipartFormData(req.getContentType());
        }

        @Override
        @SuppressWarnings("resource")
        public boolean tryAdvance(Consumer<? super InDoipSegment> action) {
            if (terminalException != null) throw new UncheckedIOException(terminalException);
            if (isClosed) return false;
            try {
                if (isMultipart) {
                    return tryAdvanceMultipart(action);
                } else {
                    return tryAdvanceNonMultipart(action);
                }
            } catch (IOException | FileUploadException e) {
                throw new RuntimeException(e);
            }
        }

        public boolean tryAdvanceNonMultipart(Consumer<? super InDoipSegment> action) throws IOException {
            if (!sentFirstSegment) {
                JsonElement doipRequestJson = GsonUtility.getGson().toJsonTree(doipRequestHeaders);
                InDoipSegment firstSegment = new InDoipSegmentFromJson(doipRequestJson);
                if (!isGetOrFormPostOrEmptyPost) {
                    String contentType = req.getContentType();
                    if (!DoipRequestUtil.isJson(contentType)) {
                        //Support for non-json single body request
                        //e.g somebody posts some bytes to a custom op.
                        //We need to construct the first segment headers, which is JSON, and assign it to curr
                        //And we need to construct the second segment, which is binary, and hold on to it in possibleSecondPart
                        pendingSegment = new InDoipSegmentFromInputStream(false, req.getInputStream());
                    }
                }
                sentFirstSegment = true;
                action.accept(firstSegment);
                return true;
            } else if (pendingSegment != null) {
                InDoipSegment pendingSegmentToSend = pendingSegment;
                pendingSegment = null;
                action.accept(pendingSegmentToSend);
                return true;
            } else {
                isClosed = true;
                if (completer != null) completer.complete(null);
                return false;
            }
        }

        @SuppressWarnings("resource")
        public boolean tryAdvanceMultipart(Consumer<? super InDoipSegment> action) throws IOException, FileUploadException {
            if (!sentFirstSegment) {
                JsonElement doipRequestJson = GsonUtility.getGson().toJsonTree(doipRequestHeaders);
                InDoipSegment firstSegment = new InDoipSegmentFromJson(doipRequestJson);
                sentFirstSegment = true;
                action.accept(firstSegment);
                return true;
            } else if (pendingSegment != null) {
                InDoipSegment pendingSegmentToSend = pendingSegment;
                pendingSegment = null;
                action.accept(pendingSegmentToSend);
                return true;
            }
            if (partIterator == null) {
                ServletFileUpload upload = new ServletFileUpload();
                partIterator = upload.getItemIterator(req);
            }
            if (partIterator.hasNext()) {
                FileItemStream item = partIterator.next();
                String contentType = item.getContentType();
                String id = item.getFieldName();
                InputStream in = item.openStream();
                boolean isJson = DoipRequestUtil.isJson(contentType);
                InDoipSegment contentSegment = new InDoipSegmentFromInputStream(isJson, in);
                if (isMultipartFormData) {
                    if (!ignoredFirstFormData) {
                        ignoredFirstFormData = true;
                        action.accept(contentSegment);
                    } else {
                        JsonObject json = new JsonObject();
                        json.addProperty("id", id);
                        InDoipSegment formDataSegment = new InDoipSegmentFromJson(json);
                        pendingSegment = contentSegment;
                        action.accept(formDataSegment);
                    }
                } else {
                    action.accept(contentSegment);
                }
                return true;
            } else {
                isClosed = true;
                if (completer != null) completer.complete(null);
                return false;
            }
        }
    }
}
