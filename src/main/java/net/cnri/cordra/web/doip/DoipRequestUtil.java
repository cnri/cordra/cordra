package net.cnri.cordra.web.doip;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import net.cnri.cordra.api.BadRequestCordraException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class DoipRequestUtil {

    public static final String TARGET_ID = "targetId";
    public static final String OPERATION_ID = "operationId";
    public static final String REQUEST_ID = "requestId";
    public static final String CLIENT_ID = "clientId";
    public static final String ATTRIBUTES = "attributes";
    public static final String AUTHENTICATION = "authentication";
    public static final String INPUT = "input";

    public static final Map<String, String> longToShortNameMap = new HashMap<>();
    static {
        longToShortNameMap.put(TARGET_ID, "t");
        longToShortNameMap.put(OPERATION_ID, "o");
        longToShortNameMap.put(REQUEST_ID, "r");
        longToShortNameMap.put(CLIENT_ID, "c");
        longToShortNameMap.put(ATTRIBUTES, "a");
        longToShortNameMap.put(AUTHENTICATION, "u");
        longToShortNameMap.put(INPUT, "i");
    }

    private static boolean isReserved(String parameterName) {
        if (parameterName.length() == 1) return true;
        if (parameterName.length() > 1 && parameterName.charAt(1) == '.') return true;
        for (String reservedName : longToShortNameMap.keySet()) {
            if (reservedName.equals(parameterName)) return true;
            if (parameterName.startsWith(reservedName + ".")) return true;
        }
        return false;
    }

    public static boolean isJson(String mediaType) {
        if (mediaType == null) return false;
        String type = mediaType.toLowerCase(Locale.ROOT);
        if ("application/json".equals(type)) return true;
        if (type.endsWith("+json")) return true;
        if (type.contains("+json ") || type.contains("+json\t") || type.contains("+json;")) return true;
        if (!type.startsWith("application/json")) return false;
        char nextChar = type.charAt("application/json".length());
        if (nextChar == ' ' || nextChar == '\t' || nextChar ==';') return true;
        return false;
    }

    public static String getStringParameter(Map<String, String[]> params, String longName) throws BadRequestCordraException {
        String result = null;
        String[] vals = params.get(longName);
        vals = eliminateRepeats(vals);
        if (vals != null && vals.length > 0) {
            if (vals.length > 1) throw new BadRequestCordraException("Repeated parameter " + longName);
            result = vals[0];
        }
        String shortName = longToShortNameMap.get(longName);
        if (shortName == null) return null;
        vals = params.get(shortName);
        vals = eliminateRepeats(vals);
        if (vals != null && vals.length > 0) {
            if (result != null && !result.equals(vals[0])) throw new BadRequestCordraException("Repeated parameter " + longName + " and " + shortName);
            if (vals.length > 1) throw new BadRequestCordraException("Repeated parameter " + shortName);
            result = vals[0];
        }
        return result;
    }

    private static String[] eliminateRepeats(String[] vals) {
        if (vals == null) return null;
        if (vals.length <= 1) return vals;
        return Arrays.stream(vals).collect(Collectors.toSet()).toArray(new String[0]);
    }

    public static JsonElement buildJsonParameter(String longName, Map<String, String[]> params, JsonElement initialValue, boolean includeNonReservedParams) throws BadRequestCordraException {
        JsonElement result = initialValue;
        JsonElement valueFromLongName = getOrAugmentResultFromValues(longName, params.get(longName), result);
        if (valueFromLongName != null) result = valueFromLongName;
        result = augmentFromDotParams(longName, params, result);
        String shortName = longToShortNameMap.get(longName);
        if (shortName != null) {
            JsonElement valueFromShortName = getOrAugmentResultFromValues(shortName, params.get(shortName), result);
            if (valueFromShortName != null) result = valueFromShortName;
            result = augmentFromDotParams(shortName, params, result);
        }
        if (result != null && !result.isJsonObject()) return result;
        if (includeNonReservedParams) {
            result = augmentFromDotParams(null, params, result);
        }
        return result;
    }

    private static JsonElement augmentFromDotParams(String name, Map<String, String[]> params, JsonElement result) throws BadRequestCordraException {
        boolean forNonReservedParams = name == null;
        if (forNonReservedParams && (result != null && !result.isJsonObject())) return result;
        for (Map.Entry<String, String[]> param : params.entrySet()) {
            String paramName = param.getKey();
            if (name == null && isReserved(paramName)) continue;
            if (name != null && !paramName.startsWith(name + ".")) continue;
            String[] nameParts = paramName.split("\\.");
            if (name != null) {
                nameParts = tail(nameParts);
            }
            String[] values = param.getValue();
            if (values == null || values.length == 0) continue;
            values = eliminateRepeats(values);
            for (String value : values) {
                JsonElement el = paramValueAsJson(value);
                if (el == null) continue;
                if (!forNonReservedParams && result != null && !result.isJsonObject()) throw new BadRequestCordraException("Combination of object and non-object parameters for " + name);
                if (result == null) result = new JsonObject();
                setIntoObject(paramName, result.getAsJsonObject(), nameParts, el, forNonReservedParams);
            }
        }
        return result;
    }

    private static JsonElement getOrAugmentResultFromValues(String name, String[] values, JsonElement result) throws BadRequestCordraException {
        if (values == null) return result;
        values = eliminateRepeats(values);
        for (String value : values) {
            JsonElement jsonValue = paramValueAsJson(value);
            if (jsonValue == null) continue;
            if (!jsonValue.isJsonObject()) {
                if (result == null) result = jsonValue;
                else if (result.isJsonObject()) throw new BadRequestCordraException("Combination of object and non-object parameters for " + name);
                else throw new BadRequestCordraException("Multiple non-object parameters for " + name);
            } else {
                if (result == null) result = jsonValue.getAsJsonObject();
                else if (result.isJsonObject()) augmentJsonObject(name, result.getAsJsonObject(), jsonValue.getAsJsonObject());
                else throw new BadRequestCordraException("Combination of object and non-object parameters for " + name);
            }
        }
        return result;
    }

    private static void augmentJsonObject(String name, JsonObject result, JsonObject additional) throws BadRequestCordraException {
        for (Map.Entry<String, JsonElement> entry : additional.entrySet()) {
            if (result.has(entry.getKey())) {
                throw new BadRequestCordraException("Multiple values for property " + entry.getKey() + " of " + name);
            }
            result.add(entry.getKey(), entry.getValue());
        }
    }

    private static JsonElement paramValueAsJson(String value) {
        if (value == null) return null;
        if (value.trim().equals("null") || value.trim().equals("undefined")) return null;
        try {
            return JsonParser.parseString(value);
        } catch (JsonParseException e) {
            // interpret as string
            return new JsonPrimitive(value);
        }
    }

    private static String[] tail(String[] arr) {
        return Arrays.copyOfRange(arr, 1, arr.length);
    }

    private static void setIntoObject(String diagnosticName, JsonObject obj, String[] nameParts, JsonElement value, boolean forNonReservedParam) throws BadRequestCordraException {
        String name = nameParts[0];
        if (nameParts.length == 1) {
            if (obj.has(name)) {
                if (forNonReservedParam) return;
                throw new BadRequestCordraException("Collision processing parameter " + diagnosticName);
            }
            obj.add(name, value);
            return;
        } else {
            JsonObject sub = null;
            if (obj.has(name)) {
                JsonElement existingSub = obj.get(name);
                if (existingSub.isJsonObject()) {
                    sub = existingSub.getAsJsonObject();
                } else {
                    if (forNonReservedParam) return;
                    throw new BadRequestCordraException("Collision processing parameter " + diagnosticName);
                }
            }
            if (sub == null) {
                sub = new JsonObject();
                obj.add(name, sub);
            }
            String[] tail = tail(nameParts);
            setIntoObject(diagnosticName, sub, tail, value, forNonReservedParam);
        }
    }
}
