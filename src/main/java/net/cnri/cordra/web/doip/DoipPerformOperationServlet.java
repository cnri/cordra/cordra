package net.cnri.cordra.web.doip;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.CordraServiceFactory;
import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.doip.CordraClientDoipProcessor;
import net.cnri.cordra.doip.DoipServerRequestImplWithCachedOptions;
import net.cnri.cordra.util.HttpUtil;
import net.cnri.cordra.web.EmptyCheckingHttpServletRequestWrapper;
import net.cnri.cordra.web.ServletUtil;
import net.dona.doip.*;
import net.dona.doip.server.DoipProcessor;
import net.dona.doip.server.DoipServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;

import net.cnri.cordra.auth.AuthenticationBackOffCordraException;
import net.cnri.cordra.auth.AuthenticationBackOffFilter;
import net.cnri.cordra.auth.Credentials;
import net.cnri.cordra.auth.InternalRequestOptions;
import net.cnri.cordra.auth.ServletAuthUtil;

@WebServlet(value = "/doip/*", asyncSupported = true)
public class DoipPerformOperationServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(DoipPerformOperationServlet.class);

    private CordraService cordra;
    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            cordra = CordraServiceFactory.getCordraService();
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        process(req, resp, true);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (ServletUtil.isForm(req)) {
            process(req, resp, true);
            return;
        }
        EmptyCheckingHttpServletRequestWrapper reqWrapper = EmptyCheckingHttpServletRequestWrapper.wrap(req);
        if (reqWrapper.isEmpty()) {
            process(reqWrapper, resp, true);
            return;
        }
        process(reqWrapper, resp, false);
    }

    private void process(HttpServletRequest req, HttpServletResponse resp, boolean isGetOrFormPostOrEmptyPost) {
        String requestId = null;
        try {
            Map<String, String[]> params = req.getParameterMap();
            requestId = DoipRequestUtil.getStringParameter(params, DoipRequestUtil.REQUEST_ID);
            DoipProcessor doipProcessor = cordra.getDoipProcessor();
            if (doipProcessor == null) {
                sendErrorResponse(resp, requestId, "DOIP API for HTTP clients is not enabled", DoipConstants.STATUS_ERROR);
                return;
            }
            DoipRequestHeadersWithRequestId doipRequestHeaders = buildDoipRequest(req, params, isGetOrFormPostOrEmptyPost);
            if (doipRequestHeaders.operationId == null) {
                sendErrorResponse(resp, requestId, "Missing operationId", DoipConstants.STATUS_BAD_REQUEST);
                return;
            }
            if (doipRequestHeaders.targetId == null) {
                sendErrorResponse(resp, requestId, "Missing targetId", DoipConstants.STATUS_BAD_REQUEST);
                return;
            }
            if (isDisallowedAuthenticatingOverHttp(doipRequestHeaders, req)) {
                sendErrorResponse(resp, requestId, "Authentication requires HTTPS", DoipConstants.STATUS_FORBIDDEN);
                return;
            }
            if (isGetOrFormPostOrEmptyPost &&
                !CordraClientDoipProcessor.isNullOrEmpty(doipRequestHeaders.attributes) &&
                !CordraClientDoipProcessor.isBuiltInOperationWhichNeverUsedAttributesAsInputForDoipGet(doipRequestHeaders.operationId) &&
                cordra.getDesign().useLegacyAttributesAsInputForDoipGet == Boolean.TRUE &&
                doipRequestHeaders.input == null) {
                doipRequestHeaders.input = doipRequestHeaders.attributes;
                // With useLegacyAttributesAsInputForDoipGet, for GET we copy the attributes into input for custom operations
                // also for form POST and for empty-body POST
            }
            try (
                InDoipMessage inDoipMessage = new InDoipMessageFromHttpRequest(req, doipRequestHeaders, isGetOrFormPostOrEmptyPost);
                OutDoipMessageHttpResponse outDoipMessage = new OutDoipMessageHttpResponse(resp);
            ) {
                DoipServerRequestImplWithCachedOptions doipRequest = new DoipServerRequestImplWithCachedOptions(inDoipMessage, null, null, null);
                if (!"POST".equals(req.getMethod())) doipRequest.setIsGet(true);
                doipRequest.setOptions((InternalRequestOptions) req.getAttribute(ServletAuthUtil.INTERNAL_REQUEST_OPTIONS_ATT_NAME));
                if (doipProcessor instanceof CordraClientDoipProcessor && !cordra.isDisableBackOffRequestParking()) {
                    if (handleAuthTokenBackoff((CordraClientDoipProcessor) doipProcessor, doipRequest, req, resp)) {
                        // was async dispatched
                        return;
                    }
                }
                DoipServerResponse doipResponse = new DoipServerHttpResponse(requestId, outDoipMessage);
                doipProcessor.process(doipRequest, doipResponse);
                doipResponse.commit();
            }
        } catch (BadRequestCordraException e) {
            sendErrorResponse(resp, requestId, e.getMessage(), DoipConstants.STATUS_BAD_REQUEST);
        } catch (BadDoipException bx) {
            sendErrorResponse(resp, requestId, "Bad Request", DoipConstants.STATUS_BAD_REQUEST);
        } catch (Exception e) {
            logger.error("Internal error in DOIP API for HTTP clients", e);
            sendErrorResponse(resp, requestId, "Internal error", DoipConstants.STATUS_ERROR);
        }
    }

    private boolean handleAuthTokenBackoff(CordraClientDoipProcessor doipProcessor, DoipServerRequestImplWithCachedOptions doipRequest, HttpServletRequest req, HttpServletResponse resp) {
        if (!doipProcessor.isAuthToken(doipRequest)) return false;
        try {
            InternalRequestOptions options = CordraClientDoipProcessor.optionsOfAuthTokenRequest(doipRequest);
            req.setAttribute(ServletAuthUtil.INTERNAL_REQUEST_OPTIONS_ATT_NAME, options);
            internalCordra.internalAuthenticate(options);
        } catch (AuthenticationBackOffCordraException e) {
            AuthenticationBackOffFilter.handle(req, resp, e);
            return true;
        } catch (Exception e) {
            // ignore
        }
        return false;
    }

    private boolean isDisallowedAuthenticatingOverHttp(DoipRequestHeadersWithRequestId doipRequestHeaders, HttpServletRequest req) {
        if (cordra.getDesign().allowInsecureAuthentication == Boolean.TRUE) return false;
        if (req.isSecure()) return false;
        if (!CordraClientDoipProcessor.isNullOrEmpty(doipRequestHeaders.authentication)) return true;
        // no authentication; check 20.DOIP/Op.Auth.Token
        if (doipRequestHeaders.operationId == null) return false;
        if (CordraClientDoipProcessor.unaliasOperation(doipRequestHeaders.operationId).equals(CordraClientDoipProcessor.OP_AUTH_TOKEN)) return true;
        return false;
    }

    private void sendErrorResponse(HttpServletResponse resp, String requestId, String msg, String doipStatus) {
        try (OutDoipMessageHttpResponse outDoipMessage = new OutDoipMessageHttpResponse(resp)) {
            DoipServerResponse doipResponse = new DoipServerHttpResponse(requestId, outDoipMessage);
            doipResponse.setStatus(doipStatus);
            JsonObject errorOutput = new JsonObject();
            errorOutput.addProperty(DoipConstants.MESSAGE_ATT, msg);
            doipResponse.writeCompactOutput(errorOutput);
            doipResponse.commit();
        } catch (Exception e) {
            logger.error("Exception sending error in DOIP API for HTTP clients", e);
        }
    }

    public static InternalRequestOptions getOptionsFromRequest(HttpServletRequest req) throws BadRequestCordraException, IOException {
        // this caching is also used to prevent backing off the same request twice; see AuthenticationBackOffFilter.handle
        InternalRequestOptions options = (InternalRequestOptions) req.getAttribute(ServletAuthUtil.INTERNAL_REQUEST_OPTIONS_ATT_NAME);
        if (options != null) return options;
        Map<String, String[]> params = req.getParameterMap();
        // ignore input in this method, as we don't need it
        DoipRequestHeadersWithRequestId doipRequestHeaders = buildDoipRequest(req, params, true);
        if (doipRequestHeaders.operationId != null && CordraClientDoipProcessor.OP_AUTH_TOKEN.equals(CordraClientDoipProcessor.unaliasOperation(doipRequestHeaders.operationId))) {
            // handled in this servlet instead of in the filter
            return null;
        }
        options = new InternalRequestOptions();
        options.doipClientId = doipRequestHeaders.clientId;
        if (doipRequestHeaders.authentication != null && doipRequestHeaders.authentication.isJsonObject()) {
            options.doipAuthentication = doipRequestHeaders.authentication.getAsJsonObject();
            if (options.doipAuthentication.has("asUserId")) {
                options.setAsUserId(options.doipAuthentication.get("asUserId").getAsString());
            }
        }
        req.setAttribute(ServletAuthUtil.INTERNAL_REQUEST_OPTIONS_ATT_NAME, options);
        if (CordraClientDoipProcessor.OP_CHANGE_PASSWORD.equals(CordraClientDoipProcessor.unaliasOperation(doipRequestHeaders.operationId))) {
            options.isChangePassword = true;
        }
        return options;
    }

    public static DoipRequestHeadersWithRequestId buildDoipRequest(HttpServletRequest req, Map<String, String[]> params, boolean isGetOrFormPostOrEmptyPost) throws BadRequestCordraException, IOException {
        DoipRequestHeadersWithRequestId res = new DoipRequestHeadersWithRequestId(new DoipRequestHeaders());
        res.requestId = DoipRequestUtil.getStringParameter(params, DoipRequestUtil.REQUEST_ID);
        res.targetId = DoipRequestUtil.getStringParameter(params, DoipRequestUtil.TARGET_ID);
        res.operationId = getOperationId(req, params);
        res.clientId = DoipRequestUtil.getStringParameter(params, DoipRequestUtil.CLIENT_ID);
        res.authentication = getAuthenticationFromAuthHeader(req);
        res.authentication = DoipRequestUtil.buildJsonParameter(DoipRequestUtil.AUTHENTICATION, params, res.authentication, false);
        if (!isGetOrFormPostOrEmptyPost) {
            String mediaType = req.getContentType();
            if (DoipRequestUtil.isJson(mediaType)) {
                try {
                    res.input = JsonParser.parseReader(req.getReader());
                } catch (JsonParseException e) {
                    throw new BadRequestCordraException("Unable to parse request body as JSON", e);
                }
            }
        }
        res.input = DoipRequestUtil.buildJsonParameter(DoipRequestUtil.INPUT, params, res.input, false);
        JsonElement attributesEl = DoipRequestUtil.buildJsonParameter(DoipRequestUtil.ATTRIBUTES, params, null, true);
        if (attributesEl != null) {
            if (!attributesEl.isJsonObject()) {
                // TODO consider loosening this restriction
                throw new BadRequestCordraException("attributes must be a JSON object");
            }
            res.attributes = attributesEl.getAsJsonObject();
        }
        augmentAttributesFromHttpRequestIfNecessary(res, req);
        return res;
    }

    private static String getOperationId(HttpServletRequest req, Map<String, String[]> params) throws BadRequestCordraException {
        String operationId = DoipRequestUtil.getStringParameter(params, DoipRequestUtil.OPERATION_ID);
        String operationIdFromPath;
        try {
            operationIdFromPath = getOperationIdFromPath(req, operationId);
        } catch (Exception e) {
            if (operationId == null) throw e;
            operationIdFromPath = null;
        }
        if (operationId == null) return operationIdFromPath;
        if (operationIdFromPath == null) return operationId;
        if (operationId.equals(operationIdFromPath)) return operationId;
        throw new BadRequestCordraException("Repeated parameter " + DoipRequestUtil.OPERATION_ID + " in path");
    }

    private static String getOperationIdFromPath(HttpServletRequest req, String operationId)
        throws BadRequestCordraException {
        try {
            String pathInfo = ServletUtil.getPath(req);
            while (pathInfo.startsWith("/")) {
                pathInfo = pathInfo.substring(1);
            }
            if (!pathInfo.isEmpty()) operationId = pathInfo;
        } catch (Exception e) {
            throw new BadRequestCordraException(e);
        }
        return operationId;
    }

    private static void augmentAttributesFromHttpRequestIfNecessary(DoipRequestHeadersWithRequestId doipRequestHeaders, HttpServletRequest req) {
        String mediaType = req.getContentType();
        String filename = HttpUtil.getContentDispositionFilename(req.getHeader("Content-Disposition"));
        if (mediaType != null || filename != null) {
            JsonObject attributes = doipRequestHeaders.attributes;
            if (attributes == null) {
                attributes = new JsonObject();
                doipRequestHeaders.attributes = attributes;
            }
            if (filename != null && !attributes.has("filename")) {
                attributes.addProperty("filename", filename);
            }
            if (mediaType != null && !attributes.has("mediaType")) {
                attributes.addProperty("mediaType", mediaType);
            }
        }
    }

    private static JsonElement getAuthenticationFromAuthHeader(HttpServletRequest req) {
        String authHeader = req.getHeader("Authorization");
        if (authHeader == null) return null;
        return translateAuthHeader(authHeader);
    }

    private static JsonElement translateAuthHeader(String authHeader) {
        if (isBasicAuth(authHeader)) {
            JsonObject authentication = new JsonObject();
            authHeader = authHeader.trim();
            Credentials c = new Credentials(authHeader);
            authentication.addProperty("username", c.getUsername());
            authentication.addProperty("password", c.getPassword());
            return authentication;
        } else if (isBearerToken(authHeader)) {
            JsonObject authentication = new JsonObject();
            String token = getTokenFromAuthHeader(authHeader);
            authentication.addProperty("token", token);
            return authentication;
        } else {
            if (authHeader == null) return null;
            String[] parts = authHeader.trim().split(" +");
            if (parts.length != 2) return null;
            if (!"Doip".equalsIgnoreCase(parts[0])) return null;
            try {
                String json = new String(Base64.getDecoder().decode(parts[1]), StandardCharsets.UTF_8);
                return JsonParser.parseString(json);
            } catch (Exception e) {
                // ignore
                return null;
            }
        }
    }

    private static String getTokenFromAuthHeader(String authHeader) {
        return authHeader.substring(authHeader.indexOf(" ") + 1);
    }

    private static boolean isBearerToken(String authHeader) {
        if (authHeader == null) return false;
        String[] parts = authHeader.trim().split(" +");
        if (parts.length != 2) return false;
        if (!"Bearer".equalsIgnoreCase(parts[0])) return false;
//        if (parts[1].contains(".")) return false;
        return true;
    }

    private static boolean isBasicAuth(String authHeader) {
        return authHeader.length() > 5 && authHeader.substring(0, 6).equalsIgnoreCase("Basic ");
    }
}
