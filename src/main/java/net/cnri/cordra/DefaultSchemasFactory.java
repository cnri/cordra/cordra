package net.cnri.cordra;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import net.cnri.util.StreamUtil;

public class DefaultSchemasFactory {

    public static String getSchemaSchema() {
        return getResourceAsString("schema.schema.json");
    }
    
    public static String getCordraDesignSchema() {
        try (InputStream resource = DefaultSchemasFactory.class.getResourceAsStream("cordradesign.schema.json")) {
            return StreamUtil.readFully(new InputStreamReader(resource, StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    public static String getDefaultUserSchema() {
        return getResourceAsString("user.schema.json");
    }
    
    public static String getDefaultUserJavaScript() {
        return getResourceAsString("user.schema.js");
    }

    public static String getLegacyDefaultUserJavaScript() {
        return getResourceAsString("user.legacy.schema.js");
    }
    
    public static String getLegacyUpdateDefaultUserJavaScript() {
        return getResourceAsString("user.legacy-update.schema.js");
    }

    public static String getDefaultGroupSchema() {
        return getResourceAsString("group.schema.json");
    }

    public static String getDefaultDocumentSchema() {
        return getResourceAsString("document.schema.json");
    }
    
    public static String getResourceAsString(String name) {
        try (InputStream resource = DefaultSchemasFactory.class.getResourceAsStream(name)) {
            return StreamUtil.readFully(new InputStreamReader(resource, StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }
}
