package net.cnri.cordra.indexer.solr;

import net.cnri.cordra.api.FacetBucket;
import net.cnri.cordra.api.FacetResult;
import net.cnri.cordra.api.FacetSpecification;
import net.cnri.cordra.api.QueryParams;
import net.cnri.cordra.collections.AbstractSearchResults;
import net.cnri.cordra.collections.SearchResultsFromIterator;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class SolrSearchResults  extends AbstractSearchResults<SolrDocument> {
    private final SearchResultsFromIterator<SolrDocument> results;
    private final List<FacetResult> facets = new ArrayList<>();

    public SolrSearchResults(QueryResponse response, QueryParams params, Function<String, String> getSortFieldName) {
        SolrDocumentList solrResults = response.getResults();
        this.results = new SearchResultsFromIterator<>((int)solrResults.getNumFound(), solrResults.iterator());
        List<FacetField> facetFields = response.getFacetFields();
        if (facetFields != null) {
            Map<String, String> facetFieldToOrigField = new HashMap<>();
            if (params.getFacets() != null) {
                for (FacetSpecification facetSpec : params.getFacets()) {
                    facetFieldToOrigField.put(getSortFieldName.apply(facetSpec.field), facetSpec.field);
                }
            }
            for (FacetField field : facetFields) {
                FacetResult facet = new FacetResult();
                facet.field = facetFieldToOrigField.get(field.getName());
                if (facet.field == null) facet.field = field.getName();
                for (FacetField.Count count : field.getValues()) {
                    FacetBucket bucket = new FacetBucket();
                    bucket.value = count.getName();
                    bucket.count = count.getCount();
                    bucket.filterQuery = count.getAsFilterQuery();
                    facet.buckets.add(bucket);
                }
                this.facets.add(facet);
            }
        }
    }

    @Override
    public List<FacetResult> getFacets() {
        return this.facets;
    }

    @Override
    public int size() {
        return results.size();
    }

    @Override
    protected SolrDocument computeNext() {
        if (results.iterator().hasNext()) {
            return results.iterator().next();
        } else {
            return null;
        }
    }
}
