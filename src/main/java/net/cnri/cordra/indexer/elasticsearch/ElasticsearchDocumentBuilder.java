package net.cnri.cordra.indexer.elasticsearch;

import net.cnri.cordra.indexer.DocumentBuilder;
import net.cnri.cordra.storage.CordraStorage;

import java.time.Instant;
import java.util.*;

import com.google.gson.JsonObject;

public class ElasticsearchDocumentBuilder extends DocumentBuilder<Map<String, List<Object>>> {

    public ElasticsearchDocumentBuilder(boolean isStoreFields, CordraStorage storage) {
        super(isStoreFields, storage);
    }

    @Override
    protected Map<String, List<Object>> create() {
        return new HashMap<>();
    }

    private void addFieldToDocument(Map<String, List<Object>> doc, String fieldName, Object fieldValue) {
        doc.putIfAbsent(fieldName, new ArrayList<>());
        doc.get(fieldName).add(fieldValue);        
    }
    
    @Override
    protected void addStringFieldToDocument(Map<String, List<Object>> doc, String fieldName, String fieldValue, boolean isStoreFieldsParam) {
        addFieldToDocument(doc, fieldName, fieldValue);
    }

    @Override
    protected void addTextFieldToDocument(Map<String, List<Object>> doc, String fieldName, String fieldValue, boolean isStoreFieldsParam) {
        addFieldToDocument(doc, fieldName, fieldValue);
    }

    @Override
    protected void addTxnIdFieldToDocument(Map<String, List<Object>> doc, String fieldName, long fieldValue, boolean isStoreFieldsParam) {
        addFieldToDocument(doc, fieldName, fieldValue);
    }

    @Override
    protected void addNumericFieldToDocument(Map<String, List<Object>> doc, String fieldName, double fieldValue, boolean isStoreFieldsParam) {
        Object addend = fieldValue;
        if (fieldValue == Double.POSITIVE_INFINITY) {
            addend = Double.MAX_VALUE;
        } else if (fieldValue == Double.NEGATIVE_INFINITY) {
            addend = Double.MIN_VALUE;
        }
        addFieldToDocument(doc, fieldName, addend);
    }
    
    private JsonObject getDateTimeRange(String text) {
        String startDateTime = dateTimeFormatterThatFormats.format(dateTimeFormatterThatParsesAndDefaultsEarly.parse(text, Instant::from));
        String endDateTime = dateTimeFormatterThatFormats.format(dateTimeFormatterThatParsesAndDefaultsLate.parse(text, Instant::from));
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("gte", startDateTime);
        jsonObject.addProperty("lte", endDateTime);
        return jsonObject;
    }
    
    @Override
    protected void addDateTimeFieldToDocument(Map<String, List<Object>> doc, String fieldName, String text, boolean isStoreFields) {
        addFieldToDocument(doc, fieldName, getDateTimeRange(text));
    }

    @Override
    protected void addSortTxnIdFieldToDocument(Map<String, List<Object>> doc, String fieldName, long fieldValue) {
        // no-op, no separate sort_txnId field
    }

    @Override
    protected void addSortNumericFieldToDocument(Map<String, List<Object>> doc, String fieldName, double fieldValue) {
        addNumericFieldToDocument(doc, fieldName, fieldValue, false);
    }

    @Override
    protected void addSortDateTimeFieldToDocument(Map<String, List<Object>> doc, String sortFieldName, String text) {
        String startDateTime = dateTimeFormatterThatFormats.format(dateTimeFormatterThatParsesAndDefaultsEarly.parse(text, Instant::from));
        addFieldToDocument(doc, sortFieldName, startDateTime);
    }
    
    @Override
    protected void addSortFieldToDocument(Map<String, List<Object>> doc, String fieldName, String fieldValue) {
        String sortFieldName = getSortFieldName(fieldName);
        String sortFieldValue = truncateForSorting(fieldValue);
        addFieldToDocument(doc, sortFieldName, sortFieldValue);
    }

    private String truncateForSorting(String s) {
        if (s.length() < 1024) return s;
        return s.substring(0, 1024);
    }

    @Override
    public String getSortFieldName(String field) {
        if ("score".equals(field)) return "_score";
        if (field == null || "undefined".equals(field)) return null;
        if ("txnId".equals(field)) return "txnId";
        if (field.startsWith("sort_")) return field;
        return "sort_" + field;
    }

}
