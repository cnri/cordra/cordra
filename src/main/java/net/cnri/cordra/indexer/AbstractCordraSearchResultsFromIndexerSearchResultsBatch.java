package net.cnri.cordra.indexer;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.api.*;
import net.cnri.cordra.collections.AbstractSearchResults;
import net.cnri.cordra.collections.SearchResultsFromStream;
import net.cnri.cordra.storage.CordraStorage;

public abstract class AbstractCordraSearchResultsFromIndexerSearchResultsBatch<T, Doc> extends AbstractSearchResults<T> {
    private final SearchResults<Doc> results;
    private final Iterator<Doc> searchResultsIter;
    private final CordraStorage storage;
    private final Class<T> klass;
    private final boolean includeScore;

    private final int batchSize = 1000;
    private SearchResults<CordraObject> batch = null;
    private Iterator<CordraObject> batchIter = null;

    public AbstractCordraSearchResultsFromIndexerSearchResultsBatch(SearchResults<Doc> results, CordraStorage storage, Class<T> klass, boolean includeScore) {
        this.results = results;
        this.searchResultsIter = results.iterator();
        this.storage = storage;
        this.klass = klass;
        this.includeScore = includeScore;
    }

    private SearchResults<CordraObject> addScores(SearchResults<CordraObject> batchWithoutScores, Map<String, Float> idScoreBatch) {
        return new SearchResultsFromStream<>(batchWithoutScores.size(),
                batchWithoutScores.getFacets(),
                batchWithoutScores.stream().map(co -> {
                    Float score = idScoreBatch.get(co.id);
                    if ((score != null) && (!score.isNaN())) {
                        CordraObject result = CordraService.copyOfCordraObjectWithNewResponseContext(co);
                        result.responseContext.addProperty("score", score);
                        return result;
                    } else {
                        return co;
                    }
                }));
    }

    private void getNextBatch() {
        try {
            if (batch != null) {
                batch.close();
            }
            Map<String, Float> idScoreBatch = readNextBatchOfIdScoresFromResultsIter();
            SearchResults<CordraObject> batchWithoutScores = storage.get(idScoreBatch.keySet());
            if (includeScore) {
                batch = addScores(batchWithoutScores, idScoreBatch);
            } else {
                batch = batchWithoutScores;
            }
            batchIter = batch.iterator();
        } catch (CordraException e) {
            throw new UncheckedCordraException(e);
        }
    }

    private Map<String, Float> readNextBatchOfIdScoresFromResultsIter() {
        Map<String, Float> idScoreBatch = new LinkedHashMap<>();
        for (int i = 0; i < batchSize; i++) {
            if (!searchResultsIter.hasNext()) {
                break;
            }
            Doc from = searchResultsIter.next();
            String id = getIdFromDocument(from);
            Float score = null;
            if (includeScore) score = getScoreFromDocument(from);
            idScoreBatch.put(id, score);
        }
        return idScoreBatch;
    }

    abstract public String getIdFromDocument(Doc document);
    abstract public Float getScoreFromDocument(Doc document);
    abstract public String getTypeFromDocument(Doc document);

    @Override
    public int size() {
        return results.size();
    }

    @Override
    public List<FacetResult> getFacets() {
        return results.getFacets();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected T computeNext() {
        if (klass == String.class) {
            return (T) computeNextStringId();
        } else if (klass == IdType.class) {
            return (T) computeNextIdType();
        } else {
            return (T) computeNextCordraObjectViaBatch();
        }
    }

    private CordraObject computeNextCordraObjectViaBatch() {
        if (batch == null) {
            getNextBatch();
        }
        if (batchIter.hasNext()) {
            CordraObject next = batchIter.next();
            return next;
        } else if (searchResultsIter.hasNext()) {
            getNextBatch();
            return (CordraObject) computeNext();
        } else {
            return null;
        }
    }

    private IdType computeNextIdType() {
        if (!searchResultsIter.hasNext()) return null;
        Doc from = searchResultsIter.next();
        String id = getIdFromDocument(from);
        String type = getTypeFromDocument(from);
        return new IdType(id, type);
    }

    private String computeNextStringId() {
        if (!searchResultsIter.hasNext()) return null;
        Doc from = searchResultsIter.next();
        String id = getIdFromDocument(from);
        return id;
    }

    @Override
    protected void closeOnlyOnce() {
        results.close();
        if (batch != null) {
            batch.close();
        }
    }
}
