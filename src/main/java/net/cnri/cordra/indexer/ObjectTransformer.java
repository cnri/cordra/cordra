package net.cnri.cordra.indexer;

import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;

public interface ObjectTransformer {

    CordraObject transform(String type, CordraObject co) throws CordraException;
}
