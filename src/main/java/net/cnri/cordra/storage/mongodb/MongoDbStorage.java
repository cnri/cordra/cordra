package net.cnri.cordra.storage.mongodb;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;
import com.mongodb.client.model.FindOneAndDeleteOptions;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.IndexOptions;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.*;
import net.cnri.cordra.collections.AbstractSearchResults;
import net.cnri.cordra.collections.SearchResultsFromStream;
import net.cnri.cordra.storage.CordraStorage;
import net.cnri.cordra.storage.LimitedInputStream;
import net.cnri.cordra.util.JsonUtil;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class MongoDbStorage implements CordraStorage {
    private static Logger logger = LoggerFactory.getLogger(MongoDbStorage.class);
    public static long DEFAULT_MAX_TIME_MS = 30_000;
    public static long DEFAULT_MAX_TIME_MS_LONG_RUNNING = 1000 * 60 * 60 * 24 * 10 ; //10 days

    private final static Gson gson = GsonUtility.getGson();
    private MongoClient client;
    private MongoDatabase db;
    private MongoCollection<Document> collection;
    private GridFSBucket gridFSBucket;
    private MongoCollection<Document> fsCollection;
    private long maxTimeMs;
    private long maxTimeMsLongRunning;
    private boolean useLegacyIdField = true;

    public MongoDbStorage(MongoClient client) throws InternalErrorCordraException {
        this(client, null, null, null, DEFAULT_MAX_TIME_MS, DEFAULT_MAX_TIME_MS_LONG_RUNNING);
    }

    public MongoDbStorage(MongoClient client, String databaseName, String collectionName, String gridFsBucketName, long maxTimeMs, long maxTimeMsLongRunning) throws InternalErrorCordraException {
        initialize(client, databaseName, collectionName, gridFsBucketName, maxTimeMs, maxTimeMsLongRunning);
    }

    public MongoDbStorage(JsonObject options) throws InternalErrorCordraException {
        try {
            if (options.has("useLegacyIdField")) {
                useLegacyIdField = options.get("useLegacyIdField").getAsBoolean();
            }
            MongoClientSettings.Builder settings = MongoClientSettings.builder();
            String connectionUri = JsonUtil.getAsStringOrNull(options, "connectionUri");
            if (connectionUri != null) {
                settings.applyConnectionString(new ConnectionString(connectionUri));
            }
            @SuppressWarnings("resource")
            MongoClient mongoClient = MongoClients.create(settings.build());
            String maxTimeMsString = JsonUtil.getAsStringOrNull(options, "maxTimeMs");
            long maxTimeMsOption = -1;
            if (maxTimeMsString != null) {
                maxTimeMsOption = Long.parseLong(maxTimeMsString);
            }
            String maxTimeMsLongRunningString = JsonUtil.getAsStringOrNull(options, "maxTimeMsLongRunning");
            long maxTimeMsLongRunningOption = -1;
            if (maxTimeMsLongRunningString != null) {
                maxTimeMsLongRunningOption = Long.parseLong(maxTimeMsLongRunningString);
            }
            String databaseName = JsonUtil.getAsStringOrNull(options, "databaseName");
            String collectionName = JsonUtil.getAsStringOrNull(options, "collectionName");
            String gridFsBucketName = JsonUtil.getAsStringOrNull(options, "gridFsBucketName");
            initialize(mongoClient, databaseName, collectionName, gridFsBucketName, maxTimeMsOption, maxTimeMsLongRunningOption);
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @SuppressWarnings("hiding")
    private void initialize(MongoClient client, String databaseName, String collectionName, String gridFsBucketName, long maxTimeMs, long maxTimeMsLongRunning) throws InternalErrorCordraException {
        try {
            this.maxTimeMs = maxTimeMs < 0 ? DEFAULT_MAX_TIME_MS : maxTimeMs;
            this.maxTimeMsLongRunning = maxTimeMsLongRunning < 0 ? DEFAULT_MAX_TIME_MS_LONG_RUNNING : maxTimeMsLongRunning;
            this.client = client;
            this.db = this.client.getDatabase(databaseName == null ? "cordra" : databaseName);
            this.collection = db.getCollection(collectionName == null ? "cordra" : collectionName);
    
            if (useLegacyIdField) {
                IndexOptions legacyIdIndexOptions = new IndexOptions();
                legacyIdIndexOptions.unique(true);
                this.collection.createIndex(new Document("id", 1), legacyIdIndexOptions);
            }
    
            IndexOptions typeIndexOptions = new IndexOptions();
            typeIndexOptions.unique(false);
            typeIndexOptions.background(true);
            this.collection.createIndex(new Document("type", 1), typeIndexOptions);
    
            if (gridFsBucketName == null) gridFsBucketName = "fs";
            gridFSBucket = GridFSBuckets.create(db, gridFsBucketName);
            fsCollection = db.getCollection(gridFsBucketName + ".files");
    
            IndexOptions fsIdIndexOptions = new IndexOptions();
            fsIdIndexOptions.unique(false);
            fsCollection.createIndex(new Document("metadata.id", 1), fsIdIndexOptions);
    
            Document fsIdNameIndex = new Document().append("metadata.id", 1).append("metadata.name", 1);
    
            IndexOptions fsIdNameIndexOptions = new IndexOptions();
            fsIdNameIndexOptions.unique(true);
            fsCollection.createIndex(fsIdNameIndex, fsIdNameIndexOptions);
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private Document getIdQueryDocument(String id) {
        if (useLegacyIdField) {
            return new Document("id", id);
        } else {
            return new Document("_id", id);
        }
    }

    @Override
    public CordraObject get(String id) throws CordraException {
        try {
            Document query = getIdQueryDocument(id);
            Document doc = collection.find(query).maxTime(maxTimeMs, TimeUnit.MILLISECONDS).first();
            if (doc == null) {
                return null;
            }
            CordraObject d = documentToCordraObject(doc, useLegacyIdField);
            return d;
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public SearchResults<CordraObject> get(Collection<String> ids) throws CordraException {
        try {
            Map<String, CordraObject> resultsMap = new HashMap<>();
            Document query;
            if (useLegacyIdField) {
                query = new Document("id", new Document("$in", ids));
            } else {
                query = new Document("_id", new Document("$in", ids));
            }
            FindIterable<Document> findIter = collection.find(query).maxTime(maxTimeMsLongRunning, TimeUnit.MILLISECONDS);
            try (MongoCursor<Document> cursor = findIter.iterator()) {
                while (cursor.hasNext()) {
                    CordraObject co = documentToCordraObject(cursor.next(), useLegacyIdField);
                    resultsMap.put(co.id, co);
                }
            }
            return new SearchResultsFromStream<>(resultsMap.size(), ids.stream().map(resultsMap::get).filter(Objects::nonNull));
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    static Document cordraObjectToDocument(CordraObject d, boolean useLegacyIdField) {
        JsonObject obj = gson.toJsonTree(d).getAsJsonObject();
        Document doc = (Document) MongoDbUtil.jsonElementToMongoObject(obj);
        if (!useLegacyIdField) {
            if (d.id != null) {
                doc.append("_id", d.id);
                doc.remove("id");
            }
        }
        return doc;
    }

    static CordraObject documentToCordraObject(Document doc, boolean useLegacyIdField) {
        if (!useLegacyIdField) {
            if (doc.containsKey("_id")) {
                String id = doc.getString("_id");
                doc.append("id", id);
                doc.remove("_id");
            }
        }
        JsonElement el = MongoDbUtil.mongoObjectToJsonElement(doc);
        JsonObject obj = el.getAsJsonObject();
        CordraObject d = gson.fromJson(obj, CordraObject.class);
        return d;
    }

    @Override
    public InputStream getPayload(String id, String payloadName) throws CordraException {
        try {
            Document metadata = new Document();
            metadata.append("id", id).append("name", payloadName);
            Document query = new Document("metadata", metadata);
    
            com.mongodb.client.gridfs.model.GridFSFile f = gridFSBucket.find(query).maxTime(maxTimeMs, TimeUnit.MILLISECONDS).first();
            if (f == null) {
                return null;
            }
            ObjectId objectId = f.getObjectId();
            GridFSDownloadStream downloadStream = gridFSBucket.openDownloadStream(objectId);
            return downloadStream;
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public InputStream getPartialPayload(String id, String payloadName, Long start, Long end) throws CordraException {
        try {
            Document metadata = new Document();
            metadata.append("id", id).append("name", payloadName);
            Document query = new Document("metadata", metadata);
    
            com.mongodb.client.gridfs.model.GridFSFile f = gridFSBucket.find(query).maxTime(maxTimeMs, TimeUnit.MILLISECONDS).first();
            if (f == null) {
                return null;
            }
            ObjectId objectId = f.getObjectId();
            GridFSDownloadStream downloadStream = gridFSBucket.openDownloadStream(objectId);
            if (start == null && end == null) {
                return downloadStream;
            }
            if (start == null) {
                long size = f.getLength();
                start = size - end;
                if (start <= 0) return downloadStream;
                return new LimitedInputStream(downloadStream, start, Long.MAX_VALUE);
            }
            if (end == null) {
                return new LimitedInputStream(downloadStream, start, Long.MAX_VALUE);
            }
            long length = end - start + 1;
            if (length < 0) length = 0;
            return new LimitedInputStream(downloadStream, start, length);
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public CordraObject create(CordraObject d) throws CordraException {
        try {
            if (d.payloads != null) {
                Document documentQuery = getIdQueryDocument(d.id);
                Document foundDocument = collection.find(documentQuery).maxTime(maxTimeMs, TimeUnit.MILLISECONDS).first();
                if (foundDocument != null) {
                    throw new ConflictCordraException("Object already exists: " + d.id);
                }
                for (Payload p : d.payloads) {
                    try (InputStream in = p.getInputStream();) {
                        long length = writeInputStreamToGridFS(in, d.id, p.name);
                        p.size = length;
                    } catch (IOException e) {
                        throw new InternalErrorCordraException(e);
                    } finally {
                        p.setInputStream(null);
                    }
                }
                if (d.payloads.isEmpty()) {
                    d.payloads = null;
                }
            }
            Document doc = cordraObjectToDocument(d, useLegacyIdField);
            try {
                collection.insertOne(doc);
            } catch (MongoWriteException e) {
                WriteError error = e.getError();
                ErrorCategory category = error.getCategory();
                if (category == ErrorCategory.DUPLICATE_KEY) {
                    throw new ConflictCordraException("Object already exists: " + d.id);
                }
                throw new InternalErrorCordraException(e);
            }
            return d;
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private long writeInputStreamToGridFS(InputStream in, String id, String payloadName) throws InternalErrorCordraException {
        try {
            GridFSUploadOptions options = new GridFSUploadOptions().metadata(new Document().append("id", id).append("name", payloadName));
            ObjectId fileId = gridFSBucket.uploadFromStream(payloadName, in, options);
            long length = getFileSizeWithFind(fileId);
            return length;
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private long getFileSizeWithFind(ObjectId fileId) throws InternalErrorCordraException {
        try {
            Document query = new Document("_id", fileId);
            GridFSFile f = gridFSBucket.find(query).maxTime(maxTimeMs, TimeUnit.MILLISECONDS).first();
            return f.getLength();
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public CordraObject update(CordraObject d) throws CordraException {
        try {
            Document documentQuery = getIdQueryDocument(d.id);
            if (isUpdateChangingPayloads(d)) {
                // only do this find if we are changing payloads
                Document foundDocument = collection.find(documentQuery).maxTime(maxTimeMs, TimeUnit.MILLISECONDS).first();
                if (foundDocument == null) {
                    throw new NotFoundCordraException("Missing object: " + d.id);
                }
                List<String> payloadsToDelete = d.getPayloadsToDelete();
                String id = d.id;
                if (payloadsToDelete != null) {
                    for (String payloadName : payloadsToDelete) {
                        Document metadata = new Document();
                        metadata.append("id", id).append("name", payloadName);
                        Document query = new Document("metadata", metadata);
                        com.mongodb.client.gridfs.model.GridFSFile f = gridFSBucket.find(query).maxTime(maxTimeMs, TimeUnit.MILLISECONDS).first();
                        if (f == null) {
                            // shouldn't happen, but we can ignore since we were deleting anyway
                            logger.warn("Unexpected missing GridFSFile " + id + " " + payloadName);
                        } else {
                            ObjectId objectId = f.getObjectId();
                            logger.info("deleting file " + objectId.toString());
                            gridFSBucket.delete(objectId);
                        }
                    }
                }
                d.clearPayloadsToDelete();
                if (d.payloads != null) {
                    for (Payload p : d.payloads) {
                        if (p.getInputStream() != null) {
                            Document metadata = new Document();
                            metadata.append("id", id).append("name", p.name);
                            Document query = new Document("metadata", metadata);
                            com.mongodb.client.gridfs.model.GridFSFile f = gridFSBucket.find(query).maxTime(maxTimeMs, TimeUnit.MILLISECONDS).first();
                            if (f != null) {
                                ObjectId objectId = f.getObjectId();
                                gridFSBucket.delete(objectId);
                            }
                            try (InputStream in = p.getInputStream();) {
                                long length = writeInputStreamToGridFS(in, d.id, p.name);
                                p.size = length;
                            } catch (IOException e) {
                                throw new InternalErrorCordraException(e);
                            } finally {
                                p.setInputStream(null);
                            }
                        }
                    }
                }
            }
            if (d.payloads != null) {
                if (d.payloads.isEmpty()) {
                    d.payloads = null;
                }
            }
            Document doc = cordraObjectToDocument(d, useLegacyIdField);
            Document foundDocument = collection.findOneAndReplace(documentQuery, doc, new FindOneAndReplaceOptions().maxTime(maxTimeMs, TimeUnit.MILLISECONDS));
            if (foundDocument == null) {
                throw new NotFoundCordraException("Missing object: " + d.id);
            }
            return d;
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private boolean isUpdateChangingPayloads(CordraObject d) {
        List<String> payloadsToDelete = d.getPayloadsToDelete();
        if (payloadsToDelete != null && !payloadsToDelete.isEmpty()) {
            return true;
        }
        if (d.payloads != null) {
            for (Payload p : d.payloads) {
                if (p.getInputStream() != null) {
                    //There is at least one payload with a new stream
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void delete(String id) throws CordraException {
        try {
            Document documentQuery = getIdQueryDocument(id);
            Document foundDocument = collection.findOneAndDelete(documentQuery, new FindOneAndDeleteOptions().maxTime(maxTimeMs, TimeUnit.MILLISECONDS));
            if (foundDocument == null) {
                throw new NotFoundCordraException("Missing object: " + id);
            }
    
            Document query = new Document("metadata.id", id);
    
            GridFSFindIterable iter = gridFSBucket.find(query).maxTime(maxTimeMs, TimeUnit.MILLISECONDS);
            MongoCursor<com.mongodb.client.gridfs.model.GridFSFile> cursor = iter.iterator();
            try {
                while (cursor.hasNext()) {
                    com.mongodb.client.gridfs.model.GridFSFile f = cursor.next();
                    ObjectId objectId = f.getObjectId();
                    gridFSBucket.delete(objectId);
                }
            } finally {
                cursor.close();
            }
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public SearchResults<CordraObject> list() throws CordraException {
        return new MongoDbListSearchResults();
    }

    @Override
    public SearchResults<String> listHandles() throws CordraException {
        return new MongoDbListHandlesSearchResults();
    }

    @Override
    public SearchResults<CordraObject> listByType(List<String> types) throws CordraException {
        Document query = new Document("type", new Document("$in", types));
        return new MongoDbListSearchResults(query);
    }

    @Override
    public SearchResults<String> listHandlesByType(List<String> types) throws CordraException {
        Document query = new Document("type", new Document("$in", types));
        return new MongoDbListHandlesSearchResults(query);
    }

    public SearchResults<CordraObject> directSearch(Document query) throws InternalErrorCordraException {
        return new MongoDbListSearchResults(query);
    }

    public SearchResults<String> directSearchHandles(Document query) throws InternalErrorCordraException {
        return new MongoDbListHandlesSearchResults(query);
    }

    @Override
    public void close() throws CordraException {
        try {
            client.close();
        } catch (MongoException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private class MongoDbListSearchResults extends AbstractSearchResults<CordraObject> {
        private final MongoCursor<Document> cursor;

        public MongoDbListSearchResults(Bson query) throws InternalErrorCordraException {
            try {
                FindIterable<Document> findIter = collection.find(query).maxTime(maxTimeMsLongRunning, TimeUnit.MILLISECONDS);
                cursor = findIter.iterator();
            } catch (MongoException e) {
                throw new InternalErrorCordraException(e);
            }
        }

        public MongoDbListSearchResults() throws InternalErrorCordraException {
            try {
                FindIterable<Document> findIter = collection.find(new Document()).maxTime(maxTimeMsLongRunning, TimeUnit.MILLISECONDS);
                cursor = findIter.iterator();
            } catch (MongoException e) {
                throw new InternalErrorCordraException(e);
            }
        }

        @Override
        public int size() {
            return -1;
        }

        @Override
        protected CordraObject computeNext() {
            try {
                if (!cursor.hasNext()) return null;
                return documentToCordraObject(cursor.next(), useLegacyIdField);
            } catch (MongoException e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        }

        @Override
        protected void closeOnlyOnce() {
            try {
                cursor.close();
            } catch (MongoException e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        }
    }

    private static final Document LEGACY_ID_PROJECTION = new Document().append("id", 1).append("_id", 0);
    private static final Document ID_PROJECTION = new Document().append("_id", 1);

    private class MongoDbListHandlesSearchResults extends AbstractSearchResults<String> {
        private final MongoCursor<Document> cursor;

        public MongoDbListHandlesSearchResults(Bson query) throws InternalErrorCordraException {
            try {
                Document idProjection = getIdProjection();
                FindIterable<Document> findIter = collection.find(query).projection(idProjection).maxTime(maxTimeMsLongRunning, TimeUnit.MILLISECONDS);
                cursor = findIter.iterator();
            } catch (MongoException e) {
                throw new InternalErrorCordraException(e);
            }
        }

        private Document getIdProjection() {
            Document idProjection;
            if (useLegacyIdField) {
                idProjection = LEGACY_ID_PROJECTION;
            } else {
                idProjection = ID_PROJECTION;
            }
            return idProjection;
        }


        public MongoDbListHandlesSearchResults() throws InternalErrorCordraException {
            try {
                Document idProjection = getIdProjection();
                FindIterable<Document> findIter = collection.find(new Document()).projection(idProjection).maxTime(maxTimeMsLongRunning, TimeUnit.MILLISECONDS);
                cursor = findIter.iterator();
            } catch (MongoException e) {
                throw new InternalErrorCordraException(e);
            }
        }

        @Override
        public int size() {
            return -1;
        }

        @Override
        protected String computeNext() {
            try {
                if (!cursor.hasNext()) return null;
                if (useLegacyIdField) {
                    return cursor.next().getString("id");
                } else {
                    return cursor.next().getString("_id");
                }
            } catch (MongoException e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        }

        @Override
        protected void closeOnlyOnce() {
            try {
                cursor.close();
            } catch (MongoException e) {
                throw new UncheckedCordraException(new InternalErrorCordraException(e));
            }
        }
    }
}
