package net.cnri.cordra;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.*;
import net.cnri.cordra.model.FileMetadataResponse;
import net.cnri.cordra.model.Range;
import net.cnri.cordra.model.Version;
import net.cnri.cordra.operations.GetRelationshipsOperation;
import net.cnri.cordra.operations.versions.GetVersionsOperation;
import net.cnri.cordra.operations.versions.PublishVersionOperation;
import net.cnri.cordra.relationships.Relationships;
import net.cnri.cordra.util.HttpUtil;
import net.cnri.cordra.util.JacksonUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

import org.slf4j.MDC;

public class InternalCordraClient implements HooksSupportCordraClient, DefaultingFromCallResponseHandlerCordraClient {

    private final CordraService cordra;
    private final InternalAuthenticator authenticator;

    private volatile ExecutorService callResponseHandlerExecServ;

    public InternalCordraClient(CordraService cordra) {
        this.cordra = cordra;
        this.authenticator = new InternalAuthenticator(cordra);
    }

    private void throwIfInsideTheObjectLock(String label) throws CordraException {
        if (isInsideTheObjectLock()) {
            throw new BadRequestCordraException(label + " is not available when inside the object lock");
        }
    }

    private boolean isInsideTheObjectLock() {
        String insideObjectLock = MDC.get("insideObjectLock");
        if ("true".equals(insideObjectLock)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public ExecutorService getCallResponseHandlerExecutorService() {
        if (callResponseHandlerExecServ != null) return callResponseHandlerExecServ;
        synchronized (this) {
            if (callResponseHandlerExecServ != null) return callResponseHandlerExecServ;
            // this is Executors.newCachedThreadPool(), only modified to fix the thread-locals
            callResponseHandlerExecServ = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>()) {
                @Override
                public void execute(Runnable command) {
                    Map<String, String> contextMap = MDC.getCopyOfContextMap();
                    RequestContext requestContext = RequestContextHolder.get();
                    super.execute(() -> {
                        contextMap.forEach(MDC::put);
                        RequestContextHolder.set(requestContext);
                        try {
                            command.run();
                        } finally {
                            MDC.clear();
                            RequestContextHolder.clear();
                        }
                    });
                }
            };
            return callResponseHandlerExecServ;
        }
    }

    @Override
    public CordraObject get(String id, Options options) throws CordraException {
        //Does Not consider jsonPointer
        //Does consider options.filter, filter must be in full form
        //Does NOT consider options.full, that must be handled at the servlet level

        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co;
            if (options.filter != null) {
                boolean isFull = true;
                JsonElement jsonElement = cordra.getObjectFilterByJsonPointers(id, authResult, options.filter, isFull);
                co = getGson().fromJson(jsonElement, CordraObject.class);
            } else {
                co = cordra.getContentPlusMetaWithPostProcessing(id, authResult);
            }
            if (options.includeResponseContext) {
                co = CordraService.copyOfCordraObjectWithNewResponseContext(co);
                co.responseContext.addProperty("permission", cordra.getAclEnforcer().permittedOperations(authResult, co).toString());
            }
            return co;
        } catch (NotFoundCordraException nfce) {
            return null;
        }
    }

    @Override
    public CordraObject getType(String type) throws CordraException {
        return clone(cordra.getSchemaObject(type));
    }

    @Override
    public CordraObject getDesign() throws CordraException {
        return clone(cordra.getDesignCordraObjectFromCache());
    }

    @Override
    public CordraObject getWithBypassHooks(String id) throws CordraException {
        return cordra.getCordraObjectWithNoInternalMetadata(AuthenticationResult.admin(), id);
    }

    @Override
    public InputStream getPayloadWithBypassHooks(String id, String payloadName, Long start, Long end) throws CordraException {
        return cordra.getPayloadByNameOrNull(id, payloadName, start, end);
    }

    private static CordraObject clone(CordraObject co) {
        Gson gson = GsonUtility.getGson();
        CordraObject clone = gson.fromJson(gson.toJsonTree(co), CordraObject.class);
        return clone;
    }

    public JsonElement getJsonAtPointer(String id, String jsonPointer, Options options) throws CordraException {
        //works on Full CordraObject, servlet responsible for modifying jsonPointer to full form.
        //New method
        AuthenticationResult authResult = authenticator.authenticate(options);
        JsonElement jsonElement = cordra.getAtJsonPointer(id, authResult, jsonPointer);
        return jsonElement;
    }

    public CordraObject updateAtJsonPointer(String objectId, String jsonPointer, JsonElement replacementJsonData, Options options) throws CordraException {
        throwIfInsideTheObjectLock("updateAtJsonPointer");
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co = cordra.modifyObjectAtJsonPointer(objectId, jsonPointer, replacementJsonData.toString(), authResult, options.isDryRun);
            co = CordraService.copyOfCordraObjectRemovingInternalMetadata(co);
            if (cordra.getDesign().useLegacySkipOnObjectResolutionForCreateAndUpdate != Boolean.TRUE) {
                co = cordra.postProcess(authResult, co, false, true);
            }
            return co;
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        }
    }

    public JsonAndMediaType getJsonAndMediaTypeAtPointer(String id, String jsonPointer, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        JsonElement jsonElement = cordra.getAtJsonPointer(id, authResult, jsonPointer);
        CordraObject co = cordra.getCordraObject(authResult, id);
        String mediaType = cordra.getMediaType(co.type, JacksonUtil.gsonToJackson(co.content), jsonPointer);
        return new JsonAndMediaType(jsonElement, mediaType);
    }

    public static class JsonAndMediaType {
        public final JsonElement json;
        public final String mediaType;

        public JsonAndMediaType(JsonElement json, String mediaType) {
            this.json = json;
            this.mediaType = mediaType;
        }
    }

    public FileMetadataResponse getPayloadMetadata(String id, String payloadName, Options options) throws CordraException {
        //New method
        AuthenticationResult authResult = authenticator.authenticate(options);
        return cordra.getPayloadMetadata(id, payloadName, authResult);
    }

    @Override
    public void getPartialPayload(String id, String payloadName, Long start, Long end, CallResponseHandler handler, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co = cordra.getCordraObjectForPayload(id, payloadName, start, end, authResult);
            Payload payload = CordraService.getCordraObjectPayloadByName(co, payloadName);
            if (payload == null) {
                throw new NotFoundCordraException("No payload " + payloadName + " in object " + id);
            }
            if (setPayloadResponseHeadersReturnRangeOkay(handler, payload, start, end)) {
                DirectIo directIo = new DirectIoForCallResponseHandler(handler);
                // Note that authorization happened at the previous call to CordraService
                cordra.streamPayload(co, payload, new Range(start, end), authResult, directIo);
            }
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private boolean setPayloadResponseHeadersReturnRangeOkay(CallResponseHandler handler, Payload payload, Long start, Long end) {
        Range range = new Range(start, end);
        String mediaType = payload.mediaType;
        String filename = payload.filename;
        long size = payload.size;
        if (size == 0) {
            // ignore range request for empty payload
            handler.setLength(0);
        } else if (size > 0) {
            if (!range.isPartial()) {
                handler.setLength(size);
            } else {
                if (!range.isSatisfiable(size)) {
                    handler.setRange(true, true, null, null, size);
                    return false;
                }
                range = range.withSize(size);
                if (!range.isPartial()) {
                    handler.setLength(size);
                } else {
                    handler.setRange(true, false, range.getStart(), range.getEnd(), size);
                    handler.setLength(range.getEnd() - range.getStart() + 1);
                }
            }
        } else if (size < 0) {
            // unknown size
            if (range.isPartial()) {
                handler.setRange(true, false, null, null, null);
            }
        }
        if (mediaType == null) {
            mediaType = "application/octet-stream";
        }
        handler.setFilename(filename);
        handler.setMediaType(mediaType);
        return true;
    }

    @Override
    public CordraObject create(CordraObject d, Options options) throws CordraException {
        throwIfInsideTheObjectLock("create");
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co = cordra.writeJsonAndPayloadsIntoCordraObjectIfValid(d.type, d.getContentAsString(), d.acl, d.userMetadata, d.payloads, d.id, authResult, options.isDryRun);
            co = CordraService.copyOfCordraObjectRemovingInternalMetadata(co);
            if (cordra.getDesign().useLegacySkipOnObjectResolutionForCreateAndUpdate != Boolean.TRUE) {
                co = cordra.postProcess(authResult, co, true, false);
            }
            if (options.includeResponseContext) {
                co = CordraService.copyOfCordraObjectWithNewResponseContext(co);
                co.responseContext.addProperty("permission", cordra.getAclEnforcer().permittedOperations(authResult, co).toString());
            }
            return co;
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        }
    }

    @Override
    public CordraObject update(CordraObject d, Options options) throws CordraException {
        throwIfInsideTheObjectLock("update");
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co = cordra.writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(d.id, d.type, d.getContentAsString(), d.acl, d.userMetadata, d.payloads, authResult, d.getPayloadsToDelete(), options.isDryRun);
            co = CordraService.copyOfCordraObjectRemovingInternalMetadata(co);
            if (cordra.getDesign().useLegacySkipOnObjectResolutionForCreateAndUpdate != Boolean.TRUE) {
                co = cordra.postProcess(authResult, co, false, true);
            }
            if (options.includeResponseContext) {
                co = CordraService.copyOfCordraObjectWithNewResponseContext(co);
                co.responseContext.addProperty("permission", cordra.getAclEnforcer().permittedOperations(authResult, co).toString());
            }
            return co;
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        }
    }

    @Override
    public List<String> listMethods(String objectId, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        return cordra.listMethodsForUser(authResult, null, objectId, false, options.includeCrud);
    }

    @Override
    public List<String> listMethodsForType(String type, boolean isStatic, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        return cordra.listMethodsForUser(authResult, type, null, isStatic, options.includeCrud);
    }

    @Override
    public void call(String objectId, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException {
        throwIfInsideTheObjectLock("call");
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            String result = cordra.call(objectId, null, authResult, methodName, new DirectIoForCall(handler, input, options), isGet(options), isCordraCallApi(options), options.attributes);
            if (result != null) {
                // don't set content-type for empty response
                handler.setMediaType("application/json");
                handler.getWriter().write(result);
            }
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public void callForType(String type, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            String result = cordra.call(null, type, authResult, methodName, new DirectIoForCall(handler, input, options), isGet(options), isCordraCallApi(options), options.attributes);
            if (result != null) {
                // don't set content-type for empty response
                handler.setMediaType("application/json");
                handler.getWriter().write(result);
            }
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private boolean isGet(Options options) {
        if (options instanceof InternalRequestOptions) {
            return ((InternalRequestOptions) options).isGet;
        } else {
            return false;
        }
    }

    private boolean isCordraCallApi(Options options) {
        if (options instanceof InternalRequestOptions) {
            return ((InternalRequestOptions) options).isCordraCallApi;
        } else {
            return false;
        }
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads, Options options) throws CordraException {
        throwIfInsideTheObjectLock("publishVersion");
        JsonObject attributes = new JsonObject();
        if (versionId != null) {
            attributes.addProperty("versionId", versionId);
        }
        attributes.addProperty("clonePayloads", clonePayloads);
        options.attributes = attributes;
        JsonObject params = null;
        JsonElement result = call(objectId, PublishVersionOperation.ID, params, options);
        VersionInfo versionInfo = GsonUtility.getGson().fromJson(result, VersionInfo.class);
        return versionInfo;
    }

    @Override
    public List<VersionInfo> getVersionsFor(String id, Options options) throws CordraException {
        JsonElement versionInfosJson = call(id, GetVersionsOperation.ID, new JsonObject(), options);
        List<VersionInfo> versionInfos = GsonUtility.getGson().fromJson(versionInfosJson, new TypeToken<List<VersionInfo>>(){}.getType());
        return versionInfos;
    }

    @Override
    public void delete(String id, Options options) throws CordraException {
        throwIfInsideTheObjectLock("delete");
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.delete(id, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        }
    }

    public void deletePayload(String id, String payloadName, Options options) throws CordraException {
        throwIfInsideTheObjectLock("deletePayload");
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.deletePayload(id, payloadName, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public void deleteJsonAtPointer(String id, String jsonPointer, Options options) throws CordraException {
        //New method
        //works on content CordraObject, servlet responsible for ensuring the full option is not set.
        throwIfInsideTheObjectLock("deleteJsonAtPointer");
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.deleteJsonPointer(id, jsonPointer, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public SearchResults<CordraObject> list(Options options) throws CordraException {
        throw new UnsupportedOperationException();
    }

    @Override
    public SearchResults<String> listHandles(Options options) throws CordraException {
        throw new UnsupportedOperationException();
    }

    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        cordra.ensureIndexUpToDate();
        boolean excludeVersions = !params.getIncludeVersions();
        boolean isPostProcess = true;
        SearchResults<CordraObject> results = cordra.searchWithQueryCustomizationAndRestriction(query, params,
            isPostProcess, authResult, excludeVersions);
        return results;
    }

    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        cordra.ensureIndexUpToDate();
        boolean excludeVersions = !params.getIncludeVersions();
        boolean isPostProcess = true;
        SearchResults<String> results = cordra.searchHandlesWithQueryCustomizationAndRestriction(query, params,
            isPostProcess, authResult, excludeVersions);
        return results;
    }

    public AuthenticationResult internalAuthenticate(Options options) throws CordraException {
        return authenticator.authenticate(options);
    }

    @Override
    public AuthResponse authenticateAndGetResponse(Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        List<String> typesPermittedToCreate = null;
        if (options.full) {
            typesPermittedToCreate = cordra.getTypesPermittedToCreate(authResult);
        }
        return new AuthResponse(authResult.active, authResult.userId, authResult.username, typesPermittedToCreate, authResult.groupIds, authResult.exp);
    }

    @Override
    public AuthTokenResponse getAuthToken(Options options) throws CordraException {
        return getAuthToken(options, null);
    }

    public AuthTokenResponse getAuthToken(Options options, Function<Map<String, Object>, String> sessionCreator)  throws CordraException {
        AuthenticationResult authResult;
        try {
            authResult = authenticator.authenticate(options);
            if (authResult.anonymous) {
                throw invalidAuthRequestException();
            }
        } catch (UnauthorizedCordraException e) {
            JsonObject errorObject = new JsonObject();
            errorObject.addProperty("error", "invalid_grant");
            errorObject.addProperty("error_description", "Authentication failed");
            errorObject.addProperty("message", "Authentication failed");
            if (e.isPasswordChangeRequired()) {
                errorObject.addProperty("passwordChangeRequired", true);
            }
            throw new BadRequestCordraException(errorObject);

        }
        String token = cordra.getToken(authResult, sessionCreator);
        List<String> typesPermittedToCreate = null;
        if (options.full) {
            typesPermittedToCreate = cordra.getTypesPermittedToCreate(authResult);
        }
        return new AuthTokenResponse(authResult.active, token, authResult.userId, authResult.username, typesPermittedToCreate, authResult.groupIds, authResult.exp);
    }

    @Override
    public AuthResponse introspectToken(Options options) throws CordraException {
        return introspectToken(options, null);
    }

    public AuthResponse introspectToken(Options options, Function<String, Function<String, Object>> sessionGetter) throws CordraException {
        if (options == null || options.token == null) throw invalidAuthRequestException();
        return cordra.introspectToken(options.token, options.full, sessionGetter);
    }

    @Override
    public void revokeToken(Options options) throws CordraException {
        revokeToken(options, null);
    }

    public void revokeToken(Options options, Consumer<String> sessionRevoker) throws CordraException {
        if (options == null || options.token == null) throw invalidAuthRequestException();
        cordra.revokeToken(options.token, sessionRevoker);
    }

    private static CordraException invalidAuthRequestException() {
        JsonObject errorObject = new JsonObject();
        errorObject.addProperty("error", "invalid_request");
        errorObject.addProperty("error_description", "Invalid request");
        errorObject.addProperty("message", "Invalid request");
        return new BadRequestCordraException(errorObject);
    }

    public void ensureAdmin(Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        if (!"admin".equals(authResult.userId)) {
            throw new ForbiddenCordraException("Forbidden");
        }
    }

    private InternalRequestOptions copyAsInternalForTesting(Options options) {
        InternalRequestOptions res = new InternalRequestOptions();
        res.authHeader = options.authHeader;
        res.authTokenInput = options.authTokenInput;
        res.doipClientId = options.doipClientId;
        res.doipAuthentication = options.doipAuthentication;
        res.userId = options.userId;
        res.username = options.username;
        res.password = options.password;
        res.asUserId = options.asUserId;
        res.privateKey = options.privateKey;
        res.token = options.token;
        res.isDryRun = options.isDryRun;
        res.full = options.full;
        res.useDefaultCredentials = options.useDefaultCredentials;
        res.reindexBatchLockObjects = options.reindexBatchLockObjects;
        res.includeCrud = options.includeCrud;
        res.includeResponseContext = options.includeResponseContext;
        res.requestContext = options.requestContext;
        res.callHeaders = options.callHeaders;
        res.filter = options.filter;
        return res;
    }

    @Override
    public void changePassword(String newPassword, Options options) throws CordraException {
        throwIfInsideTheObjectLock("changePassword");
        if (!(options instanceof InternalRequestOptions)) {
            // this is for testing
            InternalRequestOptions fixedOptions = copyAsInternalForTesting(options);
            fixedOptions.isChangePassword = true;
            options = fixedOptions;
        }
        AuthenticationResult authResult = authenticator.authenticate(options);
        if (!authResult.active || authResult.userId == null) {
            throw new ForbiddenCordraException("No user");
        }
        if (!"admin".equals(authResult.userId)) {
            // we require an actual Authorization: header, which is sure to have taken the place of any other session
            if (options instanceof PreAuthenticatedOptions && ((PreAuthenticatedOptions) options).isSession) {
                throw new ForbiddenCordraException("Authorization: header required, either Basic or Bearer with JWT for key-based authentication");
            }
        }
        try {
            cordra.updatePasswordForUser(newPassword, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public void reindexBatch(List<String> batch, Options options) throws CordraException  {
        if (options.reindexBatchLockObjects) {
            throwIfInsideTheObjectLock("reindexBatch with lock objects");
        }
        AuthenticationResult authResult = authenticator.authenticate(options);
        cordra.reindexBatchIds(batch, options.reindexBatchLockObjects, authResult);
    }

    public InitDataResponse getInitData(Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        InitDataResponse initDataResponse = new InitDataResponse();
        initDataResponse.version = Version.getInstance();
        initDataResponse.isActiveSession = authResult.active;
        initDataResponse.username = authResult.username;
        initDataResponse.userId = authResult.userId;
        initDataResponse.typesPermittedToCreate = cordra.getTypesPermittedToCreate(authResult);
        initDataResponse.design = cordra.getDesignAsUser(authResult);
        return initDataResponse;
    }

    public Relationships getRelationshipsFor(String objectId, boolean outboundOnly, Options options) throws CordraException {
        JsonObject params = new JsonObject();
        params.addProperty("outboundOnly", outboundOnly);
        JsonElement relationshipsJson = call(objectId, GetRelationshipsOperation.ID, params, options);
        Relationships relationships = GsonUtility.getGson().fromJson(relationshipsJson, Relationships.class);
        return relationships;
    }

    public CordraObject.AccessControlList getAclFor(String objectId, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        CordraObject.AccessControlList acl = cordra.getAclFor(objectId, authResult);
        return acl;
    }

    public void updateAcls(String objectId, CordraObject.AccessControlList sAcl, Options options) throws CordraException {
        throwIfInsideTheObjectLock("updateAcls");
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.updateAcls(objectId, sAcl, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        }
    }

    public void updateAllHandleRecords(Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.updateAllHandleRecords(authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        }
    }

    public AllHandlesUpdater.UpdateStatus getHandleUpdateStatus(Options options)  throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        AllHandlesUpdater.UpdateStatus status = cordra.getHandleUpdateStatus(authResult);
        return status;
    }

    public String getHandleForSuffix(String suffix) {
        return cordra.getHandleForSuffix(suffix);
    }

    public void throwIfLegacySessionsDisallowed() throws CordraException {
        if (cordra.getDesign().useLegacySessionsApi != Boolean.TRUE) {
            throw new UnauthorizedCordraException("Legacy sessions disabled on this Cordra");
        }
    }

    public boolean isDisableBackOffRequestParking() {
        return cordra.isDisableBackOffRequestParking();
    }

    @Override
    public void close() throws IOException, CordraException {
        if (callResponseHandlerExecServ != null) {
            callResponseHandlerExecServ.shutdown();
        }
    }

    private static class DirectIoForCallResponseHandler implements DirectIo {

        private final CallResponseHandler handler;

        public DirectIoForCallResponseHandler(CallResponseHandler handler) {
            this.handler = handler;
        }

        @Override
        public InputStream getInputAsInputStream() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public Reader getInputAsReader() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public OutputStream getOutputAsOutputStream() throws IOException {
            return handler.getOutputStream();
        }

        @Override
        public Writer getOutputAsWriter() throws IOException {
            return handler.getWriter();
        }

        @Override
        public String getInputMediaType() {
            throw new UnsupportedOperationException();
        }

        @Override
        public String getInputFilename() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setOutputMediaType(String mediaType) {
            handler.setMediaType(mediaType);
        }

        @Override
        public void setOutputFilename(String filename) {
            handler.setFilename(filename);
        }
    }

    private static class DirectIoForCall extends DirectIoForCallResponseHandler {

        private final InputStream in;
        private final Options options;

        private boolean gotInputStream;
        private Reader reader;

        public DirectIoForCall(CallResponseHandler handler, InputStream in, Options options) {
            super(handler);
            this.in = in;
            this.options = options;
        }

        @Override
        public InputStream getInputAsInputStream() throws IOException {
            if (reader != null) throw new IllegalStateException("Cannot use both getInputAsInputStream and getInputAsReader");
            gotInputStream = true;
            return in;
        }

        @Override
        public Reader getInputAsReader() throws IOException {
            if (reader != null) return reader;
            if (gotInputStream) throw new IllegalStateException("Cannot use both getInputAsInputStream and getInputAsReader");
            if (in == null) return null;
            String charset = HttpUtil.getCharset(getInputMediaType());
            reader = new InputStreamReader(in, charset);
            return reader;
        }

        @Override
        public String getInputMediaType() {
            if (options.callHeaders == null) return null;
            return options.callHeaders.mediaType;
        }

        @Override
        public String getInputFilename() {
            if (options.callHeaders == null) return null;
            return options.callHeaders.filename;
        }
    }

}
