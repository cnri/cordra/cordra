package net.cnri.cordra.hooks.java;

import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import net.cnri.cordra.CordraObjectHasher;
import net.cnri.cordra.CordraServiceFactory;
import net.cnri.cordra.CordraServiceHooks;
import net.cnri.cordra.CordraType;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.util.ChildFirstURLClassLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class CordraTypeJarLoader {
    private static Logger logger = LoggerFactory.getLogger(CordraTypeJarLoader.class);

    public static final String TEMP_FILE_PREFIX = "schema-jar";

    private final String jarHash;
    private final Path pathToJar;
    private final ClassLoader dataLibClassLoader;
    private URLClassLoader classLoader;
    private Map<String, Class<?>> typeToClassMap;
    private Class<?> serviceClass;

    public CordraTypeJarLoader(InputStream in, ClassLoader dataLibClassLoader) throws IOException {
        this.dataLibClassLoader = dataLibClassLoader;
        this.pathToJar = makeTempFile();
        this.jarHash = CordraObjectHasher.copyToTempFileReturnHash(in, pathToJar);
    }

    public void init() throws IOException, ClassNotFoundException, CordraException {
        Set<String> annotatedClasses = getClassesWithAnnotations(pathToJar.toString(), CordraType.class, CordraServiceHooks.class);
        ClassLoader parent = dataLibClassLoader != null ? dataLibClassLoader : getClass().getClassLoader();
        this.classLoader = createClassLoaderForJar(parent, pathToJar.toString());
        loadClasses(annotatedClasses);
        callOnLoadOnAllClasses();
    }

    public String getJarHash() { return jarHash; }

    public Class<?> getServiceClass() {
        return serviceClass;
    }

    public Class<?> getClassForType(String type) {
        if (type == null) return serviceClass;
        return typeToClassMap.get(type);
    }

    private static URLClassLoader createClassLoaderForJar(ClassLoader parent, String pathToJar) throws MalformedURLException {
        Path jar = Paths.get(pathToJar);
        URL[] urls = { jar.toUri().toURL() };
        URLClassLoader classLoader = new ChildFirstURLClassLoader(urls, parent, CordraServiceFactory::shouldLoadFromParent);
        return classLoader;
    }

    private static Path makeTempFile() throws IOException {
        Path tempFilePath = Files.createTempFile(TEMP_FILE_PREFIX, null);
        File file = tempFilePath.toFile();
        file.deleteOnExit();
        return tempFilePath;
    }

    public void loadClasses(Set<String> classNames) throws ClassNotFoundException {
        this.typeToClassMap = new HashMap<>();
        for (String className : classNames) {
            Class<?> c = classLoader.loadClass(className);
            if (this.serviceClass == null && c.getAnnotation(CordraServiceHooks.class) != null) {
                this.serviceClass = c;
            }
            CordraType type = c.getAnnotation(CordraType.class);
            if (type == null) continue;
            String name = type.name();
            if (name == null || name.isEmpty()) name = type.value();
            if (name == null || name.isEmpty()) name = c.getSimpleName();
            typeToClassMap.putIfAbsent(name, c);
            logger.trace("Loaded Java CordraType: " + name);
        }
    }

    /*
    This method uses javassist for direct bytecode inspection of the classes in the jar file to
    find those that have a particular class annotation
     */
    private static Set<String> getClassesWithAnnotations(String pathToJar, Class<?>... annotationClasses) throws IOException {
        Set<String> result = new HashSet<>();
        try (JarFile jar = new JarFile(pathToJar)) {
            for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements();) {
                JarEntry entry = entries.nextElement();
                String filename = entry.getName();
                if (filename.endsWith(".class")) {
                    try (
                        InputStream classIn = jar.getInputStream(entry);
                        InputStream bufIn = new BufferedInputStream(classIn);
                        DataInputStream dstream = new DataInputStream(bufIn);
                    ) {
                        ClassFile cf =  new ClassFile(dstream);
                        String className = cf.getName();
                        AnnotationsAttribute visible = (AnnotationsAttribute) cf.getAttribute(AnnotationsAttribute.visibleTag);
                        if (visible != null) {
                            for (javassist.bytecode.annotation.Annotation ann : visible.getAnnotations()) {
                                String annotationTypeName = ann.getTypeName();
                                for (Class<?> annotationClass : annotationClasses) {
                                    if (annotationClass.getName().equals(annotationTypeName)) {
                                        result.add(className);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    public void beforeClose() throws CordraException {
        callMethodOnAllClasses("onBeforeUnload");
    }

    private synchronized void callOnLoadOnAllClasses() throws CordraException {
        callMethodOnAllClasses("onLoad");
    }

    private synchronized void callUnloadOnAllClasses() throws CordraException {
        callMethodOnAllClasses("onUnload");
    }

    private void callMethodOnAllClasses(String methodName) throws CordraException {
        if (typeToClassMap != null) {
            for (Map.Entry<String, Class<?>> entry : typeToClassMap.entrySet()) {
                String type = entry.getKey();
                Class<?> c = entry.getValue();
                try {
                    Method m = c.getMethod(methodName);
                    if (m != null) {
                        Constructor<?> constructor = c.getDeclaredConstructor();
                        Object instance = constructor.newInstance();
                        m.invoke(instance);
                    }
                } catch (NoSuchMethodException e) {
                    //no-op
                } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    throw new InternalErrorCordraException(e);
                }
                logger.trace(methodName + " Java CordraType: " + type);
            }
        }
    }

    public synchronized void close() throws CordraException {
        try {
            if (typeToClassMap != null) callUnloadOnAllClasses();
            if (classLoader != null) classLoader.close();
            Files.deleteIfExists(pathToJar);
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

}
