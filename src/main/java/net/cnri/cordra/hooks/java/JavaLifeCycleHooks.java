package net.cnri.cordra.hooks.java;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.DefaultAcls;
import net.cnri.cordra.auth.RequestAuthenticationInfo;
import net.cnri.cordra.hooks.CallResult;
import net.cnri.cordra.hooks.HooksResult;
import net.cnri.cordra.hooks.LifeCycleHooks;
import net.cnri.cordra.hooks.LifeCycleHooksInterface;
import net.cnri.cordra.model.ObjectDelta;
import net.cnri.cordra.storage.CordraStorage;
import net.handle.hdllib.HandleValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class JavaLifeCycleHooks implements LifeCycleHooksInterface {

    private static final Logger logger = LoggerFactory.getLogger(JavaLifeCycleHooks.class);

    private final ClassLoader dataLibClassLoader;
    private final boolean traceRequests;
    private final DateTimeFormatter dateTimeFormatter = CordraService.dateTimeFormatter;
    private volatile ConcurrentMap<String, CordraTypeJarLoader> jarLoaders;
    private CordraStorage storage;

    public JavaLifeCycleHooks(boolean traceRequests, CordraStorage storage, ClassLoader dataLibClassLoader) {
        this.dataLibClassLoader = dataLibClassLoader;
        this.traceRequests = traceRequests;
        this.storage = storage;
        this.jarLoaders = new ConcurrentHashMap<>();
    }

    public synchronized void reloadAll(CordraObject designObject, List<CordraObject> cordraObjects) throws CordraException {
        List<CordraObject> allObjects = new ArrayList<>();
        allObjects.addAll(cordraObjects);
        if (designObject != null) {
            allObjects.add(designObject);
        }
        ConcurrentMap<String, CordraTypeJarLoader> newJarLoaders = new ConcurrentHashMap<>();
        Set<String> doNotClose = new HashSet<>();
        for (CordraObject co : allObjects) {
            CordraTypeJarLoader existingJarLoader = jarLoaders.get(co.id);
            if (existingJarLoader != null && isJarsMatch(co, existingJarLoader)) {
                //no need to reload since the jar hashes match
                newJarLoaders.put(co.id, existingJarLoader);
                doNotClose.add(co.id);
            } else {
                CordraTypeJarLoader newJarLoader = load(co);
                if (newJarLoader != null) {
                    if (existingJarLoader != null && isJarsMatch(newJarLoader, existingJarLoader)) {
                        newJarLoader.close();
                        newJarLoaders.put(co.id, existingJarLoader);
                        doNotClose.add(co.id);
                        continue;
                    }
                    if (existingJarLoader != null) {
                        //the existingJarLoader will be closed
                        existingJarLoader.beforeClose();
                    }
                    try {
                        newJarLoader.init();
                    } catch (CordraException | ClassNotFoundException | IOException e) {
                        throw new InternalErrorCordraException(e);
                    }
                    newJarLoaders.put(co.id, newJarLoader);
                }
            }
        }
        ConcurrentMap<String, CordraTypeJarLoader> oldJarLoaders = this.jarLoaders;
        this.jarLoaders = newJarLoaders;
        for (String id : oldJarLoaders.keySet()) {
            if (doNotClose.contains(id)) {
                continue;
            } else {
                CordraTypeJarLoader jarLoader = oldJarLoaders.get(id);
                jarLoader.close();
            }
        }
    }

    private boolean isJarsMatch(CordraObject co, CordraTypeJarLoader existingJarLoader) {
        String jarHash = getHashForPayload(co, "java");
        String loadedJarHash = existingJarLoader.getJarHash();
        if (jarHash != null && loadedJarHash != null) {
            if (jarHash.equals(loadedJarHash)) {
                return true;
            }
        }
        return false;
    }

    private boolean isJarsMatch(CordraTypeJarLoader newJarLoader, CordraTypeJarLoader existingJarLoader) {
        String newJarHash = newJarLoader.getJarHash();
        String loadedJarHash = existingJarLoader.getJarHash();
        if (newJarHash != null && loadedJarHash != null) {
            if (newJarHash.equals(loadedJarHash)) {
                return true;
            }
        }
        return false;
    }

    public synchronized void reload(CordraObject co) throws CordraException {
        CordraTypeJarLoader existingJarLoader = jarLoaders.get(co.id);
        if (existingJarLoader != null && isJarsMatch(co, existingJarLoader) ) {
            //no need to reload since the jar hash matches the existing loaded jar
            return;
        }
        CordraTypeJarLoader newJarLoader = load(co);
        if (newJarLoader != null) {
            if (existingJarLoader != null && isJarsMatch(newJarLoader, existingJarLoader)) {
                newJarLoader.close();
                return;
            }
            if (existingJarLoader != null) {
                existingJarLoader.beforeClose();
            }
            try {
                newJarLoader.init();
            } catch (IOException | ClassNotFoundException e) {
                throw new InternalErrorCordraException(e);
            }
            existingJarLoader = jarLoaders.put(co.id, newJarLoader);
        } else {
            existingJarLoader = jarLoaders.remove(co.id);
            if (existingJarLoader != null) {
                existingJarLoader.beforeClose();
            }
        }
        if (existingJarLoader != null) {
            existingJarLoader.close();
        }
    }

    private CordraTypeJarLoader load(CordraObject co) throws CordraException {
        Payload javaPayload = co.getPayload("java");
        if (javaPayload != null) {
            try (InputStream jarIn = storage.getPayload(co.id, "java")) {
                return new CordraTypeJarLoader(jarIn, dataLibClassLoader);
            } catch (IOException e) {
                throw new InternalErrorCordraException(e);
            }
        }
        return null;
    }

    private String getHashForPayload(CordraObject co, String payloadName) {
        if (co.metadata.hashes != null) {
            if (co.metadata.hashes.has("payloads")) {
                JsonObject payloads = co.metadata.hashes.getAsJsonObject("payloads");
                if (payloads.has(payloadName)) {
                    String hashForPayload = payloads.get(payloadName).getAsString();
                    return hashForPayload;
                }
            }
        }
        return null;
    }

    public void unload(CordraObject schemaCo) throws CordraException {
        CordraTypeJarLoader existingJarLoader = jarLoaders.remove(schemaCo.id);
        if (existingJarLoader != null) {
            existingJarLoader.beforeClose();
            existingJarLoader.close();
        }
    }

    @Override
    public boolean hasServiceHook(String hookName) throws CordraException {
        return hasHook(null, hookName);
    }

    @Override
    public boolean hasHook(String type, String hookName) {
        try {
            Class<?> c = getClassForType(type);
            if (c == null) {
                return false;
            }
            Method m;
            if (GET_AUTH_CONFIG.equals(hookName)) {
                m = getHookMethod(c, hookName, HooksContext.class);
            } else if (CUSTOMIZE_QUERY.equals(hookName)) {
                m = getHookMethod(c, hookName, String.class, HooksContext.class);
            } else if (CUSTOMIZE_QUERY_AND_PARAMS.equals(hookName)) {
                m = getHookMethod(c, hookName, SearchRequest.class, HooksContext.class);
            } else if (AUTHENTICATE.equals(hookName)) {
                m = getHookMethod(c, hookName, RequestAuthenticationInfo.class, HooksContext.class);
            } else {
                m = getHookMethod(c, hookName, CordraObject.class, HooksContext.class);
            }
            if (m == null) {
                return false;
            } else if (m.isDefault()) {
                return false; //Only indicate that we have the hook if the users sub class overrides the default member
            }
            return true;
        } catch (NoClassDefFoundError e) {
            return false;
        }
    }

    public boolean hasMethod(String type, String methodName, boolean isStatic) {
        Class<?> c = getClassForType(type);
        if (c == null) {
            return false;
        }
        Method m = null;
        if (isStatic) {
            m = getStaticMethod(c, methodName);
        } else {
            m = getInstanceMethod(c, methodName);
        }
        if (m == null) {
            return false;
        }
        return true;
    }

    @Override
    public HooksResult<Void> beforeStorage(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runLifeCycleHookReturningVoid(type, co, BEFORE_STORAGE, context);
    }

    @Override
    public HooksResult<Void> beforeDelete(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runLifeCycleHookReturningVoid(type, co, BEFORE_DELETE, context);
    }

    @Override
    public HooksResult<Void> afterDelete(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runLifeCycleHookReturningVoid(type, co, AFTER_DELETE, context);
    }

    @Override
    public HooksResult<Void> afterCreateOrUpdate(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runLifeCycleHookReturningVoid(type, co, AFTER_CREATE_OR_UPDATE, context);
    }

    @Override
    public HooksResult<Boolean> streamPayload(String type, CordraObject co, Map<String, Object> context, DirectIo directIo) throws CordraException {
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        Class<?> c = getClassForType(type);
        if (c == null) {
            return HooksResult.noSuchHook();
        }

        Method method = getHookMethod(c, ON_PAYLOAD_RESOLUTION, CordraObject.class, HooksContext.class);
        if (method == null) {
            return HooksResult.noSuchHook();
        } else {
            DirectIoOutputUsedWrapper directIoWrapper = new DirectIoOutputUsedWrapper(directIo);
            HooksContext hooksContext = createHooksContext(context, directIoWrapper);
            invokeMethodOnDefaultInstance(c, method, void.class, co, hooksContext);
            return HooksResult.of(directIoWrapper.hasUsedDirectOutput());
        }
    }

    @Override
    public List<String> listMethods(boolean isStatic, String type) throws CordraException {
        Class<?> c = getClassForType(type);
        if (c == null) {
            return Collections.emptyList();
        }
        List<String> result = new ArrayList<>();
        Method[] methods = c.getMethods();
        for (Method m : methods) {
            boolean isMethodStatic = Modifier.isStatic(m.getModifiers());
            Annotation ann = m.getAnnotation(CordraMethod.class);
            if (ann != null) {
                CordraMethod cordraMethod = (CordraMethod) ann;
                String[] names = getMethodNames(m, cordraMethod);
                if (isStatic && isMethodStatic) {
                    result.addAll(Arrays.asList(names));
                } else if (!isStatic && !isMethodStatic) {
                    result.addAll(Arrays.asList(names));
                }
            }
        }
        return result;
    }

    @Override
    public HooksResult<CallResult> call(String methodName, String type, boolean isStatic, String coJson, Map<String, Object> context, DirectIo directIo, boolean isGet) throws CordraException {
        String result = null;
        String before = null;
        String after = null;
        Class<?> c = getClassForType(type);
        if (c == null) {
//            throw new NotFoundCordraException("Schema does not have java");
            return HooksResult.noSuchHook();
        }
        Method method = null;
        if (isStatic) {
            method = getStaticMethod(c, methodName);
        } else {
            method = getInstanceMethod(c, methodName);
        }
        if (method == null) {
//            throw new NotFoundCordraException(type + " does not have a " + (isStatic ? "static" : "") + " method called " + method);
            return HooksResult.noSuchHook();
        }
        if (isGet) {
            if (!isAllowGet(method)) {
                throw new ForbiddenCordraException("Operation " + methodName + " does not allow GET");
            }
        }
        CordraObject obj = null;
        if (!isStatic) {
            obj = GsonUtility.getGson().fromJson(coJson, CordraObject.class);
            before = GsonUtility.getGson().toJson(obj);
        }
        DirectIoOutputUsedWrapper directIoWrapper = new DirectIoOutputUsedWrapper(directIo);
        HooksContext hooksContext = createHooksContext(context, directIoWrapper);
        if (!isStatic) {
            JsonElement resultJson = invokeMethodOnDefaultInstance(c, method, JsonElement.class, obj, hooksContext);
            if (!directIoWrapper.hasUsedDirectOutput()) {
                if (resultJson != null) {
                    result = GsonUtility.getGson().toJson(resultJson);
                }
            }
            after = GsonUtility.getGson().toJson(obj);
        } else {
            JsonElement resultJson = invokeMethodOnDefaultInstance(null, method, JsonElement.class, hooksContext);
            if (!directIoWrapper.hasUsedDirectOutput()) {
                if (resultJson != null) {
                    result = GsonUtility.getGson().toJson(resultJson);
                }
            }
        }
        return HooksResult.of(new CallResult(result, before, after));
    }

    private HooksContext createHooksContext(Map<String, Object> context, DirectIo directIo) {
        LifeCycleHooks.addRequestContextToContext(context);
        HooksContext hooksContext = HooksContext.from(context, directIo);
        return hooksContext;
    }

    @Override
    public HooksResult<CordraObject> onObjectResolution(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runLifeCycleHook(type, co, ON_OBJECT_RESOLUTION, context);
    }

    @Override
    public HooksResult<DefaultAcls> getAuthConfig(String type, Map<String, Object> context) throws CordraException {
        long start = System.currentTimeMillis();
        Class<?> c = getClassForType(type);
        if (c == null) {
            return HooksResult.noSuchHook();
        }
        HooksContext hooksContext = createHooksContext(context, null);
        try {
            Method method = getHookMethod(c, GET_AUTH_CONFIG, HooksContext.class);
            if (method != null) {
                DefaultAcls res = invokeMethodOnDefaultInstance(c, method, DefaultAcls.class, hooksContext);
                return HooksResult.of(res);
            }
            return HooksResult.noSuchHook();
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(GET_AUTH_CONFIG + " java LifeCycleHook: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    @Override
    public HooksResult<CordraObject> objectForIndexing(String type, CordraObject co) throws CordraException {
        Map<String, Object> context = new HashMap<>();
        context.put("objectId", co.id);
        CordraObject clone = CordraService.copyOfCordraObjectRemovingInternalMetadata(co); //TODO consider payloads
        HooksResult<CordraObject> result = runLifeCycleHook(type, clone, OBJECT_FOR_INDEXING, context);
        if (result.isPresent()) {
            CordraObject coResult = result.get();
            CordraService.restoreInternalMetadata(co, coResult); //Ensure the hook cannot change the internalMetadata
            coResult.id = co.id; //Ensure the hook cannot change the digital object id
            coResult = ObjectDelta.applyPayloadsToCordraObject(coResult, co.payloads);
        }
        return result;
    }

    @Override
    public HooksResult<String> customizeQuery(String query, Map<String, Object> context) throws CordraException {
        long start = System.currentTimeMillis();
        Class<?> c = getServiceClass();
        if (c == null) {
            return HooksResult.noSuchHook(); // unchanged
        }
        HooksContext hooksContext = createHooksContext(context, null);
        try {
            Method method = getHookMethod(c, CUSTOMIZE_QUERY, String.class, HooksContext.class);
            if (method != null) {
                String res = invokeMethodOnDefaultInstance(c, method, String.class, query, hooksContext);
                return HooksResult.of(res);
            }
            return HooksResult.noSuchHook(); // unchanged;
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("java " + CUSTOMIZE_QUERY + ": start " + startTime + ", " + delta + "ms");
            }
        }
    }

    @Override
    public HooksResult<SearchRequest> customizeQueryAndParams(SearchRequest queryAndParams, Map<String, Object> context) throws CordraException {
        long start = System.currentTimeMillis();
        Class<?> c = getServiceClass();
        if (c == null) {
            return HooksResult.noSuchHook(); // unchanged
        }
        HooksContext hooksContext = createHooksContext(context, null);
        try {
            Method method = getHookMethod(c, CUSTOMIZE_QUERY_AND_PARAMS, SearchRequest.class, HooksContext.class);
            if (method != null) {
                SearchRequest res = invokeMethodOnDefaultInstance(c, method, SearchRequest.class, queryAndParams, hooksContext);
                return HooksResult.of(res);
            }
            return HooksResult.noSuchHook(); // unchanged;
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("java " + CUSTOMIZE_QUERY_AND_PARAMS + ": start " + startTime + ", " + delta + "ms");
            }
        }
    }

    @Override
    public HooksResult<AuthenticationResult> authenticate(RequestAuthenticationInfo authInfo, Map<String, Object> context) throws CordraException {
        long start = System.currentTimeMillis();
        Class<?> c = getServiceClass();
        if (c == null) {
            return HooksResult.noSuchHook();
        }
        HooksContext hooksContext = createHooksContext(context, null);
        try {
            Method method = getHookMethod(c, AUTHENTICATE, RequestAuthenticationInfo.class, HooksContext.class);
            if (method != null) {
                AuthenticationResult res = invokeMethodOnDefaultInstance(c, method, AuthenticationResult.class, authInfo, hooksContext);
                return HooksResult.of(res);
            }
            return HooksResult.noSuchHook();
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("java " + AUTHENTICATE + ": start " + startTime + ", " + delta + "ms");
            }
        }
    }

    @Override
    public HooksResult<List<HandleValue>> createHandleValues(CordraObject co, Map<String, Object> context) throws CordraException {
        long start = System.currentTimeMillis();
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        Class<?> c = getServiceClass();
        if (c == null) {
            return HooksResult.noSuchHook();
        }
        HooksContext hooksContext = createHooksContext(context, null);
        try {
            Method method = getHookMethod(c, CREATE_HANDLE_VALUES, CordraObject.class, HooksContext.class);
            if (method != null) {
                List<?> output = invokeMethodOnDefaultInstance(c, method, List.class, co, hooksContext);
                @SuppressWarnings("unchecked")
                List<HandleValue> typedRes = (List<HandleValue>) output;
                return HooksResult.of(typedRes);
            }
            return HooksResult.noSuchHook();
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("java " + CREATE_HANDLE_VALUES + ": start " + startTime + ", " + delta + "ms");
            }
        }
    }

    @Override
    public HooksResult<CordraObject> generateIdWithLooping(String type, CordraObject co, Map<String, Object> context, ThrowingFunction<String, CordraObject> lockAndCreateMemoryObjectIfHandleAvailable) throws CordraException {
        Class<?> c = getClassForType(type);
        if (c == null) {
            return HooksResult.noSuchHook();
        }
        Method generateIdMethod = getHookMethod(c, GENERATE_ID, CordraObject.class, HooksContext.class);
        if (generateIdMethod == null) {
            return HooksResult.noSuchHook();
        }
        Method isGenerateIdLoopableMethod = getHookMethod(c, IS_GENERATE_ID_LOOPABLE);
        Object instance = createDefaultInstance(c);
        boolean isLoopable = false;
        if (isGenerateIdLoopableMethod != null) {
            isLoopable = invokeMethodOnInstance(instance, isGenerateIdLoopableMethod, Boolean.class) == Boolean.TRUE; // avoid NPE
        }
        HooksContext hooksContext = createHooksContext(context, null);
        String handle = invokeMethodOnInstance(instance, generateIdMethod, String.class, co, hooksContext);

        if (handle == null || handle.isEmpty()) return HooksResult.of(null);
        CordraObject result = lockAndCreateMemoryObjectIfHandleAvailable.apply(handle);
        if (result == null && !isLoopable) {
            throw new ConflictCordraException("Object already exists: " + handle);
        }
        if (isLoopable) {
            while (result == null) {
                handle = invokeMethodOnInstance(instance, generateIdMethod, String.class, co, hooksContext);
                if (handle == null || handle.isEmpty()) return HooksResult.of(null);
                result = lockAndCreateMemoryObjectIfHandleAvailable.apply(handle);
            }
        }
        return HooksResult.of(result);
    }

    @Override
    public void shutdown() {
        ConcurrentMap<String, CordraTypeJarLoader> oldJarLoaders = this.jarLoaders;
        if (oldJarLoaders == null) return;
        for (CordraTypeJarLoader jarLoader : oldJarLoaders.values()) {
            try {
                jarLoader.beforeClose();
                jarLoader.close();
            } catch (Exception e) {
                logger.error("Shutdown error", e);
            }
        }
    }

    private HooksResult<Void> runLifeCycleHookReturningVoid(String type, CordraObject co, String functionName, Map<String, Object> context) throws CordraException {
        long start = System.currentTimeMillis();
        Class<?> c = getClassForType(type);
        if (c == null) {
            return HooksResult.noSuchHook();
        }
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        HooksContext hooksContext = createHooksContext(context, null);
        try {
            Method method = getHookMethod(c, functionName, CordraObject.class, HooksContext.class);
            if (method != null) {
                invokeMethodOnDefaultInstance(c, method, void.class, co, hooksContext);
                return HooksResult.of(null);
            }
            return HooksResult.noSuchHook();
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(functionName + " java runLifeCycleHookReturningVoid: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    private HooksResult<CordraObject> runLifeCycleHook(String type, CordraObject co, String functionName, Map<String, Object> context) throws CordraException {
        long start = System.currentTimeMillis();
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        Class<?> c = getClassForType(type);
        if (c == null) {
            return HooksResult.noSuchHook(); // unchanged
        }
        HooksContext hooksContext = createHooksContext(context, null);
        try {
            Method method = getHookMethod(c, functionName, CordraObject.class, HooksContext.class);
            if (method != null) {
                CordraObject res = invokeMethodOnDefaultInstance(c, method, CordraObject.class, co, hooksContext);
                return HooksResult.of(res);
            }
            return HooksResult.noSuchHook(); // unchanged;
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(functionName + " java runLifeCycleHook: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    @Override
    public HooksResult<ObjectDelta> beforeSchemaValidationWithId(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException {
        HooksResult<ObjectDelta> res = beforeSchemaValidationGeneric(BEFORE_SCHEMA_VALIDATION_WITH_ID, type, originalObject, objectDelta, context);
        if (!res.isPresent()) return res;
        if (originalObject == null && (res.get().id == null || !res.get().id.equals(objectDelta.id))) {
            throw new InternalErrorCordraException("Cannot change id of object in beforeSchemaValidationWithId");
        }
        return res;
    }

    @Override
    public HooksResult<ObjectDelta> beforeSchemaValidation(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException {
        return beforeSchemaValidationGeneric(BEFORE_SCHEMA_VALIDATION, type, originalObject, objectDelta, context);
    }

    private HooksResult<ObjectDelta> beforeSchemaValidationGeneric(String hookName, String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException {
        originalObject = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(originalObject);
        long start = System.currentTimeMillis();
        Class<?> c = getClassForType(type);
        if (c == null) {
            return HooksResult.noSuchHook(); // unchanged
        }
        boolean isUpdate = originalObject != null;
        try {
            Method beforeSchemaValidation = getHookMethod(c, hookName, CordraObject.class, HooksContext.class);
            if (beforeSchemaValidation != null) {
                ObjectDelta res = beforeSchemaValidationFull(c, beforeSchemaValidation, originalObject, objectDelta, context, isUpdate);
                return HooksResult.of(res);
            }
            return HooksResult.noSuchHook(); // unchanged;
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(hookName + " beforeSchemaValidationGeneric: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    private ObjectDelta beforeSchemaValidationFull(Class<?> c, Method beforeSchemaValidation, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context, boolean isUpdate) throws CordraException, InvalidException {
        CordraObject inputObject;
        if (isUpdate) {
            inputObject = objectDelta.asCordraObjectForUpdate(originalObject);
        } else {
            inputObject = objectDelta.asCordraObjectForCreate();
        }
        HooksContext hooksContext = createHooksContext(context, null);
        CordraObject output = invokeMethodOnDefaultInstance(c, beforeSchemaValidation, CordraObject.class, inputObject, hooksContext);
        if (isUpdate) {
            return ObjectDelta.fromCordraObjectForUpdate(originalObject, output, objectDelta.payloads);
        } else {
            return ObjectDelta.fromCordraObjectForCreate(output, objectDelta.payloads);
        }
    }

    private Method getHookMethod(Class<?> c, String methodName, Class<?>... paramsTypes) {
        try {
            Method m = c.getMethod(methodName, paramsTypes);
            return m;
        } catch (NumberFormatException | NoSuchMethodException e) {
            return null;
        }
    }

    private Method getStaticMethod(Class<?> c, String methodName) {
        return getMethod(c, methodName, true);
    }

    private String[] getMethodNames(Method m, CordraMethod cordraMethod) {
        String[] namesFromAnnotation = cordraMethod.names();
        if (namesFromAnnotation == null || namesFromAnnotation.length == 0) {
            namesFromAnnotation = cordraMethod.name();
        }
        if (namesFromAnnotation == null || namesFromAnnotation.length == 0) {
            namesFromAnnotation = cordraMethod.value();
        }
        if (namesFromAnnotation == null || namesFromAnnotation.length == 0) {
            return new String[] { m.getName() };
        }
        return namesFromAnnotation;
    }

    private Method getInstanceMethod(Class<?> c, String methodName) {
        return getMethod(c, methodName, false);
    }

    private boolean isAllowGet(Method m) {
        Annotation ann = m.getAnnotation(CordraMethod.class);
        if (ann != null) {
            if (ann instanceof CordraMethod) {
                CordraMethod cordraMethod = (CordraMethod) ann;
                return cordraMethod.allowGet();
            }
        }
        return false;
    }

    private Method getMethod(Class<?> c, String methodName, boolean isStatic) {
        Method[] methods = c.getMethods();
        for (Method m : methods) {
            boolean isMethodStatic = Modifier.isStatic(m.getModifiers());
            if (isMethodStatic == isStatic) {
                Annotation ann = m.getAnnotation(CordraMethod.class);
                CordraMethod cordraMethod = (CordraMethod) ann;
                if (ann != null) {
                    String[] names = getMethodNames(m, cordraMethod);
                    if (names == null) continue;
                    for (String name : names) {
                        if (methodName.equals(name)) {
                            return m;
                        }
                    }
                }
            }
        }
        return null;
    }

    public Class<?> getServiceClass() {
        for (CordraTypeJarLoader jarLoader : jarLoaders.values()) {
            Class<?> c = jarLoader.getServiceClass();
            if (c != null) {
                return c;
            }
        }
        return null;
    }

    public Class<?> getClassForType(String type) {
        for (CordraTypeJarLoader jarLoader : jarLoaders.values()) {
            Class<?> c;
            if (type == null) {
                c = jarLoader.getServiceClass();
            } else {
                c = jarLoader.getClassForType(type);
            }
            if (c != null) {
                return c;
            }
        }
        return null;
    }

    public static Object createDefaultInstance(Class<?> c) throws CordraException {
        try {
            Constructor<?> constructor = c.getDeclaredConstructor();
            return constructor.newInstance();
        } catch (NoSuchMethodException e) {
            return null;
        } catch (IllegalAccessException | InstantiationException e) {
            throw new InternalErrorCordraException(e);
        } catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause != null) {
                if (cause instanceof CordraException) {
                    throw (CordraException) cause;
                }
            }
            throw new InternalErrorCordraException(e);
        }
    }

    private <T> T invokeMethodOnDefaultInstance(Class<?> c, Method method, Class<T> returnType, Object... params) throws CordraException {
        Object instance = null;
        if (c != null) {
            instance = createDefaultInstance(c);
        }
        return invokeMethodOnInstance(instance, method, returnType, params);
    }

    @SuppressWarnings("unchecked")
    private <T> T invokeMethodOnInstance(Object instance, Method method, Class<T> returnType, Object... params) throws CordraException {
        try {
            Object response = method.invoke(instance, params);
            if (response == null) return null;
            if (!returnType.isInstance(response)) {
                throw new InternalErrorCordraException("Unexpected return type for " + method.getName() + "; expected " + returnType + " got " + response.getClass());
            }
            return (T) response;
        } catch (IllegalAccessException e) {
            throw new InternalErrorCordraException(e);
        } catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause != null) {
                if (cause instanceof CordraException) {
                    throw (CordraException) cause;
                }
            }
            throw new InternalErrorCordraException(e);
        }
    }
}
