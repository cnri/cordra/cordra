package net.cnri.cordra.hooks;

import com.google.gson.JsonObject;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.DirectIo;
import net.cnri.cordra.InvalidException;
import net.cnri.cordra.RequestContext;
import net.cnri.cordra.RequestContextHolder;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.SearchRequest;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.DefaultAcls;
import net.cnri.cordra.auth.RequestAuthenticationInfo;
import net.cnri.cordra.hooks.java.*;
import net.cnri.cordra.hooks.javascript.*;
import net.cnri.cordra.model.ObjectDelta;
import net.handle.hdllib.HandleValue;

import java.util.*;

public class LifeCycleHooks implements LifeCycleHooksInterface {

    private JavaLifeCycleHooks javaHooks;
    private JavaScriptLifeCycleHooks javaScriptHooks;

    public LifeCycleHooks(JavaLifeCycleHooks javaHooks, JavaScriptLifeCycleHooks javaScriptHooks) {
        this.javaHooks = javaHooks;
        this.javaScriptHooks = javaScriptHooks;
    }

    @Override
    public HooksResult<ObjectDelta> beforeSchemaValidationWithId(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException {
        HooksResult<ObjectDelta> res = javaHooks.beforeSchemaValidationWithId(type, originalObject, objectDelta, context);
        if (!res.isPresent()) res = javaScriptHooks.beforeSchemaValidationWithId(type, originalObject, objectDelta, context);
        if (!res.isPresent()) res = javaHooks.beforeSchemaValidationWithId(null, originalObject, objectDelta, context);
        if (!res.isPresent()) res = javaScriptHooks.beforeSchemaValidationWithId(null, originalObject, objectDelta, context);
        return res;
    }

    @Override
    public HooksResult<ObjectDelta> beforeSchemaValidation(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException {
        HooksResult<ObjectDelta> res = javaHooks.beforeSchemaValidation(type, originalObject, objectDelta, context);
        if (!res.isPresent()) res = javaScriptHooks.beforeSchemaValidation(type, originalObject, objectDelta, context);
        if (!res.isPresent()) res = javaHooks.beforeSchemaValidation(null, originalObject, objectDelta, context);
        if (!res.isPresent()) res = javaScriptHooks.beforeSchemaValidation(null, originalObject, objectDelta, context);
        return res;
    }

    @Override
    public HooksResult<Void> beforeStorage(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        HooksResult<Void> res = javaHooks.beforeStorage(type, co, context);
        if (!res.isPresent()) res = javaScriptHooks.beforeStorage(type, co, context);
        if (!res.isPresent()) res = javaHooks.beforeStorage(null, co, context);
        if (!res.isPresent()) res = javaScriptHooks.beforeStorage(null, co, context);
        return res;
    }

    @Override
    public HooksResult<Void> beforeDelete(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        HooksResult<Void> res = javaHooks.beforeDelete(type, co, context);
        if (!res.isPresent()) res = javaScriptHooks.beforeDelete(type, co, context);
        if (!res.isPresent()) res = javaHooks.beforeDelete(null, co, context);
        if (!res.isPresent()) res = javaScriptHooks.beforeDelete(null, co, context);
        return res;
    }

    @Override
    public HooksResult<Void> afterDelete(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        HooksResult<Void> res = javaHooks.afterDelete(type, co, context);
        if (!res.isPresent()) res = javaScriptHooks.afterDelete(type, co, context);
        if (!res.isPresent()) res = javaHooks.afterDelete(null, co, context);
        if (!res.isPresent()) res = javaScriptHooks.afterDelete(null, co, context);
        return res;
    }

    @Override
    public HooksResult<Void> afterCreateOrUpdate(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        HooksResult<Void> res = javaHooks.afterCreateOrUpdate(type, co, context);
        if (!res.isPresent()) res = javaScriptHooks.afterCreateOrUpdate(type, co, context);
        if (!res.isPresent()) res = javaHooks.afterCreateOrUpdate(null, co, context);
        if (!res.isPresent()) res = javaScriptHooks.afterCreateOrUpdate(null, co, context);
        return res;
    }

    @Override
    public HooksResult<Boolean> streamPayload(String type, CordraObject co, Map<String, Object> context, DirectIo directIo) throws CordraException {
        HooksResult<Boolean> res = javaHooks.streamPayload(type, co, context, directIo);
        if (!res.isPresent()) res = javaScriptHooks.streamPayload(type, co, context, directIo);
        if (!res.isPresent()) res = javaHooks.streamPayload(null, co, context, directIo);
        if (!res.isPresent()) res = javaScriptHooks.streamPayload(null, co, context, directIo);
        return res;
    }

    @Override
    public HooksResult<CordraObject> onObjectResolution(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        HooksResult<CordraObject> res = javaHooks.onObjectResolution(type, co, context);
        if (!res.isPresent()) res = javaScriptHooks.onObjectResolution(type, co, context);
        if (!res.isPresent()) res = javaHooks.onObjectResolution(null, co, context);
        if (!res.isPresent()) res = javaScriptHooks.onObjectResolution(null, co, context);
        return res;
    }

    @Override
    public HooksResult<DefaultAcls> getAuthConfig(String type, Map<String, Object> context) throws CordraException {
        HooksResult<DefaultAcls> res = javaHooks.getAuthConfig(type, context);
        if (!res.isPresent()) res = javaScriptHooks.getAuthConfig(type, context);
        return res;
    }

    @Override
    public HooksResult<CordraObject> objectForIndexing(String type, CordraObject co) throws CordraException {
        HooksResult<CordraObject> res = javaHooks.objectForIndexing(type, co);
        if (!res.isPresent()) res = javaScriptHooks.objectForIndexing(type, co);
        if (!res.isPresent()) res = javaHooks.objectForIndexing(null, co);
        if (!res.isPresent()) res = javaScriptHooks.objectForIndexing(null, co);
        return res;
    }

    @Override
    public HooksResult<String> customizeQuery(String query, Map<String, Object> context) throws CordraException {
        //Only on design
        HooksResult<String> res = javaHooks.customizeQuery(query, context);
        if (!res.isPresent()) res = javaScriptHooks.customizeQuery(query, context);
        return res;
    }

    @Override
    public HooksResult<SearchRequest> customizeQueryAndParams(SearchRequest queryAndParams, Map<String, Object> context) throws CordraException {
        //Only on design
        HooksResult<SearchRequest> res = javaHooks.customizeQueryAndParams(queryAndParams, context);
        if (!res.isPresent()) res = javaScriptHooks.customizeQueryAndParams(queryAndParams, context);
        return res;
    }

    @Override
    public HooksResult<AuthenticationResult> authenticate(RequestAuthenticationInfo authInfo, Map<String, Object> context) throws CordraException {
        //Only on design
        HooksResult<AuthenticationResult> res = javaHooks.authenticate(authInfo, context);
        if (!res.isPresent()) res = javaScriptHooks.authenticate(authInfo, context);
        return res;
    }

    @Override
    public HooksResult<List<HandleValue>> createHandleValues(CordraObject co, Map<String, Object> context) throws CordraException {
        //Only on design
        HooksResult<List<HandleValue>> res = javaHooks.createHandleValues(co, context);
        if (!res.isPresent()) res = javaScriptHooks.createHandleValues(co, context);
        return res;
    }

    @Override
    public HooksResult<CordraObject> generateIdWithLooping(String type, CordraObject co, Map<String, Object> context, ThrowingFunction<String, CordraObject> lockAndCreateMemoryObjectIfHandleAvailable) throws CordraException {
        HooksResult<CordraObject> res = javaHooks.generateIdWithLooping(type, co, context, lockAndCreateMemoryObjectIfHandleAvailable);
        if (!res.isPresent()) res = javaScriptHooks.generateIdWithLooping(type, co, context, lockAndCreateMemoryObjectIfHandleAvailable);
        if (!res.isPresent()) res = javaHooks.generateIdWithLooping(null, co, context, lockAndCreateMemoryObjectIfHandleAvailable);
        if (!res.isPresent()) res = javaScriptHooks.generateIdWithLooping(null, co, context, lockAndCreateMemoryObjectIfHandleAvailable);
        return res;
    }

    @Override
    public void shutdown() {
        javaHooks.shutdown();
        javaScriptHooks.shutdown();
    }

    // LifeCycleHooks.listMethods and LifeCycleHooks.call make it so "CordraDesign" static methods are service methods,
    // for backward compatibility
    @Override
    public List<String> listMethods(boolean isStatic, String type) throws CordraException {
        Set<String> unique = new LinkedHashSet<>();
        List<String> javaMethods = javaHooks.listMethods(isStatic, type);
        unique.addAll(javaMethods);
        if (type == null) {
            List<String> javaDesignMethods = javaHooks.listMethods(isStatic, CordraService.DESIGN_OBJECT_TYPE);
            unique.addAll(javaDesignMethods);
        }
        List<String> javaScriptMethods = javaScriptHooks.listMethods(isStatic, type);
        unique.addAll(javaScriptMethods);
        if (type == null) {
            List<String> javaScriptDesignMethods = javaScriptHooks.listMethods(isStatic, CordraService.DESIGN_OBJECT_TYPE);
            unique.addAll(javaScriptDesignMethods);
        }
        return new ArrayList<>(unique);
    }

    // LifeCycleHooks.listMethods and LifeCycleHooks.call make it so "CordraDesign" static methods are service methods,
    // for backward compatibility
    @Override
    public HooksResult<CallResult> call(String method, String type, boolean isStatic, String coJson, Map<String, Object> context, DirectIo directIo, boolean isGet) throws CordraException {
        HooksResult<CallResult> res = javaHooks.call(method, type, isStatic, coJson, context, directIo, isGet);
        if (type == null && !res.isPresent()) {
            res = javaHooks.call(method, CordraService.DESIGN_OBJECT_TYPE, isStatic, coJson, context, directIo, isGet);
        }
        if (!res.isPresent()) res = javaScriptHooks.call(method, type, isStatic, coJson, context, directIo, isGet);
        if (type == null && !res.isPresent()) {
            res = javaScriptHooks.call(method, CordraService.DESIGN_OBJECT_TYPE, isStatic, coJson, context, directIo, isGet);
        }
        return res;
    }

    @Override
    public boolean hasHook(String type, String hookName) throws CordraException {
        if (javaHooks.hasHook(type, hookName)) {
            return true;
        } else {
            return javaScriptHooks.hasHook(type, hookName);
        }
    }

    @Override
    public boolean hasServiceHook(String hookName) throws CordraException {
        if (javaHooks.hasServiceHook(hookName)) {
            return true;
        } else {
            return javaScriptHooks.hasServiceHook(hookName);
        }
    }

    public static void addRequestContextToContext(Map<String, Object> context) {
        RequestContext requestContextObj = RequestContextHolder.get();
        if (requestContextObj != null) {
            JsonObject requestContext = requestContextObj.getRequestContext();
            if (requestContext != null) {
                context.put("requestContext", requestContext);
            }
            JsonObject attributes = requestContextObj.getAttributes();
            if (attributes != null) {
                context.put("attributes", attributes);
            }
        }
    }
}
