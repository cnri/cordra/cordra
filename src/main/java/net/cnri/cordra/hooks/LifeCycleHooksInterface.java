package net.cnri.cordra.hooks;

import net.cnri.cordra.InvalidException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.DirectIo;
import net.cnri.cordra.api.SearchRequest;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.DefaultAcls;
import net.cnri.cordra.auth.RequestAuthenticationInfo;
import net.cnri.cordra.model.ObjectDelta;
import net.handle.hdllib.HandleValue;

import java.util.List;
import java.util.Map;

public interface LifeCycleHooksInterface {
    String ON_OBJECT_RESOLUTION = "onObjectResolution";
    String ON_PAYLOAD_RESOLUTION = "onPayloadResolution";
    String BEFORE_DELETE = "beforeDelete";
    String BEFORE_SCHEMA_VALIDATION = "beforeSchemaValidation";
    String BEFORE_SCHEMA_VALIDATION_WITH_ID = "beforeSchemaValidationWithId";
    String BEFORE_STORAGE = "beforeStorage";
    String OBJECT_FOR_INDEXING = "objectForIndexing";
    String GENERATE_ID = "generateId";
    String IS_GENERATE_ID_LOOPABLE = "isGenerateIdLoopable";
    String CREATE_HANDLE_VALUES = "createHandleValues";
    String CUSTOMIZE_QUERY = "customizeQuery";
    String CUSTOMIZE_QUERY_AND_PARAMS = "customizeQueryAndParams";
    String AUTHENTICATE = "authenticate";
    String GET_AUTH_CONFIG = "getAuthConfig";
    String AFTER_CREATE_OR_UPDATE = "afterCreateOrUpdate";
    String AFTER_DELETE = "afterDelete";

    //void warmUp()

    HooksResult<ObjectDelta> beforeSchemaValidationWithId(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException;

    HooksResult<ObjectDelta> beforeSchemaValidation(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException;

    HooksResult<Void> beforeStorage(String type, CordraObject co, Map<String, Object> context) throws CordraException;

    HooksResult<Void> beforeDelete(String type, CordraObject co, Map<String, Object> context) throws CordraException;

    HooksResult<Void> afterDelete(String type, CordraObject co, Map<String, Object> context) throws CordraException;

    HooksResult<Void> afterCreateOrUpdate(String type, CordraObject co, Map<String, Object> context) throws CordraException;

    HooksResult<Boolean> streamPayload(String type, CordraObject co, Map<String, Object> context, DirectIo directIo) throws CordraException;

    HooksResult<CordraObject> onObjectResolution(String type, CordraObject co, Map<String, Object> context) throws CordraException;

    HooksResult<DefaultAcls> getAuthConfig(String type, Map<String, Object> context) throws CordraException;

    List<String> listMethods(boolean isStatic, String type) throws CordraException;

    HooksResult<CallResult> call(String method, String type, boolean isStatic, String coJson, Map<String, Object> context, DirectIo directIo, boolean isGet) throws CordraException;

    boolean hasHook(String type, String hookName) throws CordraException;

    boolean hasServiceHook(String hookName) throws CordraException;

    HooksResult<CordraObject> objectForIndexing(String type, CordraObject co) throws CordraException;

    HooksResult<String> customizeQuery(String query, Map<String, Object> context) throws CordraException;

    HooksResult<SearchRequest> customizeQueryAndParams(SearchRequest queryAndParams, Map<String, Object> context) throws CordraException;

    HooksResult<AuthenticationResult> authenticate(RequestAuthenticationInfo authInfo, Map<String, Object> context) throws CordraException;

    HooksResult<List<HandleValue>> createHandleValues(CordraObject co, Map<String, Object> context) throws CordraException;

    HooksResult<CordraObject> generateIdWithLooping(String type, CordraObject co, Map<String, Object> context, ThrowingFunction<String, CordraObject> lockAndCreateMemoryObjectIfHandleAvailable) throws CordraException;

    @FunctionalInterface
    interface ThrowingFunction<T, R> {
        R apply(T t) throws CordraException;
    }

    void shutdown();
}
