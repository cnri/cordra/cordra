package net.cnri.cordra.hooks;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

// like Optional but allows null
public class HooksResult<T> {
    private final T result;
    private final boolean hasHook;

    private HooksResult(T result, boolean hasHook) {
        this.result = result;
        this.hasHook = hasHook;
    }
    private static final HooksResult<?> NO_SUCH_HOOK = new HooksResult<>(null, false);

    public static <T> HooksResult<T> noSuchHook() {
        @SuppressWarnings("unchecked")
        HooksResult<T> res = (HooksResult<T>) NO_SUCH_HOOK;
        return res;
    }

    public static <T> HooksResult<T> of(T result) {
        return new HooksResult<>(result, true);
    }

    public T get() {
        if (!hasHook) throw new NoSuchElementException("No such hook");
        return result;
    }

    public boolean isPresent() {
        return hasHook;
    }

    public void ifPresent(Consumer<? super T> consumer) {
        if (hasHook) consumer.accept(result);
    }

    public HooksResult<T> filter(Predicate<? super T> predicate) {
        Objects.requireNonNull(predicate);
        if (hasHook && predicate.test(result)) return this;
        return noSuchHook();
    }

    public <U> HooksResult<U> map(Function<? super T, ? extends U> mapper) {
        if (hasHook) return of(mapper.apply(result));
        return noSuchHook();
    }

    public <U> HooksResult<U> flatMap(Function<? super T, HooksResult<U>> mapper) {
        if (hasHook) return mapper.apply(result);
        return noSuchHook();
    }

    public HooksResult<T> or(Supplier<? extends HooksResult<? extends T>> supplier) {
        if (hasHook) return this;
        @SuppressWarnings("unchecked")
        HooksResult<T> res = (HooksResult<T>) supplier.get();
        return res;
    }

    public T orElse(T other) {
        if (hasHook) return result;
        return other;
    }

    public T orElseGet(Supplier<? extends T> other) {
        if (hasHook) return result;
        return other.get();
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
        if (hasHook) return result;
        throw exceptionSupplier.get();
    }
}
