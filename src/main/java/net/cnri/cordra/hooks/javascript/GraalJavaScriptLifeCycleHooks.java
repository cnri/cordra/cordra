package net.cnri.cordra.hooks.javascript;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.DefaultAcls;
import net.cnri.cordra.auth.RequestAuthenticationInfo;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.hooks.CallResult;
import net.cnri.cordra.hooks.HooksResult;
import net.cnri.cordra.hooks.LifeCycleHooks;
import net.cnri.cordra.hooks.LifeCycleHooksInterface;
import net.cnri.cordra.model.ObjectDelta;
import net.cnri.util.javascript.JavaScriptEnvironment;
import net.cnri.util.javascript.JavaScriptRunner;
import net.cnri.util.javascript.WrappedPolyglotException;
import net.handle.hdllib.HandleValue;

import org.graalvm.polyglot.PolyglotException;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

public class GraalJavaScriptLifeCycleHooks implements JavaScriptLifeCycleHooks {

    private static final Logger logger = LoggerFactory.getLogger(GraalJavaScriptLifeCycleHooks.class);
    static final Gson gson = GsonUtility.getPrettyGson();

    private final DateTimeFormatter dateTimeFormatter = CordraService.dateTimeFormatter;

    private final JavaScriptEnvironment javaScriptEnvironment;
    private final boolean traceRequests;
    private final CordraRequireLookup cordraRequireLookup;
    private final Supplier<Design> designSupplier;

    public GraalJavaScriptLifeCycleHooks(boolean traceRequests, CordraRequireLookup cordraRequireLookup, Supplier<Design> designSupplier, ClassLoader dataLibClassLoader) {
        this.javaScriptEnvironment = new JavaScriptEnvironment(cordraRequireLookup, dataLibClassLoader);
        this.traceRequests = traceRequests;
        this.cordraRequireLookup = cordraRequireLookup;
        this.designSupplier = designSupplier;
    }

    @Override
    public void init(CordraService cordraService) {
        CordraServiceForLifeCycleHooks cordraServiceForLifeCycleHooks = new CordraServiceForLifeCycleHooks();
        cordraServiceForLifeCycleHooks.init(cordraService);
        javaScriptEnvironment.putExtraGlobal("_cordraReturningStrings", cordraServiceForLifeCycleHooks);
        CordraUtilForLifeCycleHooks cordraUtilForLifeCycleHooks = new CordraUtilForLifeCycleHooks();
        cordraUtilForLifeCycleHooks.init(cordraService);
        javaScriptEnvironment.putExtraGlobal("_cordraUtil", cordraUtilForLifeCycleHooks);
        initialWarmUp();
    }

    @Override
    public void clearCache() {
        javaScriptEnvironment.clearCache();
    }

    private void initialWarmUp() {
        Thread t = new Thread(() -> {
            logger.info("Warming up JavaScriptLifeCycleHooks");
            try {
                javaScriptEnvironment.warmUp();
                logger.info("Warmed up JavaScriptLifeCycleHooks");
            } catch (Exception e) {
                logger.error("Error warming up JavaScriptLifeCycleHooks", e);
            }
        });
        t.setDaemon(true);
        t.start();
    }

    // workaround for https://github.com/oracle/graaljs/issues/595d
    private Value safeGetMember(Value value, String member) {
        if (value.hasMember(member)) return value.getMember(member);
        return null;
    }

    public JavaScriptRunner getJavaScriptRunner() {
        JavaScriptRunner runner = javaScriptEnvironment.getRunner(null, logger);
        Map<String, String> contextMap = MDC.getCopyOfContextMap();
        RequestContext requestContext = RequestContextHolder.get();
        runner.submit(() -> {
            contextMap.forEach(MDC::put);
            RequestContextHolder.set(requestContext);
        });
        return runner;
    }

    public void recycleJavaScriptRunner(JavaScriptRunner runner) {
        long start = System.currentTimeMillis();
        try {
            runner.submit(() -> {
                MDC.clear();
                RequestContextHolder.clear();
            });
            javaScriptEnvironment.recycle(runner);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("javascript recycleJavaScriptRunner: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    @Override
    public void shutdown() {
        try { javaScriptEnvironment.shutdown(); } catch (Exception e) { logger.error("Shutdown error", e); }
    }

    @Override
    public HooksResult<List<HandleValue>> createHandleValues(CordraObject co, Map<String, Object> context) throws CordraException {
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        String inputString = gson.toJson(co);
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            return runner.awaitPromise(() -> {
                Value methodPromise = getCreateHandleValuesMethod(runner, CordraRequireLookup.HANDLE_MINTING_CONFIG_MODULE_ID);
                methodPromise = runner.getSyncMethods().promiseThen(methodPromise, method -> {
                    if (method == null || method.isNull()) {
                        return getCreateHandleValuesMethod(runner, CordraRequireLookup.DESIGN_MODULE_ID);
                    }
                    return method;
                });
                return runner.getSyncMethods().promiseThen(methodPromise, method -> {
                    if (method == null || method.isNull()) return HooksResult.noSuchHook();
                    if (!method.canExecute()) return HooksResult.of(Collections.emptyList());
                    Value obj = runner.getSyncMethods().jsonParse(inputString);
                    Value contextValue = jsonObjectifyContext(runner, context);
                    Object[] params = new Object[] { obj, contextValue };
                    Value res =  method.execute(params);
                    if (res == null || res.isNull()) return HooksResult.of(Collections.emptyList());
                    String outputString = runner.getSyncMethods().jsonStringify(res);
                    List<HandleValue> values = net.handle.hdllib.GsonUtility.getGson().fromJson(outputString, new TypeToken<List<HandleValue>>() {}.getType());
                    return HooksResult.of(values);
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }

    // returns a promise
    private Value getCreateHandleValuesMethod(JavaScriptRunner runner, String moduleId) {
        Value modulePromise = requireOrImport(runner, moduleId);
        return runner.getSyncMethods().promiseThen(modulePromise, module -> {
            if (module == null || module.isNull()) return null;
            Value moduleExports = module.getMember("exports");
            if (moduleExports == null) return null;
            Value method = safeGetMember(moduleExports, CREATE_HANDLE_VALUES);
            if (method == null && moduleExports.hasMember("default")) {
                method = safeGetMember(moduleExports.getMember("default"), CREATE_HANDLE_VALUES);
            }
            if (method == null || method.isNull()) return null;
            return method;
        });
    }

    @Override
    public HooksResult<CordraObject> generateIdWithLooping(String type, CordraObject co, Map<String, Object> context, ThrowingFunction<String, CordraObject> lockAndCreateMemoryObjectIfHandleAvailable) throws CordraException {
        long start = System.currentTimeMillis();
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            HooksResult<Map.Entry<Value, String>> hooksResult = runner.awaitPromise(() -> {
                Value methodPromise = findJavaScriptMemberWithRunner(GENERATE_ID, moduleId, false, false, 400, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodValue -> {
                    if (methodValue == null || methodValue.isNull()) return HooksResult.noSuchHook();
                    Value handlePromise = generateId(methodValue, runner, co, context);
                    return runner.getSyncMethods().promiseThen(handlePromise, handleValue -> {
                        String handle = handleValue == null ? null : handleValue.asString();
                        if (handle == null || handle.isEmpty()) return HooksResult.of(null);
                        return HooksResult.of(new AbstractMap.SimpleImmutableEntry<>(methodValue, handle));
                    });
                });
            }, Value::asHostObject, null);
            if (!hooksResult.isPresent()) return HooksResult.noSuchHook();
            Map.Entry<Value, String> methodValueAndHandle = hooksResult.get();
            if (methodValueAndHandle == null) return HooksResult.of(null);
            Value methodValue = methodValueAndHandle.getKey();
            String handle = methodValueAndHandle.getValue();
            if (handle == null) return HooksResult.of(null);
            CordraObject result = lockAndCreateMemoryObjectIfHandleAvailable.apply(handle);
            if (result != null) return HooksResult.of(result);
            // throw if not loopable
            runner.awaitPromise(() -> {
                Value isLoopablePromise = findJavaScriptMemberWithRunner(IS_GENERATE_ID_LOOPABLE, moduleId, false, false, 400, runner);
                return runner.getSyncMethods().promiseThen(isLoopablePromise, isLoopable -> {
                    if (!runner.getSyncMethods().isTruthy(isLoopable)) {
                        throw new ConflictCordraException("Object already exists: " + handle);
                    }
                });
            }, v -> v, null);
            return HooksResult.of(generateIdInnerLoop(methodValue, runner, co, context, lockAndCreateMemoryObjectIfHandleAvailable));
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            recycleJavaScriptRunner(runner);
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(GENERATE_ID + " generateIdFromJavaScript: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    private CordraObject generateIdInnerLoop(Value methodValue, JavaScriptRunner runner, CordraObject co, Map<String, Object> context, ThrowingFunction<String, CordraObject> lockAndCreateMemoryObjectIfHandleAvailable) throws Exception {
        String handle = runner.awaitPromise(() -> {
            Value handlePromise = generateId(methodValue, runner, co, context);
            return runner.getSyncMethods().promiseThen(handlePromise, handleValue -> {
                String res = handleValue == null ? null : handleValue.asString();
                if (res == null || res.isEmpty()) return null;
                return res;
            });
        }, Value::asString, null);
        if (handle == null) return null;
        CordraObject result = lockAndCreateMemoryObjectIfHandleAvailable.apply(handle);
        if (result != null) return result;
        return generateIdInnerLoop(methodValue, runner, co, context, lockAndCreateMemoryObjectIfHandleAvailable);
    }

    // returns a promise
    private Value generateId(Value methodValue, JavaScriptRunner runner, CordraObject co, Map<String, Object> context) throws CordraException {
        String input = gson.toJson(co);
        Value outputPromise = runJavaScriptFunctionWithRunner(methodValue, input, context, false, 400, runner);
        return runner.getSyncMethods().promiseThen(outputPromise, resultValue -> {
            String output = resultValue == null ? null : resultValue.asString();
            if (output == null) return null;
            JsonElement outputJsonElement = JsonParser.parseString(output);
            if (outputJsonElement.isJsonNull()) return null;
            if (outputJsonElement.isJsonPrimitive()) return outputJsonElement.getAsString();
            throw new InternalErrorCordraException(GENERATE_ID + " returned object or array");
        });
    }

    @Override
    public HooksResult<SearchRequest> customizeQueryAndParams(SearchRequest queryAndParams, Map<String, Object> context) throws CordraException {
        if (!cordraRequireLookup.exists(CordraRequireLookup.DESIGN_MODULE_ID)) {
            return HooksResult.noSuchHook();
        }
        long start = System.currentTimeMillis();
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            String moduleId = CordraRequireLookup.DESIGN_MODULE_ID;
            return runner.awaitPromise(() -> {
                Value methodPromise = findJavaScriptMemberWithRunner(CUSTOMIZE_QUERY_AND_PARAMS, moduleId, false, false, 400, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodValue -> {
                    if (methodValue == null || methodValue.isNull()) {
                        return HooksResult.noSuchHook();
                    } else {
                        String inputString = gson.toJson(queryAndParams);
                        Value outputPromise = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodValue, inputString, context, false, 400, runner);
                        return runner.getSyncMethods().promiseThen(outputPromise, outputValue -> {
                            String output = outputValue == null ? null : outputValue.asString();
                            if (output == null) throw new InternalErrorCordraException(CUSTOMIZE_QUERY_AND_PARAMS + " returned undefined");
                            JsonElement outputJsonElement = JsonParser.parseString(output);
                            if (outputJsonElement.isJsonNull()) throw new InternalErrorCordraException(CUSTOMIZE_QUERY_AND_PARAMS + " returned null");
                            if (outputJsonElement.isJsonPrimitive()) throw new InternalErrorCordraException(CUSTOMIZE_QUERY_AND_PARAMS + " returned primitive");
                            if (outputJsonElement.isJsonArray()) throw new InternalErrorCordraException(CUSTOMIZE_QUERY_AND_PARAMS + " returned array");
                            SearchRequest result = gson.fromJson(outputJsonElement, SearchRequest.class);
                            return HooksResult.of(result);
                        });
                    }
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(CUSTOMIZE_QUERY_AND_PARAMS + " runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    @Override
    public HooksResult<String> customizeQuery(String query, Map<String, Object> context) throws CordraException {
        if (!cordraRequireLookup.exists(CordraRequireLookup.DESIGN_MODULE_ID)) {
            return HooksResult.noSuchHook();
        }
        long start = System.currentTimeMillis();
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            String moduleId = CordraRequireLookup.DESIGN_MODULE_ID;
            return runner.awaitPromise(() -> {
                Value methodPromise = findJavaScriptMemberWithRunner(CUSTOMIZE_QUERY, moduleId, false, false, 400, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodValue -> {
                    if (methodValue == null || methodValue.isNull()) {
                        return HooksResult.noSuchHook();
                    } else {
                        String inputString = gson.toJson(query);
                        Value outputPromise = runJavaScriptFunctionWithRunner(methodValue, inputString, context, false, 400, runner);
                        return runner.getSyncMethods().promiseThen(outputPromise, outputValue -> {
                            String output = outputValue == null ? null : outputValue.asString();
                            if (output == null) throw new InternalErrorCordraException(CUSTOMIZE_QUERY + " returned undefined");
                            JsonElement outputJsonElement = JsonParser.parseString(output);
                            if (outputJsonElement.isJsonNull()) throw new InternalErrorCordraException(CUSTOMIZE_QUERY + " returned null");
                            if (outputJsonElement.isJsonPrimitive()) return HooksResult.of(outputJsonElement.getAsString());
                            throw new InternalErrorCordraException(CUSTOMIZE_QUERY + " returned object or array");
                        });
                    }
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("customizeQuery runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    @Override
    public HooksResult<DefaultAcls> getAuthConfig(String type, Map<String, Object> context) throws CordraException {
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        if (!cordraRequireLookup.exists(moduleId)) {
            return HooksResult.noSuchHook();
        }
        long start = System.currentTimeMillis();
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            return runner.awaitPromise(() -> {
                Value methodPromise = findJavaScriptMemberWithRunner(GET_AUTH_CONFIG, moduleId, false, false, 403, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodValue -> {
                    if (methodValue == null || methodValue.isNull()) {
                        return HooksResult.noSuchHook();
                    } else {
                        Value contextValue = jsonObjectifyContext(runner, context);
                        Value outputPromise = runJavaScriptFunctionWithRunner(methodValue, null, contextValue, true, 403, runner);
                        return runner.getSyncMethods().promiseThen(outputPromise, outputValue -> {
                            String output = outputValue == null ? null : outputValue.asString();
                            if (output == null) return HooksResult.of(null);
                            JsonElement outputJsonElement = JsonParser.parseString(output);
                            if (outputJsonElement.isJsonNull()) return HooksResult.of(null);
                            if (outputJsonElement.isJsonPrimitive()) throw new InternalErrorCordraException(AUTHENTICATE + " returned primitive");
                            if (outputJsonElement.isJsonArray()) throw new InternalErrorCordraException(AUTHENTICATE + " returned array");
                            DefaultAcls defaultAcls = gson.fromJson(outputJsonElement, DefaultAcls.class);
                            return HooksResult.of(defaultAcls);
                        });
                    }
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("getAuthConfig: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    @Override
    public HooksResult<AuthenticationResult> authenticate(RequestAuthenticationInfo authInfo, Map<String, Object> context) throws CordraException {
        if (!cordraRequireLookup.exists(CordraRequireLookup.DESIGN_MODULE_ID)) {
            return HooksResult.noSuchHook();
        }
        long start = System.currentTimeMillis();
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            String moduleId = CordraRequireLookup.DESIGN_MODULE_ID;
            return runner.awaitPromise(() -> {
                Value methodPromise = findJavaScriptMemberWithRunner(AUTHENTICATE, moduleId, false, false, 401, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodValue -> {
                    if (methodValue == null || methodValue.isNull()) {
                        return HooksResult.noSuchHook();
                    } else {
                        String inputString = gson.toJson(authInfo);
                        Value inputObj = parseJsonForJavaScript(runner, inputString);
                        Value contextValue = jsonObjectifyContext(runner, context);
                        Value outputPromise = runJavaScriptFunctionWithRunner(methodValue, inputObj, contextValue, false, 401, runner);
                        return runner.getSyncMethods().promiseThen(outputPromise, outputValue -> {
                            String output = outputValue == null ? null : outputValue.asString();
                            if (output == null) return HooksResult.of(null);
                            JsonElement outputJsonElement = JsonParser.parseString(output);
                            if (outputJsonElement.isJsonNull()) return HooksResult.of(null);
                            if (outputJsonElement.isJsonPrimitive()) throw new InternalErrorCordraException(AUTHENTICATE + " returned primitive");
                            if (outputJsonElement.isJsonArray()) throw new InternalErrorCordraException(AUTHENTICATE + " returned array");
                            AuthenticationResult authResult = gson.fromJson(outputJsonElement, AuthenticationResult.class);
                            return HooksResult.of(authResult);
                        });
                    }
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("authenticate runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    @Override
    public HooksResult<Void> beforeDelete(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runJavaScriptFunction(type, co, BEFORE_DELETE, context, 403).map(any -> null);
    }

    @Override
    public HooksResult<Void> afterDelete(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runJavaScriptFunction(type, co, AFTER_DELETE, context, 400).map(any -> null);
    }

    @Override
    public HooksResult<CordraObject> onObjectResolution(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runJavaScriptFunction(type, co, ON_OBJECT_RESOLUTION, context, 403);
    }

    private boolean isEsm(String moduleId) {
        return cordraRequireLookup.isEsm(moduleId);
    }

    // returns a promise to the module wrapped as { exports: module }
    // (this is to unify require and import but also avoid issues if the module is a thenable)
    private Value requireOrImport(JavaScriptRunner runner, String moduleId) {
        if (!cordraRequireLookup.exists(moduleId)) {
            return runner.getSyncMethods().promisify(() -> null);
        }
        Value modulePromise;
        if (isEsm(moduleId)) {
            modulePromise = runner.getSyncMethods().importById(moduleId);
            modulePromise = runner.getSyncMethods().promiseThen(modulePromise, module -> {
                return ProxyObject.fromMap(Collections.singletonMap("exports", module));
            });
        } else {
            modulePromise = runner.getSyncMethods().promisify(() -> {
                Value module = runner.getSyncMethods().requireById(moduleId);
                return ProxyObject.fromMap(Collections.singletonMap("exports", module));
            });
        }
        if (CordraRequireLookup.DESIGN_MODULE_ID.equals(moduleId)) {
            return runner.getSyncMethods().promiseThen(modulePromise, x -> x, reason -> {
                // This allows Cordra UI to continue to work even if there is bad design JavaScript
                try {
                    runner.getSyncMethods().throwValue(reason);
                } catch (Exception e) {
                    logger.error("Unexpected error in design JavaScript, bypassing", e);
                }
                return null;
            });
        }
        return modulePromise;
    }

    @Override
    public void warmUpModule(String moduleId) {
        try {
            if (!cordraRequireLookup.exists(moduleId)) return;
            // TODO investigate whether there is benefit to warming up multiple instances of JavaScriptRunner
            JavaScriptRunner runner = getJavaScriptRunner();
            try {
                runner.awaitPromise(() -> {
                    return requireOrImport(runner, moduleId);
                }, x -> x, null);
            } catch (WrappedPolyglotException e) {
                if (e.getRootCause().isGuestException()) {
                    runner.submitAndGet(() -> {
                        logger.warn("Exception warming up JavaScript for " + moduleId + ": " + getMessageFromPolyglotException(e.getRootCause()));
                    });
                } else {
                    throw e;
                }
            } finally {
                recycleJavaScriptRunner(runner);
            }
        } catch (Exception e) {
            logger.warn("Exception warming up JavaScript for " + moduleId, e);
        }
    }

    @Override
    public HooksResult<ObjectDelta> beforeSchemaValidation(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException {
        return beforeSchemaValidationGeneric(BEFORE_SCHEMA_VALIDATION, type, originalObject, objectDelta, context);
    }

    @Override
    public HooksResult<ObjectDelta> beforeSchemaValidationWithId(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException {
        HooksResult<ObjectDelta> res = beforeSchemaValidationGeneric(BEFORE_SCHEMA_VALIDATION_WITH_ID, type, originalObject, objectDelta, context);
        if (!res.isPresent()) return res;
        if (originalObject == null && (res.get().id == null || !res.get().id.equals(objectDelta.id))) {
            throw new InternalErrorCordraException("Cannot change id of object in beforeSchemaValidationWithId");
        }
        return res;
    }

    private HooksResult<ObjectDelta> beforeSchemaValidationGeneric(String hookName, String type, CordraObject originalObjectParam, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, InvalidException {
        CordraObject originalObject = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(originalObjectParam);
        long start = System.currentTimeMillis();
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        if (!cordraRequireLookup.exists(moduleId)) {
            return HooksResult.noSuchHook();
        }
        boolean isUpdate = originalObject != null;
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            return runner.awaitPromise(() -> {
                Value methodPromise = findJavaScriptMemberWithRunner(hookName, moduleId, false, false, 400, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodValue -> {
                    if (methodValue != null && !methodValue.isNull()) {
                        Design design = designSupplier.get();
                        if (!Boolean.TRUE.equals(design.useLegacyContentOnlyJavaScriptHooks)) {
                            return beforeSchemaValidationFull(methodValue, originalObject, objectDelta, context, isUpdate, runner);
                        } else {
                            context.put("useLegacyContentOnlyJavaScriptHooks", Boolean.TRUE);
                            return beforeSchemaValidationLegacy(methodValue, objectDelta, context, runner);
                        }
                    }
                    return HooksResult.noSuchHook(); // unchanged;
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraExceptionOrInvalidException(e);
        } finally {
            recycleJavaScriptRunner(runner);
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(hookName + " runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    // returns a promise to an ObjectDelta
    private Value beforeSchemaValidationLegacy(Value methodValue, ObjectDelta objectDelta, Map<String, Object> context, JavaScriptRunner runner) throws CordraException, InvalidException {
        String input = objectDelta.jsonData;
        Value outputPromise = runJavaScriptFunctionWithRunnerDefaultReturnInputThrowsInvalidException(methodValue, input, context, false, 400, runner);
        return runner.getSyncMethods().promiseThen(outputPromise, outputValue -> {
            String output = outputValue == null ? null : outputValue.asString();
            ObjectDelta res = new ObjectDelta(objectDelta.id, objectDelta.type, output, objectDelta.acl, objectDelta.userMetadata, objectDelta.payloads, objectDelta.payloadsToDelete);
            return HooksResult.of(res);
        });
    }

    // returns a promise to an ObjectDelta
    private Value beforeSchemaValidationFull(Value methodValue, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context, boolean isUpdate, JavaScriptRunner runner) throws CordraException, InvalidException {
        CordraObject inputObject;
        if (isUpdate) {
            inputObject = objectDelta.asCordraObjectForUpdate(originalObject);
        } else {
            inputObject = objectDelta.asCordraObjectForCreate();
        }
        String input = gson.toJson(inputObject);
        Value outputPromise = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodValue, input, context, false, 400, runner);
        return runner.getSyncMethods().promiseThen(outputPromise, outputValue -> {
            String output = outputValue == null ? null : outputValue.asString();
            ObjectDelta res;
            if (isUpdate) {
                res = ObjectDelta.fromStringifiedCordraObjectForUpdate(originalObject, output, objectDelta.payloads);
            } else {
                res = ObjectDelta.fromStringifiedCordraObjectForCreate(output, objectDelta.payloads);
            }
            return HooksResult.of(res);
        });
    }

    @Override
    public HooksResult<Void> afterCreateOrUpdate(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runJavaScriptFunction(type, co, AFTER_CREATE_OR_UPDATE, context, 400).map(any -> null);
    }

    @Override
    public HooksResult<Void> beforeStorage(String type, CordraObject co, Map<String, Object> context) throws CordraException {
        return runJavaScriptFunction(type, co, BEFORE_STORAGE, context, 400).map(any -> null);
    }

    // returns a promise
    private Value findJavaScriptMemberWithRunner(String memberName, String moduleId, boolean isStatic, boolean isMethod, int defaultErrorResponseCode, JavaScriptRunner runner) throws CordraException {
        long start = System.currentTimeMillis();
        try {
            Value modulePromise = requireOrImport(runner, moduleId);
            return runner.getSyncMethods().promiseThen(modulePromise, module -> {
                if (module == null || module.isNull()) return null;
                Value moduleExports = module.getMember("exports");
                if (moduleExports == null) return null;
                Value member;
                if (isMethod) {
                    String methodsMemberName = isStatic ? "staticMethods" : "methods";
                    Value methods = safeGetMember(moduleExports, methodsMemberName);
                    if (methods == null && moduleExports.hasMember("default")) {
                        methods = safeGetMember(moduleExports.getMember("default"), methodsMemberName);
                    }
                    if (methods == null) return null;
                    member = safeGetMember(methods, memberName);
                } else {
                    member = safeGetMember(moduleExports, memberName);
                    if (member == null && moduleExports.hasMember("default")) {
                        member = safeGetMember(moduleExports.getMember("default"), memberName);
                    }
                }
                return member;
            }, reason -> {
                CordraException ce = syncExtractCordraException(defaultErrorResponseCode, reason, runner);
                if (ce == null) runner.getSyncMethods().throwValue(reason);
                else throw ce;
            });
        } catch (PolyglotException e) {
            return syncExtractAndThrowCordraException(defaultErrorResponseCode, e, runner);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(memberName + " findJavaScriptMemberWithRunner: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    // returns a promise
    private Value runJavaScriptFunctionWithRunner(Value method, String input, Map<String, Object> context, boolean isStatic, int defaultErrorResponseCode, JavaScriptRunner runner) throws CordraException {
        Value obj = parseJsonForJavaScript(runner, input);
        Value contextValue = jsonObjectifyContext(runner, context);
        return runJavaScriptFunctionWithRunner(method, obj, contextValue, isStatic, defaultErrorResponseCode, runner);
    }

    // returns a promise
    private Value runJavaScriptFunctionWithRunnerDefaultReturnInput(Value method, String input, Map<String, Object> context, boolean isStatic, int defaultErrorResponseCode, JavaScriptRunner runner) throws CordraException {
        Value obj = parseJsonForJavaScript(runner, input);
        Value contextValue = jsonObjectifyContext(runner, context);
        return runJavaScriptFunctionWithRunner(method, obj, contextValue, isStatic, defaultErrorResponseCode, runner, true);
    }

    // returns a promise
    private Value runJavaScriptFunctionWithRunnerDefaultReturnInputThrowsInvalidException(Value method, String input, Map<String, Object> context, boolean isStatic, int defaultErrorResponseCode, JavaScriptRunner runner) throws CordraException, InvalidException {
        Value obj = parseJsonForJavaScript(runner, input, true);
        Value contextValue = jsonObjectifyContext(runner, context);
        Value outputPromise = runJavaScriptFunctionWithRunner(method, obj, contextValue, isStatic, defaultErrorResponseCode, runner);
        return runner.getSyncMethods().promiseThen(outputPromise, outputValue -> {
            String output = outputValue == null ? null : outputValue.asString();
            if (output == null) output = runner.getSyncMethods().jsonStringify(obj);
            return output;
        });
    }

    // returns a promise
    private Value runJavaScriptFunctionWithRunner(Value method, Value obj, Value context, boolean isStatic, int defaultErrorResponseCode, JavaScriptRunner runner) throws CordraException {
        return runJavaScriptFunctionWithRunner(method, obj, context, isStatic, defaultErrorResponseCode, runner, false);
    }

    // returns a promise
    private Value runJavaScriptFunctionWithRunner(Value method, Value obj, Value context, boolean isStatic, int defaultErrorResponseCode, JavaScriptRunner runner, boolean defaultReturnInput) throws CordraException {
        long start = System.currentTimeMillis();
        try {
            Object[] params;
            if (isStatic) {
                // static method call
                params = new Object[] { context };
            } else {
                params = new Object[] { obj, context };
            }
            Value resultPromise = runner.getSyncMethods().promisify(() -> {
                return method.execute(params);
            });
            return runner.getSyncMethods().promiseThen(resultPromise, resObj -> {
                String resString;
                if (resObj == null) {
                    resString = "null";
                } else if (resObj.isHostObject()) {
                    resString = gson.toJson(resObj.asHostObject());
                } else {
                    resString = runner.getSyncMethods().jsonStringify(resObj);
                }
                if (resString == null && defaultReturnInput) {
                    resString = runner.getSyncMethods().jsonStringify(obj);
                }
                return resString;
            }, reason -> {
                CordraException ce = syncExtractCordraException(defaultErrorResponseCode, reason, runner);
                if (ce == null) runner.getSyncMethods().throwValue(reason);
                else throw ce;
            });
        } catch (PolyglotException e) {
            return syncExtractAndThrowCordraException(defaultErrorResponseCode, e, runner);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("javascript runJavaScriptFunctionWithRunner: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    public Value parseJsonForJavaScript(JavaScriptRunner runner, String params) throws CordraException {
        try {
            return parseJsonForJavaScript(runner, params, false);
        } catch (InvalidException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private Value parseJsonForJavaScript(JavaScriptRunner runner, String params, boolean throwsInvalidException) throws CordraException, InvalidException {
        if (params == null) return null;
        long start = System.currentTimeMillis();
        try {
            return runner.getSyncMethods().jsonParse(params);
        } catch (PolyglotException e) {
            // Note: when throwsInvalidException is false, this function is only used when the "params" is known to be JSON.
            // Notably it is not used directly to parse user-supplied CallServlet input.
            String message = "Invalid JSON: " + getMessageFromPolyglotException(e);
            if (throwsInvalidException) {
                throw new InvalidException(message, e);
            } else {
                throw new InternalErrorCordraException(message, e);
            }
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("javascript parseJsonForJavaScript: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    private static String getMessageFromPolyglotException(PolyglotException e) {
        Value ee = e.getGuestObject();
        if (!e.isGuestException() || ee == null || !ee.hasMembers()) return e.getMessage();
        String message = "";
        if (ee.hasMember("name")) message += ": " + ee.getMember("name");
        if (ee.hasMember("message")) message += ": " + ee.getMember("message");
        if (ee.hasMember("cause") && ee.getMember("cause").hasMembers()) {
            if (ee.getMember("cause").hasMember("name")) message += ": " + ee.getMember("cause").getMember("name");
            if (ee.getMember("cause").hasMember("message")) message += ": " + ee.getMember("cause").getMember("message");
        }
        if (message.isEmpty()) return e.getMessage();
        return message.substring(2); // remove initial colon and space
    }

    public static <T> T syncExtractAndThrowCordraException(int defaultResponseCode, PolyglotException e, JavaScriptRunner runner) throws CordraException {
        if (e.isHostException()) {
            Throwable t = unwrapException(e.asHostException());
            if (t instanceof CordraException) {
                CordraException root = (CordraException) t;
                throw CordraException.fromStatusCode(root.getResponseCode(), root.getResponse(), e);
            }
            throw e;
        }
        if (!e.isGuestException()) throw e;
        Value errorObj = e.getGuestObject();
        CordraException ce = syncExtractCordraException(defaultResponseCode, errorObj, runner);
        if (ce != null) {
            ce.initCause(e);
            throw ce;
        } else {
            throw e;
        }
    }

    public static CordraException syncExtractCordraException(int defaultResponseCode, Value errorObj, JavaScriptRunner runner) {
        if (errorObj == null || errorObj.isNull()) return null;
        if (errorObj.isString()) {
            return cordraExceptionFromStatusCode(defaultResponseCode, CordraException.responseForMessage(errorObj.asString()), null);
        }
        if (errorObj.isHostObject()) {
            Object javaErrorObj = errorObj.asHostObject();
            if (javaErrorObj instanceof Throwable) {
                javaErrorObj = unwrapException((Throwable)javaErrorObj);
            }
            if (javaErrorObj instanceof CordraException) {
                return (CordraException)javaErrorObj;
            }
            return null;
        }
        if (errorObj.hasMembers()) {
            Value nameObj = errorObj.getMember("name");
            if (nameObj == null || !nameObj.isString()) return null;
            // "InvalidException" here is legacy, used by Fipro; leave undocumented...
            if ("CordraError".equals(nameObj.asString()) || "InvalidException".equals(nameObj.asString())) {
                int responseCode;
                try {
                    Value responseCodeObj = errorObj.getMember("responseCode");
                    if (responseCodeObj == null) {
                        responseCode = defaultResponseCode;
                    } else if (responseCodeObj.isString()) {
                        responseCode = Integer.parseInt(responseCodeObj.asString());
                    } else if (responseCodeObj.isNumber()) {
                        responseCode = responseCodeObj.asInt();
                    } else {
                        responseCode = defaultResponseCode;
                    }
                } catch (Exception sce) {
                    responseCode = defaultResponseCode;
                }
                if (!errorObj.hasMember("response")) {
                    Value messageObj = errorObj.getMember("message");
                    if (messageObj != null && messageObj.isString()) {
                        return cordraExceptionFromStatusCode(responseCode, CordraException.responseForMessage(messageObj.asString()), null);
                    }
                }
                Value responseObj = errorObj.getMember("response");
                String responseString;
                if (responseObj == null || responseObj.isNull()) {
                    responseString = null;
                } else {
                    responseString = runner.getSyncMethods().jsonStringify(responseObj);
                }
                return CordraException.fromStatusCode(responseCode, responseString);
            }
        }
        return null;
    }

    public static CordraException rethrowAsCordraException(Exception ex) throws CordraException {
        try {
            return rethrowAsCordraExceptionOrInvalidException(ex, true);
        } catch (InvalidException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public static CordraException rethrowAsCordraExceptionOrInvalidException(Exception ex) throws CordraException, InvalidException {
        return rethrowAsCordraExceptionOrInvalidException(ex, true);
    }

    public static CordraException rethrowAsCordraExceptionOrInvalidException(Exception ex, boolean allowInvalidException) throws CordraException, InvalidException {
        Throwable cause = unwrapException(ex.getCause());
        if (cause instanceof CordraException) {
            CordraException root = (CordraException)cause;
            throw CordraException.fromStatusCode(root.getResponseCode(), root.getResponse(), ex);
        }
        if (allowInvalidException && cause instanceof InvalidException) {
            InvalidException root = (InvalidException)cause;
            if (root.getReport() != null) throw new InvalidException(root.getReport(), ex);
            else throw new InvalidException(root.getMessage(), ex);
        }
        try {
            throw ex;
        } catch (CordraException e) {
            throw e;
        } catch (InvalidException e) {
            if (!allowInvalidException) throw new InternalErrorCordraException(e);
            throw e;
        } catch (WrappedPolyglotException e) {
            if (e.getRootCause().isHostException()) {
                Throwable hostThrowable = unwrapException(e.getRootCause().asHostException());
                if (hostThrowable instanceof CordraException) {
                    CordraException root = (CordraException)hostThrowable;
                    throw CordraException.fromStatusCode(root.getResponseCode(), root.getResponse(), e);
                }
                if (allowInvalidException && hostThrowable instanceof InvalidException) {
                    InvalidException root = (InvalidException)hostThrowable;
                    if (root.getReport() != null) throw new InvalidException(root.getReport(), e);
                    else throw new InvalidException(root.getMessage(), e);
                }
                throw new InternalErrorCordraException(e);
            }
            throw new InternalErrorCordraException(e);
        } catch (PolyglotException e) {
            if (e.isHostException()) {
                Throwable hostThrowable = e.asHostException();
                if (hostThrowable instanceof CordraException) {
                    CordraException root = (CordraException) e.asHostException();
                    throw CordraException.fromStatusCode(root.getResponseCode(), root.getResponse(), e);
                }
                if (allowInvalidException && hostThrowable instanceof InvalidException) {
                    InvalidException root = (InvalidException)e.asHostException();
                    if (root.getReport() != null) throw new InvalidException(root.getReport(), e);
                    else throw new InvalidException(root.getMessage(), e);
                }
                throw new InternalErrorCordraException(e);
            }
            throw new InternalErrorCordraException(e);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private static Throwable unwrapException(Throwable t) {
        while (t instanceof ExecutionException || t instanceof CompletionException) {
            t = t.getCause();
        }
        return t;
    }

    private static CordraException cordraExceptionFromStatusCode(int responseCode, JsonElement response, Throwable cause) {
        try {
            return CordraException.fromStatusCode(responseCode, response, cause);
        } catch (IncompatibleClassChangeError e) {
            logger.warn("Unexpected mismatch of cordra jar versions on call to CordraException.fromStatusCode");
            return CordraException.fromStatusCode(responseCode, gson.toJson(response), cause);
        }
    }

    public HooksResult<CordraObject> runJavaScriptFunction(String type, CordraObject co, String functionName, Map<String, Object> context, int defaultErrorResponseCode) throws CordraException {
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        return runJavaScriptFunction(co, functionName, moduleId, context, defaultErrorResponseCode);
    }

    private HooksResult<CordraObject> runJavaScriptFunction(CordraObject co, String functionName, String moduleId, Map<String, Object> context, int defaultErrorResponseCode) throws CordraException {
        long start = System.currentTimeMillis();
        if (!cordraRequireLookup.exists(moduleId)) {
            return HooksResult.noSuchHook();
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            return runner.awaitPromise(() -> {
                Value methodPromise = findJavaScriptMemberWithRunner(functionName, moduleId, false, false, defaultErrorResponseCode, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodValue -> {
                    if (methodValue != null && !methodValue.isNull()) {
                        Design design = designSupplier.get();
                        if (!Boolean.TRUE.equals(design.useLegacyContentOnlyJavaScriptHooks)) {
                            String input = gson.toJson(co);
                            Value outputPromise = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodValue, input, context, false, defaultErrorResponseCode, runner);
                            return runner.getSyncMethods().promiseThen(outputPromise, outputValue -> {
                                String output = outputValue == null ? null : outputValue.asString();
                                CordraObject res = gson.fromJson(output, CordraObject.class);
                                return HooksResult.of(res);
                            });
                        } else {
                            context.put("useLegacyContentOnlyJavaScriptHooks", Boolean.TRUE);
                            String input = co.getContentAsString();
                            Value outputPromise = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodValue, input, context, false, defaultErrorResponseCode, runner);
                            return runner.getSyncMethods().promiseThen(outputPromise, outputValue -> {
                                String output = outputValue == null ? null : outputValue.asString();
                                co.content = JsonParser.parseString(output);
                                return HooksResult.of(co);
                            });
                        }
                    }
                    return HooksResult.noSuchHook();
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(functionName + " runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    @Override
    public HooksResult<CallResult> call(String method, String type, boolean isStatic, String coJson, Map<String, Object> context, DirectIo directIo, boolean isGet) throws CordraException {
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        if (!cordraRequireLookup.exists(moduleId)) {
            return HooksResult.noSuchHook();
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            return runner.awaitPromise(() -> {
                boolean isMethod = true;
                Value methodPromise = findJavaScriptMemberWithRunner(method, moduleId, isStatic, isMethod, 400, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodValue -> {
                    if (methodValue == null || methodValue.isNull()) {
                        return HooksResult.noSuchHook();
                    }
                    if (isGet) {
                        boolean allowGet = runner.getSyncMethods().isTruthy(methodValue.getMember("allowGet"));
                        if (!allowGet) {
                            throw new ForbiddenCordraException("Operation " + method + " does not allow GET");
                        }
                    }
                    String before;
                    Value obj;
                    if (!isStatic) {
                        obj = parseJsonForJavaScript(runner, coJson);
                        before = runner.getSyncMethods().jsonStringify(obj);
                    } else {
                        obj = null;
                        before = null;
                    }
                    Value contextValue = jsonObjectifyContext(runner, context);
                    AtomicBoolean usedDirectOutput = new AtomicBoolean();
                    Value prepareCallContext = runner.getSyncMethods().requireById(CordraRequireLookup.PREPARE_CALL_CONTEXT_MODULE_NAME);
                    prepareCallContext.execute(contextValue, directIo, usedDirectOutput, true);
                    Value resultPromise = runJavaScriptFunctionWithRunner(methodValue, obj, contextValue, isStatic, 400, runner);
                    return runner.getSyncMethods().promiseThen(resultPromise, resultValue -> {
                        String result = resultValue == null ? null : resultValue.asString();
                        if (usedDirectOutput.get()) {
                            result = null;
                        }
                        String after = null;
                        if (!isStatic) {
                            after = runner.getSyncMethods().jsonStringify(obj);
                        }
                        CallResult res = new CallResult(result, before, after);
                        return HooksResult.of(res);
                    });
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }

    @Override
    public List<String> listMethods(boolean isStatic, String type) throws CordraException {
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        if (!cordraRequireLookup.exists(moduleId)) {
            //schema does not have javascript
            return Collections.emptyList();
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            return runner.awaitPromise(() -> {
                Value modulePromise = requireOrImport(runner, moduleId);
                return runner.getSyncMethods().promiseThen(modulePromise, module -> {
                    if (module == null || module.isNull()) {
                        return Collections.emptyList();
                    }
                    Value moduleExports = module.getMember("exports");
                    if (moduleExports == null || !moduleExports.hasMembers()) {
                        return Collections.emptyList();
                    }
                    String methodsMemberName = isStatic ? "staticMethods" : "methods";
                    Value methods = safeGetMember(moduleExports, methodsMemberName);
                    if (methods == null && moduleExports.hasMember("default")) {
                        methods = safeGetMember(moduleExports.getMember("default"), methodsMemberName);
                    }
                    if (methods == null || !methods.hasMembers()) {
                        return Collections.emptyList();
                    }
                    List<String> result = new ArrayList<>();
                    // Note: the iterating over the members of getMemberKeys must happen
                    // in the JavaScriptRunner thread
                    for (String key : methods.getMemberKeys()) {
                        Value value = methods.getMember(key);
                        if (value != null && value.canExecute()) {
                            result.add(key);
                        }
                    }
                    return result;
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }

    // need to be careful about internalMetadata as it is used for indexing
    @Override
    public HooksResult<CordraObject> objectForIndexing(String type, CordraObject co) throws CordraException {
        long start = System.currentTimeMillis();
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(co.type);
        if (!cordraRequireLookup.exists(moduleId)) {
            HooksResult.noSuchHook();
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            return runner.awaitPromise(() -> {
                Value methodPromise = findJavaScriptMemberWithRunner(OBJECT_FOR_INDEXING, moduleId, false, false, 400, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodValue -> {
                    if (methodValue != null && !methodValue.isNull()) {
                        CordraObject inputObject = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
                        String input = gson.toJson(inputObject);
                        Map<String, Object> context = new HashMap<>();
                        context.put("objectId", co.id);
                        Value outputJsonPromise = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodValue, input, context, false, 400, runner);
                        return runner.getSyncMethods().promiseThen(outputJsonPromise, outputJson -> {
                            CordraObject coResult;
                            try {
                                coResult = GsonUtility.getGson().fromJson(outputJson.asString(), CordraObject.class);
                                coResult.id = inputObject.id; //Ensure the JS cannot change the digital object id
                                CordraService.restoreInternalMetadata(co, coResult);
                                CordraObject res = ObjectDelta.applyPayloadsToCordraObject(coResult, co.payloads);
                                return HooksResult.of(res);
                            } catch (JsonParseException e) {
                                throw new InternalErrorCordraException("Couldn't parse json into CordraObject", e);
                            }
                        });
                    }
                    return HooksResult.noSuchHook();
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("objectForIndexing runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    private Value jsonObjectifyContext(JavaScriptRunner runner, Map<String, Object> context) throws CordraException {
        long start = System.currentTimeMillis();
        try {
            LifeCycleHooks.addRequestContextToContext(context);
            Map<String, Object> noValue = new HashMap<>();
            for (Map.Entry<String, Object> entry : context.entrySet()) {
                Object value = entry.getValue();
                if (value instanceof Value) continue;
                noValue.put(entry.getKey(), value);
            }
            Value res;
            try {
                res = runner.getSyncMethods().jsonParse(gson.toJson(noValue));
                for (Map.Entry<String, Object> entry : context.entrySet()) {
                    Object value = entry.getValue();
                    if (value instanceof Value) {
                        res.putMember(entry.getKey(), value);
                    }
                }
            } catch (PolyglotException | JsonParseException e) {
                throw new InternalErrorCordraException("Error in enrichment", e);
            }
            return res;
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("javascript jsonObjectifyContext: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    @Override
    public boolean hasHook(String type, String hookName) throws CordraException {
        //This could be improved with caching
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        return moduleHasJavaScriptFunction(hookName, moduleId, 400);
    }

    @Override
    public boolean hasServiceHook(String hookName) throws CordraException {
        if (LifeCycleHooksInterface.CREATE_HANDLE_VALUES.equals(hookName)) {
            String moduleId = CordraRequireLookup.HANDLE_MINTING_CONFIG_MODULE_ID;
            boolean res = moduleHasJavaScriptFunction(hookName, moduleId, 400);
            if (res) return true;
        }
        String moduleId = CordraRequireLookup.DESIGN_MODULE_ID;
        return moduleHasJavaScriptFunction(hookName, moduleId, 400);
    }

    private boolean moduleHasJavaScriptFunction(String functionName, String moduleId, int defaultErrorResponseCode) throws CordraException {
        if (!cordraRequireLookup.exists(moduleId)) {
            return false;
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            return runner.awaitPromise(() -> {
                return findJavaScriptMemberWithRunner(functionName, moduleId, false, false, defaultErrorResponseCode, runner);
            }, methodValue -> methodValue != null && !methodValue.isNull(), null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }

    @Override
    public HooksResult<Boolean> streamPayload(String type, CordraObject co, Map<String, Object> context, DirectIo directIo) throws CordraException {
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        if (!cordraRequireLookup.exists(moduleId)) {
            return HooksResult.noSuchHook();
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            return runner.awaitPromise(() -> {
                Value methodPromise = findJavaScriptMemberWithRunner(ON_PAYLOAD_RESOLUTION, moduleId, false, false, 403, runner);
                return runner.getSyncMethods().promiseThen(methodPromise, methodObj -> {
                    if (methodObj == null || methodObj.isNull()) {
                        return HooksResult.noSuchHook();
                    }
                    CordraObject coCopy = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
                    String coJson = gson.toJson(coCopy);
                    Value obj = parseJsonForJavaScript(runner, coJson);
                    Value contextObj = jsonObjectifyContext(runner, context);
                    AtomicBoolean usedDirectOutput = new AtomicBoolean();
                    Value prepareCallContext = runner.getSyncMethods().requireById(CordraRequireLookup.PREPARE_CALL_CONTEXT_MODULE_NAME);
                    prepareCallContext.execute(contextObj, directIo, usedDirectOutput, false);
                    Value resultPromise = runJavaScriptFunctionWithRunner(methodObj, obj, contextObj, false, 403, runner);
                    return runner.getSyncMethods().promiseThen(resultPromise, result -> {
                        return HooksResult.of(usedDirectOutput.get());
                    });
                });
            }, Value::asHostObject, null);
        } catch (Exception e) {
            throw rethrowAsCordraException(e);
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }
}
