package net.cnri.cordra.hooks.javascript;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.hooks.LifeCycleHooksInterface;

public interface JavaScriptLifeCycleHooks extends LifeCycleHooksInterface {

    void init(CordraService cordraService);

    void clearCache();

    void warmUpModule(String moduleId);
}
