package net.cnri.cordra.hooks.javascript;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class CordraClientUtil {
    // doing this in Java because using Java String methods on Strings in GraalVM JavaScript
    // is potentially not supported

    public static byte[] bytesFromText(String text) {
        return text.getBytes(StandardCharsets.UTF_8);
    }

    public static InputStream javaInputStreamFromText(String text) {
        return new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8));
    }
}
