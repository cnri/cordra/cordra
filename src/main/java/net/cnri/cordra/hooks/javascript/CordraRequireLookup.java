package net.cnri.cordra.hooks.javascript;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.InvalidException;
import net.cnri.cordra.SchemaUtil;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.Payload;
import net.cnri.cordra.api.SearchResults;
import net.cnri.cordra.api.UncheckedCordraException;
import net.cnri.cordra.util.JacksonUtil;
import net.cnri.cordra.util.SearchUtil;
import net.cnri.util.javascript.RequireLookup;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;

public class CordraRequireLookup implements RequireLookup, net.cnri.util.javascript.nashorn.RequireLookup {
    public static final String HANDLE_MINTING_CONFIG_MODULE_ID = "/cordra/handle-minting-config";
    public static final String DESIGN_MODULE_ID = "/cordra/design";
    public static final String PREPARE_CALL_CONTEXT_MODULE_NAME = "/cordra/_internal/prepareCallContext.js";

    private final CordraService cordraService;
    private final ConcurrentMap<String, List<String>> objectIdsForModule = new ConcurrentHashMap<>();
    private volatile ConcurrentMap<String, String> schemas = new ConcurrentHashMap<>();
    private volatile ConcurrentMap<String, JavaScriptInfo> schemaJavaScripts = new ConcurrentHashMap<>();
    private volatile JavaScriptInfo handleJavaScript;
    private volatile JavaScriptInfo designJavaScript;

    public CordraRequireLookup(CordraService cordraService) {
        this.cordraService = cordraService;
    }

    public void putSchemaJavaScript(String type, JavaScriptInfo js) {
        if (js == null) schemaJavaScripts.remove(type);
        else schemaJavaScripts.put(type, js);
    }

    public void putSchema(String type, String schema) {
        if (schema == null) schemas.remove(type);
        else schemas.put(type, schema);
    }

    public JavaScriptInfo getSchemaJavaScript(String type) {
        return schemaJavaScripts.get(type);
    }

    public String getSchema(String type) {
        return schemas.get(type);
    }

    public ConcurrentMap<String, JavaScriptInfo> getAllSchemaJavaScripts() {
        return schemaJavaScripts;
    }

    public ConcurrentMap<String, String> getAllSchemas() {
        return schemas;
    }

    public void replaceAllSchemaJavaScript(ConcurrentMap<String, JavaScriptInfo> newSchemaJavaScripts) {
        this.schemaJavaScripts = newSchemaJavaScripts;
    }

    public void replaceAllSchemas(ConcurrentMap<String, String> newSchemas) {
        this.schemas = newSchemas;
    }

    public void removeSchemaJavaScript(String type) {
        schemaJavaScripts.remove(type);
    }

    public void removeSchema(String type) {
        schemas.remove(type);
    }

    public void setHandleJavaScript(JavaScriptInfo js) {
        handleJavaScript = js;
    }

    public JavaScriptInfo getHandleJavaScript() {
        return handleJavaScript;
    }

    public void setDesignJavaScript(JavaScriptInfo js) {
        designJavaScript = js;
    }

    public JavaScriptInfo getDesignJavaScript() {
        return designJavaScript;
    }

    public static String moduleIdForSchemaType(String type) {
        // Uses convention null for service methods
        if (type == null) return DESIGN_MODULE_ID;
        return "/cordra/schemas/" + type;
    }

    @Override
    public boolean exists(String filename) {
        if (filename.equals("cordra")) {
            return true;
        } else if (filename.equals("cordra-util") || filename.equals("cordraUtil")) {
            return true;
        } else if (filename.equals("cordra-client")) {
            return true;
        } else if (filename.equals(PREPARE_CALL_CONTEXT_MODULE_NAME)) {
            return true;
        } else if (filename.startsWith("/cordra/schemas/")) {
            String type = filename.substring("/cordra/schemas/".length());
            if (type.endsWith(".schema.json")) {
                type = type.substring(0, type.length() - ".schema.json".length());
                return schemas.get(type) != null;
            } else {
                JavaScriptInfo schemaJavaScript = schemaJavaScripts.get(type);
                return schemaJavaScript != null;
            }
        } else if (filename.equals(HANDLE_MINTING_CONFIG_MODULE_ID)) {
            return handleJavaScript != null;
        } else if (filename.equals(DESIGN_MODULE_ID)) {
            return designJavaScript != null;
        } else {
            Collection<String> cachedObjectIds = getObjectIdsForModule(filename);
            if (cachedObjectIds != null) {
                return !cachedObjectIds.isEmpty();
            }
            try {
                cordraService.ensureIndexUpToDate();
                try (SearchResults<CordraObject> results = cordraService.searchRepo("javaScriptModuleName:\"" + SearchUtil.escape(filename) + "\" -isVersion:true -objatt_isVersion:true")) {
                    List<String> objectIds = results.stream().map(co -> co.id).collect(Collectors.toList());
                    setObjectIdsForModule(filename, objectIds);
                    return !objectIds.isEmpty();
                }
            } catch (CordraException e) {
                throw new UncheckedCordraException(e);
            }
        }
    }

    @Override
    public Boolean isEsm(String filename) {
        if (filename.equals("cordra")) {
            return true;
        } else if (filename.equals("cordra-util") || filename.equals("cordraUtil")) {
            return true;
        } else if (filename.equals("cordra-client")) {
            return true;
        } else if (filename.equals(PREPARE_CALL_CONTEXT_MODULE_NAME)) {
            return false;
        } else if (filename.startsWith("/cordra/schemas/")) {
            String type = filename.substring("/cordra/schemas/".length());
            if (type.endsWith(".schema.json")) return true;
            JavaScriptInfo schemaJavaScript = schemaJavaScripts.get(type);
            if (schemaJavaScript == null) return true;
            return schemaJavaScript.javascriptIsModule;
        } else if (filename.equals(HANDLE_MINTING_CONFIG_MODULE_ID)) {
            if (handleJavaScript == null) return true;
            return handleJavaScript.javascriptIsModule;
        } else if (filename.equals(DESIGN_MODULE_ID)) {
            if (designJavaScript == null) return true;
            return designJavaScript.javascriptIsModule;
        } else {
            // Determined by file extension or package.json
            return null;
        }
    }

    @Override
    public boolean isTypeModule(String json) {
        try {
            if (json == null) return false;
            JsonElement jsonEl = JsonParser.parseString(json);
            if (jsonEl == null) return false;
            if (!jsonEl.isJsonObject()) return false;
            JsonElement typeEl = jsonEl.getAsJsonObject().get("type");
            if (typeEl == null) return false;
            if (!typeEl.isJsonPrimitive()) return false;
            return typeEl.getAsBoolean();
        } catch (JsonParseException e) {
            return false;
        }
    }

    @Override
    public Reader getContent(String filename) {
        InputStream content = getContent(filename, false);
        if (content == null) return null;
        return new InputStreamReader(content, StandardCharsets.UTF_8);
    }

    @Override
    public InputStream getContent(String filename, boolean isEsm) {
        if (filename.equals("cordra")) {
            if (isEsm) return getClass().getResourceAsStream("cordra.mjs");
            return getClass().getResourceAsStream("cordra.js");
        } else if (filename.equals("cordra-util") || filename.equals("cordraUtil")) {
            if (isEsm) return getClass().getResourceAsStream("cordra-util.mjs");
            return getClass().getResourceAsStream("cordra-util.js");
        } else if (filename.equals("cordra-client")) {
            if (isEsm) return getClass().getResourceAsStream("cordra-client.mjs");
            return getClass().getResourceAsStream("cordra-client.js");
        } else if (filename.equals(PREPARE_CALL_CONTEXT_MODULE_NAME)) {
            return getClass().getResourceAsStream("prepareCallContext.js");
        } else if (filename.startsWith("/cordra/schemas/")) {
            String type = filename.substring("/cordra/schemas/".length());
            if (type.endsWith(".schema.json")) {
                type = type.substring(0, type.length() - ".schema.json".length());
                String schema = schemas.get(type);
                if (schema == null) return null;
                return new ByteArrayInputStream(schema.getBytes(StandardCharsets.UTF_8));
            } else {
                JavaScriptInfo schemaJavaScript = schemaJavaScripts.get(type);
                if (schemaJavaScript == null) return null;
                return new ByteArrayInputStream(schemaJavaScript.javascript.getBytes(StandardCharsets.UTF_8));
            }
        } else if (filename.equals(HANDLE_MINTING_CONFIG_MODULE_ID)) {
            if (handleJavaScript == null) return null;
            return new ByteArrayInputStream(handleJavaScript.javascript.getBytes(StandardCharsets.UTF_8));
        } else if (filename.equals(DESIGN_MODULE_ID)) {
            if (designJavaScript == null) return null;
            return new ByteArrayInputStream(designJavaScript.javascript.getBytes(StandardCharsets.UTF_8));
        } else {
            try {
                Collection<String> cachedObjectIds = getObjectIdsForModule(filename);
                if (cachedObjectIds != null) {
                    for (String objectId : cachedObjectIds) {
                        CordraObject co = cordraService.getCordraObjectOrNull(objectId);
                        if (co == null) continue;
                        InputStream res = findModuleInCordraObject(filename, co);
                        if (res != null) return res;
                    }
                }
                cordraService.ensureIndexUpToDate();
                InputStream res = null;
                List<String> objectIds = new ArrayList<>();
                try (SearchResults<CordraObject> results = cordraService.searchRepo("javaScriptModuleName:\"" + SearchUtil.escape(filename) + "\" -isVersion:true -objatt_isVersion:true")) {
                    for (CordraObject co : results) {
                        objectIds.add(co.id);
                        if (res == null) res = findModuleInCordraObject(filename, co);
                    }
                }
                setObjectIdsForModule(filename, objectIds);
                return res;
            } catch (CordraException e) {
                throw new UncheckedCordraException(e);
            }
        }
    }

    private InputStream findModuleInCordraObject(String filename, CordraObject co) throws CordraException {
        List<String> directoryNames = getDirectoryNames(co);
        if (co.payloads != null) {
            for (Payload payload : co.payloads) {
                for (String directoryName : directoryNames) {
                    String moduleName = Paths.get("/").resolve(directoryName).resolve(payload.name).normalize().toString();
                    moduleName = moduleName.replace(File.separator, "/");
                    if (filename.equals(moduleName)) {
                        // can't read from the search result object, so go back to the source
                        return cordraService.readPayload(co.id, payload.name);
                    }
                }
            }
        }
        return null;
    }

    private List<String> getDirectoryNames(CordraObject co) throws CordraException {
        String type = co.type;
        if (co.content == null) throw new InternalErrorCordraException("Missing JSON attribute on " + co.id);
        try {
            JsonNode jsonNode = JacksonUtil.gsonToJackson(co.content);
            Map<String, JsonNode> pointerToSchemaMap = cordraService.getPointerToSchemaMap(type, jsonNode);
            List<String> directoryNames = new ArrayList<>();
            for (Map.Entry<String, JsonNode> entry : pointerToSchemaMap.entrySet()) {
                String jsonPointer = entry.getKey();
                JsonNode subSchema = entry.getValue();
                if (!SchemaUtil.isPathForScriptsInPayloads(subSchema)) continue;
                JsonNode directoryNode = jsonNode.at(jsonPointer);
                if (directoryNode == null || !directoryNode.isTextual()) {
                    continue;
                } else {
                    directoryNames.add(directoryNode.asText());
                }
            }
            return directoryNames;
        } catch (InvalidException e) {
            throw new InternalErrorCordraException("Unexpected invalid json on " + co.id);
        }
    }

    public Collection<String> getObjectIdsForModule(String module) {
        return objectIdsForModule.get(module);
    }

    public void setObjectIdsForModule(String module, Collection<String> objectIds) {
        objectIdsForModule.put(module, new ArrayList<>(objectIds));
    }

    public void clearCache() {
        objectIdsForModule.clear();
    }

    public static class JavaScriptInfo {
        public boolean javascriptIsModule;
        public String javascript;

        public JavaScriptInfo(boolean javascriptIsModule, String javascript) {
            this.javascriptIsModule = javascriptIsModule;
            this.javascript = javascript;
        }

        @Override
        public int hashCode() {
            return Objects.hash(javascript, javascriptIsModule);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            JavaScriptInfo other = (JavaScriptInfo) obj;
            return Objects.equals(javascript, other.javascript) && javascriptIsModule == other.javascriptIsModule;
        }
    }
}
