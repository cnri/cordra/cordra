package net.cnri.cordra.hooks;

public class CallResult {
    public final String result;
    public final String before;
    public final String after;

    public CallResult(String result, String before, String after) {
        this.result = result;
        this.before = before;
        this.after = after;
    }
}
