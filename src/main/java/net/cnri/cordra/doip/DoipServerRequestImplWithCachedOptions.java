package net.cnri.cordra.doip;

import java.io.IOException;
import java.security.PublicKey;
import java.security.cert.X509Certificate;

import net.cnri.cordra.auth.InternalRequestOptions;
import net.dona.doip.InDoipMessage;
import net.dona.doip.server.DoipServerRequestImpl;

public class DoipServerRequestImplWithCachedOptions extends DoipServerRequestImpl {

    private InternalRequestOptions options;
    private boolean isGet;

    public DoipServerRequestImplWithCachedOptions(InDoipMessage inDoipMessage, String clientCertId, PublicKey clientCertPublicKey, X509Certificate[] clientCertChain) throws IOException {
        super(inDoipMessage, clientCertId, clientCertPublicKey, clientCertChain);
    }

    public InternalRequestOptions getOptions() {
        return options;
    }

    public void setOptions(InternalRequestOptions options) {
        this.options = options;
    }

    public boolean isGet() {
        return isGet;
    }

    public void setIsGet(boolean b) {
        isGet = b;
    }
}
