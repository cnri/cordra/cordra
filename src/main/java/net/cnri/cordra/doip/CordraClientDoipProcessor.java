package net.cnri.cordra.doip;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.gson.reflect.TypeToken;

import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.RequestContext;
import net.cnri.cordra.RequestContextHolder;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.InternalRequestOptions;
import net.cnri.cordra.auth.PreAuthenticatedOptions;
import net.cnri.cordra.operations.ReindexBatchOperation;
import net.cnri.cordra.operations.versions.GetVersionsOperation;
import net.cnri.cordra.operations.versions.PublishVersionOperation;
import net.cnri.util.StreamUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonWriter;

import net.dona.doip.DoipConstants;
import net.dona.doip.InDoipSegment;
import net.dona.doip.client.DigitalObject;
import net.dona.doip.client.Element;
import net.dona.doip.server.DoipProcessor;
import net.dona.doip.server.DoipServerRequest;
import net.dona.doip.server.DoipServerResponse;
import net.dona.doip.util.GsonUtility;
import net.dona.doip.util.InDoipMessageUtil;
import net.handle.hdllib.Common;
import net.handle.hdllib.HandleException;
import net.handle.hdllib.HandleResolver;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.Util;
import org.slf4j.MDC;

public class CordraClientDoipProcessor implements DoipProcessor {
    private static final Logger logger = LoggerFactory.getLogger(CordraClientDoipProcessor.class);

    private static final Gson gson = GsonUtility.getGson();

    private String serviceId;
    private HandleResolver resolver = new HandleResolver();

    private InternalCordraClient cordraClient;

    public static final String OP_AUTH_INTROSPECT = "20.DOIP/Op.Auth.Introspect";
    public static final String OP_AUTH_TOKEN = "20.DOIP/Op.Auth.Token";
    public static final String OP_AUTH_REVOKE = "20.DOIP/Op.Auth.Revoke";
    public static final String OP_CHECK_CREDENTIALS = "20.DOIP/Op.CheckCredentials";
    public static final String OP_CHANGE_PASSWORD = "20.DOIP/Op.ChangePassword";

    public static final String SERVICE_ALIAS = "service";

    public static final String REQUEST_CONTEXT_PARAM = "requestContext";

    private static final Set<String> builtInOperations = new HashSet<>();
    static {
        builtInOperations.add(DoipConstants.OP_HELLO);
        builtInOperations.add(DoipConstants.OP_LIST_OPERATIONS);
        builtInOperations.add(DoipConstants.OP_CREATE);
        builtInOperations.add(DoipConstants.OP_RETRIEVE);
        builtInOperations.add(DoipConstants.OP_UPDATE);
        builtInOperations.add(DoipConstants.OP_DELETE);
        builtInOperations.add(DoipConstants.OP_SEARCH);
        builtInOperations.add(OP_AUTH_INTROSPECT);
        builtInOperations.add(OP_AUTH_TOKEN);
        builtInOperations.add(OP_AUTH_REVOKE);
        builtInOperations.add(ReindexBatchOperation.ID);
        builtInOperations.add(OP_CHECK_CREDENTIALS);
        builtInOperations.add(PublishVersionOperation.ID);
        builtInOperations.add(GetVersionsOperation.ID);
    }

    public static boolean isBuiltInOperationWhichNeverUsedAttributesAsInputForDoipGet(String operationId) {
        operationId = unaliasOperation(operationId);
        return builtInOperations.contains(operationId);
    }

    public CordraClientDoipProcessor(InternalCordraClient cordraClient) {
        this.cordraClient = cordraClient;
    }

    @Override
    public void init(JsonObject config) {
        cordraClient = InternalCordraClientFactory.get();
        serviceId = config.get("serviceId").getAsString();
    }

    @Override
    public void shutdown() {
        // a no-op, since the CordraClient is passed in so some other entity should be responsible for closing it
    }

    private JsonObject getRequestContextJsonObject(DoipServerRequest req) {
        JsonElement requestContextJsonObjectAsJsonElement = req.getAttribute(REQUEST_CONTEXT_PARAM);
        JsonObject requestContextJsonObject;
        if ((requestContextJsonObjectAsJsonElement == null) || !requestContextJsonObjectAsJsonElement.isJsonObject() || (requestContextJsonObjectAsJsonElement.getAsJsonObject().size() == 0)) {
            requestContextJsonObject = null;
        } else {
            requestContextJsonObject = requestContextJsonObjectAsJsonElement.getAsJsonObject();
        }
        return requestContextJsonObject;
    }

    private void setInRequestContextHolderRequestContext(DoipServerRequest req) {
        RequestContext requestContext = new RequestContext();
        requestContext.setRequestContext(getRequestContextJsonObject(req));
        requestContext.setAttributes(req.getAttributes());
        requestContext.setSystemCall(false);
        RequestContextHolder.set(requestContext);
    }

    @Override
    public void process(DoipServerRequest req, DoipServerResponse resp) throws IOException {
        setInRequestContextHolderRequestContext(req);
        try {
            String operationId = req.getOperationId();
            if (operationId == null) {
                throw new BadRequestCordraException("Missing operationId");
            }
            String targetId = unaliasTarget(req.getTargetId());
            if (targetId == null) {
                throw new BadRequestCordraException("Missing targetId");
            }
            if (serviceId.equals(targetId)) {
                processService(req, resp);
            } else {
                processObj(req, resp);
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Unexpected exception", e);
            respondWithError(resp, DoipConstants.STATUS_ERROR, "An unexpected server error occurred");
        } catch (CordraException e) {
            respondWithError(resp, e);
        } catch (IOException e) {
            throw e;
        } catch (JsonParseException e) {
            logger.warn("JSON parse error in DOIP", e);
            respondWithError(resp, DoipConstants.STATUS_BAD_REQUEST, "Error parsing JSON");
        } catch (Exception e) {
            logger.error("Unexpected exception", e);
            respondWithError(resp, DoipConstants.STATUS_ERROR, "An unexpected server error occurred");
        } finally {
            MDC.remove("username");
            MDC.remove("asUserId");
        }
    }

    private void respondWithError(DoipServerResponse resp, String errorCode, String message) throws IOException {
        resp.setStatus(errorCode);
        JsonObject errorOutput = new JsonObject();
        errorOutput.addProperty(DoipConstants.MESSAGE_ATT, message);
        resp.writeCompactOutput(errorOutput);
    }

    private void respondWithError(DoipServerResponse resp, CordraException e) throws IOException {
        resp.setStatus(doipStatusForHttpStatus(e.getResponseCode()));
        if (e.getResponse() != null) resp.writeCompactOutput(e.getResponse());
    }

    private void processService(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        String operationId = unaliasOperation(req.getOperationId());
//        if (DoipConstants.OP_HELLO.equals(operationId)) {
//            serviceHello(req, resp);
         if (DoipConstants.OP_LIST_OPERATIONS.equals(operationId)) {
            if (isServiceIdAlsoInstanceObject()) {
                listOperationsForServiceAndServiceObject(req, resp);
            } else {
                listOperationsForService(req, resp);
            }
        } else if (DoipConstants.OP_CREATE.equals(operationId)) {
            noGet(req);
            create(req, resp);
        } else if (DoipConstants.OP_SEARCH.equals(operationId)) {
            search(req, resp);
        } else if (OP_AUTH_TOKEN.equals(operationId)) {
            noGet(req);
            authToken(req, resp);
        } else if (OP_AUTH_INTROSPECT.equals(operationId)) {
            authIntrospect(req, resp);
        } else if (OP_AUTH_REVOKE.equals(operationId)) {
            noGet(req);
            authRevoke(req, resp);
        } else if (OP_CHECK_CREDENTIALS.equals(operationId)) {
            authenticateAndGetResponse(req, resp);
        } else if (OP_CHANGE_PASSWORD.equals(operationId)) {
            noGet(req);
            changePassword(req, resp);
        } else {
            List<String> designMethods = cordraClient.listMethodsForType("CordraDesign", true);
            if (designMethods.contains(operationId)) {
                //                try {
                call(req, resp, true);
                //                } catch (NotFoundCordraException e) {
                //                    respondWithError(resp, DoipConstants.STATUS_DECLINED, "Operation not supported");
                //                }
            } else {
                if (isServiceIdAlsoInstanceObject()) {
                    processObj(req, resp);
                } else {
                    respondWithError(resp, DoipConstants.STATUS_DECLINED, "Operation not supported");
                }
            }
        }
    }

    private boolean isServiceIdAlsoInstanceObject() throws CordraException {
        CordraObject co = cordraClient.get(serviceId);
        if (co == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isAuthToken(DoipServerRequest req) {
        String targetId = unaliasTarget(req.getTargetId());
        if (!serviceId.equals(targetId)) return false;
        String operationId = unaliasOperation(req.getOperationId());
        if (!OP_AUTH_TOKEN.equals(operationId)) return false;
        return true;
    }

    public String unaliasTarget(String targetId) {
        if (SERVICE_ALIAS.equals(targetId)) return serviceId;
        return targetId;
    }

    public static String unaliasOperation(String operationId) {
        return unaliasMap.getOrDefault(operationId, operationId);
    }

    private static final Map<String, String> unaliasMap = Arrays.asList(
        DoipConstants.OP_CREATE,
        DoipConstants.OP_DELETE,
        DoipConstants.OP_HELLO,
        DoipConstants.OP_LIST_OPERATIONS,
        DoipConstants.OP_RETRIEVE,
        DoipConstants.OP_SEARCH,
        DoipConstants.OP_UPDATE,
        OP_AUTH_TOKEN,
        OP_AUTH_INTROSPECT,
        OP_AUTH_REVOKE)
        .stream()
        .collect(Collectors.toMap(CordraClientDoipProcessor::getAliasOfOperationId, Function.identity()));

    private static String getAliasOfOperationId(String op) {
        return op.substring(op.indexOf(".DOIP/Op.") + 9);
    }

    private void authIntrospect(DoipServerRequest req, DoipServerResponse resp) throws IOException, CordraException {
        InDoipSegment initialSegment = InDoipMessageUtil.getFirstSegment(req.getInput());
        JsonElement input = DoipUtil.extractJson(initialSegment);
        if (input == null) input = req.getAttributes();
        if (input == null) input = req.getAuthentication();
        if (input == null || !input.isJsonObject()) throw invalidAuthRequestException();
        if (!input.getAsJsonObject().has("token")) throw invalidAuthRequestException();
        String token = input.getAsJsonObject().get("token").getAsString();
        Options options = new Options();
        options.token = token;
        options.full = Boolean.parseBoolean(getStringAttributeOrNull("full", req));
        AuthResponse authResponse = cordraClient.introspectToken(options);
        resp.writeCompactOutput(gson.toJsonTree(authResponse));
    }

    private static CordraException invalidAuthRequestException() {
        JsonObject errorObject = new JsonObject();
        errorObject.addProperty("error", "invalid_request");
        errorObject.addProperty("error_description", "Invalid request");
        errorObject.addProperty("message", "Invalid request");
        return new BadRequestCordraException(errorObject);
    }

    private void authToken(DoipServerRequest req, DoipServerResponse resp) throws IOException, CordraException {
        try {
            InternalRequestOptions options = optionsOfAuthTokenRequest(req);
            AuthTokenResponse authTokenResponse = cordraClient.getAuthToken(options);
            AccessTokenResponse accessTokenResponse = new AccessTokenResponse();
            accessTokenResponse.access_token = authTokenResponse.token;
            accessTokenResponse.token_type = "Bearer";
            accessTokenResponse.active = authTokenResponse.active;
            accessTokenResponse.userId = authTokenResponse.userId;
            accessTokenResponse.username = authTokenResponse.username;
            accessTokenResponse.groupIds = authTokenResponse.groupIds;
            accessTokenResponse.typesPermittedToCreate = authTokenResponse.typesPermittedToCreate;
            accessTokenResponse.exp = authTokenResponse.exp;
            resp.writeCompactOutput(gson.toJsonTree(accessTokenResponse));
        } catch (BadRequestCordraException e) {
            if (e.getResponseCode() == 429) {
                JsonObject errorObject = new JsonObject();
                errorObject.addProperty("error", "too_many_requests");
                errorObject.addProperty("error_description", e.getMessage());
                errorObject.addProperty("message", e.getMessage());
                throw new BadRequestCordraException(errorObject, 429, e);
            } else {
                throw e;
            }
        } catch (UnauthorizedCordraException e) {
            JsonObject errorObject = new JsonObject();
            errorObject.addProperty("error", "invalid_grant");
            errorObject.addProperty("error_description", "Authentication failed");
            errorObject.addProperty("message", "Authentication failed");
            if (e.isPasswordChangeRequired()) {
                errorObject.addProperty("passwordChangeRequired", true);
            }
            throw new BadRequestCordraException(errorObject, e);
        }
    }

    public static InternalRequestOptions optionsOfAuthTokenRequest(DoipServerRequest req) throws IOException, CordraException, BadRequestCordraException {
        if (req instanceof DoipServerRequestImplWithCachedOptions) {
            InternalRequestOptions options = ((DoipServerRequestImplWithCachedOptions) req).getOptions();
            if (options != null) {
                if (((DoipServerRequestImplWithCachedOptions) req).isGet()) options.isGet = true;
                return options;
            }
        }
        InternalRequestOptions options = new InternalRequestOptions();
        InDoipSegment initialSegment = InDoipMessageUtil.getFirstSegment(req.getInput());
        JsonElement input = DoipUtil.extractJson(initialSegment);
        if (input == null) input = req.getAttributes();
        if (input == null) input = req.getAuthentication();
        if (input == null || !input.isJsonObject()) throw invalidAuthRequestException();
        options.authTokenInput = input.getAsJsonObject();
        options.full = Boolean.parseBoolean(getStringAttributeOrNull("full", req));
        if (req instanceof DoipServerRequestImplWithCachedOptions) {
            if (((DoipServerRequestImplWithCachedOptions) req).isGet()) options.isGet = true;
            ((DoipServerRequestImplWithCachedOptions) req).setOptions(options);
        }
        return options;
    }

    private static String getStringAttributeOrNull(String name, DoipServerRequest req) {
        JsonObject attributes = req.getAttributes();
        if (attributes != null && attributes.has(name)) {
            String att = attributes.get(name).getAsString();
            return att;
        }
        return null;
    }

    private void authRevoke(DoipServerRequest req, DoipServerResponse resp) throws IOException, CordraException {
        InDoipSegment initialSegment = InDoipMessageUtil.getFirstSegment(req.getInput());
        JsonElement input = DoipUtil.extractJson(initialSegment);
        if (input == null) input = req.getAttributes();
        if (input == null) input = req.getAuthentication();
        if (input == null || !input.isJsonObject()) throw invalidAuthRequestException();
        if (!input.getAsJsonObject().has("token")) throw invalidAuthRequestException();
        String token = input.getAsJsonObject().get("token").getAsString();
        Options options = new Options();
        options.token = token;
        cordraClient.revokeToken(options);
        AuthResponse authResponse = new AuthResponse();
        authResponse.active = false;
        resp.writeCompactOutput(gson.toJsonTree(authResponse));
    }

    private String doipStatusForHttpStatus(int statusCode) {
        if (statusCode == 200) return DoipConstants.STATUS_OK;
        if (statusCode == 400) return DoipConstants.STATUS_BAD_REQUEST;
        if (statusCode == 401) return DoipConstants.STATUS_UNAUTHENTICATED;
        if (statusCode == 403) return DoipConstants.STATUS_FORBIDDEN;
        if (statusCode == 404) return DoipConstants.STATUS_NOT_FOUND;
        if (statusCode == 409) return DoipConstants.STATUS_CONFLICT;
        if (statusCode >= 200 && statusCode <= 299) return DoipConstants.STATUS_OK;
        if (statusCode >= 400 && statusCode <= 499) return DoipConstants.STATUS_BAD_REQUEST;
        return DoipConstants.STATUS_ERROR;
    }

    private void processObj(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        String operationId = unaliasOperation(req.getOperationId());
        String targetId = unaliasTarget(req.getTargetId());
        if (DoipConstants.OP_RETRIEVE.equals(operationId)) {
            retrieve(req, resp);
        } else if (DoipConstants.OP_UPDATE.equals(operationId)) {
            noGet(req);
            update(req, resp);
        } else if (DoipConstants.OP_DELETE.equals(operationId)) {
            noGet(req);
            delete(req, resp);
        } else if (DoipConstants.OP_LIST_OPERATIONS.equals(operationId)) {
            listOperationsForObject(targetId, req, resp);
//        } else if (OP_VERSIONS_PUBLISH.equals(operationId)) {
//            publishVersion(req, resp);
//        } else if (OP_VERSIONS_GET.equals(operationId)) {
//            getVersions(req, resp);
        } else {
            call(req, resp, false);
        }
    }

    private void noGet(DoipServerRequest req) throws ForbiddenCordraException {
        if (req instanceof DoipServerRequestImplWithCachedOptions) {
            if (((DoipServerRequestImplWithCachedOptions) req).isGet()) {
                throw new ForbiddenCordraException("Operation " + req.getOperationId() + " does not allow GET");
            }
        }
    }

    private void authenticateAndGetResponse(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        Options options = authenticate(req);
        AuthResponse sessionResponse = cordraClient.authenticateAndGetResponse(options);
        String responseJson = gson.toJson(sessionResponse);
        resp.writeCompactOutput(JsonParser.parseString(responseJson));
    }

    private void changePassword(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        InternalRequestOptions options = authenticate(req);
        options.isChangePassword = true;
        // allow sending in input in case attributes (query-params in HTTP) are considered less secure
        JsonObject attributes = combineAttributesAndInput(req);
        if (attributes == null || !attributes.has("password")) {
            throw new BadRequestCordraException("No password supplied");
        }
        String newPassword = attributes.get("password").getAsString();
        cordraClient.changePassword(newPassword, options);
        resp.writeCompactOutput(JsonParser.parseString("{\"success\":true}"));
    }

    private void create(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        Options options = authenticate(req);
        CordraObject co = DoipUtil.cordraObjectFromSegments(req.getInput());
        co = cordraClient.create(co, options);
        DigitalObject dobj = DoipUtil.ofCordraObject(co);
        JsonElement dobjJson = gson.toJsonTree(dobj);
        resp.writeCompactOutput(dobjJson);
    }

//    private void publishVersion(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
//        String objectId = req.getTargetId();
//        String versionId = req.getAttributeAsString("versionId");
//        boolean clonePayloads = Boolean.parseBoolean(req.getAttributeAsString("clonePayloads"));
//        Options options = authenticate(req);
//        VersionInfo versionInfo = cordraClient.publishVersion(objectId, versionId, clonePayloads, options);
//        JsonElement versionInfoAsJson = gson.toJsonTree(versionInfo);
//        resp.writeCompactOutput(versionInfoAsJson);
//    }
//
//    private void getVersions(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
//        String objectId = req.getTargetId();
//        Options options = authenticate(req);
//        List<VersionInfo> versionInfos = cordraClient.getVersionsFor(objectId, options);
//        resp.writeCompactOutput(gson.toJsonTree(versionInfos));
//    }

//    private void reindexBatch(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
//        List<String> batchIds = gson.fromJson(req.getAttribute("batchIds").getAsJsonArray(), new TypeToken<List<String>>(){}.getType());
//        Options options = authenticate(req);
//        options.reindexBatchLockObjects = req.getAttribute("lockObjects").getAsBoolean();
//        cordraClient.reindexBatch(batchIds, options);
//        resp.writeCompactOutput(JsonParser.parseString("{\"success\":true}"));
//    }

    private void retrieve(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        Options options = authenticate(req);
        AuthenticationResult authResult = cordraClient.internalAuthenticate(options);
        if (authResult.active == true) options = PreAuthenticatedOptions.build(authResult);
        String targetId = unaliasTarget(req.getTargetId());
        String element = req.getAttributeAsString("element");
        boolean includeElementData = element == null && getBooleanAttribute(req, "includeElementData");
        JsonElement filterAsJsonElement = req.getAttribute("filter");
        if (filterAsJsonElement != null) {
            try {
                JsonArray filterAsJsonArray = filterAsJsonElement.getAsJsonArray();
                List<String> filter = GsonUtility.getGson().fromJson(filterAsJsonArray, new TypeToken<List<String>>(){}.getType());
                options.filter = filter;
            } catch (Exception e) {
                throw new BadRequestCordraException("Unable to parse filter attribute", e);
            }
        }
        CordraObject co = cordraClient.get(targetId, options);
        if (co == null) throw new NotFoundCordraException("Missing object: " + targetId);
        if (element == null) {
            DigitalObject dobj = DoipUtil.ofCordraObject(co);
            JsonElement dobjJson = gson.toJsonTree(dobj);
            if (!includeElementData) {
                resp.writeCompactOutput(dobjJson);
            } else {
                resp.getOutput().writeJson(dobjJson);
                if (dobj.elements != null) {
                    for (Element el : dobj.elements) {
                        JsonObject header = new JsonObject();
                        header.addProperty("id", el.id);
                        resp.getOutput().writeJson(header);
                        try (InputStream in = cordraClient.getPayload(targetId, el.id, options)) {
                            if (in == null) throw new NotFoundCordraException("Missing element: " + targetId + " element " + element);
                            resp.getOutput().writeBytes(in);
                        }
                    }
                }
            }
        } else { // element != null
            Payload payload = getPayloadMetadataByName(co, element);
            if (payload != null && payload.filename != null) {
                resp.setAttribute("filename", payload.filename);
            }
            if (payload != null && payload.mediaType != null) {
                resp.setAttribute("mediaType", payload.mediaType);
            }

            JsonElement rangeElement = req.getAttribute("range");

            if (rangeElement == null) {
                try (InputStream in = cordraClient.getPayload(targetId, element, options)) {
                    if (in == null) throw new NotFoundCordraException("Missing element: " + targetId + " element " + element);
                    resp.getOutput().writeBytes(in);
                }
            } else {
                JsonObject range = rangeElement.getAsJsonObject();
                Long start = null;
                Long end = null;
                if (range.has("start")) {
                    start = range.get("start").getAsLong();
                }
                if (range.has("end")) {
                    end = range.get("end").getAsLong();
                }
                try (InputStream in = cordraClient.getPartialPayload(targetId, element, start, end, options)) {
                    if (in == null) throw new NotFoundCordraException("Missing element: " + targetId + " element " + element);
                    resp.getOutput().writeBytes(in);
                }
            }
        }
    }

    private Payload getPayloadMetadataByName(CordraObject co, String payloadName) {
        Payload result = null;
        if (co.payloads != null) {
            for (Payload payload : co.payloads) {
                if (payloadName.equals(payload.name)) {
                    result = payload;
                    break;
                }
            }
        }
        return result;
    }


    private boolean getBooleanAttribute(DoipServerRequest req, String att) {
        JsonElement el = req.getAttribute(att);
        if (el == null) return false;
        if (!el.isJsonPrimitive()) return false;
        JsonPrimitive priv = el.getAsJsonPrimitive();
        if (priv.isBoolean()) return priv.getAsBoolean();
        if (priv.isString()) return "true".equals(priv.getAsString());
        return false;
    }

    private void update(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        Options options = authenticate(req);
        CordraObject co = DoipUtil.cordraObjectFromSegments(req.getInput());
        if (co.id == null) co.id = unaliasTarget(req.getTargetId());
        if (!co.id.equals(unaliasTarget(req.getTargetId()))) {
            throw new BadRequestCordraException("targetId must match id of Cordra object");
        }
        JsonObject attributes = req.getAttributes();
        if (attributes != null && attributes.has("elementsToDelete")) {
            JsonElement elementsToDeleteEl = attributes.get("elementsToDelete");
            if (!elementsToDeleteEl.isJsonArray()) {
                throw new BadRequestCordraException("elementsToDelete is not an array");
            }
            JsonArray elementsToDelete = elementsToDeleteEl.getAsJsonArray();
            for (int i = 0; i < elementsToDelete.size(); i++) {
                String elementName = elementsToDelete.get(i).getAsString();
                co.deletePayload(elementName);
            }
        }
        co = cordraClient.update(co, options);
        DigitalObject dobj = DoipUtil.ofCordraObject(co);
        JsonElement dobjJson = gson.toJsonTree(dobj);
        resp.writeCompactOutput(dobjJson);
    }

    @SuppressWarnings("unused")
    private void delete(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        Options options = authenticate(req);
        String targetId = unaliasTarget(req.getTargetId());
        cordraClient.delete(targetId, options);
    }

    private void search(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        Options options = authenticate(req);
        JsonObject attributes = combineAttributesAndInput(req);
        if (attributes == null) throw new BadRequestCordraException("Missing query");
        String query = null;
        if (attributes.has("query")) query = attributes.get("query").getAsString();
        if (query == null) throw new BadRequestCordraException("Missing query");
        String type = "full";
        if (attributes.has("type")) type = attributes.get("type").getAsString();
        fixLegacySortFields(attributes);
        fixStringyJsonArray(attributes, "facets");
        fixStringyJsonArray(attributes, "filterQueries");
        QueryParams params = gson.fromJson(attributes, QueryParams.class);
        if ("id".equals(type)) {
            try (SearchResults<String> results = cordraClient.searchHandles(query, params, options)) {
                writeSearchResults(resp, results, String.class);
            }
        } else {
            try (SearchResults<CordraObject> results = cordraClient.search(query, params, options)) {
                writeSearchResults(resp, results, CordraObject.class);
            }
        }
    }

    private void fixStringyJsonArray(JsonObject attributes, String prop) {
        if (!attributes.has(prop)) return;
        if (!attributes.get(prop).isJsonPrimitive()) return;
        attributes.add(prop, JsonParser.parseString(attributes.get(prop).getAsString()));
    }

    private void fixLegacySortFields(JsonObject attributes) {
        if (attributes.has("sortFields")) {
            JsonElement sortFieldsElem = attributes.get("sortFields");
            if (sortFieldsElem.isJsonPrimitive()) {
                String sortFieldsString = sortFieldsElem.getAsString();
                if (sortFieldsString.trim().startsWith("[")) {
                    attributes.add("sortFields", JsonParser.parseString(sortFieldsString));
                } else {
                    List<SortField> sortFields = SortField.getSortFieldsFromString(sortFieldsString);
                    attributes.add("sortFields", gson.toJsonTree(sortFields));
                }
            } else {
                JsonArray arr = sortFieldsElem.getAsJsonArray();
                for (int i = 0; i < arr.size(); i++) {
                    if (arr.get(i).isJsonPrimitive()) {
                        SortField sortField = SortField.getSortFieldFromString(arr.get(i).getAsString());
                        arr.set(i, gson.toJsonTree(sortField));
                    }
                }
            }
        }
    }

    private <T> void writeSearchResults(DoipServerResponse resp, SearchResults<T> results, Class<T> klass) throws IOException {
        try (JsonWriter writer = new JsonWriter(resp.getOutput().getJsonWriter())) {
            List<FacetResult> facetResults = null;
            try {
                facetResults = results.getFacets();
            } catch (IncompatibleClassChangeError e) {
                logger.warn("Unexpected mismatch of cordra jar versions on call to SearchResults.getFacets");
            }
            writeBeginResults(writer, results.size(), facetResults);
            for (T res : results) {
                if (klass == String.class) {
                    writer.value((String) res);
                } else {
                    DigitalObject dobj = DoipUtil.ofCordraObject((CordraObject)res);
                    gson.toJson(dobj, DigitalObject.class, writer);
                }
            }
            writer.endArray();
            writer.endObject();
        }
    }

    // We allow search to take input as well as attributes; this supports more natural POST /doip?operationId=Search
    private JsonObject combineAttributesAndInput(DoipServerRequest req) throws IOException, CordraException {
        JsonObject attributes = req.getAttributes();
        InDoipSegment initialSegment = InDoipMessageUtil.getFirstSegment(req.getInput());
        JsonElement input = DoipUtil.extractJson(initialSegment);
        if (input == null || !input.isJsonObject()) return attributes;
        JsonObject res = input.getAsJsonObject();
        if (attributes == null) return res;
        for (Map.Entry<String, JsonElement> entry : attributes.entrySet()) {
            // attributes take precedence
            res.add(entry.getKey(), entry.getValue());
        }
        return res;
    }

    private void writeBeginResults(JsonWriter writer, int size, List<FacetResult> facets) throws IOException {
        writer.setIndent("  ");
        writer.beginObject();
        writer.name("size").value(size);
        if (facets != null && !facets.isEmpty()) {
            writer.name("facets");
            gson.toJson(facets, new TypeToken<List<FacetResult>>() { }.getType(), writer);
        }
        writer.name("results").beginArray();
    }

//    private void serviceHello(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
//        if (containsAuthInfo(req)) {
//            Options options = authenticate(req);
//            AuthResponse authResponse = cordraClient.authenticateAndGetResponse(options);
//            if (authResponse.active == false) {
//                throw new UnauthorizedCordraException("Authentication failed");
//            }
//        }
//        JsonObject res = buildDoipServiceInfo(serviceId, address, serviceName, serviceDescription, port, publicKey);
//        resp.writeCompactOutput(res);
//    }

//    public static JsonObject buildDoipServiceInfo(String serviceId, String address, String serviceName, String serviceDescription, int port, PublicKey publicKey) {
//        JsonObject res = new JsonObject();
//        res.addProperty("id", serviceId);
//        res.addProperty("type", "0.TYPE/DOIPServiceInfo");
//        JsonObject atts = new JsonObject();
//        atts.addProperty("ipAddress", address);
//        atts.addProperty("port", port);
//        atts.addProperty("protocol", "TCP"); // DOIP specification: internet protocol used by DOIP; TCP is the default
//        atts.addProperty("protocolVersion", "2.0"); // DOIP specification: this is the highest version of the DOIP protocol supported
//
//        JsonObject cordraVersion = getCordraVersion();
//        if (cordraVersion != null) {
//            atts.add("cordraVersion", cordraVersion);
//        }
//        if (serviceName != null) {
//            atts.addProperty("serviceName", serviceName);
//        }
//        if (serviceDescription != null) {
//            atts.addProperty("serviceDescription", serviceDescription);
//        }
//        if (publicKey != null) {
//            atts.add("publicKey", gson.toJsonTree(publicKey));
//        }
//        res.add("attributes", atts);
//        return res;
//    }
//
//    private static JsonObject getCordraVersion() {
//        JsonObject version = null;
//        String cordraVersion = System.getProperty("cordra.version.number");
//        if (cordraVersion != null) {
//            String number = System.getProperty("cordra.version.number");
//            String timestamp = System.getProperty("cordra.version.timestamp");
//            String id = System.getProperty("cordra.version.id");
//            version = new JsonObject();
//            version.addProperty("number", number);
//            version.addProperty("timestamp", timestamp);
//            version.addProperty("id", id);
//        }
//        return version;
//    }

    private void listOperationsForServiceAndServiceObject(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        Options options = authenticate(req);
        AuthenticationResult authResult = cordraClient.internalAuthenticate(options);
        if (authResult.active == true) options = PreAuthenticatedOptions.build(authResult);
        options.setIncludeCrud(true);
        Set<String> opsForService = getOperationsForService();
        CordraObject co = null;
        try {
            co = cordraClient.get(serviceId, options);
        } catch (UnauthorizedCordraException uce) {
            //no-op
        }
        Set<String> opsForObject = new LinkedHashSet<>();
        if (co != null) {
            opsForObject = getOperationsForObject(co, options);
        }
        opsForService.addAll(opsForObject);
        JsonArray res = new JsonArray();
        opsForService.forEach(res::add);
        resp.writeCompactOutput(res);
    }

    private void listOperationsForService(DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        if (containsAuthInfo(req)) {
            Options options = authenticate(req);
            AuthResponse authResponse = cordraClient.authenticateAndGetResponse(options);
            if (authResponse.active == false) {
                throw new UnauthorizedCordraException("Authentication failed");
            }
        }
        JsonArray res = new JsonArray();
        Set<String> ops = getOperationsForService();
        ops.forEach(res::add);
        resp.writeCompactOutput(res);
    }

    private Set<String> getOperationsForService() throws CordraException {
        Set<String> ops = new LinkedHashSet<>();
        ops.add(DoipConstants.OP_HELLO);
        ops.add(DoipConstants.OP_LIST_OPERATIONS);
        ops.add(DoipConstants.OP_CREATE);
        ops.add(DoipConstants.OP_SEARCH);
        ops.add(OP_AUTH_TOKEN);
        ops.add(OP_AUTH_INTROSPECT);
        ops.add(OP_AUTH_REVOKE);
        ops.add(OP_CHANGE_PASSWORD);
        ops.add(OP_CHECK_CREDENTIALS);
        List<String> designMethods = cordraClient.listMethodsForType("CordraDesign", true);
        ops.addAll(designMethods);
        return ops;
    }

    private Set<String> getOperationsForObject(CordraObject co, Options options) throws CordraException {
        Set<String> ops = new LinkedHashSet<>();
        ops.add(DoipConstants.OP_LIST_OPERATIONS);
        //        ops.add(DoipConstants.OP_RETRIEVE);
        //        ops.add(DoipConstants.OP_UPDATE);
        //        ops.add(DoipConstants.OP_DELETE);
        List<String> methods = null;
        if ("Schema".equals(co.type)) {
            methods = cordraClient.listMethodsForType(getTypeNameForSchemaObject(co), true, options);
            //            methods.forEach(ops::add);
        } else {
            methods = cordraClient.listMethods(co.id, options);
            //            methods.forEach(ops::add);
        }
        for (String method : methods) {
            if ("read".equals(method)) {
                ops.add(DoipConstants.OP_RETRIEVE);
            } else if ("payloadRead".equals(method)) {
                //ops.add(method);
                //don't add anything
            } else if ("write".equals(method)) {
                ops.add(DoipConstants.OP_UPDATE);
            } else if ("delete".equals(method)) {
                ops.add(DoipConstants.OP_DELETE);
            } else {
                ops.add(method);
            }
        }
        return ops;
    }

    private void listOperationsForObject(String targetId, DoipServerRequest req, DoipServerResponse resp) throws CordraException, IOException {
        Options options = authenticate(req);
        AuthenticationResult authResult = cordraClient.internalAuthenticate(options);
        if (authResult.active == true) options = PreAuthenticatedOptions.build(authResult);
        options.setIncludeCrud(true);
        CordraObject co = cordraClient.get(targetId, options);
        if (co == null) {
            respondWithError(resp, DoipConstants.STATUS_NOT_FOUND, "No such object " + targetId);
        } else {
            JsonArray res = new JsonArray();
            Set<String> ops = getOperationsForObject(co, options);
            ops.forEach(res::add);
            resp.writeCompactOutput(res);
        }
    }

    private void addCallHeadersToOptionsIfNecessary(DoipServerRequest req, Options options) {
        JsonObject attributes = req.getAttributes();
        if (attributes != null) {
            JsonElement mediaTypeEl = attributes.get("mediaType");
            if (mediaTypeEl != null && mediaTypeEl.isJsonPrimitive()) {
                String mediaType = attributes.get("mediaType").getAsString();
                if (options.callHeaders == null) {
                    options.callHeaders = new CallHeaders();
                }
                options.callHeaders.mediaType = mediaType;
            }
            JsonElement filenameEl = attributes.get("filename");
            if (filenameEl != null && filenameEl.isJsonPrimitive()) {
                String filename = attributes.get("filename").getAsString();
                if (options.callHeaders == null) {
                    options.callHeaders = new CallHeaders();
                }
                options.callHeaders.filename = filename;
            }
        }
    }

    private void call(DoipServerRequest req, DoipServerResponse resp, boolean asService) throws CordraException, IOException {
        Options options = authenticate(req);
        options.attributes = req.getAttributes();
        addCallHeadersToOptionsIfNecessary(req, options);
        String operationId = unaliasOperation(req.getOperationId());
        String targetId = unaliasTarget(req.getTargetId());
        boolean isCallForType = "true".equals(req.getAttributeAsString("isCallForType"));
        InDoipSegment initialSegment = InDoipMessageUtil.getFirstSegment(req.getInput());
        InputStream in = null;
        CallResponse result = null;
        if (asService) {
            targetId = "CordraDesign";
            isCallForType = true;
        }
        try {
            if (initialSegment != null) {
                in = initialSegment.getInputStream();
            }
            if (isCallForType) {
                String type = targetId;
                result = cordraClient.callForTypeAsResponse(type, operationId, in, options);
            } else {
                CordraObject co = cordraClient.get(targetId);
                if (co == null) {
                    // throw an authorization error if appropriate (missing objects may result in auth errors depending on default defaultAclRead)
                    co = cordraClient.get(targetId, options);
                    respondWithError(resp, DoipConstants.STATUS_NOT_FOUND, "No such object " + targetId);
                    return;
                }
                if ("Schema".equals(co.type)) {
                    result = cordraClient.callForTypeAsResponse(this.getTypeNameForSchemaObject(co), operationId, in, options);
                } else {
                    result = cordraClient.callAsResponse(targetId, operationId, in, options);
                }
            }
            if (isJson(result.headers)) {
                try (InputStreamReader isr = new InputStreamReader(result.body, StandardCharsets.UTF_8)) {
                    String jsonString = StreamUtil.readFully(isr);
                    if (!jsonString.trim().isEmpty()) {
                        JsonElement json = JsonParser.parseString(jsonString);
                        if (json.isJsonNull()) {
                            // workaround for difference between explicit null result and undefined/missing result
                            resp.getOutput().writeJson(json);
                        } else {
                            resp.writeCompactOutput(json);
                        }
                    }
                }
            } else {
                if (result.headers.mediaType != null) {
                    resp.setAttribute("mediaType", result.headers.mediaType);
                }
                if (result.headers.filename != null) {
                    resp.setAttribute("filename", result.headers.filename);
                }
                resp.getOutput().writeBytes(result.body);
            }
        } finally {
            if (in != null) try { in.close(); } catch (Exception e) { }
            if (!InDoipMessageUtil.isEmpty(req.getInput())) {
                respondWithError(resp, DoipConstants.STATUS_BAD_REQUEST, "Cordra operation expects at most single segment");
                return;
            }
            if (result != null) result.close();
        }
    }

    private boolean isJson(CallHeaders headers) {
        if (headers == null || headers.mediaType == null) return false;
        String type = headers.mediaType.toLowerCase(Locale.ROOT);
        if ("application/json".equals(type)) return true;
        if (!type.startsWith("application/json")) return false;
        char nextChar = type.charAt("application/json".length());
        if (nextChar == ' ' || nextChar == '\t' || nextChar ==';') return true;
        return false;
    }

    private String getTypeNameForSchemaObject(CordraObject co) {
        return co.content.getAsJsonObject().get("name").getAsString();
    }

    private boolean containsAuthInfo(DoipServerRequest req) {
        if (isNullOrEmpty(req.getAuthentication()) && req.getConnectionClientId() == null) {
            return false;
        } else {
            return true;
        }
    }

    private InternalRequestOptions authenticate(DoipServerRequest req) throws CordraException {
        if (req instanceof DoipServerRequestImplWithCachedOptions) {
            InternalRequestOptions options = ((DoipServerRequestImplWithCachedOptions) req).getOptions();
            if (options != null) {
                if (((DoipServerRequestImplWithCachedOptions) req).isGet()) options.isGet = true;
                return options;
            }
        }
        if (req.getAuthentication() != null && !req.getAuthentication().isJsonObject()) {
            throw new UnauthorizedCordraException("Unable to parse authentication (not an object)");
        }
        if (isNullOrEmpty(req.getAuthentication())) {
            if (req.getConnectionClientId() != null) {
                return authenticateViaTls(req);
            }
            if (req.getClientId() == null) {
                // anonymous
                InternalRequestOptions options = new InternalRequestOptions();
                options.setUseDefaultCredentials(false);
                return options;
            } else {
                throw new UnauthorizedCordraException("No authentication provided for " + req.getClientId());
            }
        }
        JsonObject authentication = req.getAuthentication().getAsJsonObject();
        InternalRequestOptions options = new InternalRequestOptions();
        options.doipClientId = req.getClientId();
        options.doipAuthentication = authentication;
        if (authentication.has("asUserId")) {
            options.setAsUserId(authentication.get("asUserId").getAsString());
            MDC.put("asUserId", authentication.get("asUserId").getAsString());
        }
        if (authentication.has("username")) {
            MDC.put("username", authentication.get("username").getAsString());
        }
        return options;
    }

    public static boolean isNullOrEmpty(JsonElement authentication) {
        if (authentication == null) return true;
        if (authentication.isJsonNull()) return true;
        if (!authentication.isJsonObject()) return false;
        if (authentication.getAsJsonObject().keySet().isEmpty()) return true;
        return false;
    }

    private InternalRequestOptions authenticateViaTls(DoipServerRequest req) throws UnauthorizedCordraException {
        if (req.getClientId() != null && !req.getClientId().equals(req.getConnectionClientId())) {
            throw new UnauthorizedCordraException("No authentication provided for " + req.getClientId());
        }
        String clientId = req.getConnectionClientId();
        PublicKey clientPublicKey = req.getConnectionPublicKey();
        if (clientPublicKeyChecks(clientId, clientPublicKey)) {
            InternalRequestOptions options = new InternalRequestOptions();
            options.setAsUserId(clientId).setUseDefaultCredentials(true);
            return options;
        } else {
            throw new UnauthorizedCordraException("Client TLS certificate key does not match handle record of " + clientId);
        }
    }

    private boolean clientPublicKeyChecks(String clientId, PublicKey clientPublicKey) {
        List<PublicKey> publicKeys = getPublicKeysFor(clientId);
        for (PublicKey foundPublicKey : publicKeys) {
            if (foundPublicKey.equals(clientPublicKey)) {
                return true;
            }
        }
        return false;
    }

    private List<PublicKey> getPublicKeysFor(String iss) {
        List<PublicKey> result = new ArrayList<>();
        if ("admin".equals(iss)) {
            try {
                JsonElement adminPublicKeyElement = cordraClient.get("design").content.getAsJsonObject().get("adminPublicKey");
                if (adminPublicKeyElement != null) {
                    result.add(gson.fromJson(adminPublicKeyElement, PublicKey.class));
                }
            } catch (Exception e) {
                logger.warn("Error checking admin public key", e);
            }
            return result;
        }
        try {
            HandleValue[] values = resolver.resolveHandle(Util.encodeString(iss), Common.PUBLIC_KEY_TYPES, null);
            List<PublicKey> pubkeyValues = Util.getPublicKeysFromValues(values);
            result.addAll(pubkeyValues);
        } catch (HandleException e) {
            // error resolving handle
        }
        return result;
    }
}
