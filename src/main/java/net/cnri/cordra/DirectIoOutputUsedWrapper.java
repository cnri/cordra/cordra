package net.cnri.cordra;

import java.io.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class DirectIoOutputUsedWrapper implements DirectIo {

    private final DirectIo directIo;
    private AtomicBoolean usedDirectOutput;

    public DirectIoOutputUsedWrapper(DirectIo directIo) {
        this.directIo = directIo;
        this.usedDirectOutput = new AtomicBoolean();
    }

    public boolean hasUsedDirectOutput() {
        return usedDirectOutput.get();
    }

    @Override
    public InputStream getInputAsInputStream() throws IOException {
        return directIo.getInputAsInputStream();
    }

    @Override
    public Reader getInputAsReader() throws IOException {
        return directIo.getInputAsReader();
    }

    @Override
    public byte[] getInputAsBytes() throws IOException {
        return directIo.getInputAsBytes();
    }

    @Override
    public String getInputAsString() throws IOException {
        return directIo.getInputAsString();
    }

    @Override
    public OutputStream getOutputAsOutputStream() throws IOException {
        usedDirectOutput.set(true);
        return directIo.getOutputAsOutputStream();
    }

    @Override
    public Writer getOutputAsWriter() throws IOException {
        usedDirectOutput.set(true);
        return directIo.getOutputAsWriter();
    }

    @Override
    public void writeOutputBytes(byte[] output) throws IOException {
        usedDirectOutput.set(true);
        directIo.writeOutputBytes(output);
    }

    @Override
    public void writeOutputString(String output) throws IOException {
        usedDirectOutput.set(true);
        directIo.writeOutputString(output);
    }

    @Override
    public String getInputMediaType() {
        return directIo.getInputMediaType();
    }

    @Override
    public String getInputFilename() {
        return directIo.getInputFilename();
    }

    @Override
    public void setOutputMediaType(String mediaType) {
        directIo.setOutputMediaType(mediaType);
    }

    @Override
    public void setOutputFilename(String filename) {
        directIo.setOutputFilename(filename);
    }
}
