package net.cnri.cordra.handle;

import net.cnri.cordra.model.HandleServerConfig;
import net.cnri.cordra.util.NetworkUtil;
import net.cnri.util.GrowBeforeTransferQueueThreadPoolExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;

public class HdlTcpInterface {
    private static Logger logger = LoggerFactory.getLogger(HdlTcpInterface.class);

    public static final int DEFAULT_PORT = 2641;

    private Set<String> bindAddresses;
    private int bindPort = 2641;
    private boolean logAccesses = false;
    private final int maxHandlers = 200;
    private final int numThreads = 10;
    private final int maxIdleTime = 5 * 60 * 1000;

    private List<ServerSocket> serverSockets = new ArrayList<>();
    private volatile boolean keepServing = true;
    private LightWeightHandleServer server;
    private ExecutorService handlerPool = null;

    public HdlTcpInterface(LightWeightHandleServer server, HandleServerConfig config) {
        this.server = server;
        init(config);
    }

    public void stopRunning() {
        keepServing = false;
        for (ServerSocket serverSocket : serverSockets) {
            try {
                serverSocket.close();
            } catch (Exception e) {
                // Do nothing
            }
        }
        ExecutorService pool = handlerPool;
        if (pool != null) {
            pool.shutdown();
            boolean terminated = false;
            try {
                terminated = pool.awaitTermination(30, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            if (!terminated) {
                pool.shutdownNow();
                try {
                    terminated = pool.awaitTermination(30, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
        logger.info("Handle TCP Request Listener stopped");
    }

    public void serveRequests(ServerSocket serverSocket) {
        while (keepServing) {
            try {
                @SuppressWarnings("resource")
                Socket newsock = serverSocket.accept();
                newsock.setSoTimeout(maxIdleTime);
                long recvTime = System.currentTimeMillis();
                handlerPool.execute(new HdlTcpRequestHandler(server, logAccesses, newsock, recvTime));
            } catch (Exception e) {
                if (keepServing) {
                    logger.error("Error handling request", e);
                }
            }
        }
    }

    private void init(HandleServerConfig config) {
        Set<String> listenAddresses = new HashSet<>();
        if (config.listenAddress != null) listenAddresses.add(config.listenAddress);
        if (config.listenAddresses != null) listenAddresses.addAll(config.listenAddresses);
        if (listenAddresses.isEmpty()) listenAddresses.add("localhost");
        if (listenAddresses.contains("localhost")) {
            listenAddresses.remove("localhost");
            listenAddresses.add("127.0.0.1");
            if (NetworkUtil.canUseIPv6()) listenAddresses.add("::1");
        }
        bindAddresses = listenAddresses;
        if (config.tcpPort != null) {
            bindPort = config.tcpPort; // get the port to listen on...
        } else {
            bindPort = DEFAULT_PORT;
        }
        if (config.logAccesses != null) {
            logAccesses = config.logAccesses;
        } else {
            logAccesses = false;
        }
    }

    public void start() throws Exception {
        keepServing = true;
        for (String bindAddress : bindAddresses) {
            serverSockets.add(new ServerSocket(bindPort, -1, InetAddress.getByName(bindAddress)));
        }
        System.out.println("Initializing Handle TCP interface on port " + bindPort);
        logger.info("Initializing Handle TCP interface on port " + bindPort);
        handlerPool = new GrowBeforeTransferQueueThreadPoolExecutor(numThreads, maxHandlers, 1, TimeUnit.MINUTES, new LinkedTransferQueue<>());
        for (ServerSocket serverSocket : serverSockets) {
            new Thread(() -> serveRequests(serverSocket), "Handle-Socket-Accept-Thread-" + serverSocket.getInetAddress().getHostAddress()).start();
        }
    }
}
