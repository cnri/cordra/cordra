package net.cnri.cordra;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;

import net.cnri.cordra.util.JacksonUtil;

public class CordraDesignSchemaFactory {
    static final JsonNode cordraDesignSchemaNode;
    static final JsonSchema cordraDesignSchema;

    static {
        try {
            String cordraDesignSchemaString = DefaultSchemasFactory.getCordraDesignSchema();
            cordraDesignSchemaNode = JacksonUtil.parseJson(cordraDesignSchemaString);
            cordraDesignSchema = JsonSchemaFactoryFactory.newJsonSchemaFactory().getJsonSchema(cordraDesignSchemaNode);
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    public static JsonNode getNode() {
        return cordraDesignSchemaNode;
    }

    public static JsonSchema getSchema() {
        return cordraDesignSchema;
    }
}
