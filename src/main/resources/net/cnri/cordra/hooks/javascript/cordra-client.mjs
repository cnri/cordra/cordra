const CordraClientTypes = require('cordra-client');

export default CordraClientTypes.CordraClient;

export const CordraClient = CordraClientTypes.CordraClient;
export const Blob = CordraClientTypes.Blob;
export const Response = CordraClientTypes.Response;
export const Headers = CordraClientTypes.Headers;
export const BlobMethod = CordraClientTypes.BlobMethod;
export const BlobUtil = CordraClientTypes.BlobUtil;
