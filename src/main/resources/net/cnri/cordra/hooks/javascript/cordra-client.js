var CordraError = require('cordra').CordraError;

var Options = Java.type('net.cnri.cordra.api.Options');
var PreAuthenticatedOptions = Java.type('net.cnri.cordra.auth.PreAuthenticatedOptions');
var CordraObject = Java.type('net.cnri.cordra.api.CordraObject');
var AccessControlList = Java.type('net.cnri.cordra.api.CordraObject.AccessControlList');
var QueryParams = Java.type('net.cnri.cordra.api.QueryParams');
var JsonParser = Java.type('com.google.gson.JsonParser');
var CordraException = Java.type('net.cnri.cordra.api.CordraException');
var Exception = Java.type('java.lang.Exception');
var InputStream = Java.type('java.io.InputStream');
var StreamUtil = Java.type('net.cnri.util.StreamUtil');
var JavaString = Java.type('java.lang.String');
var HttpUtil = Java.type('net.cnri.cordra.util.HttpUtil');
var ByteArrayInputStream = Java.type('java.io.ByteArrayInputStream');
var CordraClientUtil = Java.type('net.cnri.cordra.hooks.javascript.CordraClientUtil');
var JsonUtil = Java.type('net.cnri.cordra.util.JsonUtil');
var RequestContextAndAttributesFixingInternalCordraClient = Java.type('net.cnri.cordra.RequestContextAndAttributesFixingInternalCordraClient');
var GsonUtility = Java.type('net.cnri.cordra.GsonUtility'); 
var InternalCordraClientFactory = Java.type('net.cnri.cordra.InternalCordraClientFactory');

var gson = GsonUtility.getGson();
var internalCordraClient = new RequestContextAndAttributesFixingInternalCordraClient(InternalCordraClientFactory.get());

function CordraClient(defaultOptions) {
    if (!(this instanceof CordraClient)) {
        throw new TypeError("Class constructor CordraClient cannot be invoked without 'new'");
    }
    this.defaultOptions = defaultOptions !== undefined ? defaultOptions : CordraClient.optionsForUserId('admin');
}
module.exports.CordraClient = CordraClient;

CordraClient.optionsForUserId = function (userId) {
    var options = new PreAuthenticatedOptions();
    options.userId = userId;
    return options;
};

// Note: unlike cordra-client-js, this CordraClient does not maintain a cache of session tokens for all users.
// But the authenticate call will get an auth token.

CordraClient.prototype.authenticate = function (options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.getAuthToken(javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.getAuthenticationStatus = function (full, options) {
    var javaOptions = this.processOptions(options);
    if (full === true || full === false) javaOptions.full = full;
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.authenticateAndGetResponse(javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.signOut = function (options) {
    if (options && options.token) {
        var javaOptions = this.processOptions(options);
        internalCordraClient.revokeToken(javaOptions);
    }
    return this.getAuthenticationStatus(false, options);
};

CordraClient.prototype.changePassword = function (newPassword, options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            internalCordraClient.changePassword(newPassword, javaOptions);
            resolve(new Response());
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.changeAdminPassword = function (newPassword, options) {
    this.getAuthenticationStatus(false, options).then(function (authStatus) {
	    if (!authStatus.active || authStatus.userId !== 'admin') {
		    throw new CordraError('Forbidden', 403);
		}
		return this.changePassword(newPassword, options);
    }.bind(this));
};

CordraClient.prototype.search = function (query, params, options) {
    var javaOptions = this.processOptions(options);
    var javaParams = jsToJava(params, QueryParams.class);
    if (!javaParams) {
        javaParams = QueryParams.DEFAULT;
    }
    return new Promise(function (resolve) {
        try {
            var javaSearchResults = internalCordraClient.search(query, javaParams, javaOptions);
            var searchResults = searchResultsToJson(javaSearchResults, javaParams, false);
            resolve(searchResults);
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

function searchResultsToJson(javaSearchResults, javaParams, isHandles) {
    var searchResults = {
        pageNum: javaParams.pageNum,
        pageSize: javaParams.pageSize,
        size: javaSearchResults.size(),
        results: []
    }
    var javaFacetResults = javaSearchResults.getFacets();
    if (javaFacetResults) {
        searchResults.facets = javaToJs(javaFacetResults);
    }
    var iter = javaSearchResults.iterator();
    while (iter.hasNext()) {
        if (isHandles) {
            var handle = iter.next();
            searchResults.results.push(handle);
        } else {
            var javaCordraObject = iter.next();
            var cordraObject = javaToJs(javaCordraObject);
            searchResults.results.push(cordraObject);
        }
    }
    return searchResults;
}

CordraClient.prototype.searchHandles = function (query, params, options) {
    var javaOptions = this.processOptions(options);
    var javaParams = jsToJava(params, QueryParams.class);
    if (!javaParams) {
        javaParams = QueryParams.DEFAULT;
    }
    return new Promise(function (resolve) {
        try {
            var javaSearchResults = internalCordraClient.searchHandles(query, javaParams, javaOptions);
            var searchResults = searchResultsToJson(javaSearchResults, javaParams, true);
            resolve(searchResults);
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.list = function (options) {
    return this.search('*:*', undefined, options);
};

CordraClient.prototype.listHandles = function (options) {
    return this.searchHandles('*:*', undefined, options);
};

CordraClient.prototype.getOrNull = function (id, options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.get(id, javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.get = function (id, options) {
    return this.getOrNull(id, options)
        .then(function (res) {
            if (!res) {
                var ce = new CordraError('Missing object: ' + id, 404);
                ce.status = 404; // TODO
                throw ce;
            }
            return res;
        });
};

CordraClient.prototype.create = function (cordraObject, progressCallback, options) {
    var javaOptions = this.processOptions(options);
    var javaCordraObject = jsToJava(cordraObject, CordraObject.class);
    addPayloads(cordraObject, javaCordraObject);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.create(javaCordraObject, javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.update = function (cordraObject, progressCallback, options) {
    var javaOptions = this.processOptions(options);
    var javaCordraObject = jsToJava(cordraObject, CordraObject.class);
    addPayloads(cordraObject, javaCordraObject);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.update(javaCordraObject, javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

function addPayloads(cordraObject, javaCordraObject) {
    if (!cordraObject.payloads || !javaCordraObject.payloads) return;
    for (var i = 0; i < cordraObject.payloads.length; i++) {
        var blob = cordraObject.payloads[i].body;
        if (!blob) continue;
        javaCordraObject.payloads.get(i).setInputStream(blob.javaInputStream());
    }
}

CordraClient.prototype.delete = function (id, options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            internalCordraClient.delete(id, javaOptions);
            resolve(new Response());
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.getObjectProperty = function (id, jsonPointer, options) {
    jsonPointer = '/content' + jsonPointer;
    return this.getObjectPropertyFromFullCordraObject(id, jsonPointer, options);
};

CordraClient.prototype.getObjectPropertyFromFullCordraObject = function (id, jsonPointer, options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.getJsonAtPointer(id, jsonPointer, javaOptions);
            resolve(javaToJs(javaResponse, true));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.updateObjectProperty = function (id, jsonPointer, value, options) {
    var javaOptions = this.processOptions(options);
    var javaValue = jsToJsonElement(value);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.updateAtJsonPointer(id, jsonPointer, javaValue, javaOptions);
            var property = JsonUtil.getJsonAtPointer(javaResponse.content, jsonPointer);
            resolve(javaToJs(property, true));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.getPayload = function (id, payloadName, options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.getPayloadAsResponse(id, payloadName, javaOptions);
            resolve(new Blob(javaResponse.body, { type: javaResponse.headers.mediaType }));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.getPartialPayload = function (id, payloadName, start, end, options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.getPartialPayloadAsResponse(id, payloadName, start, end, javaOptions);
            resolve(new Blob(javaResponse.body, { type: javaResponse.headers.mediaType }));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.deletePayload = function (cordraObject, payloadName, options) {
    var javaOptions = this.processOptions(options);
    var javaCordraObject = jsToJava(cordraObject, CordraObject.class);
    javaCordraObject.deletePayload(payloadName);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.update(javaCordraObject, javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.getAclForObject = function (id, options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.getAclFor(id, javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.updateAclForObject = function (id, newAcl, options) {
    var javaOptions = this.processOptions(options);
    var javaNewAcl = jsToJava(newAcl, AccessControlList.class);
    return new Promise(function (resolve) {
        try {
            internalCordraClient.updateAcls(id, javaNewAcl, javaOptions);
            resolve(newAcl);
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.publishVersion = function (id, versionId, options) {
    var javaOptions = this.processOptions(options);
    var javaVersionId = versionId === undefined ? null : versionId;
    return new Promise(function (resolve) {
        try {
            // TODO perhaps clonePayloads=true should be possible here
            var javaResponse = internalCordraClient.publishVersion(id, javaVersionId, false, javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.getVersionsFor = function (id, options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.getVersionsFor(id, javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.listMethods = function (id, options) {
    var javaOptions = this.processOptions(options);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.listMethods(id, javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.listMethodsForType = function (type, listStatic, options) {
    var javaOptions = this.processOptions(options);
    listStatic = listStatic ? true : false;
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.listMethodsForType(type, listStatic, javaOptions);
            resolve(javaToJs(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.callMethod = function (objectId, method, params, options) {
    var javaOptions = this.processOptions(options);
    var javaInputStream = callParamsToJavaInputStream(params);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.call(objectId, method, javaInputStream, javaOptions);
            resolve(javaToJs(javaResponse, true));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.callMethodAsResponse = function (objectId, method, params, options) {
    var javaOptions = this.processOptions(options);
    var javaInputStream = callParamsToJavaInputStream(params);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.callAsResponse(objectId, method, javaInputStream, javaOptions);
            resolve(new Response(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.callMethodForType = function (type, method, params, options) {
    var javaOptions = this.processOptions(options);
    var javaInputStream = callParamsToJavaInputStream(params);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.callForType(type, method, javaInputStream, javaOptions);
            resolve(javaToJs(javaResponse, true));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.callMethodForTypeAsResponse = function (objectId, method, params, options) {
    var javaOptions = this.processOptions(options);
    var javaInputStream = callParamsToJavaInputStream(params);
    return new Promise(function (resolve) {
        try {
            var javaResponse = internalCordraClient.callForTypeAsResponse(objectId, method, javaInputStream, javaOptions);
            resolve(new Response(javaResponse));
        } catch (e) {
            throw wrapJavaException(e);
        }
    });
};

CordraClient.prototype.processOptions = function (options) {
    options = options || this.defaultOptions || {};
    if (options instanceof Options) {
        return options;
    } else {
        return jsToJava(options, Options.class);
    }
};

function callParamsToJavaInputStream(params) {
    if (params === undefined) {
        return new ByteArrayInputStream(Java.to([], 'byte[]'));
    } else if (params instanceof InputStream) {
        return params;
    } else if (params instanceof Blob) {
        return params.javaInputStream();
    } else if (params instanceof ArrayBuffer) {
        return javaInputStreamFromArrayBuffer(params);
    } else if (ArrayBuffer.isView(params)) {
        return javaInputStreamFromArrayBuffer(params.buffer, params.byteOffset, params.byteLength);
    } else {
        return CordraClientUtil.javaInputStreamFromText(JSON.stringify(params));
    }
}

function jsToJava(jsObject, klass) {
    if (jsObject === null || jsObject === undefined) return null;
    return gson.fromJson(JSON.stringify(jsObject), klass);
}

function jsToJsonElement(jsObject) {
    if (jsObject === null || jsObject === undefined) return null;
    return JsonParser.parseString(JSON.stringify(jsObject));
}

function javaToJs(javaObject, useUndefined) {
    if (javaObject === null) return useUndefined ? undefined : null;
    return JSON.parse(gson.toJson(javaObject));
}

function wrapJavaException(e) {
    if (e instanceof CordraException) {
        var ce = new CordraError(javaToJs(e.getResponse()), e.getResponseCode(), { cause: e });
        // TODO consider aligning cordra-client exceptions to use responseCode instead of status
        ce.status = ce.responseCode;
        // TODO
        if (ce.status === 400) ce.statusText = 'Bad Request';
        return ce;
    } else if (e instanceof Exception) {
        return new Error(e.getMessage(), { cause: e });
    } else {
        return e;
    }
}

function int8ArrayFromJavaInputStream(javaInputStream) {
    var byteArray = StreamUtil.readFully(javaInputStream);
    var int8Array = new Int8Array(byteArray.length);
    int8Array.set(Java.from(byteArray));
    return int8Array;
}

function arrayBufferFromJavaInputStream(javaInputStream) {
    return int8ArrayFromJavaInputStream(javaInputStream).buffer;
}

function javaInputStreamFromArrayBuffer(arrayBuffer, byteOffset, byteLength) {
    if (byteOffset === undefined) byteOffset = 0;
    if (byteLength === undefined) byteLength = arrayBuffer.byteLength;
    var int8Array = new Int8Array(arrayBuffer);
    var byteArray = Java.to(Array.prototype.slice.call(int8Array), 'byte[]');
    return new ByteArrayInputStream(byteArray);
}

function textFromArrayBuffer(arrayBuffer) {
    var int8Array = new Int8Array(arrayBuffer);
    var byteArray = Java.to(Array.prototype.slice.call(int8Array), 'byte[]');
    return new JavaString(byteArray, 'UTF-8');
}

function Blob(arrayOrInputStream, options) {
    if (options && options.type) this.type = options.type;
    else if (!options) options = { type: '' };
    else options.type = '';
    if (!arrayOrInputStream) {
        this.size = 0;
        this.arrayBufferSync = new ArrayBuffer(0);
    } else if (arrayOrInputStream instanceof InputStream) {
        this.javaInputStreamSync = arrayOrInputStream;
    } else if (Array.isArray(arrayOrInputStream)) {
        if (arrayOrInputStream.length === 1 && arrayOrInputStream[0] instanceof InputStream) {
            this.javaInputStreamSync = arrayOrInputStream[0];
        } else {
            this.arrayBufferSync = buildArrayBuffer(arrayOrInputStream);
            this.size = this.arrayBufferSync.byteLength;
        }
    } else {
        throw new Error('Unimplemented');
    }
}
module.exports.Blob = Blob;

function buildArrayBuffer(array) {
    array = array.map(convertToInt8Array);
    var length = 0;
    for (var i = 0; i < array.length; i++) {
        var item = array[i];
        if (!item) continue;
        length += item.byteLength;
    }
    var int8Array = new Int8Array(length);
    var offset = 0;
    for (var i = 0; i < array.length; i++) {
        var item = array[i];
        if (!item) continue;
        int8Array.set(item, offset);
        offset += item.byteLength;
    }
    return int8Array.buffer;
}

function convertToInt8Array(item) {
    if (typeof item === 'string') {
        var byteArray = CordraClientUtil.bytesFromText(item);
        var int8Array = new Int8Array(byteArray.length);
        int8Array.set(Java.from(byteArray));
        return int8Array;
    } else if (item instanceof InputStream) {
        return int8ArrayFromJavaInputStream(item);
    } else if (item instanceof Blob) {
        if (item.arrayBufferSync) {
            return new Int8Array(item.arrayBufferSync);
        } else {
            return int8ArrayFromJavaInputStream(item.javaInputStreamSync);
        }
    } else if (item instanceof ArrayBuffer) {
        return new Int8Array(item);
    } else if (item instanceof Int8Array) {
        return item;
    } else if (ArrayBuffer.isView(item)) {
        return new Int8Array(item.buffer, item.byteOffset, item.byteLength);
    } else {
        return undefined;
    }
}

Blob.prototype.arrayBuffer = function () {
    return new Promise(function (resolve) {
        if (this.arrayBufferSync) resolve(this.arrayBufferSync);
        else {
            this.arrayBufferSync = arrayBufferFromJavaInputStream(this.javaInputStreamSync);
            this.size = this.arrayBufferSync.byteLength;
            resolve(this.arrayBufferSync);
        }
    }.bind(this));
};

Blob.prototype.slice = function () {
    throw new Error('Unimplemented');
};

Blob.prototype.text = function () {
    return this.arrayBuffer().then(textFromArrayBuffer);
};

Blob.prototype.javaInputStream = function () {
    if (this.arrayBufferSync) return javaInputStreamFromArrayBuffer(this.arrayBufferSync);
    return this.javaInputStreamSync;
};

var BlobMethod = {
    arrayBuffer: 'arrayBuffer',
    text: 'text',
    javaInputStream: 'javaInputStream'
};
module.exports.BlobMethod = BlobMethod;

module.exports.BlobUtil = {
    readBlob: function (blob, method) {
        if (!method) method = BlobMethod.arrayBuffer;
        if (method === BlobMethod.arrayBuffer) {
            return blob.arrayBuffer();
        } else if (method === BlobMethod.text) {
            return blob.text();
        } else if (method = BlobMethod.javaInputStream) {
            return blob.javaInputStream();
        } else {
            throw new Error('Unsupported');
        }
    }
};

function Response(javaResponse) {
    this.ok = true;
    this.status = 200;
    this.statusText = 'OK';
    if (javaResponse) {
        this.javaInputStreamSync = javaResponse.body;
    }
    this.headers = new Headers();
    if (javaResponse && javaResponse.headers) {
        if (javaResponse.headers.mediaType) {
            this.headers.set('Content-Type', javaResponse.headers.mediaType);
        }
        if (javaResponse.headers.filename) {
            this.headers.set('Content-Disposition', HttpUtil.contentDispositionHeaderFor('inline', javaResponse.headers.filename));
        }
    }
};
module.exports.Response = Response;

Response.prototype.arrayBuffer = function () {
    return new Promise(function (resolve) {
        if (this.arrayBufferSync) resolve(this.arrayBufferSync);
        else {
            this.arrayBufferSync = arrayBufferFromJavaInputStream(this.javaInputStreamSync);
            resolve(this.arrayBufferSync);
        }
    }.bind(this));
};

Response.prototype.blob = function () {
    var type = '';
    if (this.headers && this.headers.has('Content-Type')) type = this.headers.get('Content-Type');
    return this.arrayBuffer().then(function (arrayBuffer) { return new Blob([ arrayBuffer ], { type: type }); });
};

Response.prototype.json = function () {
    return this.text().then(function (text) {
        if (!text) return undefined;
        return JSON.parse(text);
    });
};

Response.prototype.text = function () {
    return this.arrayBuffer().then(textFromArrayBuffer);
};

Response.prototype.javaInputStream = function () {
    if (this.arrayBufferSync) return javaInputStreamFromArrayBuffer(this.arrayBufferSync);
    return this.javaInputStreamSync;
};

/* Begin whatwg-fetch */

var support = {
    iterable: 'Symbol' in global && 'iterator' in Symbol
};

function normalizeName(name) {
    if (typeof name !== 'string') {
        name = String(name);
    }
    if (/[^a-z0-9\-#$%&'*+.^_`|~!]/i.test(name) || name === '') {
        throw new TypeError('Invalid character in header field name: "' + name + '"');
    }
    return name.toLowerCase()
}

function normalizeValue(value) {
    if (typeof value !== 'string') {
        value = String(value);
    }
    return value;
}

// Build a destructive iterator for the value list
function iteratorFor(items) {
    var iterator = {
        next: function() {
            var value = items.shift();
            return {done: value === undefined, value: value};
        }
    }

    if (support.iterable) {
        iterator[Symbol.iterator] = function() {
            return iterator;
        };
    }

    return iterator;
}

function Headers(headers) {
    this.map = {};

    if (headers instanceof Headers) {
        headers.forEach(function(value, name) {
            this.append(name, value);
        }, this);
    } else if (Array.isArray(headers)) {
        headers.forEach(function(header) {
            this.append(header[0], header[1]);
        }, this);
    } else if (headers) {
        Object.getOwnPropertyNames(headers).forEach(function(name) {
            this.append(name, headers[name]);
        }, this);
    }
}
module.exports.Headers = Headers;

Headers.prototype.append = function(name, value) {
    name = normalizeName(name);
    value = normalizeValue(value);
    var oldValue = this.map[name];
    this.map[name] = oldValue ? oldValue + ', ' + value : value;
};

Headers.prototype['delete'] = function(name) {
    delete this.map[normalizeName(name)];
};

Headers.prototype.get = function(name) {
    name = normalizeName(name);
    return this.has(name) ? this.map[name] : null;
};

Headers.prototype.has = function(name) {
    return this.map.hasOwnProperty(normalizeName(name));
};

Headers.prototype.set = function(name, value) {
    this.map[normalizeName(name)] = normalizeValue(value);
};

Headers.prototype.forEach = function(callback, thisArg) {
    for (var name in this.map) {
        if (this.map.hasOwnProperty(name)) {
            callback.call(thisArg, this.map[name], name, this);
        }
    }
};

Headers.prototype.keys = function() {
    var items = [];
    this.forEach(function(value, name) {
        items.push(name);
    });
    return iteratorFor(items);
};

Headers.prototype.values = function() {
    var items = [];
    this.forEach(function(value) {
        items.push(value);
    });
    return iteratorFor(items);
};

Headers.prototype.entries = function() {
    var items = [];
    this.forEach(function(value, name) {
        items.push([name, value]);
    });
    return iteratorFor(items);
};

if (support.iterable) {
    Headers.prototype[Symbol.iterator] = Headers.prototype.entries;
}

  /* end whatwg-fetch */
