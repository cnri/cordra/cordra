const cordra = require('cordra');

export default cordra;

export const get = cordra.get;
export const search = cordra.search;
export const getPayloadAsJavaInputStream = cordra.getPayloadAsJavaInputStream;
export const getPayloadAsJavaReader = cordra.getPayloadAsJavaReader;
export const getPayloadAsUint8Array = cordra.getPayloadAsUint8Array;
export const getPayloadAsString = cordra.getPayloadAsString;
export const getPayloadAsJson = cordra.getPayloadAsJson;
export const getPartialPayloadAsJavaInputStream = cordra.getPartialPayloadAsJavaInputStream;
export const getPartialPayloadAsJavaReader = cordra.getPartialPayloadAsJavaReader;
export const getPartialPayloadAsUint8Array = cordra.getPartialPayloadAsUint8Array;
export const getPartialPayloadAsString = cordra.getPartialPayloadAsString;
export const CordraError = cordra.CordraError;
