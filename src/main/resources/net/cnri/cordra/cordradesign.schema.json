{
  "type": "object",
  "title": "CordraDesign",
  "properties": {
    "ids": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "isReadOnly": {
      "type": "boolean"
    },
    "useLegacyContentOnlyJavaScriptHooks": {
      "type": "boolean"
    },
    "useLegacyAttributesAsInputForDoipGet": {
      "type": "boolean"
    },
    "useLegacySearchPageSizeZeroReturnsAll": {
      "type": "boolean"
    },
    "useLegacySessionsApi": {
      "type": "boolean"
    },
    "allowInsecureAuthentication": {
      "type": "boolean"
    },
    "disableAuthenticationBackOff": {
      "type": "boolean"
    },
    "disableBackOffRequestParking": {
      "type": "boolean"
    },
    "enableVersionEdits": {
      "type": "boolean"
    },
    "includePayloadsInReplicationMessages": {
      "type": "boolean"
    },
    "enableTypeChangeViaUpdate": {
      "type": "boolean"
    },
    "uiConfig": {
      "type": "object",
      "properties": {
        "title": {
          "type": "string"
        },
        "relationshipsButtonText": {
          "type": "string"
        },
        "allowUserToSpecifySuffixOnCreate": {
          "type": "boolean"
        },
        "allowUserToSpecifyHandleOnCreate": {
          "type": "boolean"
        },
        "hideTypeInObjectEditor": {
          "type": "boolean"
        },
        "searchResults": {
          "type": "object",
          "properties": {
            "includeType": {
              "type": "boolean"
            },
            "includeModifiedDate": {
              "type": "boolean"
            },
            "includeCreatedDate": {
              "type": "boolean"
            }
          }
        },
        "initialQuery": {
          "type": "string"
        },
        "initialFragment": {
          "type": "string"
        },
        "initialSortFields": {
          "type": "string"
        },
        "navBarLinks": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "type": {
                "type": "string",
                "enum": [
                  "url",
                  "page",
                  "query",
                  "typeDropdown",
                  "typeObjectsDropdown",
                  "about",
                  "menu"
                ]
              },
              "title": {
                "type": "string"
              },
              "query": {
                "type": "string"
              },
              "sortFields": {},
              "url": {
                "type": "string"
              },
              "location": {
                "type": "string"
              }
            }
          }
        }
      }
    },
    "authConfig": {
      "type": "object",
      "properties": {
        "schemaAcls": {
          "type": "object",
          "additionalProperties": {
            "type": "object",
            "properties": {
              "defaultAclRead": {
                "type": "array",
                "items": {
                  "type": "string"
                }
              },
              "defaultAclWrite": {
                "type": "array",
                "items": {
                  "type": "string"
                }
              },
              "aclCreate": {
                "type": "array",
                "items": {
                  "type": "string"
                }
              }
            }
          }
        },
        "defaultAcls": {
          "type": "object",
          "properties": {
            "defaultAclRead": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "defaultAclWrite": {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "aclCreate": {
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          }
        }
      }
    },
    "handleServerConfig": {
      "type": "object",
      "required": [
        "enabled"
      ],
      "properties": {
        "enabled": {
          "type": "boolean"
        },
        "listenAddress": {
          "type": "string"
        },
        "tcpPort": {
          "type": "number"
        },
        "externalAddress": {
          "type": "string"
        },
        "externalTcpPort": {
          "type": "number"
        },
        "logAccesses": {
          "type": "boolean"
        }
      }
    },
    "handleMintingConfig": {
      "type": "object",
      "properties": {
        "prefix": {
          "type": "string"
        },
        "baseUri": {
          "type": "string"
        },
        "schemaSpecificLinks": {
          "type": "object",
          "additionalProperties": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "type": {
                  "type": "string",
                  "enum": [
                    "ui",
                    "json"
                  ]
                },
                "primary": {
                  "type": "boolean"
                }
              }
            }
          }
        },
        "handleAdminIdentity": {
          "type": "string"
        },
        "javascriptIsModule": {
          "type": "boolean"
        },
        "javascript": {
          "type": "string",
          "format": "javascript"
        },
        "defaultLinks": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "type": {
                "type": "string",
                "enum": [
                  "ui",
                  "json",
                  "payload",
                  "url"
                ]
              },
              "primary": {
                "type": "boolean"
              },
              "specific": {
                "description": "payload name or jsonPointer for url",
                "type": "string"
              },
              "all": {
                "description": "only for type payload. Indicates that a link should be created for all payloads",
                "type": "boolean"
              }
            }
          }
        },
        "ignoreHandleErrors": {
          "type": "boolean"
        }
      }
    },
    "doip": {
      "type": "object",
      "properties": {
        "enabled": {
          "type": "boolean"
        },
        "listenAddress": {
          "type": "string"
        },
        "port": {
          "type": "number"
        },
        "processorConfig": {
          "type": "object",
          "properties": {
            "serviceId": {
              "type": "string"
            },
            "address": {
              "type": "string"
            },
            "port": {
              "type": "number"
            },
            "serviceName": {
              "type": "string"
            },
            "serviceDescription": {
              "type": "string"
            }
          }
        }
      }
    },
    "cookies": {
        "type": "object",
        "properties": {
            "csrfToken": {
               "$ref": "#/definitions/cookie"
            },
            "jessionid": {
               "$ref": "#/definitions/cookie"
            }
        }
    },
    "builtInTypes": {
      "type": "object",
      "properties": {
        "CordraDesign": {
          "type": "object",
          "properties": {
            "javascriptIsModule": {
              "type": "boolean"
            },
            "javascript": {
              "type": "string",
              "format": "javascript",
              "title": "javascript (for CordraDesign type)"
            },
            "hashObject": {
              "type": "boolean",
              "title": "hashObject (for CordraDesign type)"
            },
            "authConfig": {
              "type": [ "object", "null" ],
              "format": "json",
              "title": "authConfig (for CordraDesign type)",
              "description": "Enter explicit null to delete"
            }
          }
        },
        "Schema": {
          "type": "object",
          "properties": {
            "javascriptIsModule": {
              "type": "boolean"
            },
            "javascript": {
              "type": "string",
              "format": "javascript",
              "title": "javascript (for Schema type)"
            },
            "hashObject": {
              "type": "boolean",
              "title": "hashObject (for Schema type)"
            },
            "authConfig": {
              "type": [ "object", "null" ],
              "format": "json",
              "title": "authConfig (for Schema type)",
              "description": "Enter explicit null to delete"
            }
          }
        }
      }
    },
    "javascriptIsModule": {
      "type": "boolean"
    },
    "javascript": {
      "type": "string",
      "format": "javascript",
      "title": "javascript (global)"
    }
  },
  "definitions": {
      "cookie": {
          "type": "object",
          "properties": {
              "path": { "type": "string" },
              "httpOnly": { "type": "boolean" },
              "secure": { "type": "boolean" }
          }
      }
  }
}
