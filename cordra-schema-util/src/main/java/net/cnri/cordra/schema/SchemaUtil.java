package net.cnri.cordra.schema;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.cnri.cordra.api.CordraObject;

public class SchemaUtil {

    public static JsonNode loadUsingResolver(URI uri, SchemaResolver resolver) {
        if (resolver == null) return null;
        String uriString;
        if (uri.getScheme() == null) {
            uriString = uri.getRawSchemeSpecificPart();
        } else {
            uriString = uri.getScheme() + ":" + uri.getRawSchemeSpecificPart();
        }
        SchemaNodeAndUri schemaNodeAndUri = resolver.resolve(uriString);
        if (schemaNodeAndUri == null) return null;
        JsonNode node = SchemaUtil.schemaWithBaseUri(schemaNodeAndUri.getSchemaNode(), schemaNodeAndUri.getBaseUri());
        if (uri.getFragment() == null || uri.getFragment().isEmpty()) return node;
        try {
            // reconsider when using plain name fragments
            node = node.at(uri.getFragment());
        } catch (IllegalArgumentException e) {
            return null;
        }
        if (node == null) return null;
        if (node.isMissingNode()) return null;
        return node;
    }

    public static JsonNode schemaWithoutDollarSchema(JsonNode schema) {
        if (!schema.isObject()) return schema;
        if (!schema.has("$schema")) return schema;
        ObjectNode res = JsonNodeFactory.instance.objectNode();
        res.setAll((ObjectNode)schema);
        res.remove("$schema");
        return res;
    }

    /**
     * Returns a new JsonNode representing a JSON Schema with an "id" field matching the given baseUri.
     * This will cause the schema validation library to resolve relative $ref values with respect to that baseUri.
     *
     * @param schema the schema
     * @param baseUri its new baseUri
     * @return an otherwise-identical schema with the expected top-level "id".
     */
    public static JsonNode schemaWithBaseUri(JsonNode schema, String baseUri) {
        if (!schema.isObject()) return schema;
        JsonNode currentId = schema.get("id");
        if (baseUri == null && (currentId == null || currentId.isMissingNode())) return schema;
        if (currentId != null && currentId.isTextual() && currentId.asText().equals(baseUri)) return schema;
        ObjectNode res = JsonNodeFactory.instance.objectNode();
        res.setAll((ObjectNode)schema);
        if (baseUri == null) res.remove("id");
        else res.set("id", JsonNodeFactory.instance.textNode(baseUri));
        return res;
    }

    public static String getBaseUriFromSchemaCordraObject(CordraObject co) {
        JsonObject content = co.content.getAsJsonObject();
        String name = content.get("name").getAsString();
        String baseUri = jsonElementToString(content.get("baseUri"));
        if (isEmptyOrOnlyFragment(baseUri)) baseUri = null;
        if (baseUri == null) {
            baseUri = getStringProperty(content.get("schema"), "$id");
            if (isEmptyOrOnlyFragment(baseUri)) baseUri = null;
        }
        if (baseUri == null) {
            baseUri = getStringProperty(content.get("schema"), "id");
            if (isEmptyOrOnlyFragment(baseUri)) baseUri = null;
        }
        if (baseUri == null) {
            return getDefaultBaseUriFromSchemaName(name);
        }
        if (!hasScheme(baseUri)) baseUri = addFileScheme(baseUri);
        baseUri = removeFragment(baseUri);
        return baseUri;
    }

    public static String getDefaultBaseUriFromSchemaName(String name) throws AssertionError {
        try {
            return "file:/cordra/schemas/" + URLEncoder.encode(name, "UTF-8").replace("+","%20") + ".schema.json";
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    private static String removeFragment(String baseUri) {
        int index = baseUri.indexOf('#');
        if (index < 0) return baseUri;
        return baseUri.substring(0, index);
    }

    private static boolean isEmptyOrOnlyFragment(String baseUri) {
        if (baseUri == null) return true;
        if (baseUri.isEmpty()) return true;
        if (baseUri.startsWith("#")) return true; // currently no support for "plain-name fragments"
        return false;
    }

    private static final Pattern SCHEME_PATTERN = Pattern.compile("^[-A-Za-z+.]*:");

    private static boolean hasScheme(String baseUri) {
        return SCHEME_PATTERN.matcher(baseUri).lookingAt();
    }

    private static String addFileScheme(String baseUri) {
        if (baseUri.startsWith("/")) return "file:" + baseUri;
        return "file:/" + baseUri;
    }

    public static String getNameFromCordraSchemasUri(String uri) {
        if (uri == null) return null;
        String lowercaseUri = uri.toLowerCase(Locale.ROOT);
        if (lowercaseUri.startsWith("file:///")) uri = uri.substring(7);
        else if (lowercaseUri.startsWith("file://")) uri = uri.substring(6);
        else if (lowercaseUri.startsWith("file:/")) uri = uri.substring(5);
        if (!uri.startsWith("/cordra/schemas/")) return null;
        uri = uri.substring(16);
        if (uri.endsWith(".schema.json")) uri = uri.substring(0, uri.length() - 12);
        try {
            return URLDecoder.decode(uri.replace("+", "%2B"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    private static String jsonElementToString(JsonElement el) {
        if (el == null) return null;
        return el.getAsString();
    }

    private static String getStringProperty(JsonElement el, String prop) {
        if (el == null) return null;
        if (!el.isJsonObject()) return null;
        return jsonElementToString(el.getAsJsonObject().get(prop));
    }
}
