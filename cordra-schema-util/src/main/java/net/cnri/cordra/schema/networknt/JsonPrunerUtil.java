package net.cnri.cordra.schema.networknt;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import net.cnri.cordra.util.JsonUtil;

public class JsonPrunerUtil {

    public static ThreadLocal<Map<String, Collection<String>>> prunerStateThreadLocal = new ThreadLocal<>();

    public static void mergeInto(Map<String, Collection<String>> state, Map<String, Collection<String>> thisState) {
        for (Map.Entry<String, Collection<String>> entry : thisState.entrySet()) {
            state.computeIfAbsent(entry.getKey(), k -> new HashSet<>()).addAll(entry.getValue());
        }
    }

    public static void pruneToState(JsonNode node, Map<String, Collection<String>> state) {
        pruneToState(node, state, "");
    }

    private static void pruneToState(JsonNode node, Map<String, Collection<String>> state, String jsonPointer) {
        if (node.isObject()) {
            Collection<String> fieldsToKeep = state.get(jsonPointer);
            if (fieldsToKeep != null) {
                Iterator<String> iter = node.fieldNames();
                while (iter.hasNext()) {
                    String field = iter.next();
                    if (!fieldsToKeep.contains(field)) iter.remove();
                    else {
                        pruneToState(node.get(field), state, jsonPointer + "/" + JsonUtil.encodeSegment(field));
                    }
                }
            }
        } else if (node.isArray()) {
            for (int i = 0; i < node.size(); i++) {
                pruneToState(node.get(i), state, jsonPointer + "/" + i);
            }
        }
    }
}
