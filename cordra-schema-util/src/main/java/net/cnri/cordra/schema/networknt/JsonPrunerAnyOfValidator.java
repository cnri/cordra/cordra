package net.cnri.cordra.schema.networknt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonValidator;
import com.networknt.schema.Keyword;
import com.networknt.schema.ValidationContext;
import com.networknt.schema.ValidationMessage;
import com.networknt.schema.ValidatorTypeCode;

public class JsonPrunerAnyOfValidator implements JsonValidator {

    private final List<JsonSchema> schemas = new ArrayList<>();

    @SuppressWarnings("unused")
    public JsonPrunerAnyOfValidator(String schemaPath, JsonNode schemaNode, JsonSchema parentSchema, ValidationContext validationContext, Keyword keyword) throws Exception {
        int size = schemaNode.size();
        for (int i = 0; i < size; i++) {
            schemas.add(new JsonSchema(validationContext,
                ValidatorTypeCode.ANY_OF.getValue(),
                parentSchema.getCurrentUri(),
                schemaNode.get(i),
                parentSchema));
        }
    }

    @Override
    public Set<ValidationMessage> walk(JsonNode node, JsonNode rootNode, String at, boolean shouldValidateSchema) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode rootNode) {
        return validate(rootNode, rootNode, JsonValidator.AT_ROOT);
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode node, JsonNode rootNode, String at) {
        Set<ValidationMessage> allErrors = new LinkedHashSet<>();
        boolean found = false;
        Map<String, Collection<String>> origState = JsonPrunerUtil.prunerStateThreadLocal.get();
        try {
            for (JsonSchema schema : schemas) {
                Map<String, Collection<String>> thisState = new HashMap<>();
                JsonPrunerUtil.prunerStateThreadLocal.set(thisState);
                Set<ValidationMessage> errors = schema.validate(node, rootNode, at);
                if (errors.isEmpty()) {
                    found = true;
                    JsonPrunerUtil.mergeInto(origState, thisState);
                } else {
                    allErrors.addAll(errors);
                }
            }
        } finally {
            JsonPrunerUtil.prunerStateThreadLocal.set(origState);
        }
        if (found) {
            return Collections.emptySet();
        } else {
            return Collections.unmodifiableSet(allErrors);
        }
    }

}
